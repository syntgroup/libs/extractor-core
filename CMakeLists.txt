cmake_minimum_required(VERSION 3.14)

include(cmake/prelude.cmake)

set(CORE_LIBS_REVISION 6)

project(
  extractor-core
  VERSION ${CORE_LIBS_REVISION}
  DESCRIPTION "Short description"
  HOMEPAGE_URL "https://gitlab.com/syntgroup/libs/extractor-core"
  LANGUAGES CXX)

include(cmake/project-is-top-level.cmake)
include(cmake/variables.cmake)
include(GenerateExportHeader)
include(FetchContent)
include(CTest)

option(extractor-core_BUILD_JSON "Load JSON decorators library" OFF)
option(extractor-core_BUILD_SERIAL "Load base serial library" ON)
option(extractor-core_BUILD_MOTHERBOARD "Load motherboard library" ON)
option(extractor-core_BUILD_THERMOCONTROLLERS "Load thermocontrollers library"
       ON)

# find qt
find_package(
  Qt5
  COMPONENTS Core
  REQUIRED)

# download external dependencies
set(CMAKE_TLS_VERIFY true)
if(CMAKE_VERSION VERSION_GREATER_EQUAL "3.24.0")
  cmake_policy(SET CMP0135 NEW)
endif()
FetchContent_Declare(
  strong_type
  GIT_REPOSITORY https://github.com/rollbear/strong_type.git
  GIT_TAG v12)
FetchContent_Declare(
  expected-lite
  GIT_REPOSITORY https://github.com/martinmoene/expected-lite.git
  GIT_TAG v0.6.3)
FetchContent_Declare(
  gsl-lite
  GIT_REPOSITORY https://github.com/gsl-lite/gsl-lite.git
  GIT_TAG v0.41.0)
FetchContent_MakeAvailable(strong_type expected-lite gsl-lite)

# ------------------------------------------------------------------------------

# DI library has no external dependencies
add_subdirectory(libraries/di)

# base library
add_subdirectory(libraries/common)
add_subdirectory(libraries/utils)

if(extractor-core_BUILD_JSON)
  add_subdirectory(libraries/json)
endif()

if(extractor-core_BUILD_SERIAL)
  # serial libraries
  add_subdirectory(libraries/serial)

  # parser base library
  add_subdirectory(libraries/parser)

  # device libraries
  if(extractor-core_BUILD_MOTHERBOARD)
    add_subdirectory(libraries/motherboard)
  endif()

  if(extractor-core_BUILD_THERMOCONTROLLERS)
    add_subdirectory(libraries/thermocontrollers)
  endif()
endif()

# ---- Developer mode ----

if(NOT extractor-core_DEVELOPER_MODE)
  return()
elseif(NOT PROJECT_IS_TOP_LEVEL)
  message(
    AUTHOR_WARNING
      "Developer mode is intended for developers of extractor-panel")
endif()

include(cmake/dev-mode.cmake)
