## Quick Information
<!-- This is to help replicate the issue as closeley as possible !-->
- **Operating System:**
- **Compliler:** gcc / clang / msvc / other <!-- Delete as appropriate !-->
- **Qt version:**
- **Device:** desktop / embedded <!-- Delete as appropriate !-->

## What Happened?
<!-- A brief description of what happened when you tried to perform an action !-->

## Expected result
<!-- What should have happened when you performed the actions !-->

## Steps to reproduce
<!-- List the steps required to produce the error. These should be as few as possible !-->

## Screenshots
<!-- Any relevant screenshots which show the issue !-->

