option(extractor-core_BUILD_DOCS "Build documentation using Doxygen" OFF)
if(BUILD_DOCS)
  include(cmake/docs.cmake)
endif()

if(BUILD_TESTING)
  find_package(GTest REQUIRED)
  enable_testing()
  include(GoogleTest)

  option(ENABLE_COVERAGE "Enable coverage support separate from CTest's" OFF)
  if(ENABLE_COVERAGE)
    include(cmake/coverage.cmake)
  endif()

  if(CMAKE_HOST_SYSTEM_NAME STREQUAL "Windows")
    include(cmake/open-cpp-coverage.cmake OPTIONAL)
  endif()

  add_subdirectory(libraries/common/test)
  add_subdirectory(libraries/di/test)

  if (extractor-core_BUILD_SERIAL)
    add_subdirectory(libraries/serial/test)
  endif()

  if(extractor-core_BUILD_JSON)
    add_subdirectory(libraries/json/test)
  endif()

  if(extractor-core_BUILD_MOTHERBOARD)
    add_subdirectory(libraries/motherboard/test)
  endif()

  if(extractor-core_BUILD_THERMOCONTROLLERS)
    add_subdirectory(libraries/thermocontrollers/test)
  endif()
endif()

include(cmake/lint-targets.cmake)
include(cmake/spell-targets.cmake)
