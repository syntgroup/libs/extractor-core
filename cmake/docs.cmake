# ---- Dependencies ----

find_package(Doxygen REQUIRED dot)
if(NOT DOXYGEN_FOUND)
  message(WARNING "Unable to find Doxygen, please install it first.")
  return()
endif()

# ---- Declare documentation target ----

set(DOXYGEN_PROJECT_LOGO ${PROJECT_SOURCE_DIR}/docs/icons8-core-96.png)

doxygen_add_docs(docs ${PROJECT_SOURCE_DIR}/libraries
                 COMMENT "Generate documentation")
