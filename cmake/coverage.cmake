# ---- Variables ----

# We use variables separate from what CTest uses, because those have
# customization issues

set(GCOV_COMMAND
    gcovr --exclude-unreachable-branches --exclude-throw-branches
    --print-summary --xml-pretty -o "${PROJECT_BINARY_DIR}/coverage.xml" --html
    "${PROJECT_BINARY_DIR}/coverage.html" --root "${PROJECT_SOURCE_DIR}"
    --filter "${PROJECT_SOURCE_DIR}/libraries/" --exclude "(.+/)?test/")

set(GCOV_COMMAND_DETAILED
    gcovr --exclude-unreachable-branches --exclude-throw-branches
    --print-summary --html-details "${PROJECT_BINARY_DIR}/coverage.html" --root
    "${PROJECT_SOURCE_DIR}" --filter "${PROJECT_SOURCE_DIR}/libraries"
    --exclude "(.+/)?test/")

# ---- Coverage target ----

add_custom_target(
  coverage
  COMMAND ${GCOV_COMMAND}
  COMMENT "Generating coverage report"
  VERBATIM)

add_custom_target(
  coverage-detailed
  COMMAND ${GCOV_COMMAND_DETAILED}
  COMMENT "Generating detailed coverage report"
  VERBATIM)
