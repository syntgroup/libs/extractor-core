find_program(CMAKE_CXX_CPPCHECK NAMES cppcheck)

if (CMAKE_CXX_CPPCHECK)
    file(MAKE_DIRECTORY ${CMAKE_CURRENT_BINARY_DIR}/cppcheck)

    add_custom_target(cppcheck
            COMMAND ${CMAKE_CXX_CPPCHECK}
            --enable=warning
            --force
            --inline-suppr
            --clang
            --cppcheck-build-dir=${CMAKE_CURRENT_BINARY_DIR}/cppcheck
            --language=c++
            --std=c++17
            --verbose
            -i${CMAKE_CURRENT_BINARY_DIR}/_deps
            -i${CMAKE_CURRENT_BINARY_DIR}/qrc_iconoir.cpp
            .
            WORKING_DIRECTORY ${CMAKE_CURRENT_SOURCE_DIR}
            COMMENT "Executing cppcheck"
            VERBATIM
    )
else ()
    message("Didn't find cppcheck util")
endif()