#include <gtest/gtest.h>

#include "motherboard/device/motherboard_values.h"
#include "motherboard/protocol/buttons_request.h"
#include "motherboard/protocol/device_request.h"
#include "motherboard/protocol/engine_request.h"
#include "motherboard/protocol/goto_request.h"
#include "motherboard/protocol/ping_request.h"
#include "motherboard/protocol/power_request.h"
#include "motherboard/protocol/rgb_request.h"
#include "motherboard/protocol/sns_state_request.h"
#include "motherboard/protocol/system_request.h"
#include "serial/protocol/protocol_values.h"
#include "srvint/srvint_request_factory.h"
#include <cstdint>

namespace {
const protocol::srvint::SrvIntRequestFactory factory;
constexpr protocol::PacketID pid{ 0x3b };
constexpr auto engine_id{ 0x4 };

template<typename T>
[[nodiscard]] auto
get_param(const T& request)
{
  return factory.getParam(request.pid, request.payload);
}

template<typename T>
[[nodiscard]] auto
set_param(const T& request)
{
  return factory.setParam(request.pid, request.payload);
}

} // namespace

using namespace protocol::motherboard;

TEST(TestMotherboard, CreateMotherboardIdRequest)
{
  // 55:10:03:87:02:96:00:01:01
  constexpr protocol::PacketID pid{ 0x03 };
  const DeviceRequestId request{ pid };

  EXPECT_EQ(get_param(request), QByteArray::fromHex("551003870296000101"));
}

TEST(TestMotherboard, CreateMotherboardSetIdRequest)
{
  // 55:10:3b:86:09:a4:00:01:75:f3:19:00:00:00:01:9f
  const auto request_expected{ QByteArray::fromHex(
    "55103b8609a4000175f319000000019f") };
  const DeviceSetIdRequest request{ pid, 1 };

  EXPECT_EQ(set_param(request), request_expected);
}

TEST(TestMotherboard, CreategGet24VRailRequest)
{
  // 55:10:04:87:02:91:b3:01:b2
  const Voltage24VRailRequest request{ pid };
  EXPECT_EQ(get_param(request), QByteArray::fromHex("55103b8702aeb301b2"));
}

TEST(TestMotherboard, CreategGetUvVRailRequest)
{
  const VoltageUVLampRequest request{ pid };
  EXPECT_EQ(get_param(request), QByteArray::fromHex("55103b8702aeb302b1"));
}

TEST(TestMotherboard, CreategGetR2RDACRequest)
{
  const VoltageR2RDACRequest request{ pid };
  EXPECT_EQ(get_param(request), QByteArray::fromHex("55103b8702aeb303b0"));
}

TEST(TestMotherboard, CreategGetButtonsRequest)
{
  const VoltageBtnsRequest request{ pid };
  EXPECT_EQ(get_param(request), QByteArray::fromHex("55103b8702aeb501b4"));
}

TEST(TestMotherboard, CreateMoveWhileRequest)
{
  // 55:10:3b:86:0b:a6:b1:04:57:00:00:00:01:00:00:00:02:e1
  const auto request_expected{ QByteArray::fromHex(
    "55103b860ba6b104570000000100000002e1") };

  constexpr std::chrono::seconds time{ 1 };
  constexpr device::Coordinate target{ 2 };

  const GotoMixTimeRequest request{ pid, engine_id, time, target };
  EXPECT_EQ(set_param(request), request_expected);
}

TEST(TestMotherboard, CreateSnsStateRequest)
{
  // 55:10:3b:87:02:ae:b6:01:b7
  const auto request_expected{ QByteArray::fromHex("55103b8702aeb601b7") };
  const SnsStateRequest request{ pid };

  EXPECT_EQ(get_param(request), request_expected);
}

TEST(TestMotherboard, EncodingGotoAbsolutePositionRequest)
{
  using Request = GotoAbsPosRequest;
  // one engine request
  const std::array subjects{
    std::make_tuple(
      device::Coordinate{ 42069 },
      QByteArray::fromHex("55103b860ba6b10401000000000000a45545")),

    std::make_tuple(
      device::Coordinate{ -42069 },
      QByteArray::fromHex("55103b860ba6b10401ffffffffffff5bab44")),
  };
  for (auto&& subject : subjects) {
    const Request request{ pid,
                           engine_id,
                           std::get<device::Coordinate>(subject) };
    EXPECT_EQ(set_param(request).size(), std::get<QByteArray>(subject).size());
    EXPECT_EQ(set_param(request), std::get<QByteArray>(subject));
  }

  // multi engines request
  const std::array multi_subjects{ std::make_pair(
    Request::Params{ std::make_pair(0x3, device::Coordinate{ 42069 }),
                     std::make_pair(0x4, device::Coordinate{ 123 }) },
    QByteArray::fromHex(
      "55103b8615b8b10301000000000000a4550401000000000000007b3c")) };
  for (auto&& subject : multi_subjects) {
    const Request request{ pid, std::get<Request::Params>(subject) };
    const auto bytes{ set_param(request) };
    EXPECT_EQ(bytes.size(), std::get<QByteArray>(subject).size());
    EXPECT_EQ(bytes, std::get<QByteArray>(subject));
  }
}

TEST(TestMotherboard, EncodingGotoAbsolutePositionFail)
{
  using Request = GotoAbsPosRequest;
  const std::array subjects{
    // no engines requested
    Request::Params{},
    // same engines requested
    Request::Params{ std::make_pair(0x03, device::Coordinate{ 50 }),
                     std::make_pair(0x03, device::Coordinate{ 50 }) },
    // more than 2 engines requested
    Request::Params{ std::make_pair(0x03, device::Coordinate{ 50 }),
                     std::make_pair(0x04, device::Coordinate{ 50 }),
                     std::make_pair(0x05, device::Coordinate{ 50 }) }

  };
  for (auto&& subject : subjects) {
    const Request request{ pid, subject };
    EXPECT_TRUE(request.payload.empty());
  }
}

TEST(TestMotherboard, EncodingGotoNTicks)
{
  constexpr protocol::PacketID pid{ 0x78 };

  // one engine request
  const std::array subjects{
    std::make_tuple(
      0x4,
      device::Coordinate{ 42069 },
      QByteArray::fromHex("551078860be5b10400000000000000a45544")),

    std::make_tuple(
      0x4,
      device::Coordinate{ -42069 },
      QByteArray::fromHex("551078860be5b10400ffffffffffff5bab45")),
  };
  for (auto&& subject : subjects) {
    const GotoNTicksRequest request{ pid,
                                     static_cast<uint8_t>(
                                       std::get<int>(subject)),
                                     std::get<device::Coordinate>(subject) };
    EXPECT_EQ(set_param(request).size(), std::get<QByteArray>(subject).size());
    EXPECT_EQ(set_param(request), std::get<QByteArray>(subject));
  }

  // multi engines request
  const std::array multi_subjects{ std::make_pair(
    GotoNTicksRequest::Params{ std::make_pair(0x3, device::Coordinate{ 42069 }),
                               std::make_pair(0x4, device::Coordinate{ 123 }) },
    QByteArray::fromHex(
      "5510788615fbb10300000000000000a4550400000000000000007b3c")) };
  for (auto&& subject : multi_subjects) {
    const GotoNTicksRequest request{
      pid, std::get<GotoNTicksRequest::Params>(subject)
    };
    const auto bytes{ set_param(request) };
    EXPECT_EQ(bytes.size(), std::get<QByteArray>(subject).size());
    EXPECT_EQ(bytes, std::get<QByteArray>(subject));
  }
}

TEST(TestMotherboard, EncodingGotoNTicksFail)
{
  const std::array subjects{
    // no engines requested
    GotoNTicksRequest::Params{},
    // same engines requested
    GotoNTicksRequest::Params{ std::make_pair(0x03, device::Coordinate{ 50 }),
                               std::make_pair(0x03, device::Coordinate{ 50 }) },
    // more than 2 engines requested
    GotoNTicksRequest::Params{ std::make_pair(0x03, device::Coordinate{ 50 }),
                               std::make_pair(0x04, device::Coordinate{ 50 }),
                               std::make_pair(0x05, device::Coordinate{ 50 }) }

  };
  for (auto&& subject : subjects) {
    const GotoNTicksRequest request{ pid, subject };
    EXPECT_TRUE(request.payload.empty());
  }
}

TEST(TestMotherboard, EncodingGotoNTicksUNC)
{
  constexpr protocol::PacketID pid{ 0x78 };
  const std::array targets{
    std::make_tuple(
      device::Coordinate{ 42069 },
      QByteArray::fromHex("551078860be5b10404000000000000a45540")),
    std::make_tuple(
      device::Coordinate{ -42069 },
      QByteArray::fromHex("551078860be5b10404ffffffffffff5bab41"))
  };

  for (auto&& target : targets) {
    const protocol::motherboard::GotoNTicksUNCRequest request{
      pid, engine_id, std::get<device::Coordinate>(target)
    };
    EXPECT_EQ(set_param(request), std::get<QByteArray>(target));
  }
}

TEST(TestMotherboard, EncodingGotoNTicksUNCWoOpts)
{
  // declaring constants and testing data format
  constexpr protocol::PacketID pid{ 0x78 };
  const std::array targets{
    std::make_tuple(
      device::Coordinate{ 42069 },
      QByteArray::fromHex("551078860be5b104f7000000000000a455b3")),
    std::make_tuple(
      device::Coordinate{ -42069 },
      QByteArray::fromHex("551078860be5b104f7ffffffffffff5babb2"))
  };

  for (auto&& target : targets) {
    const GotoNTicksUNCRequestWoOpts request{
      pid, engine_id, std::get<device::Coordinate>(target)
    };
    EXPECT_EQ(set_param(request), std::get<QByteArray>(target));
  }
}

TEST(TestMotherboard, CreateMotherboardFwRevRequest)
{
  // 55:10:03:87:02:96:00:01:01
  constexpr protocol::PacketID pid_{ 0x03 };
  const DeviceRequestFwRev request{ pid_ };

  EXPECT_EQ(get_param(request), QByteArray::fromHex("551003870296000202"));
}

TEST(TestMotherboard, CreateEngineGetMaxSpeedRequest)
{
  constexpr uint8_t engine_id_{ 0x03 };
  const EngineRequestMaxSpeed request{ pid, engine_id_ };

  EXPECT_EQ(get_param(request), QByteArray::fromHex("55103b8703afb20300b1"));
}

TEST(TestMotherboard, CreateEngineGetRepeatabilityRequest)
{
  constexpr uint8_t engine_id_{ 0x03 };
  const EngineRequestRepeatability request{ pid, engine_id_ };

  EXPECT_EQ(get_param(request), QByteArray::fromHex("55103b8703afb20303b2"));
}

TEST(TestMotherboard, CreateEngineGetMicrostepRequest)
{
  constexpr uint8_t engine_id_{ 0x03 };
  const EngineRequestMicrostep request{ pid, engine_id_ };

  EXPECT_EQ(get_param(request), QByteArray::fromHex("55103b8703afb20304b5"));
}

TEST(TestMotherboard, CreateEngineGetHomePositionRequest)
{
  constexpr uint8_t engine_id_{ 0x03 };
  const EngineRequestHomePosition request{ pid, engine_id_ };
  EXPECT_EQ(get_param(request), QByteArray::fromHex("55103b8703afb20305b4"));
}

TEST(TestMotherboard, CreateEngineGetF0Request)
{
  constexpr uint8_t engine_id_{ 0x03 };
  const EngineRequestF0 request{ pid, engine_id_ };
  EXPECT_EQ(get_param(request), QByteArray::fromHex("55103b8703afb20306b7"));
}

TEST(TestMotherboard, CreateEngineGetFmaxRequest)
{
  constexpr uint8_t engine_id_{ 0x03 };
  const EngineRequestFMax request{ pid, engine_id_ };
  EXPECT_EQ(get_param(request), QByteArray::fromHex("55103b8703afb20307b6"));
}

TEST(TestMotherboard, CreateEngineGetdFmaxRequest)
{
  constexpr uint8_t engine_id_{ 0x03 };
  const EngineRequestDFMax request{ pid, engine_id_ };
  EXPECT_EQ(get_param(request), QByteArray::fromHex("55103b8703afb20308b9"));
}

TEST(TestMotherboard, CreateEngineGetCurrentPositionRequest)
{
  constexpr uint8_t engine_id_{ 0x03 };
  const EngineRequestPosition request{ pid, engine_id_ };
  EXPECT_EQ(get_param(request), QByteArray::fromHex("55103b8703afb2031cad"));
}

TEST(TestMotherboard, CreateEngineGetCurrentsRequest)
{
  constexpr uint8_t engine_id_{ 0x03 };
  const EnginesCurrentsGetRequest request{ pid, engine_id_ };
  EXPECT_EQ(get_param(request), QByteArray::fromHex("55103b8703afb2032293"));
}

TEST(TestMotherboard, CreateEngineGetMixModeStateRequest)
{
  constexpr uint8_t engine_id_{ 0x03 };
  const EngineRequestMixModeState request{ pid, engine_id_ };
  EXPECT_EQ(get_param(request), QByteArray::fromHex("55103b8703afb2032190"));
}

TEST(TestMotherboard, CreateGerconGetStateRequest)
{
  const GerconGetStateRequest request{ pid };
  EXPECT_EQ(get_param(request), QByteArray::fromHex("55103b8702aeb701b6"));
}

TEST(TestMotherboard, CreateGerconClearTriggerRequest)
{
  const GerconClearTriggerRequest request{ pid };
  EXPECT_EQ(set_param(request), QByteArray::fromHex("55103b8602afb701b6"));
}

TEST(TestMotherboard, CreateRgbSetOnOffRequest)
{
  constexpr protocol::PacketID pid_{ 123 };
  using ColorMap = std::vector<device::Color>;
  const std::array subjects{
    std::make_tuple(QByteArray::fromHex("55107b8606ebb50100004a9967"),
                    ColorMap{ device::Color::Blue,
                              device::Color::Cyan,
                              device::Color::Green,
                              device::Color::Magenta,
                              device::Color::Red }),
    std::make_tuple(QByteArray::fromHex("55107b8606ebb50100000000b4"),
                    ColorMap{ device::Color::Black,
                              device::Color::Black,
                              device::Color::Black,
                              device::Color::Black,
                              device::Color::Black }),
    std::make_tuple(QByteArray::fromHex("55107b8606ebb50100006df62f"),
                    ColorMap{ device::Color::Yellow,
                              device::Color::Yellow,
                              device::Color::White,
                              device::Color::Yellow,
                              device::Color::Yellow })
  };
  for (auto&& subject : subjects) {
    const RgbSetOnOffRequest request{ pid_, std::get<ColorMap>(subject) };
    EXPECT_EQ(set_param(request), std::get<QByteArray>(subject));
  }
}

TEST(TestMotherboard, CreateSystemSoftRebootRequest)
{
  const SystemSoftRebootRequest request{ pid };
  EXPECT_EQ(factory.create(request.pid, request.command),
            QByteArray::fromHex("55103b8300a8"));
}

TEST(TestMotherboard, CreateSystemHardRebootRequest)
{
  const SystemHardRebootRequest request{ pid };
  EXPECT_EQ(factory.create(request.pid, request.command),
            QByteArray::fromHex("55103b8200a9"));
}

TEST(TestMotherboard, CreateSystemGoToBootloaderRequest)
{
  const SystemGoToBootloaderRequest request{ pid };
  EXPECT_EQ(factory.create(request.pid, request.command),
            QByteArray::fromHex("55103b77005c"));
}

TEST(TestMotherboard, CreateSystemGetErrorRequest)
{
  using Subject = std::tuple<uint8_t, QByteArray>;
  std::vector<Subject> subjects;
  subjects.reserve(128);
  for (uint8_t idx = 0; idx < 128; ++idx) {
    auto subject{ QByteArray::fromHex("55103b8401aeAA00") };
    subject[6] = idx;
    subject[7] = idx;
    subjects.emplace_back(idx, subject);
  }

  for (auto&& subject : subjects) {
    const SystemGetErrorRequest request{ pid, std::get<uint8_t>(subject) };
    EXPECT_EQ(factory.create(request.pid, request.command, request.payload),
              std::get<QByteArray>(subject));
  }
}

TEST(TestMotherboard, CreateSystemZeroizeErrorRequest)
{
  const SystemZeroizeErrorRequest request{ pid };
  EXPECT_EQ(factory.create(request.pid, request.command),
            QByteArray::fromHex("55103b8500ae"));
}

TEST(TestMotherboard, CreateEngineDriverFactoryDefaultRequest)
{
  constexpr protocol::PacketID pid_{ 0x05 };

  const std::array subjects{
    std::make_tuple(UINT8_C(1), QByteArray::fromHex("551005860390b201f043")),
  };
  for (auto&& subject : subjects) {
    const EngineDriverFactoryDefault request{
      pid_, static_cast<uint8_t>(std::get<0>(subject))
    };
    EXPECT_EQ(set_param(request), std::get<QByteArray>(subject));
  }
}

TEST(TestMotherboard, CreateEngineWriteMaxSpeedRequest)
{
  constexpr device::SpeedIndex new_speed{ 300U };
  const EngineWriteMaxSpeed request{ pid, engine_id, new_speed };

  EXPECT_EQ(set_param(request),
            QByteArray::fromHex("55103b8607aab204000000012c9b"));
}

TEST(TestMotherboard, CreateEngineWriteRepeatabilityRequest)
{
  constexpr device::Repeatability new_rep{ 2 };
  const EngineWriteRepeatability request{ pid, engine_id, new_rep };
  EXPECT_EQ(set_param(request), QByteArray::fromHex("55103b8604a9b2040302b7"));
}

TEST(TestMotherboard, CreateEngineWriteMicrostepRequest)
{
  const auto new_ms{ device::Microstep::Step_1_16 };
  const EngineWriteMicrostep request{ pid, engine_id, new_ms };

  EXPECT_EQ(set_param(request), QByteArray::fromHex("55103b8604a9b2040404b6"));
}

TEST(TestMotherboard, CreateEngineWriteHomePositionRequest)
{
  constexpr device::Coordinate new_home{ 2 };
  const EngineWriteHomePosition request{ pid, engine_id, new_home };

  EXPECT_EQ(set_param(request),
            QByteArray::fromHex("55103b860ba6b204050000000000000002b1"));
}

TEST(TestMotherboard, CreateEngineWriteFRequest)
{
  const EngineWriteF0 request_f0{ pid, engine_id, device::Frequency{ 50.2 } };
  EXPECT_EQ(set_param(request_f0),
            QByteArray::fromHex("55103b860ba6b20406404919999999999a3a"));

  const EngineWriteFMax request_fmax{ pid,
                                      engine_id,
                                      device::Frequency{ 100.3 } };
  EXPECT_EQ(set_param(request_fmax),
            QByteArray::fromHex("55103b860ba6b20407405913333333333388"));

  const EngineWriteDFMax request_dfmax{ pid,
                                        engine_id,
                                        device::Frequency{ 123.1 } };
  EXPECT_EQ(set_param(request_dfmax),
            QByteArray::fromHex("55103b860ba6b204083fbf837b4a2339c157"));
}

TEST(TestMotherboard, CreateEngineWritePositionRequest)
{
  constexpr device::Coordinate new_pos{ 22435 };
  const EngineWritePosition request{ pid, engine_id, new_pos };

  EXPECT_EQ(set_param(request),
            QByteArray::fromHex("55103b860ba6b2041c00000000000057a35e"));
}

TEST(TestMotherboard, CreateEngineSavePositionRequest)
{
  const EngineSavePosition request{ pid, engine_id };
  EXPECT_EQ(set_param(request), QByteArray::fromHex("55103b8603aeb2040cba"));
}

TEST(TestMotherboard, CreateEngineLoadPositionRequest)
{
  const EngineLoadPosition request{ pid, engine_id };
  EXPECT_EQ(set_param(request), QByteArray::fromHex("55103b8603aeb2040dbb"));
}

TEST(TestMotherboard, CreateEngineEnableRequest)
{
  const std::array subjects{
    std::make_tuple(true, QByteArray::fromHex("55103b8603aeb2041aac")),
    std::make_tuple(false, QByteArray::fromHex("55103b8603aeb2041bad"))
  };
  for (auto&& subject : subjects) {
    const EngineEnableRequest request{ pid,
                                       engine_id,
                                       std::get<bool>(subject) };
    EXPECT_EQ(set_param(request), std::get<QByteArray>(subject));
  }
}

TEST(TestMotherboard, CreateGotoZeroRequest)
{
  const std::array subjects{
    std::make_tuple(device::ZeroSwitch::ZeroIsSw1,
                    QByteArray::fromHex("55103b8604a9b1040200b7")),
    std::make_tuple(device::ZeroSwitch::ZeroIsSw2,
                    QByteArray::fromHex("55103b8604a9b1040201b6"))
  };
  for (auto&& subject : subjects) {
    const GotoZeroRequest request{ pid,
                                   engine_id,
                                   std::get<device::ZeroSwitch>(subject) };
    EXPECT_EQ(set_param(request), std::get<QByteArray>(subject));
  }
}

TEST(TestMotherboard, CreateEmergencyStopRequest)
{
  using Request = GotoEmStopRequest;
  const Request request{ pid, engine_id };
  EXPECT_EQ(set_param(request), QByteArray::fromHex("55103b8603aeb10403b6"));

  const Request::Params multi_params{ 0x03, 0x04 };
  const Request multi_request{ pid, multi_params };
  EXPECT_EQ(set_param(multi_request),
            QByteArray::fromHex("55103b8605a8b103030403b6"));
}

TEST(TestMotherboard, CreateEmergencyStopRequestFail)
{
  using Request = GotoEmStopRequest;
  const std::array subjects{ // no engines requested
                             Request::Params{},
                             // same engines requested
                             Request::Params{ 0x03, 0x03 },
                             // more than 2 engines requested
                             Request::Params{ 0x03, 0x04, 0x05 }

  };
  for (auto&& subject : subjects) {
    const Request request{ pid, subject };
    ASSERT_TRUE(request.payload.empty());
  }
}

TEST(TestMotherboard, CreateSmoothStopRequest)
{
  using Request = GotoSmoothStopRequest;
  const Request request{ pid, engine_id };
  EXPECT_EQ(set_param(request), QByteArray::fromHex("55103b8603aeb10406b3"));

  const Request::Params multi_params{ 0x03, 0x04 };
  const Request multi_request{ pid, multi_params };
  EXPECT_EQ(set_param(multi_request),
            QByteArray::fromHex("55103b8605a8b103060406b6"));
}

TEST(TestMotherboard, CreateSmoothStopRequestFail)
{
  using Request = GotoSmoothStopRequest;
  const std::array subjects{ // no engines requested
                             Request::Params{},
                             // same engines requested
                             Request::Params{ 0x03, 0x03 },
                             // more than 2 engines requested
                             Request::Params{ 0x03, 0x04, 0x05 }

  };
  for (auto&& subject : subjects) {
    const Request request{ pid, subject };
    ASSERT_TRUE(request.payload.empty());
  }
}

TEST(TestMotherboard, CreateSpeedSetRequest)
{
  constexpr device::SpeedIndex speed{ 1234 };
  const GotoSpeedSetRequest::Params params{ std::make_pair(engine_id, speed) };
  const GotoSpeedSetRequest request{ pid, params };

  EXPECT_EQ(set_param(request),
            QByteArray::fromHex("55103b8607aab10405000004d266"));
}

TEST(TestMotherboard, CreateMixCyclesRequest)
{
  constexpr device::Cycles ticks{ 1243 };
  constexpr device::Coordinate amplitude{ 1235 };
  const GotoMixCyclesRequest request{ pid, engine_id, ticks, amplitude };

  EXPECT_EQ(set_param(request),
            QByteArray::fromHex("55103b860ba6b10456000004db000004d3eb"));
}

TEST(TestMotherboard, CreateStopMixRequest)
{
  const GotoMixStop requets{ pid, engine_id };
  EXPECT_EQ(set_param(requets), QByteArray::fromHex("55103b8603aeb10458ed"));
}

TEST(TestMotherboard, CreateSetEngineCurrentsRequest)
{
  constexpr auto accel{ device::Current::Current_23_32 };
  constexpr auto holding{ device::Current::Current_13_32 };
  const GotoCurrentsSetRequest request{ pid, engine_id, accel, holding };

  EXPECT_EQ(set_param(request),
            QByteArray::fromHex("55103b8605a8b1045a160cf5"));
}

TEST(TestMotherboard, CreatePowerSetCoolerRequest)
{
  constexpr device::PwmLevel pwm{ 123 };
  const PowerSetCoolerRequest request{ pid, pwm };

  //! @todo неправильный пример???
  EXPECT_EQ(set_param(request), QByteArray::fromHex("55103b8604a9b401007bce"));
}

TEST(TestMotherboard, CreatePowerSetLedsRequest)
{
  constexpr device::PwmLevel pwm{ 234 };
  const PowerSetLedsRequest request{ pid, pwm };

  EXPECT_EQ(set_param(request), QByteArray::fromHex("55103b8604a9b40200ea5c"));
}

TEST(TestMotherboard, CreatePowerSetUvLampRequest)
{
  const std::array subjects{
    std::make_pair(true, QByteArray::fromHex("55103b8603aeb40301b6")),
    std::make_pair(false, QByteArray::fromHex("55103b8603aeb40300b7"))
  };
  for (auto&& subject : subjects) {
    const PowerSetUVLampRequest request{ pid, std::get<bool>(subject) };
    EXPECT_EQ(set_param(request), std::get<QByteArray>(subject));
  }
}

TEST(TestMotherboard, CreatePowerSetBuzzerRequest)
{
  const std::array subjects{
    std::make_pair(true, QByteArray::fromHex("55103b8603aeb40401b1")),
    std::make_pair(false, QByteArray::fromHex("55103b8603aeb40400b0"))
  };
  for (auto&& subject : subjects) {
    const PowerSetBuzzerRequest request{ pid, std::get<bool>(subject) };
    EXPECT_EQ(set_param(request), std::get<QByteArray>(subject));
  }
}

TEST(TestMotherboard, CreatePowerSetShdnRequest)
{
  const std::array subjects{
    std::make_pair(true, QByteArray::fromHex("55103b8603aeb40501b0")),
    std::make_pair(false, QByteArray::fromHex("55103b8603aeb40500b1"))
  };
  for (auto&& subject : subjects) {
    const PowerSetSHDNRequest request{ pid, std::get<bool>(subject) };
    EXPECT_EQ(set_param(request), std::get<QByteArray>(subject));
  }
}

TEST(TestMotherboard, CreatePowerSetTurnOffRequest)
{
  const PowerSetTurnOff request{ pid };
  EXPECT_EQ(set_param(request), QByteArray::fromHex("55103b8602afb407b3"));
}

TEST(TestMotherboard, CreatePowerSetWdtTimeoutRequest)
{
  const std::array subjects{
    std::make_tuple(UINT32_C(10),
                    QByteArray::fromHex("55103b8606abb4060000000ab8")),
    std::make_tuple(UINT32_C(50),
                    QByteArray::fromHex("55103b8606abb4060000003280")),
    std::make_tuple(UINT32_C(100),
                    QByteArray::fromHex("55103b8606abb40600000064d6")),
    std::make_tuple(UINT32_C(125),
                    QByteArray::fromHex("55103b8606abb4060000007dcf")),
    std::make_tuple(UINT32_C(150),
                    QByteArray::fromHex("55103b8606abb4060000009624")),
    std::make_tuple(UINT32_C(250),
                    QByteArray::fromHex("55103b8606abb406000000fa48")),
    std::make_tuple(UINT32_C(500),
                    QByteArray::fromHex("55103b8606abb406000001f447")),
    std::make_tuple(UINT32_C(1000),
                    QByteArray::fromHex("55103b8606abb406000003e859")),
    std::make_tuple(UINT32_C(1250),
                    QByteArray::fromHex("55103b8606abb406000004e254")),
    std::make_tuple(UINT32_C(1500),
                    QByteArray::fromHex("55103b8606abb406000005dc6b")),
    std::make_tuple(UINT32_C(5000),
                    QByteArray::fromHex("55103b8606abb4060000138829"))
  };
  for (auto&& subject : subjects) {
    const PowerSetWdtTimeout request{ pid, std::get<uint32_t>(subject) };
    EXPECT_EQ(set_param(request), std::get<QByteArray>(subject));
  }
}

TEST(TestMotherboard, CreateEngineDriverRawWriteRequest)
{
  const auto payload{ QByteArray::fromHex("9191919") };
  const EngineDriverRawWrite request{ pid, engine_id, payload };
  EXPECT_EQ(set_param(request),
            QByteArray::fromHex("55103b8607aab204300919191996"));
}

TEST(TestMotherboard, CreateMotherboardPingRequest)
{
  const StatusPingRequest request{ pid };
  EXPECT_EQ(factory.create(request.pid, request.command),
            QByteArray::fromHex("55103b8100aa"));
}
