#include <gtest/gtest.h>

#include "common/discrete_input.h"
#include "motherboard/device/ir_sensors.h"
#include "motherboard/device/rgb_leds.h"

TEST(TestMotherboard, IrSensorsCTOR)
{
  device::IrSensors::Container cont;
  for (auto idx = 0; idx < 5; ++idx) {
    cont.push_back(std::make_unique<device::IrSensor>(idx));
  }

  device::IrSensors sensors{ std::move(cont) };
  ASSERT_EQ(sensors.count(), 5);
}

TEST(TestMotherboard, RGBLedsCTOR)
{
  device::RgbLeds::Container cont;
  for (auto idx = 0; idx < 5; ++idx) {
    cont.push_back(std::make_unique<device::RgbLed>(idx));
  }

  device::RgbLeds leds{ std::move(cont) };
  ASSERT_EQ(leds.count(), 5);
}

TEST(TestMotherboard, DiscreteInputUpdateValue)
{
  auto object = std::make_unique<device::DiscreteOutput>(false);
  ASSERT_FALSE(object->state());
  ASSERT_FALSE(object->trigger());

  object->setState(true);
  object->setTrigger(true);
  ASSERT_TRUE(object->state());
  ASSERT_TRUE(object->trigger());

  object->resetState();
  object->resetTrigger();
  ASSERT_FALSE(object->state());
  ASSERT_FALSE(object->trigger());

  object = std::make_unique<device::DiscreteOutput>(true);
  ASSERT_TRUE(object->state());
  object->resetState();
  ASSERT_TRUE(object->state());
}
