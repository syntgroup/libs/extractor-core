#include <gtest/gtest.h>

#include "device/engine_speed.h"

TEST(UnitTests, REalLife)
{
  struct Subject
  {
    double start, stop, step;
  };

  constexpr static std::array subjects{
    // from defaults
    std::make_pair(Subject{ 20.0, 50'000.0, 10'000.0 }, 15'759),
    // axises X and A
    std::make_pair(Subject{ 1'000.0, 100'000.0, 100'000.0 }, 3'113),
    // axises Z and B
    std::make_pair(Subject{ 1'000.0, 50'000.0, 100'000.0 }, 1'537),
    // axis C
    std::make_pair(Subject{ 500.0, 10'000.0, 100'000.0 }, 295)
  };

  for (auto&& subject : subjects) {
    const device::Frequency f0{ subject.first.start };
    const device::Frequency fmax{ subject.first.stop };
    const device::Frequency dfmax{ subject.first.step };
    const device::AccelerationProfile profile{ f0, fmax, dfmax };
    EXPECT_EQ(profile.maxSpeedIndex().value_of(), subject.second);
  }
}

TEST(UnitTests, frequencyToIndexCalcs)
{
  using Profile    = device::AccelerationProfile;
  using Frequency  = device::Frequency;
  using SpeedIndex = device::SpeedIndex;

  struct Subject
  {
    device::AccelerationProfile profile;
    device::SpeedIndex testIndex;
  };

  std::array subjects{
    Subject{ Profile{ Frequency{ 10 }, Frequency{ 100 }, Frequency{ 10000 } },
             SpeedIndex{ 20 } },
    Subject{ Profile{
               Frequency{ 1000 },
               Frequency{ 50000 },
               Frequency{ 100000 },
             },
             SpeedIndex{ 500 } },
    Subject{ Profile{ Frequency{ 20 }, Frequency{ 50000 }, Frequency{ 10000 } },
             SpeedIndex{ 250 } },
    Subject{
      Profile{ Frequency{ 1000 }, Frequency{ 100000 }, Frequency{ 100000 } },
      SpeedIndex{ 1500 } }
  };
  for (auto&& subject : subjects) {
    // частота на максимальном индексе по идее должна быть равна fmax
    EXPECT_NEAR(
      subject.profile.indexToFrequency(subject.profile.maxSpeedIndex())
        .value_of(),
      subject.profile.fmax().first.value_of(),
      2.5);

    // а частота на нулевом индексе -- минимальному f0
    EXPECT_NEAR(
      subject.profile.indexToFrequency(device::SpeedIndex{ 0 }).value_of(),
      subject.profile.f0().first.value_of(),
      0.1);

    // валимся, если хотим протестировать индекс, который больше, чем их вообще
    // рассчитано
    ASSERT_LE(subject.testIndex, subject.profile.maxSpeedIndex());

    // по идее можно запихать в AccelerationProfile условия, чтобы при расчетах
    // проверять значение аргумента на выход из допустииых значений и возвращать
    // nullopt, но тогда придется очень много дополнительно проверять
    // возвращаемое значение, что грустно

    // два раза преобразовываем индекс, хотим получить его же
    EXPECT_EQ(
      subject.profile
        .frequencyToIndex(subject.profile.indexToFrequency(subject.testIndex))
        .value_of(),
      subject.testIndex.value_of());
  }
}
