#include <gtest/gtest.h>

#include "common/abstract_operation.h"
#include "common/discrete_input.h"
#include "engines_factory.hpp"
#include "motherboard/device/engine_values.h"
#include "motherboard/device/engines.h"
#include "motherboard/gcode/parser_auto_home.h"
#include "motherboard/gcode/parser_linear_move.h"
#include "motherboard/gcode/parser_mix.h"
#include "motherboard/gcode/parser_rapid_move.h"
#include <gsl/gsl-lite.hpp>

using namespace device;

namespace {
constexpr auto deleter = [](gsl::owner<cmd::AbstractOperation*> operation) {
  delete operation;
};
} // namespace

TEST(TestMotherboard, AutoHomeSuccess)
{
  const auto pool = Factory::createEnginesPool();
  const gcode::ParserAutoHome parser{ pool };

  using Sample = std::pair<int, QString>;
  const std::array samples{ Sample{ 5, "" }, // all axises
                            Sample{ 1, "x" },        Sample{ 2, "x z" },
                            Sample{ 3, "x z a" },    Sample{ 4, "x z a b" },
                            Sample{ 5, "z x a b c" } };
  for (const Sample& sample : samples) {
    const auto result = parser.generate(sample.second);
    ASSERT_TRUE(result.has_value())
      << sample.second.toStdString() << ' ' << result.error().toStdString();
    ASSERT_EQ(result->size(), sample.first);

    std::ranges::for_each(result.value(), deleter);
  }
}

TEST(TestMotherboard, AutoHomeFailureNoEngines)
{
  const std::shared_ptr<Engines> pool{ nullptr };
  const gcode::ParserAutoHome parser{ pool };

  const auto result = parser.generate("test");
  ASSERT_FALSE(result.has_value());
  ASSERT_EQ(result.error(), "error accessing engines");
}

TEST(TestMotherboard, AutoHomeFailureUnknownAxis)
{
  const auto pool = Factory::createEnginesPool();
  const gcode::ParserAutoHome parser{ pool };

  const auto result = parser.generate("o");
  ASSERT_FALSE(result.has_value());
  ASSERT_EQ(result.error(), "unknown axis requested: o");
}

TEST(TestMotherboard, LinearMoveRealSpeedSuccess)
{ // new mode
  const auto pool   = Factory::createEnginesPool();
  const auto sensor = std::make_shared_for_overwrite<DiscreteOutput>();
  const gcode::ParserLinearMove parser{ pool,
                                        sensor,
                                        device::SpeedMode::Modern };

  // assume that G1 part has been filtered earlier
  const QStringList samples{
    "x500",        // one axis
    "x0",          // one axis target zero
    "x500 f5000",  // one axis + feedrate
    "x0 f10",      // one axis + feedrate
    "x50 z50 f10", // two axises + feedrate
    "X50 z50",     // two axises mixed case
    "x50.2",       // one axis float
    "x50 a50",     // two axises
    "x50.1 a50",   // two axises float
    "x50 a5.01",   // two axises float
  };

  for (const auto& sample : samples) {
    const auto result = parser.generate(sample);
    ASSERT_TRUE(result.has_value())
      << sample.toStdString() << result.error().toStdString();
    ASSERT_EQ(result.value().size(), 1);

    std::ranges::for_each(result.value(), deleter); // clean up operations
  }
}

TEST(TestMotherboard, LinearMoveSuccessGrabber)
{
  const auto pool   = Factory::createEnginesPool();
  const auto sensor = std::make_shared_for_overwrite<DiscreteOutput>();
  const gcode::ParserLinearMove parser{ pool,
                                        sensor,
                                        device::SpeedMode::Modern };

  // assume that G1 part has been filtered earlier
  const QStringList samples{
    "c50",     // one axis grabber!
    "x50 c12", // two axises grabber!
  };

  for (const auto& sample : samples) {
    const auto result = parser.generate(sample);
    ASSERT_TRUE(result.has_value())
      << sample.toStdString() << result.error().toStdString();
    ASSERT_EQ(result.value().size(), 2);

    std::ranges::for_each(result.value(), deleter); // clean up operations
  }
}

TEST(TestMotherboard, LinearMoveRealSpeedFailure)
{
  // new mode
  const auto pool   = Factory::createEnginesPool();
  const auto sensor = std::make_shared_for_overwrite<DiscreteOutput>();
  const gcode::ParserLinearMove parser{ pool,
                                        sensor,
                                        device::SpeedMode::Modern };

  const QStringList samples{
    "",            // empty string
    "lorem ipsum", // random text
    "o50",         // unknwon axis
    "x50 x50",     // two same axises
    "x50 z50 b50", // more than two axises
    "X-234.1",     // one axis negative float
    "x-13",        // one axis negative
    "x-24.1 a112", // two axises negative float
    "x24.1 a-112", // two axises negative float
    "x-24 a112.1", // two axises negative float
    "x24 a-112.1", // two axises negative float
  };
  for (const auto& sample : samples) {
    const auto result = parser.generate(sample);
    ASSERT_FALSE(result.has_value()) << sample.toStdString();
  }
}

TEST(TestMotherboard, LinearMoveRelativeSpeedSuccess)
{
  // legacy mode
  const auto pool   = Factory::createEnginesPool(/*with_profiles=*/false);
  const auto sensor = std::make_shared_for_overwrite<DiscreteOutput>();
  const gcode::ParserLinearMove parser{ pool,
                                        sensor,
                                        device::SpeedMode::Legacy };
  const std::array samples{ std::make_pair(1, QStringLiteral("x60")),
                            std::make_pair(1, QStringLiteral("x60 z50")) };
  for (const auto& sample : samples) {
    const auto result = parser.generate(std::get<QString>(sample));
    ASSERT_TRUE(result.has_value()) << std::get<QString>(sample).toStdString();
    ASSERT_EQ(result->size(), std::get<int>(sample));
  }
}

TEST(TestMotherboard, LinearMoveRelativeSpeedFailure)
{
  // legacy mode
  const auto pool   = Factory::createEnginesPool(/*with_profiles=*/false);
  const auto sensor = std::make_shared_for_overwrite<DiscreteOutput>();
  const gcode::ParserLinearMove parser{ pool,
                                        sensor,
                                        device::SpeedMode::Legacy };
  const QStringList samples;
  for (const auto& sample : samples) {
  }
}

TEST(TestMotherboard, LinearMoveFailureNoSensor)
{
  const auto pool = Factory::createEnginesPool();
  const std::shared_ptr<DiscreteOutput> sensor{ nullptr };
  const gcode::ParserLinearMove parser{ pool, sensor };

  const auto result = parser.generate("c50");
  ASSERT_FALSE(result.has_value());
  ASSERT_EQ(result.error(), "error accessing sensor");
}

TEST(TestMotherboard, LinearMoveFailureNoEngines)
{
  const std::shared_ptr<Engines> engines{ nullptr };
  const auto sensor = std::make_shared_for_overwrite<DiscreteOutput>();
  const gcode::ParserLinearMove parser{ engines, sensor };
  const QStringList subjects{ "x5 z5", "x5" };
  for (const auto& subject : subjects) {
    const auto result = parser.generate(subject);
    ASSERT_FALSE(result.has_value());
    ASSERT_EQ(result.error(), "error accessing engines");
  }
}

TEST(TestMotherboard, RapidMoveSuccess)
{
  const auto pool   = Factory::createEnginesPool();
  const auto sensor = std::make_shared_for_overwrite<DiscreteOutput>();
  const gcode::ParserRapidMove parser{ pool, sensor };
  const std::array samples{
    std::make_pair(1, QStringLiteral("x60")),         // one axis
    std::make_pair(1, QStringLiteral("x60 z50")),     // two axises
    std::make_pair(1, QStringLiteral("Z10 X112")),    // two axises uppercase
    std::make_pair(1, QStringLiteral("z10.1 x34")),   // two axises
    std::make_pair(1, QStringLiteral("z10 x34.5")),   // two axises
    std::make_pair(1, QStringLiteral("z10.1 x34.5")), // two axises all float
    std::make_pair(1, QStringLiteral("Z10 x34")),
    std::make_pair(2, QStringLiteral("C50")),     // with grabber
    std::make_pair(2, QStringLiteral("x50 c50")), // with grabber
  };
  for (const auto& sample : samples) {
    const auto result = parser.generate(std::get<QString>(sample));
    ASSERT_TRUE(result.has_value()) << std::get<QString>(sample).toStdString();
    ASSERT_EQ(result->size(), std::get<int>(sample));

    std::ranges::for_each(result.value(), deleter);
  }
}

TEST(TestMotherboard, RapidMoveFailureNoSensor)
{
  const auto pool = Factory::createEnginesPool();
  const std::shared_ptr<DiscreteOutput> sensor{ nullptr };
  const gcode::ParserRapidMove parser{ pool, sensor };

  const auto result = parser.generate("c5");
  ASSERT_FALSE(result.has_value());
  ASSERT_EQ(result.error(), "error accessing sensor");
}

TEST(TestMotherboard, RapidMoveFailureNoEngines)
{
  const std::shared_ptr<Engines> engines{ nullptr };
  const auto sensor = std::make_shared_for_overwrite<DiscreteOutput>();
  const gcode::ParserRapidMove parser{ engines, sensor };
  const QStringList subjects{ "x5 z5", "x5" };
  for (const auto& subject : subjects) {
    const auto result = parser.generate(subject);
    ASSERT_FALSE(result.has_value());
    ASSERT_EQ(result.error(), "error accessing engines");
  }
}

TEST(TestMotherboard, RapidMoveFailureEmptyString)
{
  const auto pool   = Factory::createEnginesPool();
  const auto sensor = std::make_shared_for_overwrite<DiscreteOutput>();
  const gcode::ParserRapidMove parser{ pool, sensor };

  const auto result = parser.generate("test");
  ASSERT_FALSE(result.has_value());
  ASSERT_EQ(result.error(),
            "incorrect syntax of G0 command -- no operands provided "
            "or incorrect axis name\ntest");
}

TEST(TestMotherboard, RapidMoveFailureTooManyRequests)
{
  const auto pool   = Factory::createEnginesPool();
  const auto sensor = std::make_shared_for_overwrite<DiscreteOutput>();
  const gcode::ParserRapidMove parser{ pool, sensor };

  const auto result = parser.generate("x5 z5 c4");
  ASSERT_FALSE(result.has_value());
  ASSERT_EQ(result.error(),
            "cant move more than 2 axis simultaneously, requested: 3");
}

TEST(TestMotherboard, RapidMoveFailureUnknownAxis)
{
  const auto pool   = Factory::createEnginesPool();
  const auto sensor = std::make_shared_for_overwrite<DiscreteOutput>();
  const gcode::ParserRapidMove parser{ pool, sensor };

  const QStringList subjects_multiple{ "x5 o9", "o9 x5" };

  for (const auto& subject : subjects_multiple) {
    const auto result = parser.generate(subject);
    ASSERT_FALSE(result.has_value()) << subject.toStdString();
    ASSERT_EQ(result.error(), "Error accessing internal memory")
      << result.error().toStdString();
  }

  const QStringList subjects_single{ "o9" };
  for (const auto& subject : subjects_single) {
    const auto result = parser.generate(subject);
    ASSERT_FALSE(result.has_value()) << subject.toStdString();
    ASSERT_EQ(result.error(), "unknown axis requested: o\no9");
  }
}

TEST(TestMotherboard, ParserMixCyclesSuccess)
{
  const auto pool = Factory::createEnginesPool();
  const gcode::ParserMixCycles parser{ pool };

  const QStringList samples{ "x50 p50", "x50 p50 f51", "z50.5 p100 f51" };

  for (const auto& sample : samples) {
    const auto result = parser.generate(sample);
    ASSERT_TRUE(result.has_value())
      << sample.toStdString() << ' ' << result.error().toStdString();
  }
}

TEST(TestMotherboard, ParserMixCyclesFailures)
{
  const auto pool = Factory::createEnginesPool();
  const gcode::ParserMixCycles parser{ pool };

  struct Subject
  {
    QString block;
    QString error;
  };
  const std::array subjects{
    Subject{ "", "Empty block is not allowed" }, // empty string
    Subject{
      "x50 f500",
      "incorrect syntax of G56 command // no cycles mentioned" }, // no cycles
    Subject{
      "x40",
      "incorrect syntax of G56 command // no cycles mentioned" }, // no cycles
                                                                  // no feedrate
    Subject{
      "p500 f500",
      "incorrect syntax of G56 command // no axis mentioned" }, // no axis
    Subject{ "o10 p50", "unknown axis requested: o" },          // unknown axis

    // multiple axises?
  };

  for (const auto& subject : subjects) {
    const auto result = parser.generate(subject.block);
    ASSERT_FALSE(result.has_value()) << subject.block.toStdString();
    ASSERT_EQ(result.error(), subject.error)
      << subject.block.toStdString() << ' ' << result.error().toStdString();
  }
}

TEST(TestMotherboard, ParserMixCyclesFailureNoEngines)
{
  const std::shared_ptr<Engines> pool{ nullptr };
  const gcode::ParserMixCycles parser{ pool };

  const auto result = parser.generate("x500 p500 f400");
  ASSERT_FALSE(result.has_value());
  ASSERT_EQ(result.error(), "error accessing engines")
    << result.error().toStdString();
}

TEST(TestMotherboard, ParserMixTimeSuccess)
{
  const auto pool = Factory::createEnginesPool();
  const gcode::ParserMixTime parser{ pool };

  const QStringList samples{
    "x50 p50", "x50 p50 f51", "z50.5 p100 f51", "X50 S123", "x50 s123 f50"
  };

  for (const auto& sample : samples) {
    const auto result = parser.generate(sample);
    ASSERT_TRUE(result.has_value())
      << sample.toStdString() << ' ' << result.error().toStdString();
  }
}

TEST(TestMotherboard, ParserMixTimeFailures)
{
  const auto pool = Factory::createEnginesPool();
  const gcode::ParserMixTime parser{ pool };

  struct Subject
  {
    QString block;
    QString error;
  };
  const std::array subjects{
    Subject{ "", "Empty block is not allowed" }, // empty string
    Subject{
      "x50 f500",
      "incorrect syntax of G57 command // no time mentioned" }, // no time
    Subject{
      "p500 f500",
      "incorrect syntax of G57 command // no axis mentioned" }, // no axis
    Subject{ "o10 p50", "unknown axis requested: o" },          // unknown axis
    Subject{ "o10 s50", "unknown axis requested: o" },          // unknown axis
  };

  for (const auto& subject : subjects) {
    const auto result = parser.generate(subject.block);
    ASSERT_FALSE(result.has_value()) << subject.block.toStdString();
    ASSERT_EQ(result.error(), subject.error)
      << subject.block.toStdString() << ' ' << result.error().toStdString();
  }
}

TEST(TestMotherboard, ParserMixTimeFailureNoEngines)
{
  const std::shared_ptr<Engines> pool{ nullptr };
  const gcode::ParserMixTime parser{ pool };

  const auto result = parser.generate("x500 p500 f400");
  ASSERT_FALSE(result.has_value());
  ASSERT_EQ(result.error(), "error accessing engines")
    << result.error().toStdString();
}
