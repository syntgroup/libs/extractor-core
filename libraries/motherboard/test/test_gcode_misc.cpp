#include <gtest/gtest.h>

#include "common/abstract_operation.h"
#include "common/discrete_input.h"
#include "engines_factory.hpp"
#include "motherboard/device/ir_sensors.h"
#include "motherboard/gcode/parser_buzzer.h"
#include "motherboard/gcode/parser_check_plates.h"
#include "motherboard/gcode/parser_check_sleeves.h"
#include "motherboard/gcode/parser_grabber_open.h"
#include "motherboard/gcode/parser_release_sleeves.h"
#include "qstring_support.hpp"
#include <gsl/gsl-lite.hpp>

namespace {
constexpr auto deleter = [](gsl::owner<cmd::AbstractOperation*> operaiton) {
  delete operaiton;
};
} // namespace

TEST(TestMotherboard, CheckPlatesFailureResourceAccess)
{
  const std::shared_ptr<device::IrSensors> sensors;
  const gcode::ParserCheckPlates parser{ sensors };

  const auto result = parser.generate("s2");
  ASSERT_FALSE(result.has_value());
  ASSERT_EQ(result.error(), "error accessing internal memory");
}

TEST(TestMotherboard, CheckPlatesFailures)
{
  const auto sensors = std::make_shared<device::IrSensors>(5);
  const gcode::ParserCheckPlates parser{ sensors };

  ASSERT_EQ(sensors->count(), 5);

  struct Subject
  {
    QString block;
    QString error;
  };
  const auto test = [&parser](const QString& block, const QString& error) {
    const auto result = parser.generate(block);
    ASSERT_FALSE(result.has_value()) << block;
    ASSERT_EQ(result.error(), error) << block << ' ' << error;
  };

  // case 0
  test("", "Empty block is not allowed");

  // case 1
  const QStringList case1{
    "p3",  // incorrect command
    "t0",  // incorrect command
    "o10", // incorrect command
  };
  const QString case1_error{
    "incorrect syntax of M600 command: no sensor index mentioned"
  };
  for (const auto& subject : case1) {
    test(subject, case1_error);
  }

  // case 2

  const QStringList case2{
    "S0",  // out of avaliable sensors
    "S10", // out of avaliable sensors
  };
  const QString case2_error{
    "incorrect syntax of M600 command: invalid index of sensor"
  };
  for (const auto& subject : case2) {
    test(subject, case2_error);
  }
}

TEST(TestMotherboard, BuzzerOnSuccess)
{
  const gcode::ParserBuzzerOn parser;
  const auto result = parser.generate("");
  ASSERT_TRUE(result.has_value());
  ASSERT_EQ(result->size(), 1);
  ASSERT_EQ(result->front()->description(), "Setting Buzzer ON");

  std::ranges::for_each(*result, deleter);
}

TEST(TestMotherboard, BuzzerOffSuccess)
{
  const gcode::ParserBuzzerOff parser;
  const auto result = parser.generate("");
  ASSERT_TRUE(result.has_value());
  ASSERT_EQ(result->size(), 1);
  ASSERT_EQ(result->front()->description(), "Setting Buzzer OFF");

  std::ranges::for_each(*result, deleter);
}

TEST(TestMotherboard, ParserJustCheckSuccess)
{
  const auto sensor = std::make_shared_for_overwrite<device::DiscreteOutput>();
  const gcode::ParserJustCheck parser{ sensor };
  const auto result = parser.generate("");
  ASSERT_TRUE(result.has_value());
  ASSERT_EQ(result->size(), 1);
  ASSERT_EQ(result->front()->description(),
            "Getting Diagnostic optical sensor state");

  std::ranges::for_each(*result, deleter);
}

TEST(TestMotherboard, ParserCacheSuccess)
{
  const auto sensor = std::make_shared_for_overwrite<device::DiscreteOutput>();
  sensor->setObjectName("SENSOR");

  const gcode::ParserSleevesCache parser{ sensor };
  const auto result = parser.generate("");
  ASSERT_TRUE(result.has_value());
  ASSERT_EQ(result->size(), 1);
  ASSERT_EQ(result->front()->description(), "REPORT CACHED BUTTON: SENSOR");

  std::ranges::for_each(*result, deleter);
}

TEST(TestMotherboard, ParserCheckSleevesSuccess)
{
  const auto pool   = Factory::createEnginesPool();
  const auto sensor = std::make_shared_for_overwrite<device::DiscreteOutput>();
  const gcode::ParserCheckSleeves parser{ pool, sensor };

  // magnets aren't down yet
  auto result = parser.generate("");
  ASSERT_TRUE(result.has_value());
  ASSERT_EQ(result->size(), 2);

  auto magnets = pool->engineByName(device::AxisAddress::Magnets);
  magnets->setCurrentPosition(magnets->mmToSteps(5.0));
  result = parser.generate("");
  ASSERT_TRUE(result.has_value());
  ASSERT_EQ(result->size(), 3);

  std::ranges::for_each(*result, deleter);
}

TEST(TestMotherboard, ParserCheckSleevesFailResources)
{
  std::shared_ptr<device::Engines> pool;
  std::shared_ptr<device::DiscreteOutput> sensor;

  auto parser = std::make_unique<gcode::ParserCheckSleeves>(pool, sensor);

  auto result = parser->generate("");
  ASSERT_FALSE(result.has_value());
  ASSERT_EQ(result.error(), "internal error");

  pool   = Factory::createEnginesPool();
  parser = std::make_unique<gcode::ParserCheckSleeves>(pool, sensor);

  result = parser->generate("");
  ASSERT_FALSE(result.has_value());
  ASSERT_EQ(result.error(), "internal error");
}

TEST(TestMotherboard, ParserGrabberOpenSuccess)
{
  const auto pool = Factory::createEnginesPool();

  const auto sensor = std::make_shared_for_overwrite<device::DiscreteOutput>();
  sensor->setObjectName("SENSOR");

  // target should be loaded from settings in target application
  // or be provided otherwise
  const gcode::ParserGrabberOpen parser{ pool, /*target=*/10.0, sensor };
  const auto result = parser.generate("");

  ASSERT_TRUE(result.has_value());
  ASSERT_EQ(result->size(), 2);
  ASSERT_EQ(result->at(0)->description(),
            "MULTI Moving Axis 4 to absolute position 400")
    << result->at(0)->description();
  ASSERT_EQ(result->at(1)->description(), "RESET CACHED BUTTON: SENSOR");

  std::ranges::for_each(*result, deleter);
}

TEST(TestMotherboard, ParserGrabberOpenFailure)
{
  std::shared_ptr<device::Engines> pool;
  std::shared_ptr<device::DiscreteOutput> sensor;

  // negative target
  auto parser = std::make_unique<gcode::ParserGrabberOpen>(pool, -5.5, sensor);
  auto result = parser->generate("");
  ASSERT_FALSE(result.has_value());
  ASSERT_EQ(result.error(), "Invalid target value: -5.5") << result.error();

  // no engines
  parser = std::make_unique<gcode::ParserGrabberOpen>(pool, 5.0, sensor);
  result = parser->generate("");
  ASSERT_FALSE(result.has_value());
  ASSERT_EQ(result.error(), "Error accessing internal memory");

  // no sensors
  pool   = Factory::createEnginesPool();
  parser = std::make_unique<gcode::ParserGrabberOpen>(pool, 5.0, sensor);
  result = parser->generate("");
  ASSERT_FALSE(result.has_value());
  ASSERT_EQ(result.error(), "Error accessing internal memory");
}

TEST(TestMotherboard, ParserReleaseSleevesSuccess)
{
  const auto pool = Factory::createEnginesPool();
  auto magnets    = pool->engineByName(device::AxisAddress::Magnets);
  magnets->setCurrentPosition(magnets->mmToSteps(5.0));

  const gcode::ParserReleaseSleeves parser{ pool };

  const auto result = parser.generate("");
  ASSERT_TRUE(result.has_value());

  const QStringList descriptions{
    "MULTI Moving Axis 3 to absolute position 0",
    "MULTI Moving Axis 4 to absolute position 3000",
    "MULTI Moving Axis 5 to absolute position 4800",
    "MULTI Moving Axis 4 to absolute position 2920",
    "MULTI Moving Axis 5 to absolute position 200",
    "MULTI Moving Axis 4 to absolute position 0"
  };
  ASSERT_EQ(result->size(), descriptions.size());
  for (size_t idx = 0; const auto& subject : descriptions) {
    auto* operation = result->at(idx++);
    ASSERT_EQ(operation->description(), subject)
      << operation->description() << " // " << subject;
  }

  std::ranges::for_each(*result, deleter);
}

TEST(TestMotherboard, ParserReleaseSleevesFailure)
{
  const std::shared_ptr<device::Engines> pool{ nullptr };
  const gcode::ParserReleaseSleeves parser{ pool };

  const auto result = parser.generate("");
  ASSERT_FALSE(result.has_value());
  ASSERT_EQ(result.error(), "Error accessing internal memory");
}

TEST(TestMotherboard, ParserCollectSleevesSuccess)
{
  const auto pool = Factory::createEnginesPool();
  const gcode::ParserCollectSleeves parser{ pool };

  const auto result = parser.generate("");
  ASSERT_TRUE(result.has_value());

  const QStringList descriptions{
    "MULTI Moving Axis 5 to absolute position 4800",
    "MULTI Moving Axis 4 to absolute position 3000",
    "MULTI Moving Axis 5 to absolute position 200",
    "MULTI Moving Axis 4 to absolute position 0",
  };
  ASSERT_EQ(result->size(), descriptions.size());
  for (size_t idx = 0; const auto& subject : descriptions) {
    auto* operation = result->at(idx++);
    ASSERT_EQ(operation->description(), subject)
      << operation->description() << " // " << subject;
  }

  std::ranges::for_each(*result, deleter);
}

TEST(TestMotherboard, ParserCollectSleevesFailure)
{
  const std::shared_ptr<device::Engines> pool{ nullptr };
  const gcode::ParserCollectSleeves parser{ pool };

  const auto result = parser.generate("");
  ASSERT_FALSE(result.has_value());
  ASSERT_EQ(result.error(), "Error accessing internal memory");
}
