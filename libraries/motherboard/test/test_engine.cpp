#include <gtest/gtest.h>

#include "motherboard/device/engine.h"
#include "motherboard/device/engine_defaults.h"
#include "motherboard/device/engine_params.h"
#include "motherboard/device/engine_speed.h"
#include "motherboard/device/engine_status.h"
#include "motherboard/device/engines.h"
#include "qstring_support.hpp"

TEST(TestMotherboard, EngineStatusCheckIndividualBits)
{
  auto status = std::make_unique<device::EngineStatus>(0b00000000);
  EXPECT_EQ(status->undefinedPos, false);
  EXPECT_EQ(status->switchSelect, false);
  EXPECT_EQ(status->engineGoToZero, false);
  EXPECT_EQ(status->switchFound, false);
  EXPECT_EQ(status->engineGo, false);
  EXPECT_EQ(status->engineGoWoOpts, false);
  EXPECT_EQ(status->engineErrorSaved, false);

  status = std::make_unique<device::EngineStatus>(0b00000001);
  EXPECT_EQ(status->undefinedPos, true);

  status = std::make_unique<device::EngineStatus>(0b00000010);
  EXPECT_EQ(status->switchSelect, true);

  status = std::make_unique<device::EngineStatus>(0b00000100);
  EXPECT_EQ(status->engineGoToZero, true);

  status = std::make_unique<device::EngineStatus>(0b00001000);
  EXPECT_EQ(status->switchFound, true);

  status = std::make_unique<device::EngineStatus>(0b00010000);
  EXPECT_EQ(status->engineGo, true);

  status = std::make_unique<device::EngineStatus>(0b00100000);
  EXPECT_EQ(status->engineGoWoOpts, true);

  status = std::make_unique<device::EngineStatus>(0b01000000);
  EXPECT_EQ(status->engineErrorSaved, true);

  status = std::make_unique<device::EngineStatus>(6);
  EXPECT_EQ(status->switchSelect, true);
  EXPECT_EQ(status->engineGoToZero, true);
}

TEST(TestMotherboard, MotherboardGetEngineByAxis)
{
  const std::unordered_map labels{
    std::make_pair(device::AxisAddress::Column, 'X'),
    std::make_pair(device::AxisAddress::Shield, 'A'),
    std::make_pair(device::AxisAddress::Magnets, 'B'),
    std::make_pair(device::AxisAddress::Sleeves, 'Z'),
    std::make_pair(device::AxisAddress::Grabber, 'C')
  };

  device::Engines::Container container;
  for (const auto& [axis, label] : labels) {
    container.emplace(
      axis,
      std::make_unique<device::Engine>(static_cast<uint8_t>(axis), label));
  }

  const device::Engines engines{ std::move(container) };

  const QStringList subjects{ "a", "b", "c", "x", "Z" };
  const QStringList errors{ "o" };

  for (const auto& subject : subjects) {
    const auto* engine = engines.engineByAxis(subject);
    EXPECT_TRUE(engine != nullptr);
  }

  for (const auto& subject : errors) {
    const auto* engine = engines.engineByAxis(subject);
    EXPECT_FALSE(engine != nullptr);
  }
}

TEST(TestMotherboard, EngineConvertMmToSteps)
{
  using namespace device;
  using enum AxisAddress;
  struct Subject
  {
    AxisAddress address;
    double millimmeters;
    qint64 steps;
    Microstep microstep;
    Coordinate homePosition;
    double stepsPerMm;
    double offset;
  };
  static constexpr std::array subjects{
    Subject{ Grabber, 3, 6, Microstep::Step_1_2, Coordinate{ 10 }, 1.0, 0.0 },
    Subject{
      Grabber, 234, 468, Microstep::Step_1_2, Coordinate{ 10 }, 1.0, 0.0 },
    Subject{
      Column, 250, 20208, Microstep::Step_1_16, Coordinate{ 10 }, 5.0, 2.6 },
    Subject{
      Column, 1234, 98928, Microstep::Step_1_16, Coordinate{ 10 }, 5.0, 2.6 },
    Subject{
      Sleeves, 500, 20020, Microstep::Step_1_8, Coordinate{ 2 }, 5.0, 0.5 },
    Subject{
      Sleeves, 42069, 1682780, Microstep::Step_1_8, Coordinate{ 2 }, 5.0, 0.5 }
  };
  for (auto&& subject : subjects) {
    auto params             = std::make_unique<device::EngineParams>();
    params->microstep       = subject.microstep;
    params->stepsPerMmRatio = subject.stepsPerMm;
    params->homePosition    = subject.homePosition;
    params->axisOffset      = subject.offset;

    const device::Engine engine{ std::move(params) };
    EXPECT_EQ(engine.mmToSteps(subject.millimmeters),
              device::Coordinate{ subject.steps });
    EXPECT_NEAR(engine.stepsToMm(subject.steps), subject.millimmeters, 0.1);
  }
}

TEST(TestMotherboard, SpeedToFrequencyConversion)
{
  using namespace device;
  struct Subject
  {
    AxisAddress axis;
    Speed milimetersPerMinute;
    Frequency stepsPerSecond;
  };

  static constexpr std::array subjects{
    Subject{ AxisAddress::Column, Speed{ 600 }, Frequency{ 800 } },
    Subject{ AxisAddress::Column, Speed{ 3600 }, Frequency{ 4800 } }
  };

  for (auto&& subject : subjects) {
    auto params             = std::make_unique<device::EngineParams>();
    params->microstep       = Microstep::Step_1_16;
    params->stepsPerMmRatio = 5.0;
    params->axisOffset      = 2.6;

    const device::Engine engine{ std::move(params) };
    EXPECT_DOUBLE_EQ(
      engine.speedToFrequency(subject.milimetersPerMinute).value_of(),
      subject.stepsPerSecond.value_of());
  }
}

TEST(TestMotherboard, AccelerationProfileConversions)
{
  static constexpr device::Frequency f0{ 1000 };
  static constexpr device::Frequency fmax{ 50000 };
  static constexpr device::Frequency dfmax{ 100000 };
  static constexpr device::SpeedIndex calculated{ 1537 };

  const device::AccelerationProfile profile{ f0, fmax, dfmax };

  ASSERT_DOUBLE_EQ(profile.f0().first.value_of(), f0.value_of());
  ASSERT_EQ(profile.f0().second, device::FrequencyType::F0);

  ASSERT_DOUBLE_EQ(profile.fmax().first.value_of(), fmax.value_of());
  ASSERT_EQ(profile.fmax().second, device::FrequencyType::Fmax);

  ASSERT_DOUBLE_EQ(profile.dfmax().first.value_of(), dfmax.value_of());
  ASSERT_EQ(profile.dfmax().second, device::FrequencyType::dFmax);

  ASSERT_EQ(profile.maxSpeedIndex(), calculated);

  static constexpr device::SpeedIndex subject_index{ 1000 };
  static constexpr device::Frequency subject_freq{ 45188.1 };

  ASSERT_NEAR(profile.indexToFrequency(subject_index).value_of(),
              subject_freq.value_of(),
              1);
  ASSERT_EQ(profile.frequencyToIndex(subject_freq), subject_index);
}

TEST(TestMotherboard, AccelerationProfileUpdates)
{
  static constexpr device::Frequency f0{ 1000 };
  static constexpr device::Frequency fmax{ 50000 };
  static constexpr device::Frequency dfmax{ 100000 };

  device::AccelerationProfile profile{ f0, fmax, dfmax };

  profile.setF0(device::Frequency{ 2000 });
  ASSERT_EQ(profile.maxSpeedIndex(), device::SpeedIndex{ 1497 })
    << profile.maxSpeedIndex().value_of();

  profile.setFmax(device::Frequency{ 60000 });
  ASSERT_EQ(profile.maxSpeedIndex(), device::SpeedIndex{ 1812 })
    << profile.maxSpeedIndex().value_of();

  profile.setdFmax(device::Frequency{ 150000 });
  // looks kinda strange
  ASSERT_EQ(profile.maxSpeedIndex(), device::SpeedIndex{ 4650 })
    << profile.maxSpeedIndex().value_of();
}

TEST(TestMotherboard, EngineMixingCycles)
{
  device::Engine engine{ 3 };
  ASSERT_EQ(engine.currentMixCycle(), device::Cycles{ 0 });
  ASSERT_EQ(engine.currentMixTime(), std::chrono::seconds{ 0 });

  engine.setCurrentMixTime(std::chrono::seconds{ 0 });
  engine.setCurrentMixCycle(device::Cycles{ 0 });
  ASSERT_EQ(engine.currentMixCycle(), device::Cycles{ 0 });
  ASSERT_EQ(engine.currentMixTime(), std::chrono::seconds{ 0 });

  static constexpr device::Cycles subject_cycle{ 100500 };
  static constexpr std::chrono::seconds subject_time{ 600 };
  engine.setCurrentMixCycle(subject_cycle);
  engine.setCurrentMixTime(subject_time);
  ASSERT_EQ(engine.currentMixCycle(), subject_cycle);
  ASSERT_EQ(engine.currentMixTime(), subject_time);
}

TEST(TestMotherboard, EngineCtorDefault)
{
  const device::Engine engine{ 0x5 };

  EXPECT_EQ(engine.address(), 0x5);

  EXPECT_TRUE(
    engine.accelerationProfile(device::AccelerationCurve::Slow).has_value());
  EXPECT_TRUE(
    engine.accelerationProfile(device::AccelerationCurve::Normal).has_value());
  EXPECT_TRUE(
    engine.accelerationProfile(device::AccelerationCurve::Fast).has_value());
}

TEST(TestMotherboard, EngineLoadParams)
{
  device::Engine engine{ 0x05 };
  ASSERT_EQ(engine.address(), 5);

  auto params     = std::make_unique_for_overwrite<device::EngineParams>();
  params->address = 0x01;

  engine.loadSettings(std::move(params));
  ASSERT_EQ(engine.address(), 1);

  auto test = engine.saveSettings();
  ASSERT_EQ(test.get().address, 1);
}

TEST(TestMotherboard, EngineCtorWParams)
{
  device::Engine::ParamsContainer params =
    std::make_unique<device::EngineParams>();
  params->address         = 0x05;
  params->name            = "test";
  params->axisOffset      = 0.5;
  params->stepsPerMmRatio = 5.0;
  params->currentStandby  = device::Current::Current_10_32;
  params->currentMoving   = device::Current::Current_10_32;
  params->zeroSwitch      = device::ZeroSwitch::ZeroIsSw1;
  params->homingSpeed     = device::SpeedIndex{ 40 };

  const device::Engine engine{ std::move(params) };

  EXPECT_EQ(engine.address(), 0x05);
  EXPECT_EQ(engine.name(), "test");
  EXPECT_EQ(engine.axisOffset(), 0.5);
  EXPECT_EQ(engine.stepsPerMmRatio(), 5.0);
  EXPECT_EQ(engine.currentStdby(), device::Current::Current_10_32);
  EXPECT_EQ(engine.currentAccel(), device::Current::Current_10_32);
  EXPECT_EQ(engine.zeroSwitch(), device::ZeroSwitch::ZeroIsSw1);
  EXPECT_EQ(engine.homingSpeed().value_of(), 40);
}

TEST(TestMotherboard, EngineCtorWParamsWProfiles)
{
  constexpr device::Frequency normal_f0{ 100 };
  constexpr device::Frequency normal_fmax{ 10000 };
  constexpr device::Frequency normal_dfmax{ 1000 };

  device::Engine::ParamsContainer params =
    std::make_unique<device::EngineParams>();
  params->address = 0x05;

  auto slow   = std::make_unique<device::AccelerationProfile>();
  auto fast   = std::make_unique<device::AccelerationProfile>();
  auto normal = std::make_unique<device::AccelerationProfile>(
    normal_f0, normal_fmax, normal_dfmax);

  const device::Engine engine{
    std::move(params), std::move(slow), std::move(normal), std::move(fast)
  };
  EXPECT_EQ(engine.address(), 0x05);

  const auto normal_profile =
    engine.accelerationProfile(device::AccelerationCurve::Normal);
  EXPECT_TRUE(normal_profile.has_value());
  EXPECT_FLOAT_EQ(normal_profile->get().f0().first.value_of(),
                  normal_f0.value_of());
  EXPECT_FLOAT_EQ(normal_profile->get().fmax().first.value_of(),
                  normal_fmax.value_of());
  EXPECT_FLOAT_EQ(normal_profile->get().dfmax().first.value_of(),
                  normal_dfmax.value_of());
}

TEST(TestMotherboard, EngineCtorWParamsWProfilesMap)
{
  device::Engine::ParamsContainer params =
    std::make_unique<device::EngineParams>();
  params->address = 0x05;

  device::Engine::SpeedProfileContainer profiles;
  profiles.emplace(device::AccelerationCurve::Normal,
                   std::make_unique<device::AccelerationProfile>());

  const device::Engine engine{ std::move(params), std::move(profiles) };
  EXPECT_EQ(engine.address(), 0x05);
  EXPECT_TRUE(
    engine.accelerationProfile(device::AccelerationCurve::Normal).has_value());
  EXPECT_FALSE(
    engine.accelerationProfile(device::AccelerationCurve::Fast).has_value());
}

TEST(TestMotherboard, EngineCtorWParamsWProfilesMapFailure)
{
  device::Engine::ParamsContainer params =
    std::make_unique<device::EngineParams>();
  params->address = 0x05;

  device::Engine::SpeedProfileContainer profiles;

  const device::Engine engine{ std::move(params), std::move(profiles) };
  EXPECT_EQ(engine.address(), 0x05);
  EXPECT_FALSE(
    engine.accelerationProfile(device::AccelerationCurve::Normal).has_value());
  EXPECT_FALSE(
    engine.accelerationProfile(device::AccelerationCurve::Fast).has_value());
}

TEST(TestMotherboard, UpdateValuesInSpeedProfile)
{
  constexpr auto address = 0x05;
  constexpr auto curve   = device::AccelerationCurve::Normal;
  constexpr device::Frequency initial_value{ 500 };
  constexpr device::Frequency modified_value{ 1000 };

  // ---------------------------------------------------------------------------

  device::Engine::ParamsContainer params =
    std::make_unique<device::EngineParams>();
  params->address = address;

  device::Engine::SpeedProfileContainer profiles;
  device::AccelerationProfile normal{ initial_value,
                                      initial_value,
                                      initial_value };
  profiles.emplace(curve,
                   std::make_unique<device::AccelerationProfile>(normal));

  const device::Engine engine{ std::move(params), std::move(profiles) };

  // --------------------------------------------------------------------------
  {
    const auto profile = engine.accelerationProfile(curve);
    ASSERT_TRUE(profile.has_value());
    EXPECT_DOUBLE_EQ(profile->get().f0().first.value_of(),
                     initial_value.value_of());
    profile->get().setF0(modified_value);
  }
  // ---------------------------------------------------------------------------
  {
    const auto profile = engine.accelerationProfile(curve);
    ASSERT_TRUE(profile.has_value());
    EXPECT_DOUBLE_EQ(profile->get().f0().first.value_of(),
                     modified_value.value_of());
  }
}

TEST(TestMotherboard, EngineUpdateStepsPerMmRatio)
{
  using namespace device;
  struct Subject
  {
    AxisAddress axis;
    double newRatio;
    double target;
    device::Coordinate oldSteps;
    device::Coordinate newSteps;
  };
  const std::array subjects{ Subject{ AxisAddress::Column,
                                      5.0 + 0.007,
                                      50.0,
                                      device::Coordinate{ 4208 },
                                      device::Coordinate{ 4213 } } };
  for (const auto& subject : subjects) {
    auto params             = std::make_unique<device::EngineParams>();
    params->microstep       = Microstep::Step_1_16;
    params->axisOffset      = 2.6;
    params->stepsPerMmRatio = 5.0;

    device::Engine engine{ std::move(params) };
    EXPECT_EQ(engine.mmToSteps(subject.target), subject.oldSteps);

    engine.setStepsPerMmRatio(5.0);
    EXPECT_EQ(engine.mmToSteps(subject.target), subject.oldSteps);

    engine.setStepsPerMmRatio(subject.newRatio);
    EXPECT_EQ(engine.mmToSteps(subject.target), subject.newSteps);
  }
}

TEST(TestMotherboard, EngineUpdateTempAxisOffset)
{
  auto params             = std::make_unique<device::EngineParams>();
  params->address         = static_cast<uint8_t>(device::AxisAddress::Grabber);
  params->microstep       = device::Microstep::Step_1_1;
  params->axisOffset      = 1;
  params->stepsPerMmRatio = 1;

  device::Engine engine{ std::move(params) };
  EXPECT_EQ(engine.mmToSteps(10).value_of(), 11);
  EXPECT_DOUBLE_EQ(engine.tempOffset(), 0);

  engine.setTempOffset(20);
  EXPECT_EQ(engine.mmToSteps(10).value_of(), 31);
  EXPECT_DOUBLE_EQ(engine.tempOffset(), 20.0);

  engine.resetTempOffset();
  EXPECT_DOUBLE_EQ(engine.tempOffset(), 0);
}

TEST(TestMotherboard, EngineUpdateAxisOffset)
{
  auto params        = std::make_unique_for_overwrite<device::EngineParams>();
  params->axisOffset = 5.0;

  device::Engine engine{ std::move(params) };
  ASSERT_DOUBLE_EQ(engine.axisOffset(), 5.0);
  ASSERT_DOUBLE_EQ(engine.tempOffset(), 0);

  engine.setAxisOffset(5.0);
  ASSERT_DOUBLE_EQ(engine.axisOffset(), 5.0); // same value, won't change

  engine.setAxisOffset(5.5);
  ASSERT_DOUBLE_EQ(engine.axisOffset(), 5.5); // new value, will chagne
}

TEST(TestMotherboard, EngineUpdateSpeeds)
{
  auto params         = std::make_unique_for_overwrite<device::EngineParams>();
  params->actionSpeed = device::SpeedIndex{ 500 };
  params->homingSpeed = device::SpeedIndex{ 100 };
  params->travelSpeed = device::SpeedIndex{ 1000 };

  device::Engine engine{ std::move(params) };
  ASSERT_EQ(engine.actionSpeed(), device::SpeedIndex{ 500 });
  ASSERT_EQ(engine.homingSpeed(), device::SpeedIndex{ 100 });
  ASSERT_EQ(engine.travelSpeed(), device::SpeedIndex{ 1000 });

  engine.setActionSpeed(device::SpeedIndex{ 500 });
  engine.setHomingSpeed(device::SpeedIndex{ 100 });
  engine.setTravelSpeed(device::SpeedIndex{ 1000 });
  ASSERT_EQ(engine.actionSpeed(), device::SpeedIndex{ 500 });
  ASSERT_EQ(engine.homingSpeed(), device::SpeedIndex{ 100 });
  ASSERT_EQ(engine.travelSpeed(), device::SpeedIndex{ 1000 });

  engine.setActionSpeed(device::SpeedIndex{ 501 });
  engine.setHomingSpeed(device::SpeedIndex{ 101 });
  engine.setTravelSpeed(device::SpeedIndex{ 1001 });
  ASSERT_EQ(engine.actionSpeed(), device::SpeedIndex{ 501 });
  ASSERT_EQ(engine.homingSpeed(), device::SpeedIndex{ 101 });
  ASSERT_EQ(engine.travelSpeed(), device::SpeedIndex{ 1001 });

  ASSERT_EQ(engine.maxSpeed(), device::SpeedIndex{ 0 });

  engine.setMaxSpeed(device::SpeedIndex{ 0 });
  ASSERT_EQ(engine.maxSpeed(), device::SpeedIndex{ 0 });

  engine.setMaxSpeed(device::SpeedIndex{ 1 });
  ASSERT_EQ(engine.maxSpeed(), device::SpeedIndex{ 1 });
}

TEST(TestMotherboard, EngineUpdatePosition)
{
  constexpr device::Coordinate target_start{ 0 };
  constexpr device::Coordinate target_end{ 100500 };
  constexpr device::Coordinate home_start{ 0 };
  constexpr device::Coordinate home_end{ 42 };

  device::Engine engine{ 0x01 };

  ASSERT_EQ(engine.targetPosition(), target_start);
  ASSERT_EQ(engine.homePosition(), home_start);

  engine.setTargetPosition(target_start);
  engine.setHomePosition(home_start);

  ASSERT_EQ(engine.targetPosition(), target_start);
  ASSERT_EQ(engine.homePosition(), home_start);

  engine.setTargetPosition(target_end);
  engine.setHomePosition(home_end);

  ASSERT_EQ(engine.targetPosition(), target_end);
  ASSERT_EQ(engine.homePosition(), home_end);
}

TEST(TestMotherboard, EngineUpdateRepeatability)
{
  device::Engine engine{ 0x01 };
  ASSERT_EQ(engine.repeatability(), device::Repeatability{ 1 });

  engine.setRepeatability(device::Repeatability{ 1 });
  ASSERT_EQ(engine.repeatability(), device::Repeatability{ 1 });

  engine.setRepeatability(device::Repeatability{ 2 });
  ASSERT_EQ(engine.repeatability(), device::Repeatability{ 2 });
}

TEST(TestMotherboard, EngineUpdateCurrents)
{
  device::Engine engine{ 0 };
  ASSERT_EQ(engine.currentAccel(), device::Current::Current_13_32);
  ASSERT_EQ(engine.currentStdby(), device::Current::Current_7_32);

  engine.setCurrentAccel(device::Current::Current_13_32);
  engine.setCurrentHolding(device::Current::Current_7_32);
  ASSERT_EQ(engine.currentAccel(), device::Current::Current_13_32);
  ASSERT_EQ(engine.currentStdby(), device::Current::Current_7_32);

  engine.setCurrentAccel(device::Current::Current_14_32);
  engine.setCurrentHolding(device::Current::Current_8_32);
  ASSERT_EQ(engine.currentAccel(), device::Current::Current_14_32);
  ASSERT_EQ(engine.currentStdby(), device::Current::Current_8_32);
}