#include <gtest/gtest.h>

#include "common/discrete_input.h"
#include "motherboard/cmd/engine_driver_raw_operation.h"
#include "motherboard/cmd/goto_n_ticks_operation.h"
#include "motherboard/device/engine.h"
#include "motherboard/device/engine_defaults.h"
#include "motherboard/device/engine_values.h"
#include "motherboard/device/motherboard_params.h"
#include "motherboard/protocol/motherboard_decoder.h"
#include "motherboard/protocol/motherboard_encoder.h"
#include "motherboard/protocol/motherboard_error.h"
#include "motherboard/protocol/motherboard_response.h"
#include "motherboard/protocol/motherboard_response_type.h"
#include "serial/protocol/request_interface.h"

#include "motherboard/cmd/button_clear_trigger_diag_opt.h"
#include "motherboard/cmd/button_clear_trigger_power.h"
#include "motherboard/cmd/button_request_diag_opt.h"
#include "motherboard/cmd/button_request_door.h"
#include "motherboard/cmd/button_request_power.h"
#include "motherboard/cmd/device_get_fw_operation.h"
#include "motherboard/cmd/device_get_id_operation.h"
#include "motherboard/cmd/device_write_id_operation.h"
#include "motherboard/cmd/engine_driver_factory_reset.h"
#include "motherboard/cmd/engine_enable_operation.h"
#include "motherboard/cmd/engine_get_acceleration.h"
#include "motherboard/cmd/engine_get_currents_operation.h"
#include "motherboard/cmd/engine_get_max_speed.h"
#include "motherboard/cmd/engine_get_microstep.h"
#include "motherboard/cmd/engine_get_mix_mode_status_operation.h"
#include "motherboard/cmd/engine_get_position_current.h"
#include "motherboard/cmd/engine_get_repeatability.h"
#include "motherboard/cmd/engine_write_acceleration_operation.h"
#include "motherboard/cmd/engine_write_microstep_operation.h"
#include "motherboard/cmd/engine_write_position_current_operation.h"
#include "motherboard/cmd/engine_write_position_home_operation.h"
#include "motherboard/cmd/engine_write_repeatability_operation.h"
#include "motherboard/cmd/goto_absolute_pos_operation.h"
#include "motherboard/cmd/goto_em_stop_operation.h"
#include "motherboard/cmd/goto_mix_cycles_operation.h"
#include "motherboard/cmd/goto_mix_time_operation.h"
#include "motherboard/cmd/goto_write_currents_operation.h"
#include "motherboard/cmd/multi_goto_home_position_operation.h"
#include "motherboard/cmd/power_get_24v_operation.h"
#include "motherboard/cmd/power_request_turn_off.h"
#include "motherboard/cmd/power_set_led_operation.h"
#include "motherboard/cmd/power_set_uv_lamp_operation.h"
#include "motherboard/cmd/rgb_led_set_operation.h"
#include "motherboard/cmd/status_ping_operation.h"
#include "motherboard/cmd/system_zeroize_error_operation.h"
#include "motherboard/cmd/voltage_get_uv_operation.h"

namespace {
const protocol::motherboard::MotherboardEncoder encoder;
const protocol::motherboard::MotherboardDecoder decoder;
} // namespace

TEST(TestMotherboard, EngineRequestAbsolutePosition)
{
  constexpr protocol::PacketID pid{ 0x01 };
  constexpr device::Coordinate target{ 0 };
  const auto request_bytes{ QByteArray::fromHex("551001870395b2021cac") };
  const auto response_bytes{ QByteArray::fromHex(
    "55ca01870d41b2011cffffffffffffffff4000ef") };

  device::Engine engine{ 2 };
  cmd::motherboard::EngineGetPositionCurrentOperation cmd{ pid, &engine };

  EXPECT_EQ(encoder.encode(cmd.createRequest(), cmd.responseType()),
            request_bytes);

  // В следующем варианте ответа текущая позиция двигателя не определена -- это
  // отражено в куче 0xff и в байте статуса двигателя. В результьате обработки
  // такого пакета не должна измениьтся текущая координата, записанная в
  // объекте. Т.к. объект создан пустым -> координата должна быть равна 0
  const auto response{ decoder.createResponse(response_bytes,
                                              cmd.responseType()) };
  cmd.feedResponse(response.value());
  EXPECT_EQ(engine.currentPosition(), target);
}

TEST(TestMotherboard, MotherboardPingOperation)
{
  constexpr protocol::PacketID pid{ 0x07 };
  const auto request_bytes{ QByteArray::fromHex("551007810096") };
  const auto response_bytes{ QByteArray::fromHex("55ca0781014d0000") };

  cmd::motherboard::StatusPingOperation operation{ pid };

  const auto request{ encoder.encode(operation.createRequest(),
                                     operation.responseType()) };
  EXPECT_EQ(request, request_bytes);

  const auto response{ decoder.createResponse(response_bytes,
                                              operation.responseType()) };
  ASSERT_TRUE(response.has_value());

  operation.feedResponse(response.value());
  EXPECT_FALSE(operation.isError());
}

TEST(TestMotherboard, MotherboardButtonsRequestDoorOperation)
{
  constexpr protocol::PacketID pid{ 0x06 };
  constexpr auto type{ protocol::motherboard::ResponseType::GerconGetState };
  const auto description{ QStringLiteral("requesting door sensor") };
  const auto request_bytes{ QByteArray::fromHex("551006870293b701b6") };

  const std::array responses{
    std::make_pair(false, QByteArray::fromHex("55ca0687054eb701010100b6")),
    std::make_pair(true, QByteArray::fromHex("55ca0787054fb701000100b7"))
  };

  device::DiscreteOutput door;
  cmd::motherboard::ButtonsRequestDoor operation{ pid, &door };

  EXPECT_EQ(operation.description(), description);
  EXPECT_EQ(operation.responseType(), type);

  const auto request{ encoder.encode(operation.createRequest(),
                                     operation.responseType()) };
  EXPECT_EQ(request, request_bytes);

  for (auto&& subject : responses) {
    const auto response{ decoder.createResponse(std::get<QByteArray>(subject),
                                                operation.responseType()) };
    ASSERT_TRUE(response.has_value());
    operation.feedResponse(response.value());

    // operation mutates buttons state, so check there
    EXPECT_EQ(door.state(), std::get<bool>(subject));
  }
}

TEST(TestMotherboard, MotherboardButtonsRequestPowerButtonOperation)
{
  constexpr protocol::PacketID pid{ 0x0d };
  constexpr auto type{ protocol::motherboard::ResponseType::PwrBtnGetState };
  const auto description{ QStringLiteral("request power button state") };
  const auto request_bytes{ QByteArray::fromHex("55100d870298b901b8") };
  const std::array responses{
    std::make_pair(false, QByteArray::fromHex("55ca0d870545b901000100b9")),
    std::make_pair(true, QByteArray::fromHex("55ca0c870544b901010100b8"))
  };

  device::DiscreteOutput power_button;
  cmd::motherboard::ButtonsRequestPowerButton operation{ pid, &power_button };

  EXPECT_EQ(operation.description(), description);
  EXPECT_EQ(operation.responseType(), type);

  const auto request{ encoder.encode(operation.createRequest(),
                                     operation.responseType()) };
  EXPECT_EQ(request, request_bytes);

  for (auto&& subject : responses) {
    const auto response{ decoder.createResponse(std::get<QByteArray>(subject),
                                                operation.responseType()) };
    ASSERT_TRUE(response.has_value());
    operation.feedResponse(response.value());

    // operation mutates buttons states, so chek there
    EXPECT_EQ(power_button.state(), std::get<bool>(subject));
  }
}

TEST(TestMotherboard, MotherboardGetFirmwareRevisionOperation)
{
  constexpr protocol::PacketID pid{ 0x10 };
  constexpr uint8_t firmware_rev{ 8 };
  constexpr auto type{ protocol::motherboard::ResponseType::DeviceGetFwRev };
  const auto description{ QStringLiteral(
    "Requesting frimware revision from MB") };
  const auto tx_bytes{ QByteArray::fromHex("551010870285000202") };
  const auto rx_bytes{ QByteArray::fromHex("55ca10870459000208000a") };

  device::MotherboardParameters parameters;
  cmd::motherboard::DeviceGetFWOperation operation{ pid, &parameters };

  EXPECT_EQ(operation.description(), description);
  EXPECT_EQ(operation.responseType(), type);

  const auto request{ encoder.encode(operation.createRequest(),
                                     operation.responseType()) };
  EXPECT_EQ(request, tx_bytes);

  const auto response{ decoder.createResponse(rx_bytes,
                                              operation.responseType()) };
  ASSERT_TRUE(response.has_value());
  operation.feedResponse(response.value());

  EXPECT_EQ(parameters.fwRev, firmware_rev);
}

#if 0
// сама операция требует доработки, пока не готова
TEST(TestMotherboard, MotherboardGetMotherboardIdOperation)
{
  constexpr protocol::PacketID pid{ 0x10 };
  constexpr auto type{ protocol::motherboard::ResponseType::DeviceGetId };
  const auto description{ QStringLiteral("Requesting ID from MB") };
  const auto tx_bytes{ QByteArray::fromHex("55101a87028f000101") };
  const auto rx_bytes{ QByteArray::fromHex("") };

  auto parameters{ std::make_shared<device::MotherboardParameters>() };
  cmd::motherboard::DeviceGetFWOperation operation{ pid, parameters };

  EXPECT_EQ(operation.description(), description);
  EXPECT_EQ(operation.responseType(), type);

  const auto request{ encoder.encode(operation.createRequest(),
                                     operation.responseType()) };
  EXPECT_EQ(request, tx_bytes);

  const auto response{ decoder.createResponse(rx_bytes,
                                              operation.responseType()) };
  ASSERT_TRUE(response.has_value());
  operation.feedResponse(response.value());

  EXPECT_EQ(parameters->fwRev, firmware_rev);
}
#endif

TEST(TestMotherboard, MotherboardResetDiagTriggerOperation)
{
  constexpr protocol::PacketID pid{ 0x1 };
  constexpr auto type{
    protocol::motherboard::ResponseType::OptDiagClearTrigger
  };
  const auto description{ QStringLiteral(
    "RESET Diagnostic optical sensor trigger") };

  const auto tx_bytes{ QByteArray::fromHex("551001860295b801b9") };
  const auto rx_bytes{ QByteArray::fromHex("55ca01860548b9010000a119") };

  device::DiscreteOutput sensors{ /*state=*/true };
  cmd::motherboard::DiagOptResetTriggerOperation operation{ pid, &sensors };
  operation.start();

  ASSERT_FALSE(operation.isFinished());
  ASSERT_FALSE(operation.isError());

  EXPECT_EQ(operation.responseType(), type);
  EXPECT_EQ(operation.description(), description);

  const auto request{ encoder.encode(operation.createRequest(),
                                     operation.responseType()) };
  EXPECT_EQ(request, tx_bytes);

  EXPECT_TRUE(sensors.state());
  EXPECT_FALSE(sensors.trigger());

  const auto response{ decoder.createResponse(rx_bytes,
                                              operation.responseType()) };
  ASSERT_TRUE(response.has_value());

  operation.feedResponse(response.value());

  EXPECT_TRUE(sensors.state()); // resetting trigger does not affect state?
  EXPECT_FALSE(sensors.trigger());

  EXPECT_TRUE(operation.isFinished());
}

TEST(TestMotherboard, GotoMixCycleFailIfTooLow)
{
  constexpr static protocol::PacketID pid{ 0x10 };
  constexpr static device::Cycles cycles{ 500 };
  constexpr static std::chrono::seconds time{ 500 };
  constexpr static auto amplitude_mm{ 5 };
  constexpr static auto millimeters_ok{ 60 };
  constexpr static auto millimeters{ 74 };
  constexpr static auto expected_type{
    protocol::motherboard::ResponseType::GotoMixCycles
  };

  auto params             = std::make_unique<device::EngineParams>();
  params->address         = 2;
  params->axis            = 'Z';
  params->microstep       = device::Microstep::Step_1_8;
  params->axisOffset      = 0.5;
  params->stepsPerMmRatio = 5.0;

  device::Engine sleeves{ std::move(params) }; // sleeves

  const auto check = [&sleeves](cmd::AbstractOperation& operation,
                                const QString& description,
                                const QString& error) {
    EXPECT_EQ(operation.description(), description);

    // "move" sleeves to ok height to start mixing
    sleeves.setCurrentPosition(sleeves.mmToSteps(millimeters_ok));
    operation.start();
    EXPECT_FALSE(operation.isError());

    // "move" sleeves too low
    sleeves.setCurrentPosition(sleeves.mmToSteps(millimeters));
    operation.start();
    EXPECT_TRUE(operation.isError());
    EXPECT_EQ(operation.error(), BackendError::OperationError);
    EXPECT_EQ(operation.errorString(), error);
  };

  cmd::motherboard::GotoMixCyclesOperation mix_amplitude{
    pid, &sleeves, cycles, sleeves.mmToSteps(amplitude_mm)
  };
  check(
    mix_amplitude,
    QStringLiteral("Requesting MIX on engine 2 for 500 times at 220 amplitude"),
    QStringLiteral("Engine Z is too low to start mixing, check protocol"));

  cmd::motherboard::GotoMixTimeOperation mix_time{
    pid, &sleeves, time, sleeves.mmToSteps(amplitude_mm)
  };
  check(mix_time,
        QStringLiteral("Moving engine 2 for 500 seconds with 220 amplitude"),
        QStringLiteral("Engine Z is too low to start mixing, check protocol"));
}

TEST(TestMotherboard, RequestIdOperation)
{
  constexpr protocol::PacketID pid_expected{ 3 };

  const auto tx_bytes{ QByteArray::fromHex("551003870296000101") };
  const auto rx_bytes{ QByteArray::fromHex("55ca038707490001000000000001") };

  device::MotherboardParameters parameters;
  cmd::motherboard::DeviceGetIdOperation operation{ pid_expected, &parameters };

  EXPECT_EQ(operation.description(), QStringLiteral("Requesting ID from MB"));
  EXPECT_EQ(operation.responseType(),
            protocol::motherboard::ResponseType::DeviceGetId);

  const auto request{ encoder.encode(operation.createRequest(),
                                     operation.responseType()) };
  EXPECT_EQ(request, tx_bytes);

  const auto response{ decoder.createResponse(rx_bytes,
                                              operation.responseType()) };
  ASSERT_TRUE(response.has_value());

  operation.feedResponse(response.value());
  ASSERT_TRUE(parameters.motherboardId.has_value());
  EXPECT_EQ(parameters.motherboardId.value(), 0);
}

TEST(TestMotherboard, Request24vRailOperation)
{
  constexpr protocol::PacketID pid{ 0x3b };
  const auto tx_bytes{ QByteArray::fromHex("55103b8702aeb301b2") };
  const std::array subjects{
    std::make_tuple(
      device::Voltage{ 22.9 },
      QByteArray::fromHex("55ca03870b45b3014036ec8408efcd5c5288")),
    std::make_tuple(
      device::Voltage{ 23.9 },
      QByteArray::fromHex("55ca01870b47b3014037f05dee67e3b400b6"))
  };

  device::AnalogInput<device::Voltage> powers;
  cmd::motherboard::VoltageGet24VOperation operation{ pid, &powers };

  EXPECT_EQ(operation.description(),
            QStringLiteral("Requesting 24V Rail voltage"));
  EXPECT_EQ(operation.responseType(),
            protocol::motherboard::ResponseType::VoltageGet24VRail);

  const auto request =
    encoder.encode(operation.createRequest(), operation.responseType());
  EXPECT_EQ(request, tx_bytes);

  for (const auto& subject : subjects) {
    const auto response = decoder.createResponse(std::get<QByteArray>(subject),
                                                 operation.responseType());
    ASSERT_TRUE(response.has_value());

    operation.feedResponse(response.value());
    EXPECT_NEAR(powers.value().value_of(),
                std::get<device::Voltage>(subject).value_of(),
                0.1);
  }
}

//! @todo
TEST(TestMotherboard, DiagOptStateOperation)
{
  static constexpr protocol::PacketID pid{ 0x3b };
  EXPECT_TRUE(true);
}

TEST(TestMotherboard, DriverFactoryResetOperation)
{
  static constexpr protocol::PacketID pid{ 0x05 };

  const auto tx_bytes{ QByteArray::fromHex("551005860390b201f043") };
  const auto rx_bytes{ QByteArray::fromHex("55ca01860548b201f0010042") };

  device::Engine engine{ 1 };
  cmd::motherboard::EngineDriverFactoryReset operation{ pid, &engine };

  EXPECT_EQ(
    operation.description(),
    QStringLiteral("Resetting engine 1 parameters to factory defaults"));
  EXPECT_EQ(operation.operationType(),
            cmd::AbstractOperation::OperationType::Motherboard);
  EXPECT_EQ(operation.responseType(),
            protocol::motherboard::ResponseType::EnginesDriverFactoryDefaults);

  operation.start();
  ASSERT_FALSE(operation.isFinished());
  ASSERT_FALSE(operation.isError());

  const auto request{ encoder.encode(operation.createRequest(),
                                     operation.responseType()) };
  EXPECT_EQ(request, tx_bytes);

  const auto response{ decoder.createResponse(rx_bytes,
                                              operation.responseType()) };
  ASSERT_TRUE(response.has_value());
  operation.feedResponse(response.value());
  EXPECT_EQ(engine.statusByte(), 0x1);
  EXPECT_TRUE(operation.isFinished());
}

TEST(TestMotherboard, DriverWriteRawCommandOperation)
{
  static constexpr protocol::PacketID pid{ 0x3b };
  const auto tx_bytes{ QByteArray::fromHex("55103b8607aab201300919191993") };
  const auto rx_bytes{ QByteArray::fromHex("55ca01860548b20130010082") };

  device::Engine engine{ 1 };
  auto raw_bytes{ QByteArray::fromHex("9191919") };
  cmd::motherboard::EngineDriverRawWriteOperation operation{ pid,
                                                             &engine,
                                                             raw_bytes };

  EXPECT_EQ(
    operation.description(),
    QStringLiteral("Writing raw command to stepper driver #1: 09:19:19:19"));
  EXPECT_EQ(operation.operationType(),
            cmd::AbstractOperation::OperationType::Motherboard);
  EXPECT_EQ(operation.responseType(),
            protocol::motherboard::ResponseType::EnginesDriverRawWrite);

  operation.start();
  ASSERT_FALSE(operation.isFinished());
  ASSERT_FALSE(operation.isError());

  const auto request{ encoder.encode(operation.createRequest(),
                                     operation.responseType()) };
  EXPECT_EQ(request, tx_bytes);

  const auto response{ decoder.createResponse(rx_bytes,
                                              operation.responseType()) };
  ASSERT_TRUE(response.has_value());
  operation.feedResponse(response.value());
  EXPECT_EQ(engine.statusByte(), 0x1);
  EXPECT_TRUE(operation.isFinished());
}

TEST(TestMotherboard, EngineEnableOperation)
{
  static constexpr protocol::PacketID pid{ 0x3b };

  const std::array tx_subjects{
    std::make_pair(true, QByteArray::fromHex("55103b8603aeb2031aab")),
    std::make_pair(false, QByteArray::fromHex("55103b8603aeb2031baa"))
  };
  const std::array rx_subjects{
    std::make_pair(true, QByteArray::fromHex("55ca01860548b2011a0100a8")),
    std::make_pair(false, QByteArray::fromHex("55ca01860548b2011b0100a9")),
  };

  device::Engine engine{ 3 };
  cmd::motherboard::EngineEnableOperation operation_enable{ pid,
                                                            &engine,
                                                            true };
  cmd::motherboard::EngineEnableOperation operation_disable{ pid,
                                                             &engine,
                                                             false };

  EXPECT_EQ(operation_enable.description(),
            QStringLiteral("Turning engine 3 On"));
  EXPECT_EQ(operation_disable.description(),
            QStringLiteral("Turning engine 3 Off"));

  EXPECT_EQ(operation_enable.operationType(),
            cmd::AbstractOperation::OperationType::Motherboard);
  EXPECT_EQ(operation_enable.responseType(),
            protocol::motherboard::ResponseType::EnginesEnable);

  const auto test_packets{ [&engine](auto& operation,
                                     const auto& tx_bytes,
                                     const auto& rx_bytes,
                                     bool enabled) {
    operation.start();
    ASSERT_FALSE(operation.isFinished());
    ASSERT_FALSE(operation.isError());

    const auto request{ encoder.encode(operation.createRequest(),
                                       operation.responseType()) };
    EXPECT_EQ(request, tx_bytes);

    const auto response{ decoder.createResponse(rx_bytes,
                                                operation.responseType()) };
    ASSERT_TRUE(response.has_value());
    operation.feedResponse(response.value());
    EXPECT_EQ(engine.enabled(), enabled);
    EXPECT_TRUE(operation.isFinished());
  } };

  test_packets(
    operation_enable, tx_subjects.at(0).second, rx_subjects.at(0).second, true);
  test_packets(operation_disable,
               tx_subjects.at(1).second,
               rx_subjects.at(1).second,
               false);
}

TEST(TestMotherboard, EngineRequestMaxSpeed)
{
  static constexpr protocol::PacketID pid{ 0x3b };

  const auto tx_bytes{ QByteArray::fromHex("55103b8703afb20300b1") };
  const std::array rx_subjects{
    std::make_tuple(device::SpeedIndex{ 500 },
                    QByteArray::fromHex("55ca01870945b20100000001f4010047")),
  };

  device::Engine engine{ 3 };
  cmd::motherboard::EngineGetMaxSpeedOperation operation{ pid, &engine };

  EXPECT_EQ(operation.description(),
            QStringLiteral("Requesting engine 3 max speed"));
  EXPECT_EQ(operation.operationType(),
            cmd::AbstractOperation::OperationType::Motherboard);
  EXPECT_EQ(operation.responseType(),
            protocol::motherboard::ResponseType::EnginesGetMaxSpeed);

  operation.start();
  ASSERT_FALSE(operation.isFinished());
  ASSERT_FALSE(operation.isError());

  const auto request{ encoder.encode(operation.createRequest(),
                                     operation.responseType()) };
  EXPECT_EQ(request, tx_bytes);

  for (const auto& subject : rx_subjects) {
    const auto response{ decoder.createResponse(std::get<QByteArray>(subject),
                                                operation.responseType()) };
    ASSERT_TRUE(response.has_value());
    operation.feedResponse(response.value());
    EXPECT_EQ(engine.statusByte(), 0x1);
    EXPECT_EQ(engine.maxSpeed(), std::get<device::SpeedIndex>(subject));
    EXPECT_TRUE(operation.isFinished());
  }
}

TEST(TestMotherboard, EngineRequestMicrostep)
{
  static constexpr protocol::PacketID pid{ 0x3b };

  const auto tx_bytes{ QByteArray::fromHex("55103b8703afb20304b5") };
  const std::array rx_subjects{
    std::make_pair(device::Microstep::Step_1_1,
                   QByteArray::fromHex("55ca0187064ab20104000100b6")),
    std::make_pair(device::Microstep::Step_1_2,
                   QByteArray::fromHex("55ca0187064ab20104010100b7")),
    std::make_pair(device::Microstep::Step_1_4,
                   QByteArray::fromHex("55ca0187064ab20104020100b4")),
    std::make_pair(device::Microstep::Step_1_8,
                   QByteArray::fromHex("55ca0187064ab20104030100b5")),
    std::make_pair(device::Microstep::Step_1_16,
                   QByteArray::fromHex("55ca0187064ab20104040100b2")),
    std::make_pair(device::Microstep::Step_1_32,
                   QByteArray::fromHex("55ca0187064ab20104050100b3")),
    std::make_pair(device::Microstep::Step_1_64,
                   QByteArray::fromHex("55ca0187064ab20104060100b0")),
    std::make_pair(device::Microstep::Step_1_128,
                   QByteArray::fromHex("55ca0187064ab20104070100b1")),
    std::make_pair(device::Microstep::Step_1_256,
                   QByteArray::fromHex("55ca0187064ab20104080100be")),
    std::make_pair(device::Microstep::Undefined,
                   QByteArray::fromHex("55ca0187064ab20104ff010049"))

  };

  device::Engine engine{ 3 };
  cmd::motherboard::EngineGetMicrostepOperation operation{ pid, &engine };

  EXPECT_EQ(operation.description(),
            QStringLiteral("Requesting engine 3 microstep"));
  EXPECT_EQ(operation.operationType(),
            cmd::AbstractOperation::OperationType::Motherboard);
  EXPECT_EQ(operation.responseType(),
            protocol::motherboard::ResponseType::EnginesGetMicrostep);

  operation.start();
  ASSERT_FALSE(operation.isFinished());
  ASSERT_FALSE(operation.isError());

  const auto request{ encoder.encode(operation.createRequest(),
                                     operation.responseType()) };
  EXPECT_EQ(request, tx_bytes);

  for (const auto& subject : rx_subjects) {
    const auto response{ decoder.createResponse(std::get<QByteArray>(subject),
                                                operation.responseType()) };
    ASSERT_TRUE(response.has_value());
    operation.feedResponse(response.value());
    EXPECT_EQ(engine.statusByte(), 0x1);
    EXPECT_EQ(engine.microstep(), std::get<device::Microstep>(subject));
    EXPECT_TRUE(operation.isFinished());
  }
}

TEST(TestMotherboard, EngineRequestMixMode)
{
  static constexpr protocol::PacketID pid{ 0x3b };

  const auto tx_bytes{ QByteArray::fromHex("55103b8703afb2032190") };
  const std::array rx_subjects{
    std::make_tuple(
      device::EngineStatus{ 0b10011 },
      device::Cycles{ 88 },
      std::chrono::seconds{ 4 },
      uint8_t{ 0b10000001 },
      QByteArray::fromHex("55ca06870e45b20421810000005800000004130059")),
    std::make_tuple(
      device::EngineStatus{ 0b11 },
      device::Cycles{ 572 },
      std::chrono::seconds{ 30 },
      uint8_t{ 0b10000000 },
      QByteArray::fromHex("55ca07870e44b20421800000023c0000001e030034"))
  };

  device::Engine engine{ 3 };
  cmd::motherboard::EngineGetMixModeStatusOperation operation{ pid, &engine };

  EXPECT_EQ(operation.description(),
            QStringLiteral("Requesting engine 3 mix mode status"));
  EXPECT_EQ(operation.operationType(),
            cmd::AbstractOperation::OperationType::Motherboard);
  EXPECT_EQ(operation.responseType(),
            protocol::motherboard::ResponseType::EnginesGetMixModeState);

  operation.start();
  ASSERT_FALSE(operation.isFinished());
  ASSERT_FALSE(operation.isError());

  const auto request{ encoder.encode(operation.createRequest(),
                                     operation.responseType()) };
  EXPECT_EQ(request, tx_bytes);

  for (const auto& subject : rx_subjects) {
    const auto response{ decoder.createResponse(std::get<QByteArray>(subject),
                                                operation.responseType()) };
    ASSERT_TRUE(response.has_value());
    operation.feedResponse(response.value());
    EXPECT_EQ(engine.status(), std::get<device::EngineStatus>(subject));
    EXPECT_EQ(engine.currentMixCycle(), std::get<device::Cycles>(subject));
    EXPECT_EQ(engine.currentMixTime(), std::get<std::chrono::seconds>(subject));
    EXPECT_TRUE(operation.isFinished());
  }
}

TEST(TestMotherboard, EngineWriteCurrentPositionOperation)
{
  static constexpr protocol::PacketID pid{ 0x3b };
  static constexpr device::Coordinate new_pos{ 22435 };

  const auto tx_bytes{ QByteArray::fromHex(
    "55103b860ba6b2041c00000000000057a35e") };
  const auto rx_bytes{ QByteArray::fromHex("55ca01860548b2011c0100ae") };

  device::Engine engine{ 4 };
  cmd::motherboard::EngineWritePositionCurrentOperation operation{ pid,
                                                                   &engine,
                                                                   new_pos };

  EXPECT_EQ(operation.description(),
            QStringLiteral("Setting new current position 22435 for engine 4"))
    << operation.description().toStdString();
  EXPECT_EQ(operation.operationType(),
            cmd::AbstractOperation::OperationType::Motherboard);
  EXPECT_EQ(operation.responseType(),
            protocol::motherboard::ResponseType::EnginesSetPosition);

  operation.start();
  ASSERT_FALSE(operation.isFinished());
  ASSERT_FALSE(operation.isError());

  const auto request{ encoder.encode(operation.createRequest(),
                                     operation.responseType()) };
  EXPECT_EQ(request, tx_bytes);

  const auto response{ decoder.createResponse(rx_bytes,
                                              operation.responseType()) };
  ASSERT_TRUE(response.has_value());
  operation.feedResponse(response.value());
  EXPECT_EQ(engine.statusByte(), 0x1);
  EXPECT_EQ(engine.currentPosition(), device::Coordinate{ 0 });
  EXPECT_TRUE(operation.isFinished());
}

TEST(TestMotherboard, EngineWriteHomePositionOperation)
{
  static constexpr protocol::PacketID pid{ 0x3b };
  static constexpr device::Coordinate new_pos{ 2 };

  const auto tx_bytes{ QByteArray::fromHex(
    "55103b860ba6b204050000000000000002b1") };
  const auto rx_bytes{ QByteArray::fromHex("55ca01860548b201050100b7") };

  device::Engine engine{ 4 };
  cmd::motherboard::EngineWritePositionHomeOperation operation{ pid,
                                                                &engine,
                                                                new_pos };

  EXPECT_EQ(operation.description(),
            QStringLiteral("Setting home position 2 for engine 4"))
    << operation.description().toStdString();
  EXPECT_EQ(operation.operationType(),
            cmd::AbstractOperation::OperationType::Motherboard);
  EXPECT_EQ(operation.responseType(),
            protocol::motherboard::ResponseType::EnginesSetHomePosition);

  operation.start();
  ASSERT_FALSE(operation.isFinished());
  ASSERT_FALSE(operation.isError());

  const auto request{ encoder.encode(operation.createRequest(),
                                     operation.responseType()) };
  EXPECT_EQ(request, tx_bytes);

  const auto response{ decoder.createResponse(rx_bytes,
                                              operation.responseType()) };
  ASSERT_TRUE(response.has_value());
  operation.feedResponse(response.value());
  EXPECT_EQ(engine.statusByte(), 0x1);
  EXPECT_EQ(engine.homePosition(), device::Coordinate{ 0 });
  EXPECT_TRUE(operation.isFinished());
}

TEST(TestMotherboard, EngineEmergencyStopOperation)
{
  static constexpr protocol::PacketID pid{ 0x3b };

  const auto tx_bytes{ QByteArray::fromHex("55103b8603aeb10403b6") };

  device::Engine engine{ 4 };
  cmd::motherboard::GotoEmStopOperation::Param params{ &engine };
  cmd::motherboard::GotoEmStopOperation operation{ pid, std::move(params) };

  EXPECT_EQ(operation.description(),
            QStringLiteral("Emergency stopping axis 4"))
    << operation.description().toStdString();
  EXPECT_EQ(operation.operationType(),
            cmd::AbstractOperation::OperationType::Motherboard);
  EXPECT_EQ(operation.responseType(),
            protocol::motherboard::ResponseType::GotoEmStop);

  operation.start();
  ASSERT_FALSE(operation.isFinished());
  ASSERT_FALSE(operation.isError());

  const auto request{ encoder.encode(operation.createRequest(),
                                     operation.responseType()) };
  EXPECT_EQ(request, tx_bytes);
}

TEST(TestMotherboard, EngineEmergencyStopMultiOperation)
{
  static constexpr protocol::PacketID pid{ 0x3b };

  const auto tx_bytes{ QByteArray::fromHex("55103b8605a8b103030403b6") };

  device::Engine magnets{ 3 };
  device::Engine sleeves{ 4 };

  cmd::motherboard::GotoEmStopOperation::Param params{ &magnets, &sleeves };
  cmd::motherboard::GotoEmStopOperation operation{ pid, std::move(params) };

  EXPECT_EQ(operation.description(),
            QStringLiteral("Emergency stopping axis 3 & 4"))
    << operation.description().toStdString();
  EXPECT_EQ(operation.operationType(),
            cmd::AbstractOperation::OperationType::Motherboard);
  EXPECT_EQ(operation.responseType(),
            protocol::motherboard::ResponseType::GotoEmStop);

  operation.start();
  ASSERT_FALSE(operation.isFinished());
  ASSERT_FALSE(operation.isError());

  const auto request{ encoder.encode(operation.createRequest(),
                                     operation.responseType()) };
  EXPECT_EQ(request, tx_bytes);
}

TEST(TestMotherboard, EngineEmergencyStopFailedDataOperation)
{
  constexpr protocol::PacketID pid{ 0x03 };
  device::Engine column{ 1 };
  device::Engine magnets{ 3 };
  device::Engine sleeves{ 4 };

  // no engines
  cmd::motherboard::GotoEmStopOperation::Param params_1;
  cmd::motherboard::GotoEmStopOperation operation_no_engines{
    pid, std::move(params_1)
  };
  operation_no_engines.start();
  ASSERT_FALSE(operation_no_engines.isFinished());
  ASSERT_TRUE(operation_no_engines.isError());
  ASSERT_EQ(operation_no_engines.error(), BackendError::DataError);
  ASSERT_EQ(operation_no_engines.errorString(),
            QStringLiteral("no engines requested"));

  // two same engines
  cmd::motherboard::GotoEmStopOperation::Param params_2{ &magnets, &magnets };
  cmd::motherboard::GotoEmStopOperation operation_two_same{
    pid, std::move(params_2)
  };
  operation_two_same.start();
  ASSERT_FALSE(operation_two_same.isFinished());
  ASSERT_TRUE(operation_two_same.isError());
  ASSERT_EQ(operation_two_same.error(), BackendError::DataError);
  ASSERT_EQ(operation_two_same.errorString(),
            QStringLiteral("same engines requested"));

  // too many engines
  cmd::motherboard::GotoEmStopOperation::Param params_3{ &column,
                                                         &magnets,
                                                         &sleeves };
  cmd::motherboard::GotoEmStopOperation operation_too_many{
    pid, std::move(params_3)
  };
  operation_too_many.start();
  ASSERT_FALSE(operation_too_many.isFinished());
  ASSERT_TRUE(operation_too_many.isError());
  ASSERT_EQ(operation_too_many.error(), BackendError::DataError);
  ASSERT_EQ(operation_too_many.errorString(),
            QStringLiteral("too many engines requested"));
}

TEST(TestMotherboard, EngineGotoNTicksOperation)
{
  constexpr protocol::PacketID pid{ 0x78 };
  const std::array subjects{
    std::make_pair(device::Coordinate{ 42069 },
                   QByteArray::fromHex("551078860be5b10400000000000000a45544")),
    std::make_pair(device::Coordinate{ -42069 },
                   QByteArray::fromHex("551078860be5b10400ffffffffffff5bab45")),
  };
  device::Engine engine{ 4 };
  ASSERT_EQ(engine.targetPosition(), device::Coordinate{ 0 });

  for (const auto& subject : subjects) {
    engine.setCurrentPosition(device::Coordinate{ 0 });
    cmd::motherboard::GotoNTicksOperation operation{
      pid, &engine, std::get<device::Coordinate>(subject)
    };

    EXPECT_EQ(operation.description(),
              QStringLiteral("Axis 4 goto from 0 to %1")
                .arg(std::get<device::Coordinate>(subject).value_of()))
      << operation.description().toStdString();
    EXPECT_EQ(operation.operationType(),
              cmd::AbstractOperation::OperationType::Motherboard);
    EXPECT_EQ(operation.responseType(),
              protocol::motherboard::ResponseType::GotoNTicks);

    operation.start();
    ASSERT_FALSE(operation.isFinished());
    ASSERT_FALSE(operation.isError());

    const auto request{ encoder.encode(operation.createRequest(),
                                       operation.responseType()) };
    EXPECT_EQ(request, std::get<QByteArray>(subject));
  }
}

TEST(TestMotherboard, EngineGotoAbsolutePositionOperation)
{
  using Operation = cmd::motherboard::GotoAbsolutePosOperation;

  constexpr protocol::PacketID pid{ 0x3b };
  const std::array subjects{
    std::make_pair(device::Coordinate{ 42069 },
                   QByteArray::fromHex("55103b860ba6b10401000000000000a45545")),
    std::make_pair(device::Coordinate{ -42069 },
                   QByteArray::fromHex("55103b860ba6b10401ffffffffffff5bab44")),
  };
  device::Engine engine{ 4 };

  for (const auto& subject : subjects) {
    engine.setTargetPosition(device::Coordinate{ 0 });

    Operation::Params params{ std::make_pair(
      &engine, std::get<device::Coordinate>(subject)) };
    Operation operation{ pid, std::move(params) };

    EXPECT_EQ(operation.description(),
              QStringLiteral("MOVING AXISES: 4 @ %1")
                .arg(std::get<device::Coordinate>(subject).value_of()))
      << operation.description().toStdString();
    EXPECT_EQ(operation.operationType(),
              cmd::AbstractOperation::OperationType::Motherboard);
    EXPECT_EQ(operation.responseType(),
              protocol::motherboard::ResponseType::GotoAbsPos);

    operation.start();
    ASSERT_FALSE(operation.isFinished());
    ASSERT_FALSE(operation.isError());

    const auto request{ encoder.encode(operation.createRequest(),
                                       operation.responseType()) };
    EXPECT_EQ(request, std::get<QByteArray>(subject));
  }
}

TEST(TestMotherboard, EngineGotoAbsolutePositionMultiOperation)
{
  using Operation = cmd::motherboard::GotoAbsolutePosOperation;

  constexpr protocol::PacketID pid{ 0x3b };

  const auto tx_bytes{ QByteArray::fromHex(
    "55103b8615b8b10301000000000000a4550401000000000000007b3c") };
  device::Engine magnets{ 3 };
  device::Engine sleeves{ 4 };

  Operation::Params params{
    std::make_pair(&magnets, device::Coordinate{ 42069 }),
    std::make_pair(&sleeves, device::Coordinate{ 123 })
  };
  Operation operation{ pid, std::move(params) };

  EXPECT_EQ(operation.description(),
            QStringLiteral("MOVING AXISES: 3 @ 42069 && 4 @ 123"))
    << operation.description().toStdString();
  EXPECT_EQ(operation.operationType(),
            cmd::AbstractOperation::OperationType::Motherboard);
  EXPECT_EQ(operation.responseType(),
            protocol::motherboard::ResponseType::GotoAbsPos);

  operation.start();
  ASSERT_FALSE(operation.isFinished());
  ASSERT_FALSE(operation.isError());

  const auto request{ encoder.encode(operation.createRequest(),
                                     operation.responseType()) };
  EXPECT_EQ(request, tx_bytes);
}

TEST(TestMotherboard, EngineGotoAbsolutePositionFailedDataOperation)
{
  constexpr protocol::PacketID pid{ 0x03 };
  device::Engine column{ 1 };
  device::Engine magnets{ 3 };
  device::Engine sleeves{ 4 };

  using Operation = cmd::motherboard::GotoAbsolutePosOperation;

  // no engines
  Operation::Params params_1;
  Operation operation_no_engines{ pid, std::move(params_1) };
  operation_no_engines.start();
  ASSERT_FALSE(operation_no_engines.isFinished());
  ASSERT_TRUE(operation_no_engines.isError());
  ASSERT_EQ(operation_no_engines.error(), BackendError::DataError);
  ASSERT_EQ(operation_no_engines.errorString(),
            QStringLiteral("no engines requested"));

  // two same engines
  Operation::Params params_2{ std::make_pair(&magnets, device::Coordinate{ 1 }),
                              std::make_pair(&magnets,
                                             device::Coordinate{ 1 }) };
  Operation operation_two_same{ pid, std::move(params_2) };
  operation_two_same.start();
  ASSERT_FALSE(operation_two_same.isFinished());
  ASSERT_TRUE(operation_two_same.isError());
  ASSERT_EQ(operation_two_same.error(), BackendError::DataError);
  ASSERT_EQ(operation_two_same.errorString(),
            QStringLiteral("same engines requested"));

  // too many engines
  Operation::Params params_3{ std::make_pair(&column, device::Coordinate{ 1 }),
                              std::make_pair(&magnets, device::Coordinate{ 1 }),
                              std::make_pair(&sleeves,
                                             device::Coordinate{ 1 }) };
  Operation operation_too_many{ pid, std::move(params_3) };
  operation_too_many.start();
  ASSERT_FALSE(operation_too_many.isFinished());
  ASSERT_TRUE(operation_too_many.isError());
  ASSERT_EQ(operation_too_many.error(), BackendError::DataError);
  ASSERT_EQ(operation_too_many.errorString(),
            QStringLiteral("too many engines requested"));
}

TEST(TestMotherboard, EngineGotoWriteCurrentsOperation)
{
  static constexpr protocol::PacketID pid{ 0x3b };
  constexpr auto accel{ device::Current::Current_23_32 };
  constexpr auto holding{ device::Current::Current_13_32 };

  const auto tx_bytes{ QByteArray::fromHex("55103b8605a8b1045a160cf5") };
  const auto rx_bytes{ QByteArray::fromHex("55ca0687074cb201221306030087") };

  const std::array subjects{
    // 55:ca:06:87:07:4c:b2:01:22:13:06:03:00:87
    std::make_tuple(device::Current::Current_20_32,
                    device::Current::Current_7_32,
                    0b00000011,
                    QByteArray::fromHex("55ca0687074cb201221306030087")),
  };

  device::Engine engine{ 4, 'Z' };
  cmd::motherboard::GotoWriteCurrentsOperation operation{
    pid, &engine, accel, holding
  };

  EXPECT_EQ(operation.description(),
            QStringLiteral("WRITING ENGINE Z ACCEL CURRENT Current_23_32 AND "
                           "STANDBY CURRENT Current_13_32"))
    << operation.description().toStdString();
  EXPECT_EQ(operation.operationType(),
            cmd::AbstractOperation::OperationType::Motherboard);
  EXPECT_EQ(operation.responseType(),
            protocol::motherboard::ResponseType::GotoWriteCurrents);

  operation.start();
  ASSERT_FALSE(operation.isFinished());
  ASSERT_FALSE(operation.isError());

  const auto request{ encoder.encode(operation.createRequest(),
                                     operation.responseType()) };
  EXPECT_EQ(request, tx_bytes);

  const auto response{ decoder.createResponse(rx_bytes,
                                              operation.responseType()) };
  ASSERT_TRUE(response.has_value());
  operation.feedResponse(response.value());
  EXPECT_TRUE(operation.isFinished());
}

TEST(TestMotherboard, PowerButtonClearTriggerOperation)
{
  static constexpr protocol::PacketID pid{ 0x3b };
  static constexpr device::Coordinate new_pos{ 2 };

  const auto tx_bytes{ QByteArray::fromHex("55103b8602afb901b8") };

  device::DiscreteOutput button;
  cmd::motherboard::PowerButtonClearTrigger operation{ pid, &button };

  EXPECT_EQ(operation.description(),
            QStringLiteral("clear power button trigger state"));
  EXPECT_EQ(operation.operationType(),
            cmd::AbstractOperation::OperationType::Motherboard);
  EXPECT_EQ(operation.responseType(),
            protocol::motherboard::ResponseType::PwrBtnClearTrigger);

  operation.start();
  ASSERT_FALSE(operation.isFinished());
  ASSERT_FALSE(operation.isError());

  const auto request{ encoder.encode(operation.createRequest(),
                                     operation.responseType()) };
  EXPECT_EQ(request, tx_bytes);
}

TEST(TestMotherboard, PowerRequestTurnOff)
{
  static constexpr protocol::PacketID pid{ 0x3b };
  const auto tx_bytes{ QByteArray::fromHex("55103b8602afb407b3") };

  cmd::motherboard::PowerRequestTurnOff operation{ pid };

  EXPECT_EQ(operation.description(), QStringLiteral("requesting poweroff"))
    << operation.description().toStdString();
  EXPECT_EQ(operation.operationType(),
            cmd::AbstractOperation::OperationType::Motherboard);
  EXPECT_EQ(operation.responseType(),
            protocol::motherboard::ResponseType::PowerTurnOff);

  operation.start();
  ASSERT_FALSE(operation.isFinished());
  ASSERT_FALSE(operation.isError());

  const auto request{ encoder.encode(operation.createRequest(),
                                     operation.responseType()) };
  EXPECT_EQ(request, tx_bytes);
}

TEST(TestMotherboard, SystemZeroizeErrorOperation)
{
  static constexpr protocol::PacketID pid{ 0x3b };
  static constexpr device::Coordinate new_pos{ 2 };

  const auto tx_bytes{ QByteArray::fromHex("55103b8500ae") };

  cmd::motherboard::SystemZeroizeErrorOperation operation{ pid };

  EXPECT_EQ(operation.description(), QStringLiteral("Clearing last error"))
    << operation.description().toStdString();
  EXPECT_EQ(operation.operationType(),
            cmd::AbstractOperation::OperationType::Motherboard);
  EXPECT_EQ(operation.responseType(),
            protocol::motherboard::ResponseType::SystemResetError);

  operation.start();
  ASSERT_FALSE(operation.isFinished());
  ASSERT_FALSE(operation.isError());

  const auto request{ encoder.encode(operation.createRequest(),
                                     operation.responseType()) };
  EXPECT_EQ(request, tx_bytes);
}
