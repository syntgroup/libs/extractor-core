#include <gtest/gtest.h>

#include "motherboard/device/ir_sensors.h"
#include "motherboard/device/motherboard_values.h"
#include "motherboard/device/rgb_leds.h"

TEST(TestMotherboard, RgbLedsFailIfInvalidRefresh)
{
  const std::vector wrong_subject{ Qt::blue, Qt::cyan, Qt::green };
  device::RgbLeds leds{ 1 };
  ASSERT_FALSE(leds.setLedColors(wrong_subject));
}

TEST(TestMotherboard, RgbLedsSuccessRefresh)
{
  const std::vector test_colors{
    Qt::blue, Qt::cyan, Qt::green, Qt::magenta, Qt::white
  };
  device::RgbLeds leds{ test_colors.size() };
  ASSERT_TRUE(leds.setLedColors(test_colors));
}

TEST(TestMotherboard, RgbLedInitAndModify)
{
  device::RgbLed led{ 4 };
  ASSERT_EQ(led.idx(), 4);
  ASSERT_EQ(led.value(), Qt::black);

  led.setValue(Qt::cyan);
  ASSERT_EQ(led.value(), Qt::cyan);
}

TEST(TestMotherboard, IrSensorInitAndModify)
{
  constexpr size_t max_size = 5;
  device::IrSensors sensors{ max_size };

  for (auto index = 0; index < max_size; ++index) {
    ASSERT_FALSE(sensors.sensorState(index));
  }

  static constexpr std::array pattern{ false, false, true, false, true };
  static_assert(pattern.size() == max_size);
  for (auto index = 0; index < max_size; ++index) {
    sensors.setSensor(index, pattern.at(index));
    ASSERT_EQ(sensors.sensorState(index), pattern.at(index));
  }
}
