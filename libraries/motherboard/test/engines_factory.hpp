/*!
 * @file engines_factory.hpp
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 * @brief
 */

#ifndef MOTHERBOARD_TEST_ENGINES_FACTORY_HPP
#define MOTHERBOARD_TEST_ENGINES_FACTORY_HPP

#include "engines_defaults.hpp"
#include "motherboard/device/engine.h"
#include "motherboard/device/engines.h"

class Factory
{
public:
  [[nodiscard]] static auto createEnginesPool(bool with_profiles = true)
  {
    using namespace device;
    constexpr std::array axises{ AxisAddress::Column,
                                 AxisAddress::Shield,
                                 AxisAddress::Magnets,
                                 AxisAddress::Sleeves,
                                 AxisAddress::Grabber };
    Engines::Container engines;
    engines.reserve(axises.size());
    std::transform(axises.begin(),
                   axises.end(),
                   std::inserter(engines, engines.end()),
                   [with_profiles](auto axis) {
                     return std::make_pair(axis,
                                           with_profiles
                                             ? createEngineWProfile(axis)
                                             : createEngineWoProfile(axis));
                   });

    return std::make_shared<device::Engines>(std::move(engines));
  }

private:
  using Axis = device::AxisAddress;

  [[nodiscard]] static auto createEngineParams(Axis axis)
    -> std::unique_ptr<device::EngineParams>
  {
    const auto idx = static_cast<std::underlying_type_t<Axis>>(axis);

    auto params     = std::make_unique_for_overwrite<device::EngineParams>();
    params->axis    = data::axises.at(idx);
    params->address = static_cast<uint8_t>(idx + 1);
    params->travelSpeed     = data::travel_speed.at(idx);
    params->homingSpeed     = data::homing_speed.at(idx);
    params->actionSpeed     = data::action_speed.at(idx);
    params->repeatability   = data::repeatability;
    params->stepsPerMmRatio = data::steps_per_mm.at(idx);
    params->microstep       = data::microstep.at(idx);
    return params;
  }

  [[nodiscard]] static auto createEngineWProfile(Axis axis)
    -> std::unique_ptr<device::Engine>
  {
    using namespace device;
    const auto idx = static_cast<std::underlying_type_t<Axis>>(axis);

    auto params = createEngineParams(axis);
    auto profile_low =
      std::make_unique<AccelerationProfile>(data::freq_slow_start.at(idx),
                                            data::freq_slow_stop.at(idx),
                                            data::freq_slow_delta.at(idx));
    auto profile_med =
      std::make_unique<AccelerationProfile>(data::freq_normal_start.at(idx),
                                            data::freq_normal_stop.at(idx),
                                            data::freq_normal_delta.at(idx));
    auto profile_high =
      std::make_unique<AccelerationProfile>(data::freq_fast_start.at(idx),
                                            data::freq_fast_stop.at(idx),
                                            data::freq_fast_delta.at(idx));

    return std::make_unique<Engine>(std::move(params),
                                    std::move(profile_low),
                                    std::move(profile_med),
                                    std::move(profile_high));
  }

  [[nodiscard]] static auto createEngineWoProfile(Axis axis)
    -> std::unique_ptr<device::Engine>
  {
    auto params = createEngineParams(axis);
    return std::make_unique<device::Engine>(std::move(params));
  }
};

#endif // MOTHERBOARD_TEST_ENGINES_FACTORY_HPP