#include <gtest/gtest.h>

#include "motherboard/device/engine_status.h"
#include "motherboard/protocol/device_response.h"
#include "motherboard/protocol/engine_response.h"
#include "motherboard/protocol/goto_response.h"
#include "motherboard/protocol/motherboard_cmd.h"
#include "motherboard/protocol/motherboard_response.h"
#include "motherboard/protocol/motherboard_response_type.h"
#include "motherboard/protocol/power_response.h"
#include "motherboard/protocol/sns_state_response.h"
#include "motherboard/protocol/system_response.h"
#include "srvint/srvint_packet.h"
#include "srvint/srvint_response_factory.h"

namespace {
const protocol::srvint::SrvIntResponseFactory factory;
constexpr auto expected_error{ 0.1 };
} // namespace

using namespace protocol::motherboard;

TEST(TestMotherboard, DecodingMotherboardIdResponse)
{
  // 55:ca:03:87:07:49:00:01:00:00:00:00:00:01
  const protocol::PacketID pid_expected{ 3 };
  const auto response_raw{ QByteArray::fromHex(
    "55ca038707490001000000000001") };

  auto packet{ factory.create(response_raw) };
  ASSERT_TRUE(packet.has_value());

  const DeviceGetIdResponse response{ packet.value() };
  EXPECT_EQ(response.id(), pid_expected);
  EXPECT_EQ(response.deviceId, 0);
  EXPECT_EQ(response.encodedSize(), response_raw.size());
}

TEST(TestMotherboard, Decoding24VRailResponse)
{
  const std::array subjects{
    std::make_tuple(
      device::Voltage{ 22.9 },
      QByteArray::fromHex("55ca03870b45b3014036ec8408efcd5c5288")),
    std::make_tuple(
      device::Voltage{ 23.9 },
      QByteArray::fromHex("55ca01870b47b3014037f05dee67e3b400b6"))
  };

  for (auto&& subject : subjects) {
    auto packet{ factory.create(std::get<QByteArray>(subject)) };
    ASSERT_TRUE(packet.has_value());

    const GetRailResponse response{ packet.value() };
    EXPECT_NEAR(response.voltage.value_of(),
                std::get<device::Voltage>(subject).value_of(),
                expected_error);
  }
}

TEST(TestMotherboard, DecodingDeviceGetFWResponse)
{
  const auto response_raw{ QByteArray::fromHex("55ca098704400002030001") };
  auto packet{ factory.create(response_raw) };
  ASSERT_TRUE(packet.has_value());

  const DeviceGetFWResponse response{ packet.value() };
  EXPECT_EQ(response.fwRev, 3);
}

TEST(TestMotherboard, DecodingGotoMixTimeResponse)
{
  const auto response_raw{ QByteArray::fromHex("55ca2486056db101570300e4") };
  auto packet{ factory.create(response_raw) };
  ASSERT_TRUE(packet.has_value());

  const GotoResponse response{ packet.value() };
  EXPECT_EQ(response.engineAddress, 1);
  EXPECT_EQ(response.encodedSize(), response_raw.size());
  EXPECT_EQ(response.id(), protocol::PacketID{ 0x24 });
}

TEST(TestMotherboard, DecodingSnsStateResponse)
{
  const auto response_raw{ QByteArray::fromHex("55ca0f870446b6011b00ac") };
  auto packet{ factory.create(response_raw) };
  ASSERT_TRUE(packet.has_value());

  const SnsStateResponse response{ packet.value() };
  EXPECT_EQ(response.snsStates, 27);
}

TEST(TestMotherboard, DecodingGetOnBtnsResponse)
{
  const auto response_raw{ QByteArray::fromHex("55ca01870549b501010000b5") };
  auto packet{ factory.create(response_raw) };
  ASSERT_TRUE(packet.has_value());

  const GetBtnsResponse response{ packet.value() };
  EXPECT_FALSE(response.isError());
  EXPECT_FALSE(response.stateGercon);
  EXPECT_TRUE(response.statePushButon);
}

TEST(TestMotherboard, DecodingGetOffBtnsResponse)
{
  const auto response_raw{ QByteArray::fromHex("55ca0c870544b501010100b4") };
  auto packet{ factory.create(response_raw) };
  ASSERT_TRUE(packet.has_value());

  const GetBtnsResponse response{ packet.value() };
  EXPECT_FALSE(response.isError());
  EXPECT_TRUE(response.stateGercon);
  EXPECT_TRUE(response.statePushButon);
}

TEST(TestMotherboard, DecodingEngineSetParamResponse)
{
  constexpr protocol::PacketID pid{ 0x01 };
  constexpr auto engine_state{ 0b00000001 };
  constexpr auto engine_id{ 1 };

  const std::array subjects{
    std::make_tuple(EnginesSet::MaximumSpeed,
                    QByteArray::fromHex("55ca01860548b201000100b2")),
    std::make_tuple(EnginesSet::Repeatability,
                    QByteArray::fromHex("55ca01860548b201030100b1")),
    std::make_tuple(EnginesSet::Microstep,
                    QByteArray::fromHex("55ca01860548b201040100b6")),
    std::make_tuple(EnginesSet::HomePosition,
                    QByteArray::fromHex("55ca01860548b201050100b7")),
    std::make_tuple(EnginesSet::F0,
                    QByteArray::fromHex("55ca01860548b201060100b4")),
    std::make_tuple(EnginesSet::FMAX,
                    QByteArray::fromHex("55ca01860548b201070100b5")),
    std::make_tuple(EnginesSet::DFMAX,
                    QByteArray::fromHex("55ca01860548b201080100ba")),
    std::make_tuple(EnginesSet::CurrentPosition,
                    QByteArray::fromHex("55ca01860548b2011c0100ae")),
    std::make_tuple(EnginesSet::SavePosition,
                    QByteArray::fromHex("55ca01860548b2010c0100be")),
    std::make_tuple(EnginesSet::LoadPosition,
                    QByteArray::fromHex("55ca01860548b2010d0100bf")),
    std::make_tuple(EnginesSet::EngineOn,
                    QByteArray::fromHex("55ca01860548b2011a0100a8")),
    std::make_tuple(EnginesSet::EngineOff,
                    QByteArray::fromHex("55ca01860548b2011b0100a9")),
    std::make_tuple(EnginesSet::DriverWrite,
                    QByteArray::fromHex("55ca01860548b20130010082")),
    std::make_tuple(EnginesSet::FactoryDefault,
                    QByteArray::fromHex("55ca01860548b201f0010042")),
  };

  for (auto&& subject : subjects) {
    auto packet{ factory.create(std::get<QByteArray>(subject)) };
    ASSERT_TRUE(packet.has_value());

    const EnginesSetParamResponse response{ packet.value() };
    EXPECT_FALSE(response.isError());
    EXPECT_EQ(response.errorCode(), MotherboardError::NoError)
      << response.errorCode();
    EXPECT_EQ(response.engineState, engine_state);
    EXPECT_EQ(response.engineId, engine_id);
    EXPECT_EQ(response.id(), pid);
    EXPECT_EQ(response.pcode, std::get<EnginesSet>(subject));
  }
}

TEST(TestMotherboard, DecodingEngineGetMaxSpeedResponse)
{
  constexpr auto engine_state{ 0b00000001 };
  constexpr auto engine_id{ 0x01 };

  const std::array subjects{
    std::make_tuple(device::SpeedIndex{ 500 },
                    QByteArray::fromHex("55ca01870945b20100000001f4010047")),
  };

  for (auto&& subject : subjects) {
    auto packet{ factory.create(std::get<QByteArray>(subject)) };
    ASSERT_TRUE(packet.has_value());

    const EnginesGetMaxSpeedResponse response{ packet.value() };
    EXPECT_FALSE(response.isError());
    EXPECT_EQ(response.maxSpeed, std::get<device::SpeedIndex>(subject));
    EXPECT_EQ(response.engineState, engine_state);
    EXPECT_EQ(response.engineAdr, engine_id);
    EXPECT_EQ(response.errorCode(), MotherboardError::NoError);
  }
}

TEST(TestMotherboard, DecodingEngineGetRepeatabilityResponse)
{
  constexpr auto engine_state{ 0b00000001 };
  constexpr auto engine_id{ 0x01 };

  const std::array subjects{
    std::make_tuple(device::Repeatability{ 0 },
                    QByteArray::fromHex("55ca0187064ab20103000100b1")),
    std::make_tuple(device::Repeatability{ 1 },
                    QByteArray::fromHex("55ca0187064ab20103010100b0")),
    std::make_tuple(device::Repeatability{ 2 },
                    QByteArray::fromHex("55ca0187064ab20103020100b3"))
  };

  for (auto&& subject : subjects) {
    auto packet{ factory.create(std::get<QByteArray>(subject)) };
    ASSERT_TRUE(packet.has_value());

    const EnginesGetRepeatabilityResponse response{ packet.value() };
    EXPECT_FALSE(response.isError());
    EXPECT_EQ(response.repeatability, std::get<device::Repeatability>(subject));
    EXPECT_EQ(response.engineState.byte, engine_state);
    EXPECT_EQ(response.engineAdr, engine_id);
    EXPECT_EQ(response.errorCode(), MotherboardError::NoError);
  }
}

TEST(TestMotherboard, DecodingEngineGetMicrostepResponse)
{
  constexpr auto engine_id{ 0x01 };
  constexpr auto packet_error{ MotherboardError::NoError };
  const device::EngineStatus engine_state{ 0b00000001 };

  const std::array subjects{
    std::make_pair(device::Microstep::Step_1_1,
                   QByteArray::fromHex("55ca0187064ab20104000100b6")),
    std::make_pair(device::Microstep::Step_1_2,
                   QByteArray::fromHex("55ca0187064ab20104010100b7")),
    std::make_pair(device::Microstep::Step_1_4,
                   QByteArray::fromHex("55ca0187064ab20104020100b4")),
    std::make_pair(device::Microstep::Step_1_8,
                   QByteArray::fromHex("55ca0187064ab20104030100b5")),
    std::make_pair(device::Microstep::Step_1_16,
                   QByteArray::fromHex("55ca0187064ab20104040100b2")),
    std::make_pair(device::Microstep::Step_1_32,
                   QByteArray::fromHex("55ca0187064ab20104050100b3")),
    std::make_pair(device::Microstep::Step_1_64,
                   QByteArray::fromHex("55ca0187064ab20104060100b0")),
    std::make_pair(device::Microstep::Step_1_128,
                   QByteArray::fromHex("55ca0187064ab20104070100b1")),
    std::make_pair(device::Microstep::Step_1_256,
                   QByteArray::fromHex("55ca0187064ab20104080100be")),
    std::make_pair(device::Microstep::Undefined,
                   QByteArray::fromHex("55ca0187064ab20104ff010049"))

  };

  for (auto&& subject : subjects) {
    auto packet{ factory.create(std::get<QByteArray>(subject)) };
    ASSERT_TRUE(packet.has_value());

    const EnginesGetMicrostepResponse response{ packet.value() };
    EXPECT_FALSE(response.isError());
    EXPECT_EQ(response.microstep, std::get<device::Microstep>(subject));
    EXPECT_EQ(response.engineState, engine_state.byte);
    EXPECT_EQ(response.engineAdr, engine_id);
    EXPECT_EQ(response.errorCode(), packet_error);
  }
}

TEST(TestMotherboard, DecodingEngineGetHomePositionResponse)
{
  const std::array subjects{ std::make_tuple(
    device::Coordinate{ 2 },
    device::EngineStatus{ 0b11 },
    QByteArray::fromHex("55ca0a870d4ab2040500000000000000020300b2")) };

  for (auto&& subject : subjects) {
    auto packet{ factory.create(std::get<QByteArray>(subject)) };
    ASSERT_TRUE(packet.has_value());

    const EnginesGetHomePositionResponse response{ packet.value() };
    EXPECT_FALSE(response.isError());
    EXPECT_EQ(response.homePosition, std::get<device::Coordinate>(subject));
    EXPECT_EQ(response.engineState,
              std::get<device::EngineStatus>(subject).byte);
  }
}

TEST(TestMotherboard, DecodingEngineGetFResponse)
{
  constexpr uint8_t engineAddress{ 1 };
  constexpr uint8_t engineState{ 0 };

  const std::array subjects{ std::make_tuple(
    EnginesGet::F0,
    device::Frequency{ 50.2 },
    QByteArray::fromHex("55ca01870c40b201064049199999990101003c")) };

  for (auto&& subject : subjects) {
    auto packet{ factory.create(std::get<QByteArray>(subject)) };
    ASSERT_TRUE(packet.has_value());

    const EnginesGetFResponse response{ packet.value() };
    EXPECT_FALSE(response.isError());
    EXPECT_EQ(response.errorCode(), MotherboardError::NoError);
    EXPECT_EQ(response.id(), protocol::PacketID{ 1 });
    EXPECT_EQ(response.engineAdr, engineAddress);
    EXPECT_EQ(response.engineState, engineState);
    EXPECT_NEAR(response.acceleration.value_of(),
                std::get<device::Frequency>(subject).value_of(),
                0.1)
      << response.acceleration.value_of();
  }
}

TEST(TestMotherboard, DecodingEngineGetPositionResponse)
{
  const std::array subjects{ std::make_tuple(
    device::Coordinate{ 220 },
    device::EngineStatus{ 0b11 },
    uint8_t{ 4 },
    QByteArray::fromHex("55ca09870d49b2041c00000000000000dc030075")) };

  for (auto&& subject : subjects) {
    auto packet{ factory.create(std::get<QByteArray>(subject)) };
    ASSERT_TRUE(packet.has_value());

    const EnginesGetPositionResponse response{ packet.value() };
    EXPECT_FALSE(response.isError());
    EXPECT_EQ(response.opCode, EnginesGet::CurrentPosition);
    EXPECT_EQ(response.engineAdr, std::get<uint8_t>(subject));
    EXPECT_EQ(response.position, std::get<device::Coordinate>(subject));
    EXPECT_EQ(response.engineState,
              std::get<device::EngineStatus>(subject).byte);
  }
}

TEST(TestMotherboard, DecodingEngineGetCurrentsResopnse)
{
  enum Fields
  {
    Acceleration = 0,
    Standby
  };

  const std::array subjects{
    // 55:ca:06:87:07:4c:b2:01:22:13:06:03:00:87
    std::make_tuple(device::Current::Current_20_32,
                    device::Current::Current_7_32,
                    0b00000011,
                    QByteArray::fromHex("55ca0687074cb201221306030087")),
  };

  for (auto&& subject : subjects) {
    auto packet{ factory.create(std::get<QByteArray>(subject)) };
    ASSERT_TRUE(packet.has_value());

    const EngineGetCurrentsResponse response{ packet.value() };
    EXPECT_FALSE(response.isError());
    EXPECT_EQ(response.opCode, EnginesGet::AccelStandbyCurrents);
    EXPECT_EQ(response.currentStdby, std::get<Standby>(subject));
    EXPECT_EQ(response.currentAccel, std::get<Acceleration>(subject));
    EXPECT_EQ(response.engineState, std::get<int>(subject));
  }
}

TEST(TestMotherboard, DecodingEngineGetMixModeStateResponse)
{
  const std::array subjects{
    std::make_tuple(
      device::EngineStatus{ 0b10011 },
      device::Cycles{ 88 },
      std::chrono::seconds{ 4 },
      uint8_t{ 0b10000001 },
      QByteArray::fromHex("55ca06870e45b20421810000005800000004130059")),
    std::make_tuple(
      device::EngineStatus{ 0b11 },
      device::Cycles{ 572 },
      std::chrono::seconds{ 30 },
      uint8_t{ 0b10000000 },
      QByteArray::fromHex("55ca07870e44b20421800000023c0000001e030034"))
  };

  for (auto&& subject : subjects) {
    auto packet{ factory.create(std::get<QByteArray>(subject)) };
    ASSERT_TRUE(packet.has_value());

    const EngineGetMixModeStateResponse response{ packet.value() };
    EXPECT_FALSE(response.isError());
    EXPECT_EQ(response.engineState,
              std::get<device::EngineStatus>(subject).byte);
    EXPECT_EQ(response.mixmodeState, std::get<uint8_t>(subject));
    EXPECT_EQ(response.currentCycle, std::get<device::Cycles>(subject));
    EXPECT_EQ(response.currentTime, std::get<std::chrono::seconds>(subject));
  }
}

TEST(TestMotherboard, DecodingSystemGetErrorResponse)
{
  const std::array subjects{
    std::make_tuple(uint8_t{ 0 },
                    MotherboardError::NoError,
                    QByteArray::fromHex("55ca0b84034600000000")),
    std::make_tuple(uint8_t{ 127 },
                    MotherboardError::SfuncCanNotStartFromUndefinedPosition,
                    QByteArray::fromHex("55ca0b8403467f42003d"))
  };
  for (auto&& subject : subjects) {
    auto packet{ factory.create(std::get<QByteArray>(subject)) };
    ASSERT_TRUE(packet.has_value());

    const GetErrorResponse response{ packet.value() };
    EXPECT_EQ(response.errorPosition, std::get<uint8_t>(subject));
    EXPECT_EQ(response.error, std::get<MotherboardError::Error>(subject));
  }
}
