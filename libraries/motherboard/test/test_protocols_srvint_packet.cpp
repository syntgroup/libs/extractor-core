#include <gtest/gtest.h>

#include "srvint/srvint_packet.h"
#include "srvint/srvint_request_factory.h"
#include "srvint/srvint_response_factory.h"

using namespace protocol;

namespace {
const srvint::SrvIntRequestFactory request_factory;
const srvint::SrvIntResponseFactory response_factory;
constexpr protocol::PacketID pid{ 0x78 };
} // namespace

TEST(TestSrvInt, CreateSrvIntPacketWOPayload)
{
  const auto packet = request_factory.create(pid, srvint::Command::Ping);
  EXPECT_EQ(packet, QByteArray::fromHex("5510788100e9"));
}

TEST(TestSrvInt, CreateSrvIntPacketWPayload)
{
  constexpr auto new_pwm = 42069;

  const srvint::Payload payload{ static_cast<uint8_t>(0xb4),
                                 static_cast<uint8_t>(0x02),
                                 static_cast<uint8_t>(new_pwm >> 8),
                                 static_cast<uint8_t>(new_pwm) };
  const auto packet =
    request_factory.create(pid, srvint::Command::SetParam, payload);

  EXPECT_EQ(packet, QByteArray::fromHex("5510788604eab402a45547"));
}

TEST(TestSrvInt, CreateSrvIntPacketFromNoise)
{
  const std::vector<QByteArray> subjects{ QByteArray::fromHex("00000000"),
                                          QByteArray::fromHex("0"),
                                          QByteArray::fromHex("") };

  std::for_each(subjects.cbegin(), subjects.cend(), [](const auto& test) {
    const auto packet = response_factory.create(test);

    ASSERT_FALSE(packet.has_value());
    EXPECT_EQ(packet.error(), srvint::Error::NoData);
  });
}

TEST(TestSrvInt, CreateSrvIntPacketWWrongMasterAddress)
{
  const auto test =
    QByteArray::fromHex("55003a870d7ab2011c000000000000138753d4bc");
  const auto packet = response_factory.create(test);
  ASSERT_FALSE(packet.has_value());
  EXPECT_EQ(packet.error(), srvint::Error::AddressMismatch);
}
#if 0
TEST(TestSrvInt, CreateSrvIntPacketFromManyBytes)
{
  //! @warning этот тест некорректен!!
  const auto test = QByteArray::fromHex(
    "55ca0c870d4cb2011cffffffffffffffff5600f955ca0d870d4db2011cffffffffffffffff"
    "5600f9");
  const auto packet = response_factory.create(test);
  ASSERT_TRUE(packet) << static_cast<int>(packet.error());
  EXPECT_EQ(packet.value()->dataHash(), protocol::srvint::Hash{ 0xf9 });
  EXPECT_EQ(packet.value()->dataCount(), 0x0d);
  EXPECT_EQ(packet.value()->payload().size(), 0x0d);

  EXPECT_EQ(packet.value()->encode().size(), 20);
}
#endif
TEST(TestSrvInt, CreateSrvIntPacketWithWrongStartByte)
{
  const auto test =
    QByteArray::fromHex("00ca3a870d7ab2011c000000000000138753d4bc");
  const auto packet = response_factory.create(test);

  ASSERT_FALSE(packet.has_value());
  EXPECT_EQ(packet.error(), srvint::Error::StartByteMismatch);
}

TEST(TestSrvInt, CreateSrvIntPacketWithWrongPacketHash)
{
  const auto test =
    QByteArray::fromHex("55ca3a870d00b2011c000000000000138753d4bc");
  const auto packet = response_factory.create(test);

  ASSERT_FALSE(packet.has_value());
  EXPECT_EQ(packet.error(), srvint::Error::PacketHashMismatch);
}

TEST(TestSrvInt, CreateSrvIntPacketWithEmptyPayload)
{
  const auto subject = QByteArray::fromHex("55ca0181004a");
  const auto result  = response_factory.create(subject);

  ASSERT_TRUE(result.has_value());

  const auto& packet = result.value();
  EXPECT_EQ(packet->dataCount(), 0);
  EXPECT_TRUE(packet->payload().empty());
  EXPECT_TRUE(packet->payloadBytes().isEmpty());
}

TEST(TestSrvInt, SrvIntErrorPacketWithExtraPromisedPayload)
{
  const auto test =
    QByteArray::fromHex("55ca3a870077b2011c000000000000138753d4bc");

  const auto result = response_factory.create(test);
  ASSERT_FALSE(result.has_value());
  EXPECT_EQ(result.error(), srvint::Error::PromisedNoPayload);
}

TEST(TestSrvInt, CreateSrvIntPacketWithoutPromisedPayload)
{
  const auto test   = QByteArray::fromHex("55ca3a870d7ab2011c000000000000bc");
  const auto packet = response_factory.create(test);

  ASSERT_FALSE(packet.has_value());
  EXPECT_EQ(packet.error(), srvint::Error::NoExpectedPayload);
}

TEST(TestSrvInt, CreateSrvIntPacketWithWrongDataHash)
{
  const auto test =
    QByteArray::fromHex("55ca3a870d7ab2011c000000000000138753d400");
  const auto packet = response_factory.create(test);

  ASSERT_FALSE(packet.has_value());
  EXPECT_EQ(packet.error(), srvint::Error::DataHashMismatch);
}

TEST(TestSrvInt, SrvIntPacketEmptyPayload)
{
  constexpr PacketID id{ 0x53 };
  constexpr srvint::Address address{ srvint::address_device };
  const srvint::SrvIntPacket packet{ id, address, srvint::Command::Ping };

  EXPECT_EQ(packet.id(), id);
  EXPECT_EQ(packet.dataCount(), 0);
  EXPECT_EQ(packet.dataHash(), srvint::Hash{ 0 });
  EXPECT_EQ(packet.packetHash(), srvint::Hash{ 0xC2 });
  EXPECT_TRUE(packet.payload().empty());
  EXPECT_TRUE(packet.payloadBytes().isEmpty());
  EXPECT_TRUE(packet.payloadBytes().isNull());
}
