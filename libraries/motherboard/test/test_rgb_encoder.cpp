#include <gtest/gtest.h>

#include "motherboard/device/motherboard_values.h"
#include "motherboard/protocol/rgb_encoder.h"
#include <bitset>
#include <map>

TEST(TestMotherboard, RgbEncoderEncode)
{
  using Colors = std::vector<device::Color>;
  const std::unordered_map<uint32_t, Colors> subjects{
    { 19097,
      { device::Color::Blue,
        device::Color::Cyan,
        device::Color::Green,
        device::Color::Magenta,
        device::Color::Red } },
    { 0,
      { device::Color::Black,
        device::Color::Black,
        device::Color::Black,
        device::Color::Black,
        device::Color::Black } },
    { 28150,
      { device::Color::Yellow,
        device::Color::Yellow,
        device::Color::White,
        device::Color::Yellow,
        device::Color::Yellow } }
  };

  for (const auto& [encoded, colors] : subjects) {
    const auto result{ protocol::motherboard::RgbEncoder::encode(colors) };
    EXPECT_EQ(result, encoded);

    const auto width{ std::numeric_limits<uint32_t>::digits };
    const std::bitset<width> result_bit{ result };
    const std::bitset<width> compare_bit{ encoded };
    EXPECT_EQ(result_bit, compare_bit);
  }
}
