/*!
 * @file engines_defaults.hpp
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 * @brief
 */

#ifndef MOTHERBOARD_TEST_ENGINES_DEFAULTS_HPP
#define MOTHERBOARD_TEST_ENGINES_DEFAULTS_HPP

#include "motherboard/device/engine_values.h"

namespace data {

constexpr std::array<QChar, 5> axises{ 'X', 'A', 'B', 'Z', 'C' };
constexpr std::array homing_speed{ device::SpeedIndex{ 100 },
                                   device::SpeedIndex{ 100 },
                                   device::SpeedIndex{ 50 },
                                   device::SpeedIndex{ 50 },
                                   device::SpeedIndex{ 50 } };
constexpr std::array travel_speed{ device::SpeedIndex{ 500 },
                                   device::SpeedIndex{ 500 },
                                   device::SpeedIndex{ 100 },
                                   device::SpeedIndex{ 100 },
                                   device::SpeedIndex{ 100 } };
constexpr std::array action_speed{ device::SpeedIndex{ 500 },
                                   device::SpeedIndex{ 100 },
                                   device::SpeedIndex{ 100 },
                                   device::SpeedIndex{ 100 },
                                   device::SpeedIndex{ 100 } };
constexpr std::array steps_per_mm{ 5.0, 5.0, 5.0, 5.0, 1.0 };
constexpr std::array offset{ 2.6, 2.6, 1.0, 0.5, 0.0 };
constexpr device::Repeatability repeatability{ 2 };
constexpr std::array microstep{ device::Microstep::Step_1_16,
                                device::Microstep::Step_1_16,
                                device::Microstep::Step_1_8,
                                device::Microstep::Step_1_8,
                                device::Microstep::Step_1_2 };
constexpr std::array freq_slow_start{ device::Frequency{ 50 },
                                      device::Frequency{ 50 },
                                      device::Frequency{ 50 },
                                      device::Frequency{ 50 },
                                      device::Frequency{ 50 } };
constexpr std::array freq_slow_stop{ device::Frequency{ 100000 },
                                     device::Frequency{ 100000 },
                                     device::Frequency{ 100000 },
                                     device::Frequency{ 100000 },
                                     device::Frequency{ 100000 } };
constexpr std::array freq_slow_delta{ device::Frequency{ 100 },
                                      device::Frequency{ 100 },
                                      device::Frequency{ 100 },
                                      device::Frequency{ 100 },
                                      device::Frequency{ 100 } };

constexpr std::array freq_normal_start{ device::Frequency{ 1000 },
                                        device::Frequency{ 1000 },
                                        device::Frequency{ 1000 },
                                        device::Frequency{ 1000 },
                                        device::Frequency{ 1000 } };
constexpr std::array freq_normal_stop{ device::Frequency{ 50000 },
                                       device::Frequency{ 50000 },
                                       device::Frequency{ 50000 },
                                       device::Frequency{ 50000 },
                                       device::Frequency{ 50000 } };
constexpr std::array freq_normal_delta{ device::Frequency{ 100000 },
                                        device::Frequency{ 100000 },
                                        device::Frequency{ 100000 },
                                        device::Frequency{ 100000 },
                                        device::Frequency{ 100000 } };

constexpr std::array freq_fast_start{ device::Frequency{ 1000 },
                                      device::Frequency{ 1000 },
                                      device::Frequency{ 1000 },
                                      device::Frequency{ 1000 },
                                      device::Frequency{ 1000 } };
constexpr std::array freq_fast_stop{ device::Frequency{ 50000 },
                                     device::Frequency{ 50000 },
                                     device::Frequency{ 50000 },
                                     device::Frequency{ 50000 },
                                     device::Frequency{ 50000 } };
constexpr std::array freq_fast_delta{ device::Frequency{ 100000 },
                                      device::Frequency{ 100000 },
                                      device::Frequency{ 100000 },
                                      device::Frequency{ 100000 },
                                      device::Frequency{ 100000 } };

} // namespace data

#endif // MOTHERBOARD_TEST_ENGINES_DEFAULTS_HPP