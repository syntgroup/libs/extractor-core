#include <gtest/gtest.h>

#include "common/abstract_operation.h"
#include "motherboard/cmd/rgb_led_set_operation.h"
#include "motherboard/device/rgb_leds.h"
#include "motherboard/gcode/parser_rgb_leds.h"
#include <memory>
#include <vector>

namespace {
const auto leds = std::make_shared<device::RgbLeds>(5);

const auto deleter = [](gsl::owner<cmd::AbstractOperation*> operation) {
  delete operation;
};
} // namespace

TEST(TestMotherboard, RgbParserParseSuccess)
{
  const gcode::ParserRgbEnableLeds parser{ leds };
  const auto parse_result = parser.generate("С4");
  ASSERT_EQ(parse_result->size(), 1);

  auto* casted_operation =
    qobject_cast<cmd::motherboard::RgbLedSetOperation*>(parse_result->front());
  ASSERT_TRUE(casted_operation);

  constexpr std::array expected_leds_vector{
    Qt::red, Qt::red, Qt::red, Qt::red, Qt::red
  };
  ASSERT_EQ(casted_operation->colors().size(), expected_leds_vector.size());
  for (size_t index = 0; index < expected_leds_vector.size(); ++index) {
    ASSERT_EQ(expected_leds_vector.at(index),
              casted_operation->colors().at(index));
  }

  std::for_each(parse_result->begin(), parse_result->end(), deleter);
}

TEST(TestMotherboard, RgbParserParseDifferentPatternsSuccess)
{
  struct Subject
  {
    QString block;
    std::vector<Qt::GlobalColor> colors;
  };
  const gcode::ParserRgbSetLeds parser{ leds };
  const std::array subjects{
    Subject{ QStringLiteral("C1 C2 C4 C2 C1"),
             { Qt::blue, Qt::green, Qt::red, Qt::green, Qt::blue } },
    Subject{
      QStringLiteral("C7 C5 C7 C5 C7"),
      { Qt::white, Qt::magenta, Qt::white, Qt::magenta, Qt::white },
    },
    Subject{ QStringLiteral("C7C5C7C5C7"),
             { Qt::white, Qt::magenta, Qt::white, Qt::magenta, Qt::white } }
  };

  for (auto&& subject : subjects) {
    const auto parse_result = parser.generate(subject.block);
    ASSERT_TRUE(parse_result.has_value());
    ASSERT_EQ(parse_result->size(), 1);

    const auto* casted_operation =
      qobject_cast<const cmd::motherboard::RgbLedSetOperation*>(
        parse_result->front());
    ASSERT_TRUE(casted_operation);
    ASSERT_EQ(casted_operation->colors().size(), subject.colors.size());
    ASSERT_EQ(casted_operation->colors(), subject.colors);

    std::for_each(parse_result->begin(), parse_result->end(), deleter);
  }
}

TEST(TestMotherboard, RgbParserParseDifferentPatternsFail)
{
  const gcode::ParserRgbSetLeds parser{ leds };

  struct Subject
  {
    QString block;
    QString error;
  };
  const std::array subjects{
    Subject{ "C8 C5 C8 C5 C8", "mentioned less than 5 colors" },
    Subject{ "C2", "mentioned less than 5 colors" },
    Subject{ "C7C5C7C5C7C3", "mentioned less than 5 colors" }
  };

  for (const auto& subject : subjects) {
    const auto parse_result{ parser.generate(subject.block) };
    ASSERT_FALSE(parse_result.has_value()) << subject.block.toStdString();
    ASSERT_EQ(parse_result.error(), subject.error)
      << parse_result.error().toStdString();
  }
}
