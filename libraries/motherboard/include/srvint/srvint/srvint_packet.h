#ifndef PROTOCOL_MOTHERBOARDPACKET_H
#define PROTOCOL_MOTHERBOARDPACKET_H

#include "serial/protocol/protocol_values.h"
#include "srvint_defs.h"
#include "srvint_export.h"
#include <QByteArray>
#include <array>

namespace protocol::srvint {

enum class Error : uint8_t
{
  NoError,
  NoData,
  MuchData,
  StartByteMismatch,
  AddressMismatch,
  PacketHashMismatch,
  PromisedNoPayload,
  NoExpectedPayload,
  DataHashMismatch,
};

struct CORE_SRVINT_EXPORT SrvIntPacket
{
  SrvIntPacket() = default;
  SrvIntPacket(PacketID pid,
               Address adr,
               Command cmd,
               const Payload& payload = {});

  [[nodiscard]] auto encode() const -> QByteArray;
  [[nodiscard]] auto payloadBytes() const -> QByteArray;

  [[nodiscard]] auto id() const { return m_pid; }
  [[nodiscard]] auto command() const { return m_command; }
  [[nodiscard]] auto dataCount() const { return m_dataCount; }
  [[nodiscard]] auto packetHash() const { return m_cmdHash; }
  [[nodiscard]] auto dataHash() const { return m_dataHash; }

  [[nodiscard]] auto payload() -> Payload& { return m_data; }
  [[nodiscard]] auto payload() const -> const Payload& { return m_data; }

  void setMasterAddress(Address master_address) { m_address = master_address; }
  void setPacketId(PacketID pid) { m_pid = pid; }
  void setCommandCode(Command cmd) { m_command = cmd; }
  void setDataCount(uint8_t data_count) { m_dataCount = data_count; }

  [[nodiscard]] auto setPreambleHash(Hash hash) -> bool;
  [[nodiscard]] auto setDataHash(Hash hash) -> bool;

private:
  Address m_address{ 0 };
  PacketID m_pid{ 0 };
  Command m_command{ 0 };
  uint8_t m_dataCount{ 0 };
  Hash m_cmdHash{ 0 };
  Payload m_data;
  Hash m_dataHash{ 0 };

  bool m_async{ false };

  [[nodiscard]] auto calculateCmdHash() const -> Hash;
  [[nodiscard]] auto calculateDataHash() const -> Hash;
  [[nodiscard]] auto createPacketId() const -> PacketID;
};

} // namespace protocol::srvint

#endif // PROTOCOL_MOTHERBOARDPACKET_H
