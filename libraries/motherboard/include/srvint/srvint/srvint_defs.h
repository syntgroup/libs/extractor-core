#ifndef SRVINT_DEFS_H
#define SRVINT_DEFS_H

#include <cstdint>
#include <strong_type/strong_type.hpp>
#include <vector>

namespace protocol::srvint {

static constexpr uint8_t start_byte{ 0x55 };

using Address =
  strong::type<uint8_t, struct AddressTag, strong::equality, strong::ordered>;
using Hash =
  strong::type<uint8_t, struct HashTag, strong::equality, strong::ordered>;

// Constants for device addressing in interface SrvInt
static constexpr Address address_device{ 0x10 };
static constexpr Address address_null{ 0x00 };
static constexpr Address address_broadcast{ 0x55 };
static constexpr Address address_master{ 0xCA };

enum class Command : uint8_t
{
  Unknown           = 0x00,
  GoToBootMode      = 0x77,
  DevInfo           = 0x80,
  Ping              = 0x81,
  HwReset           = 0x82,
  SwReset           = 0x83,
  GetError          = 0x84,
  ResetError        = 0x85,
  SetParam          = 0x86,
  GetParam          = 0x87,
  AsyncDataTransfer = 0x8B,
};

enum FieldPos : uint8_t
{
  FPStartByte  = 0,
  FPSAdr       = 1,
  FPPacketId   = 2,
  FPCommand    = 3,
  FPDataCount  = 4,
  FPPacketHash = 5,
  FPPacketData = 6,
};

enum PacketIdPos : uint8_t
{
  PIPNumber = 0,
  PIPAsync  = 7
};

using Payload     = std::vector<uint8_t>;
using PayloadType = Payload::value_type;

} // namespace protocol::srvint

#endif // SRVINT_DEFS_H
