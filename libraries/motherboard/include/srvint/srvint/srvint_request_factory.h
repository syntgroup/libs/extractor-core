#ifndef SRVINT_PACKET_FACTORY_H
#define SRVINT_PACKET_FACTORY_H

#include "serial/protocol/protocol_values.h"
#include "srvint_defs.h"
#include "srvint_export.h"
#include <QByteArray>

namespace protocol::srvint {

struct CORE_SRVINT_EXPORT SrvIntRequestFactory
{
  [[nodiscard]] auto create(PacketID pid,
                            Command cmd,
                            const Payload& payload = {}) const -> QByteArray;

  [[nodiscard]] auto getParam(PacketID pid, const Payload& payload) const
    -> QByteArray;
  [[nodiscard]] auto setParam(PacketID pid, const Payload& payload) const
    -> QByteArray;
};

} // namespace protocol::srvint

#endif // SRVINT_PACKET_FACTORY_H
