#ifndef PROTOCOL_SRVINT_SRVINTRESPONSEFACTORY_H
#define PROTOCOL_SRVINT_SRVINTRESPONSEFACTORY_H

#include "srvint_export.h"
#include <QByteArray>
#include <memory>
#include <nonstd/expected.hpp>

namespace protocol::srvint {

struct SrvIntPacket;
enum class Error : uint8_t;

struct CORE_SRVINT_EXPORT SrvIntResponseFactory
{
  [[nodiscard]] auto create(const QByteArray& data) const
    -> nonstd::expected<std::unique_ptr<SrvIntPacket>, Error>;
};

} // namespace protocol::srvint

#endif // PROTOCOL_SRVINT_SRVINTRESPONSEFACTORY_H
