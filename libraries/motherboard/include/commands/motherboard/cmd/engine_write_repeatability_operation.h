#ifndef CMD_MOTHERBOARD_ENGINEWRITEREPEATABILITYOPERATION_H
#define CMD_MOTHERBOARD_ENGINEWRITEREPEATABILITYOPERATION_H

#include "abstract_motherboard_operation.h"
#include "motherboard/device/engine_values.h"

namespace device {
class Engine;
} // namespace device

namespace protocol::motherboard {
struct EngineWriteRepeatability;
struct EnginesSetParamResponse;
} // namespace protocol::motherboard

namespace cmd::motherboard {

class MOTHERBOARD_COMMAND_EXPORT EngineWriteRepeatabilityOperation final
  : public AbstractMotherboardOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(EngineWriteRepeatabilityOperation)
public:
  using TRequest  = protocol::motherboard::EngineWriteRepeatability;
  using TResponse = protocol::motherboard::EnginesSetParamResponse;

  explicit EngineWriteRepeatabilityOperation(
    protocol::PacketID pid,
    gsl::not_null<device::Engine*> engine,
    device::Repeatability repeatability,
    QObject* parent = nullptr);
  ~EngineWriteRepeatabilityOperation() override = default;

  [[nodiscard]] auto description() const -> QString override;
  [[nodiscard]] auto responseType() const -> int override;
  [[nodiscard]] auto createRequest() const -> Request override;

protected:
  [[nodiscard]] auto processResponse(const Response& t_response)
    -> bool override;

private:
  gsl::not_null<device::Engine*> m_engine;
  device::Repeatability m_repeatability{ 0 };
};

} // namespace cmd::motherboard

#endif // CMD_MOTHERBOARD_ENGINEWRITEREPEATABILITYOPERATION_H
