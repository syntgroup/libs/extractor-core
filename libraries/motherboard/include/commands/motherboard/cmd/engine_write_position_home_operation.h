#ifndef CMD_MOTHERBOARD_ENGINEWRITEPOSITIONHOMEOPERATION_H
#define CMD_MOTHERBOARD_ENGINEWRITEPOSITIONHOMEOPERATION_H

#include "abstract_motherboard_operation.h"
#include "motherboard/device/engine_values.h"

namespace device {
class Engine;
} // namespace device

namespace protocol::motherboard {
struct EngineWriteHomePosition;
struct EnginesSetParamResponse;
} // namespace protocol::motherboard

namespace cmd::motherboard {

class MOTHERBOARD_COMMAND_EXPORT EngineWritePositionHomeOperation final
  : public AbstractMotherboardOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(EngineWritePositionHomeOperation)

public:
  using TRequest  = protocol::motherboard::EngineWriteHomePosition;
  using TResponse = protocol::motherboard::EnginesSetParamResponse;

  explicit EngineWritePositionHomeOperation(
    protocol::PacketID pid,
    gsl::not_null<device::Engine*> engine,
    device::Coordinate value,
    QObject* parent = nullptr);
  ~EngineWritePositionHomeOperation() override = default;

  [[nodiscard]] auto description() const -> QString override;
  [[nodiscard]] auto responseType() const -> int override;
  [[nodiscard]] auto createRequest() const -> Request override;

protected:
  [[nodiscard]] auto processResponse(const Response& t_response)
    -> bool override;

private:
  gsl::not_null<device::Engine*> m_engine;
  const device::Coordinate m_newPosition{ 0 };
};

} // namespace cmd::motherboard

#endif // CMD_MOTHERBOARD_ENGINEWRITEPOSITIONHOMEOPERATION_H
