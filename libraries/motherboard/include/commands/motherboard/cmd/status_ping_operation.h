#ifndef CMD_MOTHERBOARD_EXECPING_H
#define CMD_MOTHERBOARD_EXECPING_H

#include "abstract_motherboard_operation.h"

namespace cmd::motherboard {

class MOTHERBOARD_COMMAND_EXPORT StatusPingOperation final
  : public AbstractMotherboardOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(StatusPingOperation)
public:
  using AbstractMotherboardOperation::AbstractMotherboardOperation;
  ~StatusPingOperation() override = default;

  [[nodiscard]] auto description() const -> QString override;
  [[nodiscard]] auto responseType() const -> int override;
  [[nodiscard]] auto createRequest() const -> Request override;
};

} // namespace cmd::motherboard

#endif // CMD_MOTHERBOARD_EXECPING_H
