#ifndef CMD_MOTHERBOARD_POWERGETBUTTONSOPERATION_H
#define CMD_MOTHERBOARD_POWERGETBUTTONSOPERATION_H

#include "abstract_motherboard_operation.h"

namespace device {
class DiscreteOutput;
} // namespace device

namespace protocol::motherboard {
struct VoltageBtnsRequest;
struct GetBtnsResponse;
} // namespace protocol::motherboard

namespace cmd::motherboard {

class MOTHERBOARD_COMMAND_EXPORT PowerGetButtonsOperation final
  : public AbstractMotherboardOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(PowerGetButtonsOperation)

public:
  using TRequest  = protocol::motherboard::VoltageBtnsRequest;
  using TResponse = protocol::motherboard::GetBtnsResponse;

  using DoorSensor  = device::DiscreteOutput;
  using PowerButton = device::DiscreteOutput;

  explicit PowerGetButtonsOperation(protocol::PacketID pid,
                                    DoorSensor* door,
                                    PowerButton* power,
                                    QObject* parent = nullptr);
  ~PowerGetButtonsOperation() override = default;

  [[nodiscard]] auto description() const -> QString override;
  [[nodiscard]] auto responseType() const -> int override;
  [[nodiscard]] auto createRequest() const -> Request override;

protected:
  [[nodiscard]] auto processResponse(const Response& t_response)
    -> bool override;

private:
  device::DiscreteOutput* const m_doorButton;
  device::DiscreteOutput* const m_powerButton;
};

} // namespace cmd::motherboard

#endif // CMD_MOTHERBOARD_POWERGETBUTTONSOPERATION_H
