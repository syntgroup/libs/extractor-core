#ifndef CMD_MOTHERBOARD_MULTIMIXNTIMESOPERATION_H
#define CMD_MOTHERBOARD_MULTIMIXNTIMESOPERATION_H

#include "motherboard/command/export.h"
#include "motherboard/device/engine_values.h"
#include "serial/cmd/abstract_multi_stage_operation.h"
#include <gsl/gsl-lite.hpp>

namespace device {
class Engine;
} // namespace device

namespace cmd::multi {

class MOTHERBOARD_COMMAND_EXPORT MixNCyclesOperation
  : public cmd::AbstractMultiStageOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(MixNCyclesOperation)

public:
  enum OperationState : uint8_t
  {
    WritingMixingSpeed         = AbstractOperation::User,
    WritingMixingRepeatability = 0x03,
    ExecStartMixingNCycles     = 0x05,
    CheckMixingState           = 0x06,
    WritingTravelMaxSpeed      = 0x09,
    WritingTravelRepeatability = 0x10,
    SmoothStop                 = 0xaa,
    EmergencyStop              = 0xab,
    Idle                       = 0xf0,
    ZeroizeError               = 0xff
  };
  Q_ENUM(OperationState)

  MixNCyclesOperation(gsl::not_null<device::Engine*> engine,
                      device::Cycles&& cycles,
                      device::Coordinate&& amplitude,
                      device::SpeedIndex&& mixing_speed,
                      QObject* parent = nullptr);
  ~MixNCyclesOperation() override = default;

  [[nodiscard]] auto description() const -> QString override;

protected:
  // AbstractMultiStageOperation interface
  void onSubOperationError(const AbstractSerialOperation& operation) override;

private:
  gsl::not_null<device::Engine*> m_engine;
  device::Cycles m_cycles{ 0 };
  device::Coordinate m_amplitude{ 0 };
  device::SpeedIndex m_new_speed{ 0 };

  OperationState m_buffer{ OperationState::ZeroizeError };

  void writeSpeed(device::SpeedIndex speed);
  void writeRepeatability(device::Repeatability repeatability);
  void execStartMixing();
  void checkMixingCycle();
  void zeroizeError();
  void stopMixing();
  void emergencyStop();

public slots:
  void pause() override;
  void resume() override;

private slots:
  void nextStateLogic() override;
};

} // namespace cmd::multi

#endif // CMD_MOTHERBOARD_MULTIMIXNTIMESOPERATION_H
