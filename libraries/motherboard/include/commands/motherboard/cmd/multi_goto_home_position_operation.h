#ifndef CMD_MOTHERBOARD_GOTOHOMEPOSITIONMULTIOPERATION_H
#define CMD_MOTHERBOARD_GOTOHOMEPOSITIONMULTIOPERATION_H

#include "motherboard/command/export.h"
#include "motherboard/device/engine_speed.h"
#include "motherboard/device/engine_values.h"
#include "serial/cmd/abstract_multi_stage_operation.h"
#include <gsl/gsl-lite.hpp>

namespace device {
class Engine;
} // namespace device

namespace cmd::multi {

class MOTHERBOARD_COMMAND_EXPORT GotoHomeOperation final
  : public cmd::AbstractMultiStageOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(GotoHomeOperation)

public:
  enum OperationState
  {
    ResettingError = AbstractOperation::User,
    TurningEngineOn,
    WriteFrequencyStart,
    WriteFrequencyStop,
    WriteFrequencyStep,
    WritingHomingSpeed,
    CheckingHomingSpeed,
    WritingHomePosition,
    CheckingHomePosition,
    ExecGotoZero,
    CheckingZeroSuccess,
    WritingTravelSpeed,
    CheckingTravelSpeed,
    SmoothStop     = 0xaa,
    EmergencyStop  = 0xab,
    Idle           = 0xf0,
    ZeroizingError = 0xff
  };
  Q_ENUM(OperationState)

  explicit GotoHomeOperation(gsl::not_null<device::Engine*> engine,
                             QObject* parent = nullptr);
  ~GotoHomeOperation() override = default;

  [[nodiscard]] auto description() const -> QString override;

private:
  gsl::not_null<device::Engine*> m_engine;

  void resetError();
  void turnEngineOn();

  void execGotoZero();
  void writeFrequency(device::Acceleration frequency);
  void writeSpeed(device::SpeedIndex speed);
  void checkSpeed();
  void checkCompleteMovement();
  void writeHomePosition(device::Coordinate pos);
  void checkHomePosition();
  void smoothStop();
  void emergencyStop();

  OperationState m_buffer{ OperationState::ZeroizingError };

  // AbstractMultiStageOperation interface
protected:
  void onSubOperationError(const AbstractSerialOperation& operation) override;

private slots:
  void nextStateLogic() override;

  // AbstractMultiStageOperation interface
public slots:
  void pause() override;
  void resume() override;
};

} // namespace cmd::multi

#endif // CMD_MOTHERBOARD_GOTOHOMEPOSITIONMULTIOPERATION_H
