#ifndef CMD_MOTHERBOARD_GOTOWRITECURRENTSOPERATION_H
#define CMD_MOTHERBOARD_GOTOWRITECURRENTSOPERATION_H

#include "abstract_motherboard_operation.h"
#include "motherboard/device/engine_values.h"

namespace device {
class Engine;
} // namespace device

namespace protocol::motherboard {
struct GotoCurrentsSetRequest;
struct GotoResponse;
} // namespace protocol::motherboard

namespace cmd::motherboard {

class MOTHERBOARD_COMMAND_EXPORT GotoWriteCurrentsOperation final
  : public AbstractMotherboardOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(GotoWriteCurrentsOperation)

public:
  using TRequest  = protocol::motherboard::GotoCurrentsSetRequest;
  using TResponse = protocol::motherboard::GotoResponse;

  explicit GotoWriteCurrentsOperation(protocol::PacketID pid,
                                      gsl::not_null<device::Engine*> engine,
                                      device::Current current_accel,
                                      device::Current current_stdby,
                                      QObject* parent = nullptr);
  ~GotoWriteCurrentsOperation() override = default;

  [[nodiscard]] auto description() const -> QString override;
  [[nodiscard]] auto responseType() const -> int override;
  [[nodiscard]] auto createRequest() const -> Request override;

protected:
  [[nodiscard]] auto processResponse(const Response& t_response)
    -> bool override;

private:
  gsl::not_null<device::Engine*> m_engine;
  device::Current m_currentAccel{ device::Current::Current_11_32 };
  device::Current m_currentStdby{ device::Current::Current_8_32 };
};

} // namespace cmd::motherboard

#endif // CMD_MOTHERBOARD_GOTOWRITECURRENTSOPERATION_H
