#ifndef EXTRACTOR_TOUCHPANEL_SNS_STATE_OPERATION_H
#define EXTRACTOR_TOUCHPANEL_SNS_STATE_OPERATION_H

#include "abstract_motherboard_operation.h"

namespace device {
class IrSensors;
} // namespace device

namespace protocol::motherboard {
struct SnsStateRequest;
struct SnsStateResponse;
} // namespace protocol::motherboard

namespace cmd::motherboard {
class MOTHERBOARD_COMMAND_EXPORT SnsStateOperation final
  : public AbstractMotherboardOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(SnsStateOperation)
public:
  using TRequest  = protocol::motherboard::SnsStateRequest;
  using TResponse = protocol::motherboard::SnsStateResponse;

  explicit SnsStateOperation(protocol::PacketID pid,
                             gsl::not_null<device::IrSensors*> sensors,
                             unsigned int index_to_check,
                             QObject* parent = nullptr);
  ~SnsStateOperation() override = default;

  [[nodiscard]] auto description() const -> QString override;
  [[nodiscard]] auto responseType() const -> int override;
  [[nodiscard]] auto createRequest() const -> Request override;

protected:
  [[nodiscard]] auto processResponse(const Response& t_response)
    -> bool override;

private:
  gsl::not_null<device::IrSensors*> m_sensors;
  unsigned int m_indexToCheck{ 1 };
};

} // namespace cmd::motherboard

#endif // EXTRACTOR_TOUCHPANEL_SNS_STATE_OPERATION_H
