#ifndef CMD_MOTHERBOARD_DIAGOPTSTATEOPERATION_H
#define CMD_MOTHERBOARD_DIAGOPTSTATEOPERATION_H

#include "abstract_motherboard_operation.h"

namespace device {
class DiscreteOutput;
} // namespace device

namespace protocol::motherboard {
struct OptDiagRequest;
struct ButtonsResponse;
} // namespace protocol::motherboard

namespace cmd::motherboard {

class MOTHERBOARD_COMMAND_EXPORT DiagOptStateOperation final
  : public AbstractMotherboardOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(DiagOptStateOperation)
public:
  using TRequest  = protocol::motherboard::OptDiagRequest;
  using TResponse = protocol::motherboard::ButtonsResponse;

  explicit DiagOptStateOperation(protocol::PacketID pid,
                                 gsl::not_null<device::DiscreteOutput*> sensors,
                                 QObject* parent = nullptr);
  ~DiagOptStateOperation() override = default;

  [[nodiscard]] auto description() const -> QString override;
  [[nodiscard]] auto responseType() const -> int override;
  [[nodiscard]] auto createRequest() const -> Request override;

protected:
  [[nodiscard]] auto processResponse(const Response& t_response)
    -> bool override;

private:
  gsl::not_null<device::DiscreteOutput*> m_sensors;
};

} // namespace cmd::motherboard

#endif // CMD_MOTHERBOARD_DIAGOPTSTATEOPERATION_H
