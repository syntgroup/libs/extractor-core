#ifndef CMD_MOTHERBOARD_SYSTEMZEROIZEERROROPERATION_H
#define CMD_MOTHERBOARD_SYSTEMZEROIZEERROROPERATION_H

#include "abstract_motherboard_operation.h"

namespace protocol::motherboard {
struct SystemZeroizeErrorRequest;
} // namespace protocol::motherboard

namespace cmd::motherboard {

class MOTHERBOARD_COMMAND_EXPORT SystemZeroizeErrorOperation final
  : public AbstractMotherboardOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(SystemZeroizeErrorOperation)
public:
  using TRequest = protocol::motherboard::SystemZeroizeErrorRequest;
  using AbstractMotherboardOperation::AbstractMotherboardOperation;
  ~SystemZeroizeErrorOperation() override = default;

  [[nodiscard]] auto description() const -> QString override;
  [[nodiscard]] auto responseType() const -> int override;
  [[nodiscard]] auto createRequest() const -> Request override;
};

} // namespace cmd::motherboard

#endif // CMD_MOTHERBOARD_SYSTEMZEROIZEERROROPERATION_H
