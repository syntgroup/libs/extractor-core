#ifndef CMD_MOTHERBOARD_GOTONTICKS_UNC_OPERATION_H
#define CMD_MOTHERBOARD_GOTONTICKS_UNC_OPERATION_H

#include "abstract_motherboard_operation.h"
#include "motherboard/device/engine_values.h"

namespace device {
class Engine;
} // namespace device

namespace protocol::motherboard {
struct GotoNTicksUNCRequest;
struct GotoResponse;
} // namespace protocol::motherboard

namespace cmd::motherboard {

class MOTHERBOARD_COMMAND_EXPORT GotoNTicksUncOperation final
  : public AbstractMotherboardOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(GotoNTicksUncOperation)
public:
  using TRequest  = protocol::motherboard::GotoNTicksUNCRequest;
  using TResponse = protocol::motherboard::GotoResponse;

  explicit GotoNTicksUncOperation(protocol::PacketID pid,
                                  gsl::not_null<device::Engine*> engine,
                                  device::Coordinate ticks,
                                  QObject* parent = nullptr);
  ~GotoNTicksUncOperation() override = default;

  [[nodiscard]] auto description() const -> QString override;
  [[nodiscard]] auto responseType() const -> int override;
  [[nodiscard]] auto createRequest() const -> Request override;

protected:
  [[nodiscard]] auto processResponse(const Response& t_response)
    -> bool override;

private:
  gsl::not_null<device::Engine*> m_engine;
  const device::Coordinate m_ticks{ 0 };
};

} // namespace cmd::motherboard

#endif // CMD_MOTHERBOARD_GOTONTICKS_UNC_OPERATION_H
