#ifndef CMD_MOTHERBOARD_VOLTAGEGET24VOPERATION_H
#define CMD_MOTHERBOARD_VOLTAGEGET24VOPERATION_H

#include "abstract_motherboard_operation.h"
#include "common/analog_input.h"
#include "motherboard/device/motherboard_values.h"

namespace device {
class Rail24V;
} // namespace device

namespace protocol::motherboard {
struct Voltage24VRailRequest;
struct GetRailResponse;
} // namespace protocol::motherboard

namespace cmd::motherboard {

class MOTHERBOARD_COMMAND_EXPORT VoltageGet24VOperation final
  : public AbstractMotherboardOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(VoltageGet24VOperation)
public:
  using TRequest  = protocol::motherboard::Voltage24VRailRequest;
  using TResponse = protocol::motherboard::GetRailResponse;
  using TDevice   = device::AnalogInput<device::Voltage>;

  explicit VoltageGet24VOperation(protocol::PacketID pid,
                                  gsl::not_null<TDevice*> powers,
                                  QObject* parent = nullptr);
  ~VoltageGet24VOperation() override = default;

  [[nodiscard]] auto description() const -> QString override;
  [[nodiscard]] auto responseType() const -> int override;
  [[nodiscard]] auto createRequest() const -> Request override;

protected:
  [[nodiscard]] auto processResponse(const Response& t_response)
    -> bool override;

private:
  gsl::not_null<TDevice*> m_voltages;
};

} // namespace cmd::motherboard

#endif // CMD_MOTHERBOARD_VOLTAGEGET24VOPERATION_H
