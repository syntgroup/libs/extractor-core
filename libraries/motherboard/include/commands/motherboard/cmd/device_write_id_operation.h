#ifndef EXTRACTOR_TOUCHPANEL_DEVICE_WRITE_ID_OPERATION_H
#define EXTRACTOR_TOUCHPANEL_DEVICE_WRITE_ID_OPERATION_H

#include "abstract_motherboard_operation.h"

namespace device {
class MotherboardParameters;
} // namespace device

namespace protocol::motherboard {
struct DeviceSetIdRequest;
} // namespace protocol::motherboard

namespace cmd::motherboard {

class MOTHERBOARD_COMMAND_EXPORT DeviceWriteIdOperation final
  : public AbstractMotherboardOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(DeviceWriteIdOperation)
public:
  using TRequest = protocol::motherboard::DeviceSetIdRequest;

  explicit DeviceWriteIdOperation(
    protocol::PacketID pid,
    gsl::not_null<device::MotherboardParameters*> parameters,
    uint32_t new_device_id,
    QObject* parent = nullptr);
  ~DeviceWriteIdOperation() override = default;

  // AbstractOperation interface
  [[nodiscard]] QString description() const override;
  [[nodiscard]] int responseType() const override;
  [[nodiscard]] Request createRequest() const override;

  // AbstractMotherboardOperation interface
protected:
  bool processResponse(const Response& t_response) override;

private:
  uint32_t m_newDeviceId;
  gsl::not_null<device::MotherboardParameters*> m_motherboardParameters;
};

} // namespace cmd::motherboard

#endif // EXTRACTOR_TOUCHPANEL_DEVICE_WRITE_ID_OPERATION_H
