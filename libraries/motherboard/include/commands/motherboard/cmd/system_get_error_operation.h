#ifndef CMD_MOTHERBOARD_SYSTEMGETERROROPERATION_H
#define CMD_MOTHERBOARD_SYSTEMGETERROROPERATION_H

#include "abstract_motherboard_operation.h"

namespace protocol::motherboard {
struct MOTHERBOARD_COMMAND_EXPORT SystemGetErrorRequest;
struct MOTHERBOARD_COMMAND_EXPORT GetErrorResponse;
} // namespace protocol::motherboard

namespace cmd::motherboard {

class MOTHERBOARD_COMMAND_EXPORT SystemGetErrorOperation final
  : public AbstractMotherboardOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(SystemGetErrorOperation)
public:
  using TRequest  = protocol::motherboard::SystemGetErrorRequest;
  using TResponse = protocol::motherboard::GetErrorResponse;

  explicit SystemGetErrorOperation(protocol::PacketID pid,
                                   uint8_t error_pos,
                                   QObject* parent = nullptr);
  ~SystemGetErrorOperation() override = default;

  [[nodiscard]] auto description() const -> QString override;
  [[nodiscard]] auto responseType() const -> int override;
  [[nodiscard]] auto createRequest() const -> Request override;

protected:
  [[nodiscard]] auto processResponse(const Response& t_response)
    -> bool override;

private:
  const uint8_t m_errorPos{ 0 };
};

} // namespace cmd::motherboard

#endif // CMD_MOTHERBOARD_SYSTEMGETERROROPERATION_H
