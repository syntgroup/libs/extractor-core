#ifndef CMD_MOTHERBOARD_POWERREQUESTTURNOFF_H
#define CMD_MOTHERBOARD_POWERREQUESTTURNOFF_H

#include "abstract_motherboard_operation.h"

namespace protocol::motherboard {
struct PowerSetTurnOff;
} // namespace protocol::motherboard

namespace cmd::motherboard {

class MOTHERBOARD_COMMAND_EXPORT PowerRequestTurnOff final
  : public AbstractMotherboardOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(PowerRequestTurnOff)

public:
  using TRequest = protocol::motherboard::PowerSetTurnOff;

  using AbstractMotherboardOperation::AbstractMotherboardOperation;
  ~PowerRequestTurnOff() override = default;

  [[nodiscard]] auto description() const -> QString override;
  [[nodiscard]] auto responseType() const -> int override;
  [[nodiscard]] auto createRequest() const -> Request override;
};

} // namespace cmd::motherboard

#endif // CMD_MOTHERBOARD_POWERREQUESTTURNOFF_H
