#ifndef CMD_MOTHERBOARD_GOTOEMSTOPOPERATION_H
#define CMD_MOTHERBOARD_GOTOEMSTOPOPERATION_H

#include "abstract_motherboard_operation.h"

namespace device {
class Engine;
} // namespace device

namespace protocol::motherboard {
struct GotoEmStopRequest;
struct GotoResponse;
} // namespace protocol::motherboard

namespace cmd::motherboard {

class MOTHERBOARD_COMMAND_EXPORT GotoEmStopOperation final
  : public AbstractMotherboardOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(GotoEmStopOperation)
public:
  using Param     = std::vector<gsl::not_null<device::Engine*>>;
  using TRequest  = protocol::motherboard::GotoEmStopRequest;
  using TResponse = protocol::motherboard::GotoResponse;

  explicit GotoEmStopOperation(protocol::PacketID pid,
                               Param&& param,
                               QObject* parent = nullptr);
  ~GotoEmStopOperation() override = default;

  [[nodiscard]] auto begin() -> bool override;
  [[nodiscard]] auto description() const -> QString override;
  [[nodiscard]] auto responseType() const -> int override;
  [[nodiscard]] auto createRequest() const -> Request override;

protected:
  [[nodiscard]] auto processResponse(const Response& t_response)
    -> bool override;

private:
  const Param m_params;
};

} // namespace cmd::motherboard

#endif // CMD_MOTHERBOARD_GOTOEMSTOPOPERATION_H
