#ifndef CMD_MOTHERBOARD_ENGINEGETMIXMODESTATUSOPERATION_H
#define CMD_MOTHERBOARD_ENGINEGETMIXMODESTATUSOPERATION_H

#include "abstract_motherboard_operation.h"

namespace device {
class Engine;
} // namespace device

namespace protocol::motherboard {
struct EngineRequestMixModeState;
struct EngineGetMixModeStateResponse;
} // namespace protocol::motherboard

namespace cmd::motherboard {

class MOTHERBOARD_COMMAND_EXPORT EngineGetMixModeStatusOperation final
  : public AbstractMotherboardOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(EngineGetMixModeStatusOperation)

public:
  using TRequest  = protocol::motherboard::EngineRequestMixModeState;
  using TResponse = protocol::motherboard::EngineGetMixModeStateResponse;

  explicit EngineGetMixModeStatusOperation(
    protocol::PacketID pid,
    gsl::not_null<device::Engine*> engine,
    QObject* parent = nullptr);
  ~EngineGetMixModeStatusOperation() override = default;

  [[nodiscard]] QString description() const override;
  [[nodiscard]] int responseType() const override;
  [[nodiscard]] Request createRequest() const override;

protected:
  [[nodiscard]] bool processResponse(const Response& t_response) override;

private:
  gsl::not_null<device::Engine*> m_engine;
};

} // namespace cmd::motherboard

#endif // CMD_MOTHERBOARD_ENGINEGETMIXMODESTATUSOPERATION_H
