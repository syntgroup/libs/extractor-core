#ifndef CMD_MOTHERBOARD_ENGINEGETPOSCUR_H
#define CMD_MOTHERBOARD_ENGINEGETPOSCUR_H

#include "abstract_motherboard_operation.h"

namespace device {
class Engine;
} // namespace device

namespace protocol::motherboard {
struct EngineRequestPosition;
struct EnginesGetPositionResponse;
} // namespace protocol::motherboard

namespace cmd::motherboard {

class MOTHERBOARD_COMMAND_EXPORT EngineGetPositionCurrentOperation final
  : public AbstractMotherboardOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(EngineGetPositionCurrentOperation)
public:
  using TRequest  = protocol::motherboard::EngineRequestPosition;
  using TResponse = protocol::motherboard::EnginesGetPositionResponse;

  explicit EngineGetPositionCurrentOperation(
    protocol::PacketID pid,
    gsl::not_null<device::Engine*> engine,
    QObject* parent = nullptr);
  ~EngineGetPositionCurrentOperation() override = default;

  [[nodiscard]] auto description() const -> QString override;
  [[nodiscard]] auto responseType() const -> int override;
  [[nodiscard]] auto createRequest() const -> Request override;

protected:
  [[nodiscard]] auto processResponse(const Response& t_response)
    -> bool override;

private:
  gsl::not_null<device::Engine*> m_engine;
};

} // namespace cmd::motherboard

#endif // CMD_MOTHERBOARD_ENGINEGETPOSCUR_H
