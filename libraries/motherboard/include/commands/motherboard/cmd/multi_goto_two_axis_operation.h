#ifndef CMD_MOTHERBOARD_MULTIGOTOTWOAXISOPERATION_H
#define CMD_MOTHERBOARD_MULTIGOTOTWOAXISOPERATION_H

#include "motherboard/command/export.h"
#include "motherboard/device/engine_speed.h"
#include "motherboard/device/engine_values.h"
#include "serial/cmd/abstract_multi_stage_operation.h"
#include <gsl/gsl-lite.hpp>

namespace device {
class Engine;
} // namespace device

namespace cmd::motherboard {
class AbstractMotherboardOperation;
} // namespace cmd::motherboard

namespace cmd::multi {

class MOTHERBOARD_COMMAND_EXPORT GotoTwoAxisOperation final
  : public cmd::AbstractMultiStageOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(GotoTwoAxisOperation)

  using Engine = gsl::not_null<device::Engine*>;

public:
  using Target = std::tuple<Engine, device::Coordinate, device::SpeedIndex>;
  using Params = std::vector<Target>;

  enum OperationState : uint8_t
  {
    ResettingError = AbstractOperation::User,
    WriteFrequencyStart,
    WriteFrequencyStop,
    WriteFrequencyStep,
    SettingNewSpeedEngine,
    CheckingNewSpeedEngine,
    ExecGotoEngines,
    CheckEngines,
    SettingTravelSpeedEngine,
    CheckingTravelSpeedEngine,
    SmoothStopEngine = 0xaa,
    Idle             = 0xf0,
    ZeroizingError   = 0xff
  };
  Q_ENUM(OperationState)

  explicit GotoTwoAxisOperation(Params&& params,
                                device::AccelerationCurve acceleration_profile,
                                QObject* parent = nullptr);
  ~GotoTwoAxisOperation() override;

  // AbstractOperation interface
  [[nodiscard]] auto description() const -> QString override;

private:
  Params _params;
  Params::iterator _currentEngine;
  device::AccelerationCurve _accelerationProfile;

  static_assert(std::is_same_v<decltype(_params)::value_type,
                               decltype(_currentEngine)::value_type>);

  OperationState _buffer{ OperationState::ZeroizingError };

  //! @brief use this in the beginning to reset all previous errors
  //! @note register this as sub-operation to finish and move on
  void resetError();
  //! @brief not the same think, emergency use only!
  //! @note DO NOT register as sub-operation to be able to switch to different
  //! state
  void zeroizeError();
  void gotoEngines(); //!< dual mode
  void checkEngine(Engine engine);
  void setEngineSpeed(Engine engine, device::SpeedIndex speed);
  void checkEngineSpeed(Engine engine);
  void smoothStops(); //!< dualmode

  void writeFrequency(Engine engine, device::Acceleration frequency);

  [[deprecated]] [[nodiscard]] auto allReached() const -> bool;

  void switcher(OperationState new_state);

protected:
  // AbstractMultiStageOperation interface
  void onSubOperationError(const AbstractSerialOperation& operation) override;

private slots:
  void nextStateLogic() override;

  // AbstractMultiStageOperation interface
public slots:
  void pause() override;
  void resume() override;
};

} // namespace cmd::multi

#endif // CMD_MOTHERBOARD_MULTIGOTOTWOAXISOPERATION_H
