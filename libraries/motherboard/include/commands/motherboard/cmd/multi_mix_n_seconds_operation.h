#ifndef CMD_MOTHERBOARD_MULTIMIXNSECONDSOPERATION_H
#define CMD_MOTHERBOARD_MULTIMIXNSECONDSOPERATION_H

#include "motherboard/command/export.h"
#include "motherboard/device/engine_values.h"
#include "serial/cmd/abstract_multi_stage_operation.h"
#include <gsl/gsl-lite.hpp>

namespace device {
class Engine;
} // namespace device

namespace cmd::multi {

class MOTHERBOARD_COMMAND_EXPORT MixNSecondsOperation
  : public cmd::AbstractMultiStageOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(MixNSecondsOperation)
public:
  enum OperationState : uint8_t
  {
    WritingMixingSpeed         = AbstractOperation::User,
    WritingMixingRepeatability = 0x03,
    ExecStartMixingNSeconds    = 0x05,
    CheckMixingTime            = 0x06,
    WritingTravelMaxSpeed      = 0x09,
    WritingTravelRepeatability = 0x10,
    SmoothStop                 = 0xaa,
    EmergencyStop              = 0xab,
    Idle                       = 0xf0,
    ZeroizeError               = 0xff
  };
  Q_ENUM(OperationState)

  MixNSecondsOperation(gsl::not_null<device::Engine*> engine,
                       std::chrono::seconds&& time,
                       device::Coordinate&& amplitude,
                       device::SpeedIndex&& mixing_speed,
                       QObject* parent = nullptr);
  ~MixNSecondsOperation() override = default;

  [[nodiscard]] auto description() const -> QString override;

protected:
  // AbstractMultiStageOperation interface
  void onSubOperationError(const AbstractSerialOperation& operation) override;

private:
  gsl::not_null<device::Engine*> m_engine;
  std::chrono::seconds m_seconds{ 0 };
  device::Coordinate m_amplitude{ 0 };
  device::SpeedIndex m_newSpeed{ 0 };

  OperationState m_buffer{ OperationState::ZeroizeError };

  void writeSpeed(device::SpeedIndex speed);
  void writeRepeatability(device::Repeatability repeatability);
  void execStartMixing();
  void checkMixingTime();
  void zeroizeError();
  void stopMixing();
  void emergencyStop();

  std::unique_ptr<QTimer> m_countdownTimer{ nullptr };
  std::chrono::milliseconds m_timeBuffer{ 0 };
  const std::chrono::milliseconds m_countdown_interval{ 100 };

  [[nodiscard]] constexpr auto timeElapsed() const
  {
    return m_timeBuffer == std::chrono::milliseconds{ 0 };
  }

public slots:
  void pause() override;
  void resume() override;

private slots:
  void nextStateLogic() override;
  void countdownRoutine();
};

} // namespace cmd::multi

#endif // CMD_MOTHERBOARD_MULTIMIXNSECONDSOPERATION_H
