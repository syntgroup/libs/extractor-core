#ifndef CMD_MOTHERBOARD_ENGINEDRIVERRAWWRITEOPERATION_H
#define CMD_MOTHERBOARD_ENGINEDRIVERRAWWRITEOPERATION_H

#include "abstract_motherboard_operation.h"

namespace device {
class Engine;
} // namespace device

namespace protocol::motherboard {
struct EngineDriverRawWrite;
struct EnginesSetParamResponse;
} // namespace protocol::motherboard

namespace cmd::motherboard {

class MOTHERBOARD_COMMAND_EXPORT EngineDriverRawWriteOperation final
  : public AbstractMotherboardOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(EngineDriverRawWriteOperation)
public:
  using TRequest  = protocol::motherboard::EngineDriverRawWrite;
  using TResponse = protocol::motherboard::EnginesSetParamResponse;

  explicit EngineDriverRawWriteOperation(protocol::PacketID idx,
                                         gsl::not_null<device::Engine*> engine,
                                         QByteArray bytes,
                                         QObject* parent = nullptr);
  ~EngineDriverRawWriteOperation() override = default;

  // AbstractOperation interface
  [[nodiscard]] auto description() const -> QString override;
  [[nodiscard]] auto responseType() const -> int override;
  [[nodiscard]] auto createRequest() const -> Request override;

protected:
  [[nodiscard]] auto processResponse(const Response& t_response)
    -> bool override;

private:
  gsl::not_null<device::Engine*> m_engine;
  QByteArray m_data;
};

} // namespace cmd::motherboard

#endif // CMD_MOTHERBOARD_ENGINEDRIVERRAWWRITEOPERATION_H
