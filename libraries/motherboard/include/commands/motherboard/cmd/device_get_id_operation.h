#ifndef CMD_MOTHERBOARD_DEVICEGETIDOPERATION_H
#define CMD_MOTHERBOARD_DEVICEGETIDOPERATION_H

#include "abstract_motherboard_operation.h"

namespace device {
class MotherboardParameters;
} // namespace device

namespace protocol::motherboard {
struct DeviceRequestId;
struct DeviceGetIdResponse;
} // namespace protocol::motherboard

namespace cmd::motherboard {

class MOTHERBOARD_COMMAND_EXPORT DeviceGetIdOperation final
  : public AbstractMotherboardOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(DeviceGetIdOperation)

public:
  using TRequest  = protocol::motherboard::DeviceRequestId;
  using TResponse = protocol::motherboard::DeviceGetIdResponse;

  explicit DeviceGetIdOperation(
    protocol::PacketID pid,
    gsl::not_null<device::MotherboardParameters*> motherboard_parameters,
    QObject* parent = nullptr);
  ~DeviceGetIdOperation() override = default;

  [[nodiscard]] QString description() const override;
  [[nodiscard]] int responseType() const override;
  [[nodiscard]] Request createRequest() const override;

protected:
  bool processResponse(const Response& t_response) override;

private:
  gsl::not_null<device::MotherboardParameters*> m_motherboardParameters;
};

} // namespace cmd::motherboard

#endif // CMD_MOTHERBOARD_DEVICEGETIDOPERATION_H
