#ifndef CMD_MOTHERBOARD_ENGINEWRITEMAXSPEEDOPERATION_H
#define CMD_MOTHERBOARD_ENGINEWRITEMAXSPEEDOPERATION_H

#include "abstract_motherboard_operation.h"
#include "motherboard/device/engine_values.h"

namespace device {
class Engine;
} // namespace device

namespace protocol::motherboard {
struct EngineWriteMaxSpeed;
struct EnginesSetParamResponse;
} // namespace protocol::motherboard

namespace cmd::motherboard {

class MOTHERBOARD_COMMAND_EXPORT EngineWriteMaxSpeedOperation final
  : public AbstractMotherboardOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(EngineWriteMaxSpeedOperation)
public:
  using TRequest  = protocol::motherboard::EngineWriteMaxSpeed;
  using TResponse = protocol::motherboard::EnginesSetParamResponse;

  explicit EngineWriteMaxSpeedOperation(protocol::PacketID pid,
                                        gsl::not_null<device::Engine*> engine,
                                        device::SpeedIndex value,
                                        QObject* parent = nullptr);
  ~EngineWriteMaxSpeedOperation() override = default;

  [[nodiscard]] QString description() const override;
  [[nodiscard]] int responseType() const override;
  [[nodiscard]] Request createRequest() const override;

protected:
  [[nodiscard]] bool processResponse(const Response& t_response) override;

private:
  gsl::not_null<device::Engine*> m_engine;
  const device::SpeedIndex m_newSpeed{ 0 };
};

} // namespace cmd::motherboard

#endif // CMD_MOTHERBOARD_ENGINEWRITEMAXSPEEDOPERATION_H
