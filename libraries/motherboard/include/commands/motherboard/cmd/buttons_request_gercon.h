#ifndef CMD_MOTHERBOARD_POWERGETGERCONOPERATION_H
#define CMD_MOTHERBOARD_POWERGETGERCONOPERATION_H

#include "abstract_motherboard_operation.h"

namespace device {
class DiscreteInput;
} // namespace device

namespace protocol::motherboard {
struct GerconGetStateRequest;
struct ButtonsResponse;
} // namespace protocol::motherboard

namespace cmd::motherboard {

class MOTHERBOARD_COMMAND_EXPORT PowerGetGerconOperation final
  : public AbstractMotherboardOperation
{
  Q_OBJECT
public:
  using TRequest  = protocol::motherboard::GerconGetStateRequest;
  using TResponse = protocol::motherboard::ButtonsResponse;

  explicit PowerGetGerconOperation(protocol::PacketID pid,
                                   gsl::not_null<device::DiscreteInput> button,
                                   QObject* parent = nullptr);

  [[nodiscard]] auto description() const -> QString override;
  [[nodiscard]] auto responseType() const -> int override;
  [[nodiscard]] auto createRequest() const -> Request override;

protected:
  [[nodiscard]] auto processResponse(const Response& t_response)
    -> bool override;

private:
  gsl::not_null<device::DiscreteInput> m_doorButton;
};

} // namespace cmd::motherboard

#endif // CMD_MOTHERBOARD_POWERGETGERCONOPERATION_H
