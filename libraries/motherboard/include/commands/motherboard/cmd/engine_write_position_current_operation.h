#ifndef CMD_MOTHERBOARD_ENGINEWRITEPOSITIONCURRENTOPERATION_H
#define CMD_MOTHERBOARD_ENGINEWRITEPOSITIONCURRENTOPERATION_H

#include "abstract_motherboard_operation.h"
#include "motherboard/device/engine_values.h"

namespace device {
class Engine;
} // namespace device

namespace protocol::motherboard {
struct EngineWritePosition;
struct EnginesSetParamResponse;
} // namespace protocol::motherboard

namespace cmd::motherboard {

class MOTHERBOARD_COMMAND_EXPORT EngineWritePositionCurrentOperation final
  : public AbstractMotherboardOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(EngineWritePositionCurrentOperation)

public:
  using TRequest  = protocol::motherboard::EngineWritePosition;
  using TResponse = protocol::motherboard::EnginesSetParamResponse;

  explicit EngineWritePositionCurrentOperation(
    protocol::PacketID pid,
    gsl::not_null<device::Engine*> engine,
    device::Coordinate value,
    QObject* parent = nullptr);
  ~EngineWritePositionCurrentOperation() override = default;

  [[nodiscard]] auto description() const -> QString override;
  [[nodiscard]] auto responseType() const -> int override;
  [[nodiscard]] auto createRequest() const -> Request override;

protected:
  // AbstractMotherboardOperation interface
  [[nodiscard]] auto processResponse(const Response& t_response)
    -> bool override;

private:
  gsl::not_null<device::Engine*> m_engine;
  device::Coordinate m_newValue{ 0 };
};

} // namespace cmd::motherboard

#endif // CMD_MOTHERBOARD_ENGINEWRITEPOSITIONCURRENTOPERATION_H
