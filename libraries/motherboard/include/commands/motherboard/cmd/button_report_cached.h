#ifndef BUTTON_REPORT_CACHED_H
#define BUTTON_REPORT_CACHED_H

#include "common/abstract_meta_operation.h"
#include "motherboard/command/export.h"
#include <gsl/gsl-lite.hpp>

namespace device {
class DiscreteOutput;
} // namespace device

namespace cmd::meta {

/*!
 * @brief re-sets current DiagSensor value to simulate loading from cache,
 * without actually requesting sensor state from device
 */
class MOTHERBOARD_COMMAND_EXPORT ReportCachedButton final
  : public AbstractMetaOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(ReportCachedButton)
public:
  ReportCachedButton(gsl::not_null<device::DiscreteOutput*> button,
                     QObject* parent = nullptr);
  ~ReportCachedButton() override;

  [[nodiscard]] auto description() const -> QString override;
  void start() override;

private:
  gsl::not_null<device::DiscreteOutput*> _button;
};

} // namespace cmd::meta

#endif // BUTTON_REPORT_CACHED_H
