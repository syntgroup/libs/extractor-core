#ifndef CMD_MOTHERBOARD_ENGINEGETACCELERATION_H
#define CMD_MOTHERBOARD_ENGINEGETACCELERATION_H

#include "abstract_motherboard_operation.h"
#include "motherboard/device/engine_speed.h"
#include "motherboard/device/engine_values.h"

namespace device {
class Engine;
} // namespace device

namespace protocol::motherboard {
struct EnginesGetFResponse;
} // namespace protocol::motherboard

namespace cmd::motherboard {

class MOTHERBOARD_COMMAND_EXPORT EngineGetAccelerationOperation final
  : public AbstractMotherboardOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(EngineGetAccelerationOperation)

public:
  using TResponse = protocol::motherboard::EnginesGetFResponse;

  EngineGetAccelerationOperation(protocol::PacketID pid,
                                 gsl::not_null<device::Engine*> engine,
                                 device::AccelerationCurve curve,
                                 device::FrequencyType mode,
                                 QObject* parent = nullptr);
  ~EngineGetAccelerationOperation() override = default;

  // AbstractOperation interface
  [[nodiscard]] auto description() const -> QString override;
  [[nodiscard]] auto responseType() const -> int override;
  [[nodiscard]] auto createRequest() const -> Request override;

protected:
  [[nodiscard]] auto processResponse(const Response& t_response)
    -> bool override;

private:
  gsl::not_null<device::Engine*> m_engine;
  device::FrequencyType m_mode;
  device::AccelerationCurve m_curve;
};

} // namespace cmd::motherboard

#endif // CMD_MOTHERBOARD_ENGINEGETACCELERATION_H
