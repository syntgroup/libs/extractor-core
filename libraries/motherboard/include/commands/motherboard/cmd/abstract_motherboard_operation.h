#ifndef CMD_ABSTRACTMOTHERBOARDOPERATION_H
#define CMD_ABSTRACTMOTHERBOARDOPERATION_H

#include "motherboard/command/export.h"
#include "serial/cmd/abstract_serial_operation.h"
#include <gsl/gsl-lite.hpp>

namespace protocol::motherboard {
struct MotherboardResponse;
} // namespace protocol::motherboard

namespace cmd::motherboard {

class MOTHERBOARD_COMMAND_EXPORT AbstractMotherboardOperation
  : public AbstractSerialOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(AbstractMotherboardOperation)
public:
  using TResponse = protocol::motherboard::MotherboardResponse;

  using AbstractSerialOperation::AbstractSerialOperation;
  ~AbstractMotherboardOperation() override = default;

  [[nodiscard]] auto operationType() const
    -> cmd::AbstractOperation::OperationType final
  {
    return OperationType::Motherboard;
  }
};

} // namespace cmd::motherboard

#endif // CMD_ABSTRACTMOTHERBOARDOPERATION_H
