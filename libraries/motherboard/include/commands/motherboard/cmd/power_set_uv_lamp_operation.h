#ifndef CMD_MOTHERBOARD_POWERSETUVLAMPOPERATION_H
#define CMD_MOTHERBOARD_POWERSETUVLAMPOPERATION_H

#include "abstract_motherboard_operation.h"
#include "common/analog_input.h"

namespace device {
class DiscreteInput;
} // namespace device

namespace protocol::motherboard {
struct PowerSetUVLampRequest;
} // namespace protocol::motherboard

namespace cmd::motherboard {

class MOTHERBOARD_COMMAND_EXPORT PowerSetUvLampOperation final
  : public AbstractMotherboardOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(PowerSetUvLampOperation)
public:
  using TRequest = protocol::motherboard::PowerSetUVLampRequest;
  using TDevice  = device::DiscreteInput;
  explicit PowerSetUvLampOperation(protocol::PacketID pid,
                                   gsl::not_null<TDevice*> lamp,
                                   bool enable,
                                   QObject* parent = nullptr);
  ~PowerSetUvLampOperation() override = default;

  [[nodiscard]] auto description() const -> QString override;
  [[nodiscard]] auto responseType() const -> int override;
  [[nodiscard]] auto createRequest() const -> Request override;

protected:
  [[nodiscard]] auto processResponse(const Response& t_response)
    -> bool override;

private:
  gsl::not_null<TDevice*> m_lamp;
  bool m_enable{ false };
};

} // namespace cmd::motherboard

#endif // CMD_MOTHERBOARD_POWERSETUVLAMPOPERATION_H
