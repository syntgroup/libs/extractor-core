#ifndef CMD_MOTHERBOARD_ENGINEGETCURRENTSOPERATION_H
#define CMD_MOTHERBOARD_ENGINEGETCURRENTSOPERATION_H

#include "abstract_motherboard_operation.h"

namespace device {
class Engine;
} // namespace device

namespace protocol::motherboard {
struct EnginesCurrentsGetRequest;
struct EngineGetCurrentsResponse;
} // namespace protocol::motherboard

namespace cmd::motherboard {

class MOTHERBOARD_COMMAND_EXPORT EngineGetCurrentsOperation final
  : public AbstractMotherboardOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(EngineGetCurrentsOperation)
public:
  using TRequest  = protocol::motherboard::EnginesCurrentsGetRequest;
  using TResponse = protocol::motherboard::EngineGetCurrentsResponse;

  explicit EngineGetCurrentsOperation(protocol::PacketID pid,
                                      gsl::not_null<device::Engine*> engine,
                                      QObject* parent = nullptr);
  ~EngineGetCurrentsOperation() override = default;

  [[nodiscard]] auto description() const -> QString override;
  [[nodiscard]] auto responseType() const -> int override;
  [[nodiscard]] auto createRequest() const -> Request override;

protected:
  [[nodiscard]] auto processResponse(const Response& t_response)
    -> bool override;

private:
  gsl::not_null<device::Engine*> m_engine;
};

} // namespace cmd::motherboard

#endif // CMD_MOTHERBOARD_ENGINEGETCURRENTSOPERATION_H
