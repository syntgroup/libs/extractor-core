#ifndef CMD_MOTHERBOARD_GOTOMIXCYCLESOPERATION_H
#define CMD_MOTHERBOARD_GOTOMIXCYCLESOPERATION_H

#include "abstract_motherboard_operation.h"
#include "motherboard/device/engine_values.h"

namespace device {
class Engine;
} // namespace device

namespace protocol::motherboard {
struct GotoMixCyclesRequest;
struct GotoResponse;
} // namespace protocol::motherboard

namespace cmd::motherboard {

class MOTHERBOARD_COMMAND_EXPORT GotoMixCyclesOperation final
  : public AbstractMotherboardOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(GotoMixCyclesOperation)
public:
  using TRequest  = protocol::motherboard::GotoMixCyclesRequest;
  using TResponse = protocol::motherboard::GotoResponse;

  explicit GotoMixCyclesOperation(protocol::PacketID pid,
                                  gsl::not_null<device::Engine*> engine,
                                  device::Cycles cycles,
                                  device::Coordinate amplitude,
                                  QObject* parent = nullptr);
  ~GotoMixCyclesOperation() override = default;

  [[nodiscard]] auto description() const -> QString override;
  [[nodiscard]] auto responseType() const -> int override;
  [[nodiscard]] auto createRequest() const -> Request override;
  [[nodiscard]] auto begin() -> bool override;

protected:
  [[nodiscard]] auto processResponse(const Response& t_response)
    -> bool override;

private:
  gsl::not_null<device::Engine*> m_engine;
  device::Cycles m_cycles{ 0 };
  device::Coordinate m_amplitude{ 0 };
};

} // namespace cmd::motherboard

#endif // CMD_MOTHERBOARD_GOTOMIXCYCLESOPERATION_H
