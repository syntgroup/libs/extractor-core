#ifndef CMD_MOTHERBOARD_GOTOABSOLUTE_OPERATION_H
#define CMD_MOTHERBOARD_GOTOABSOLUTE_OPERATION_H

#include "motherboard/command/export.h"
#include "motherboard/device/engine_speed.h"
#include "motherboard/device/engine_values.h"
#include "serial/cmd/abstract_multi_stage_operation.h"
#include <gsl/gsl-lite.hpp>

namespace device {
class Engine;
} // namespace device

namespace cmd::multi {

class MOTHERBOARD_COMMAND_EXPORT GotoAbsoluteOperation final
  : public cmd::AbstractMultiStageOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(GotoAbsoluteOperation)

public:
  enum OperationState
  {
    WriteFrequencyStart = AbstractOperation::User,
    WriteFrequencyStop,
    WriteFrequencyStep,
    CheckingMaxSpeed,
    WriteTravelSpeed,
    ExecGotoAbsolutePos,
    CheckGotoAbsolutePos,
    SmoothStop    = 0xaa,
    EmergencyStop = 0xab,
    Idle          = 0xf0,
    ZeroingError  = 0xff
  };
  Q_ENUM(OperationState)

  GotoAbsoluteOperation(
    gsl::not_null<device::Engine*> engine,
    device::Coordinate target_pos,
    device::SpeedIndex new_speed,
    device::AccelerationCurve curve = device::AccelerationCurve::Normal,
    device::SpeedMode legacy_mode   = device::SpeedMode::Modern,
    QObject* parent                 = nullptr);
  ~GotoAbsoluteOperation() override = default;

  void start() override;
  [[nodiscard]] auto description() const -> QString override;

private:
  gsl::not_null<device::Engine*> _engine;

  void execGotoAbsolutePos();
  void checkGotoAbsolutePos();
  void resetLastError();
  void writeMaxSpeed(device::SpeedIndex value);
  void checkMaxSpeed();
  void smoothStop();
  void emergencyStop();

  void execWriteAcceleration(device::Acceleration frequency);

  const device::Coordinate _target_position{ 0 };
  const device::SpeedIndex _travel_speed{ 0 };
  const device::AccelerationCurve _curve{ device::AccelerationCurve::Normal };

  device::SpeedMode _legacyMode{ false };

  OperationState _buffer{ OperationState::ZeroingError };

  // AbstractMultiStageOperation interface
protected:
  void onSubOperationError(const AbstractSerialOperation& operation) override;

public slots:
  void nextStateLogic() override;
  void pause() override;
  void resume() override;
};

} // namespace cmd::multi

#endif
