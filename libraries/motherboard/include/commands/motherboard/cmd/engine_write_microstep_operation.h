#ifndef CMD_MOTHERBOARD_ENGINEWRITEMICROSTEPOPERATION_H
#define CMD_MOTHERBOARD_ENGINEWRITEMICROSTEPOPERATION_H

#include "abstract_motherboard_operation.h"
#include "motherboard/device/engine_values.h"

namespace device {
class Engine;
} // namespace device

namespace protocol::motherboard {
struct EngineWriteMicrostep;
struct EnginesSetParamResponse;
} // namespace protocol::motherboard

namespace cmd::motherboard {

class MOTHERBOARD_COMMAND_EXPORT EngineWriteMicrostepOperation final
  : public AbstractMotherboardOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(EngineWriteMicrostepOperation)
public:
  using TRequest  = protocol::motherboard::EngineWriteMicrostep;
  using TResponse = protocol::motherboard::EnginesSetParamResponse;

  explicit EngineWriteMicrostepOperation(protocol::PacketID pid,
                                         gsl::not_null<device::Engine*> engine,
                                         device::Microstep new_value,
                                         QObject* parent = nullptr);
  ~EngineWriteMicrostepOperation() override = default;

  [[nodiscard]] auto description() const -> QString override;
  [[nodiscard]] auto responseType() const -> int override;
  [[nodiscard]] auto createRequest() const -> Request override;

protected:
  [[nodiscard]] auto processResponse(const Response& t_response)
    -> bool override;

private:
  gsl::not_null<device::Engine*> m_engine;
  device::Microstep m_newMicrostep;
};

} // namespace cmd::motherboard

#endif // CMD_MOTHERBOARD_ENGINEWRITEMICROSTEPOPERATION_H
