#ifndef CMD_MOTHERBOARD_GOTOABSOLUTEPOSOPERATION_H
#define CMD_MOTHERBOARD_GOTOABSOLUTEPOSOPERATION_H

#include "abstract_motherboard_operation.h"
#include "motherboard/device/engine_values.h"

namespace device {
class Engine;
} // namespace device

namespace protocol::motherboard {
struct GotoAbsPosRequest;
struct GotoResponse;
} // namespace protocol::motherboard

namespace cmd::motherboard {

class MOTHERBOARD_COMMAND_EXPORT GotoAbsolutePosOperation final
  : public AbstractMotherboardOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(GotoAbsolutePosOperation)
public:
  using Target    = std::pair<device::Engine*, device::Coordinate>;
  using Params    = std::vector<Target>;
  using TRequest  = protocol::motherboard::GotoAbsPosRequest;
  using TResponse = protocol::motherboard::GotoResponse;

  explicit GotoAbsolutePosOperation(protocol::PacketID id,
                                    Params&& params,
                                    QObject* parent = nullptr);
  ~GotoAbsolutePosOperation() override = default;

  [[nodiscard]] auto begin() -> bool override;
  [[nodiscard]] auto description() const -> QString override;
  [[nodiscard]] auto responseType() const -> int override;
  [[nodiscard]] auto createRequest() const -> Request override;

protected:
  [[nodiscard]] auto processResponse(const Response& t_response)
    -> bool override;

private:
  const Params m_params;
};

} // namespace cmd::motherboard

#endif // CMD_MOTHERBOARD_GOTOABSOLUTEPOSOPERATION_H
