#ifndef CMD_MOTHERBOARD_DEVICEGETFWOPERATION_H
#define CMD_MOTHERBOARD_DEVICEGETFWOPERATION_H

#include "abstract_motherboard_operation.h"

namespace device {
class MotherboardParameters;
} // namespace device

namespace protocol::motherboard {
struct DeviceRequestFwRev;
struct DeviceGetFWResponse;
} // namespace protocol::motherboard

namespace cmd::motherboard {

class MOTHERBOARD_COMMAND_EXPORT DeviceGetFWOperation final
  : public AbstractMotherboardOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(DeviceGetFWOperation)
public:
  using TRequest  = protocol::motherboard::DeviceRequestFwRev;
  using TResponse = protocol::motherboard::DeviceGetFWResponse;

  explicit DeviceGetFWOperation(
    protocol::PacketID cmd_idx,
    gsl::not_null<device::MotherboardParameters*> motherboard_parameters,
    QObject* parent = nullptr);
  ~DeviceGetFWOperation() override = default;

  // AbstractOperation interface
  [[nodiscard]] QString description() const override;
  [[nodiscard]] int responseType() const override;
  [[nodiscard]] Request createRequest() const override;

protected:
  bool processResponse(const Response& t_response) override;

private:
  gsl::not_null<device::MotherboardParameters*> m_motherboardParameters;
};

} // namespace cmd::motherboard

#endif // CMD_MOTHERBOARD_DEVICEGETFWOPERATION_H
