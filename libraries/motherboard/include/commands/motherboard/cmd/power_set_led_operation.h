#ifndef CMD_MOTHERBOARD_POWERSETLEDOPERATION_H
#define CMD_MOTHERBOARD_POWERSETLEDOPERATION_H

#include "abstract_motherboard_operation.h"
#include "common/analog_input.h"
#include "motherboard/device/motherboard_values.h"

namespace device {
class LedLamp;
} // namespace device

namespace protocol::motherboard {
struct PowerSetLedsRequest;
} // namespace protocol::motherboard

namespace cmd::motherboard {

class MOTHERBOARD_COMMAND_EXPORT PowerSetLedOperation final
  : public AbstractMotherboardOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(PowerSetLedOperation)
public:
  using TRequest = protocol::motherboard::PowerSetLedsRequest;
  using TDevice  = device::AnalogInput<device::PwmLevel>;

  explicit PowerSetLedOperation(protocol::PacketID pid,
                                gsl::not_null<TDevice*> lamp,
                                device::PwmLevel pwm,
                                QObject* parent = nullptr);
  ~PowerSetLedOperation() override = default;

  [[nodiscard]] auto description() const -> QString override;
  [[nodiscard]] auto responseType() const -> int override;
  [[nodiscard]] auto createRequest() const -> Request override;

protected:
  [[nodiscard]] auto processResponse(const Response& t_response)
    -> bool override;

private:
  gsl::not_null<TDevice*> m_lamp;
  device::PwmLevel m_pwm{ 0 };
};

} // namespace cmd::motherboard

#endif // CMD_MOTHERBOARD_POWERSETLEDOPERATION_H
