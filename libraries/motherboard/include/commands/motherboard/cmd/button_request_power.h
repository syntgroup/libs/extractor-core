#ifndef CMD_MOTHERBOARD_BUTTONSREQUESTPOWERBUTTON_H
#define CMD_MOTHERBOARD_BUTTONSREQUESTPOWERBUTTON_H

#include "abstract_motherboard_operation.h"

namespace device {
class DiscreteOutput;
} // namespace device

namespace protocol::motherboard {
struct PowerButtonGetState;
struct ButtonsResponse;
} // namespace protocol::motherboard

namespace cmd::motherboard {

class MOTHERBOARD_COMMAND_EXPORT ButtonsRequestPowerButton final
  : public AbstractMotherboardOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(ButtonsRequestPowerButton)

public:
  using TRequest  = protocol::motherboard::PowerButtonGetState;
  using TResponse = protocol::motherboard::ButtonsResponse;

  explicit ButtonsRequestPowerButton(
    protocol::PacketID pid,
    gsl::not_null<device::DiscreteOutput*> button,
    QObject* parent = nullptr);
  ~ButtonsRequestPowerButton() override = default;

  [[nodiscard]] auto description() const -> QString override;
  [[nodiscard]] auto responseType() const -> int override;
  [[nodiscard]] auto createRequest() const -> Request override;

protected:
  [[nodiscard]] auto processResponse(const Response& t_response)
    -> bool override;

private:
  gsl::not_null<device::DiscreteOutput*> m_powerButton;
};

} // namespace cmd::motherboard

#endif // CMD_MOTHERBOARD_BUTTONSREQUESTPOWERBUTTON_H
