#ifndef CMD_MOTHERBOARD_POWERSETBUZZEROPERATION_H
#define CMD_MOTHERBOARD_POWERSETBUZZEROPERATION_H

#include "abstract_motherboard_operation.h"

namespace protocol::motherboard {
struct PowerSetBuzzerRequest;
} // namespace protocol::motherboard

namespace cmd::motherboard {

class MOTHERBOARD_COMMAND_EXPORT PowerSetBuzzerOperation final
  : public AbstractMotherboardOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(PowerSetBuzzerOperation)
public:
  using TRequest = protocol::motherboard::PowerSetBuzzerRequest;

  explicit PowerSetBuzzerOperation(protocol::PacketID pid,
                                   bool enable,
                                   QObject* parent = nullptr);
  ~PowerSetBuzzerOperation() override = default;

  [[nodiscard]] auto description() const -> QString override;
  [[nodiscard]] auto responseType() const -> int override;
  [[nodiscard]] auto createRequest() const -> Request override;

private:
  bool _enable = false;
};

} // namespace cmd::motherboard

#endif // CMD_MOTHERBOARD_POWERSETBUZZEROPERATION_H
