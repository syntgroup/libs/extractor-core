#ifndef CMD_MOTHERBOARD_BUTTONSREQUESTDOOR_H
#define CMD_MOTHERBOARD_BUTTONSREQUESTDOOR_H

#include "abstract_motherboard_operation.h"

namespace device {
class DiscreteOutput;
} // namespace device

namespace protocol::motherboard {
struct GerconGetStateRequest;
struct ButtonsResponse;
} // namespace protocol::motherboard

namespace cmd::motherboard {

class MOTHERBOARD_COMMAND_EXPORT ButtonsRequestDoor final
  : public AbstractMotherboardOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(ButtonsRequestDoor)

public:
  using TRequest  = protocol::motherboard::GerconGetStateRequest;
  using TResponse = protocol::motherboard::ButtonsResponse;

  explicit ButtonsRequestDoor(protocol::PacketID pid,
                              device::DiscreteOutput* buttons,
                              QObject* parent = nullptr);
  ~ButtonsRequestDoor() override = default;

  [[nodiscard]] auto description() const -> QString override;
  [[nodiscard]] auto responseType() const -> int override;
  [[nodiscard]] auto createRequest() const -> Request override;

protected:
  [[nodiscard]] auto processResponse(const Response& t_response)
    -> bool override;

private:
  device::DiscreteOutput* m_doorButton;
};

} // namespace cmd::motherboard

#endif // CMD_MOTHERBOARD_BUTTONSREQUESTDOOR_H
