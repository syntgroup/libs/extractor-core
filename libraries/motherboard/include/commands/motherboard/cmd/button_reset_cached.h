#ifndef BUTTON_RESET_CACHED_H
#define BUTTON_RESET_CACHED_H

#include "common/abstract_meta_operation.h"
#include "motherboard/command/export.h"
#include <gsl/gsl-lite.hpp>
#include <optional>

namespace device {
class DiscreteOutput;
} // namespace device

namespace cmd::meta {

class MOTHERBOARD_COMMAND_EXPORT ResetCachedButton final
  : public AbstractMetaOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(ResetCachedButton)
public:
  ResetCachedButton(gsl::not_null<device::DiscreteOutput*> button,
                    std::optional<bool> modifier = std::nullopt,
                    QObject* parent              = nullptr);
  ~ResetCachedButton() override;

  [[nodiscard]] auto description() const -> QString override;
  void start() override;

private:
  gsl::not_null<device::DiscreteOutput*> _button;
  std::optional<bool> _modifier{ std::nullopt };
};

} // namespace cmd::meta

#endif // BUTTON_RESET_CACHED_H
