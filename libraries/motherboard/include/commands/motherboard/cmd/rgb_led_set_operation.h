#ifndef CMD_EXTRACTOR_RGBLEDSETOPERATION_H
#define CMD_EXTRACTOR_RGBLEDSETOPERATION_H

#include "abstract_motherboard_operation.h"
#include "motherboard/device/motherboard_values.h"

namespace device {
class RgbLeds;
} // namespace device

namespace protocol::motherboard {
struct RgbSetOnOffRequest;
} // namespace protocol::motherboard

namespace cmd::motherboard {
class MOTHERBOARD_COMMAND_EXPORT RgbLedSetOperation final
  : public AbstractMotherboardOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(RgbLedSetOperation)
public:
  using TRequest = protocol::motherboard::RgbSetOnOffRequest;

  explicit RgbLedSetOperation(protocol::PacketID pid,
                              gsl::not_null<device::RgbLeds*> leds,
                              std::vector<Qt::GlobalColor>&& colors,
                              QObject* parent = nullptr);
  ~RgbLedSetOperation() override = default;

  [[nodiscard]] auto description() const -> QString override;
  [[nodiscard]] auto responseType() const -> int override;
  [[nodiscard]] auto createRequest() const -> Request override;

  [[nodiscard]] auto colors() const { return m_colors; }

protected:
  [[nodiscard]] auto processResponse(const Response& response) -> bool override;

private:
  gsl::not_null<device::RgbLeds*> m_leds;
  std::vector<Qt::GlobalColor> m_colors;
};

} // namespace cmd::motherboard

#endif // CMD_EXTRACTOR_RGBLEDSETOPERATION_H
