#ifndef CMD_MOTHERBOARD_SYSTEMSOFTREBOOTOPERATION_H
#define CMD_MOTHERBOARD_SYSTEMSOFTREBOOTOPERATION_H

#include "abstract_motherboard_operation.h"

namespace protocol::motherboard {
struct SystemSoftRebootRequest;
} // namespace protocol::motherboard

namespace cmd::motherboard {

class MOTHERBOARD_COMMAND_EXPORT SystemSoftRebootOperation final
  : public AbstractMotherboardOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(SystemSoftRebootOperation)
public:
  using TRequest = protocol::motherboard::SystemSoftRebootRequest;
  using AbstractMotherboardOperation::AbstractMotherboardOperation;
  ~SystemSoftRebootOperation() override = default;

  [[nodiscard]] auto description() const -> QString override;
  [[nodiscard]] auto responseType() const -> int override;
  [[nodiscard]] auto createRequest() const -> Request override;
};

} // namespace cmd::motherboard

#endif // CMD_MOTHERBOARD_SYSTEMSOFTREBOOTOPERATION_H
