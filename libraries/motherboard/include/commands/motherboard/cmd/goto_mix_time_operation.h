#ifndef EXTRACTOR_TOUCHPANEL_GOTO_MIX_TIME_OPERATION_H
#define EXTRACTOR_TOUCHPANEL_GOTO_MIX_TIME_OPERATION_H

#include "abstract_motherboard_operation.h"
#include "motherboard/device/engine_values.h"

namespace device {
class Engine;
} // namespace device

namespace protocol::motherboard {
struct GotoMixTimeRequest;
struct GotoResponse;
} // namespace protocol::motherboard

namespace cmd::motherboard {

class MOTHERBOARD_COMMAND_EXPORT GotoMixTimeOperation final
  : public AbstractMotherboardOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(GotoMixTimeOperation)
public:
  using TRequest  = protocol::motherboard::GotoMixTimeRequest;
  using TResponse = protocol::motherboard::GotoResponse;

  explicit GotoMixTimeOperation(protocol::PacketID pid,
                                gsl::not_null<device::Engine*> engine,
                                std::chrono::seconds seconds,
                                device::Coordinate amplitude,
                                QObject* parent = nullptr);
  ~GotoMixTimeOperation() override = default;

  [[nodiscard]] auto description() const -> QString override;
  [[nodiscard]] auto responseType() const -> int override;
  [[nodiscard]] auto createRequest() const -> Request override;
  [[nodiscard]] auto begin() -> bool override;

private:
  gsl::not_null<device::Engine*> m_engine;
  std::chrono::seconds m_seconds{ 0 };
  device::Coordinate m_amplitude{ 0 };
};

} // namespace cmd::motherboard

#endif // EXTRACTOR_TOUCHPANEL_GOTO_MIX_TIME_OPERATION_H
