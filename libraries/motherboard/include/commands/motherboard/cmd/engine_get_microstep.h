#ifndef CMD_MOTHERBOARD_ENGINEGETMICROSTEP_H
#define CMD_MOTHERBOARD_ENGINEGETMICROSTEP_H

#include "abstract_motherboard_operation.h"

namespace device {
class Engine;
} // namespace device

namespace protocol::motherboard {
struct EngineRequestMicrostep;
struct EnginesGetMicrostepResponse;
} // namespace protocol::motherboard

namespace cmd::motherboard {

class MOTHERBOARD_COMMAND_EXPORT EngineGetMicrostepOperation final
  : public AbstractMotherboardOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(EngineGetMicrostepOperation)
public:
  using TRequest  = protocol::motherboard::EngineRequestMicrostep;
  using TResponse = protocol::motherboard::EnginesGetMicrostepResponse;

  explicit EngineGetMicrostepOperation(protocol::PacketID pid,
                                       gsl::not_null<device::Engine*> engine,
                                       QObject* parent = nullptr);
  ~EngineGetMicrostepOperation() override = default;

  // AbstractOperation interface
  [[nodiscard]] QString description() const override;
  [[nodiscard]] int responseType() const override;

  // AbstractMotherboardOperation interface
  [[nodiscard]] Request createRequest() const override;

private:
  [[nodiscard]] bool processResponse(const Response& t_response) override;

  gsl::not_null<device::Engine*> m_engine;
};

} // namespace cmd::motherboard

#endif // CMD_MOTHERBOARD_ENGINEGETMICROSTEP_H
