#ifndef CMD_MOTHERBOARD_GOTONTICKSOPERATION_H
#define CMD_MOTHERBOARD_GOTONTICKSOPERATION_H

#include "abstract_motherboard_operation.h"
#include "motherboard/device/engine_values.h"

namespace device {
class Engine;
} // namespace device

namespace protocol::motherboard {
struct GotoNTicksRequest;
struct GotoResponse;
} // namespace protocol::motherboard

namespace cmd::motherboard {

class MOTHERBOARD_COMMAND_EXPORT GotoNTicksOperation final
  : public AbstractMotherboardOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(GotoNTicksOperation)
public:
  using TRequest  = protocol::motherboard::GotoNTicksRequest;
  using TResponse = protocol::motherboard::GotoResponse;

  explicit GotoNTicksOperation(protocol::PacketID pid,
                               gsl::not_null<device::Engine*> engine,
                               device::Coordinate ticks,
                               QObject* parent = nullptr);
  ~GotoNTicksOperation() override = default;

  [[nodiscard]] auto description() const -> QString override;
  [[nodiscard]] auto responseType() const -> int override;
  [[nodiscard]] auto createRequest() const -> Request override;

protected:
  [[nodiscard]] auto processResponse(const Response& t_response)
    -> bool override;

private:
  gsl::not_null<device::Engine*> m_engine;
  device::Coordinate m_ticks{ 0 };
};

} // namespace cmd::motherboard

#endif // CMD_MOTHERBOARD_GOTONTICKSOPERATION_H
