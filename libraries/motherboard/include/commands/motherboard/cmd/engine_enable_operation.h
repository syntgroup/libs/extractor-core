#ifndef CMD_MOTHERBOARD_ENGINEENABLEOPERATION_H
#define CMD_MOTHERBOARD_ENGINEENABLEOPERATION_H

#include "abstract_motherboard_operation.h"

namespace device {
class Engine;
} // namespace device

namespace protocol::motherboard {
struct EngineEnableRequest;
struct EnginesSetParamResponse;
} // namespace protocol::motherboard

namespace cmd::motherboard {

class MOTHERBOARD_COMMAND_EXPORT EngineEnableOperation final
  : public AbstractMotherboardOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(EngineEnableOperation)

public:
  using TRequest  = protocol::motherboard::EngineEnableRequest;
  using TResponse = protocol::motherboard::EnginesSetParamResponse;

  explicit EngineEnableOperation(protocol::PacketID cmd_idx,
                                 gsl::not_null<device::Engine*> engine,
                                 bool enable,
                                 QObject* parent = nullptr);
  ~EngineEnableOperation() override = default;

  // AbstractOperation interface
  [[nodiscard]] auto description() const -> QString override;
  [[nodiscard]] auto responseType() const -> int override;
  [[nodiscard]] auto createRequest() const -> Request override;

protected:
  [[nodiscard]] auto processResponse(const Response& t_response)
    -> bool override;

private:
  gsl::not_null<device::Engine*> m_engine;
  bool m_enable{ false };
};

} // namespace cmd::motherboard

#endif // CMD_MOTHERBOARD_ENGINEENABLEOPERATION_H
