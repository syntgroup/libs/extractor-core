#ifndef CMD_MOTHERBOARD_ENGINEWRITEACCELERATIONOPERATION_H
#define CMD_MOTHERBOARD_ENGINEWRITEACCELERATIONOPERATION_H

#include "abstract_motherboard_operation.h"
#include "motherboard/device/engine_values.h"

namespace device {
class Engine;
} // namespace device

namespace protocol::motherboard {
struct EnginesSetParamResponse;
} // namespace protocol::motherboard

namespace cmd::motherboard {

class MOTHERBOARD_COMMAND_EXPORT EngineWriteAccelerationOperation final
  : public AbstractMotherboardOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(EngineWriteAccelerationOperation)

public:
  using TResponse = protocol::motherboard::EnginesSetParamResponse;

  EngineWriteAccelerationOperation(protocol::PacketID pid,
                                   gsl::not_null<device::Engine*> engine,
                                   device::Acceleration value,
                                   QObject* parent = nullptr);
  ~EngineWriteAccelerationOperation() override = default;

  [[nodiscard]] auto description() const -> QString override;
  [[nodiscard]] auto responseType() const -> int override;
  [[nodiscard]] auto createRequest() const -> Request override;

protected:
  [[nodiscard]] auto processResponse(const Response& t_response)
    -> bool override;

private:
  gsl::not_null<device::Engine*> _engine;
  device::Acceleration _newValue;
};

} // namespace cmd::motherboard

#endif // CMD_MOTHERBOARD_ENGINEWRITEACCELERATIONOPERATION_H
