#ifndef CMD_MOTHERBOARD_POWERSETWDGVALUE_H
#define CMD_MOTHERBOARD_POWERSETWDGVALUE_H

#include "abstract_motherboard_operation.h"

namespace protocol::motherboard {
struct PowerSetWdtTimeout;
} // namespace protocol::motherboard

namespace cmd::motherboard {

class MOTHERBOARD_COMMAND_EXPORT PowerSetWdgValue final
  : public AbstractMotherboardOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(PowerSetWdgValue)

public:
  using TRequset = protocol::motherboard::PowerSetWdtTimeout;

  explicit PowerSetWdgValue(protocol::PacketID pid,
                            uint32_t timeout,
                            QObject* parent = nullptr);
  ~PowerSetWdgValue() override = default;

  [[nodiscard]] auto description() const -> QString override;
  [[nodiscard]] auto responseType() const -> int override;
  [[nodiscard]] auto createRequest() const -> Request override;

private:
  uint32_t m_timeout{ 0 };
};

} // namespace cmd::motherboard

#endif // CMD_MOTHERBOARD_POWERSETWDGVALUE_H
