#ifndef PROTOCOLS_MOTHERBOARD_MOTHERBOARDPLUGIN_H
#define PROTOCOLS_MOTHERBOARD_MOTHERBOARDPLUGIN_H

#include "motherboard/command/export.h"
#include "serial/protocol/encoder_interface.h"

namespace protocol::srvint {
struct SrvIntRequestFactory;
} // namespace protocol::srvint

namespace protocol::motherboard {

struct MOTHERBOARD_COMMAND_EXPORT MotherboardEncoder : EncoderInterface
{
  MotherboardEncoder();
  ~MotherboardEncoder() override;

  MotherboardEncoder(const MotherboardEncoder&)                        = delete;
  MotherboardEncoder(MotherboardEncoder&&) noexcept                    = delete;
  auto operator=(const MotherboardEncoder&) -> MotherboardEncoder&     = delete;
  auto operator=(MotherboardEncoder&&) noexcept -> MotherboardEncoder& = delete;

  [[nodiscard]] auto encode(const Request& request, int type) const
    -> QByteArray override;

private:
  const std::unique_ptr<protocol::srvint::SrvIntRequestFactory> m_factory{
    nullptr
  };
};

} // namespace protocol::motherboard

#endif // PROTOCOLS_MOTHERBOARD_MOTHERBOARDPLUGIN_H
