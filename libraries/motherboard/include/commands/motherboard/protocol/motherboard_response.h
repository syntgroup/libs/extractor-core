#ifndef CMD_MOTHERBOARD_MOTHERBOARDRESPONSE_H
#define CMD_MOTHERBOARD_MOTHERBOARDRESPONSE_H

#include "motherboard/command/export.h"

#include "motherboard/protocol/motherboard_error.h"
#include "serial/protocol/response_interface.h"
#include <memory>

namespace protocol::srvint {
struct SrvIntResponseFactory;
class SrvIntPacket;
} // namespace protocol::srvint

namespace protocol::motherboard {

enum MOTHERBOARD_COMMAND_EXPORT ResponseType : int;

struct MOTHERBOARD_COMMAND_EXPORT MotherboardResponse
  : protocol::ResponseInterface
{
  using SrvIntPacket = std::unique_ptr<srvint::SrvIntPacket>;

  explicit MotherboardResponse(SrvIntPacket& packet);
  ~MotherboardResponse() override;

  // ResponseInterface interface
  [[nodiscard]] auto id() const -> PacketID override;
  [[nodiscard]] auto encodedSize() const -> size_t override;
  [[nodiscard]] auto hasNext() const -> bool override;
  [[nodiscard]] auto isError() const -> bool override;
  [[nodiscard]] auto errorString() const -> QString override;

  [[nodiscard]] auto& getPacket() const { return m_packet; }
  [[nodiscard]] auto errorCode() const { return m_errorCode; }

  static auto minimumSize(ResponseType type) -> int;

private:
  [[nodiscard]] auto detectError() const -> MotherboardError::Error;

  std::unique_ptr<srvint::SrvIntPacket> m_packet{ nullptr };
  MotherboardError::Error m_errorCode{ MotherboardError::NoError };
};

} // namespace protocol::motherboard

#endif // CMD_MOTHERBOARD_MOTHERBOARDRESPONSE_H
