#ifndef PROTOCOL_MOTHERBOARDERROR_H
#define PROTOCOL_MOTHERBOARDERROR_H

#include "motherboard/command/export.h"

#include <QMetaEnum>

namespace protocol::motherboard {

struct MOTHERBOARD_COMMAND_EXPORT MotherboardError
{
#ifdef error_32bit_enabled
  enum class ERROR32 : uint32_t
  {
    NO_ERROR                                                   = 0x00000000,
    INCORRECT_POSITION                                         = 0xFFFF0001,
    INCORRECT_SMM_NUMBER                                       = 0x000800A1,
    INCORRECT_DATA_FOR_WRITE_TO_TMC2660                        = 0x000800A2,
    CANNOT_WRITE_INTO_TMC2660_WHEN_WRITING_NOT_COMPLETE        = 0x000800A3,
    INCORRECT_ENGINE_NUMBER                                    = 0x000800A4,
    ENGINE_NOT_FREE_TIMERS_NOW                                 = 0x000800A5,
    ENGINE_CAN_NOT_FIND_ZERO_SWITCH_1                          = 0x00080001,
    ENGINE_CAN_NOT_FIND_ZERO_SWITCH_2                          = 0x00080003,
    ENGINE_CAN_NOT_MOVE_WHEN_UNDEFINED_POSITION                = 0x00080005,
    ENGINE_CAN_NOT_MOVE_WHEN_MOVING_TO_ZERO                    = 0x00080006,
    ENGINE_CROSS_UNDEFINED_SWITCH                              = 0x00080007,
    ENGINE_CROSS_SWITCH1                                       = 0x00080008,
    ENGINE_CROSS_SWITCH2                                       = 0x00080009,
    DIFFERENCE_BETWEEN_ABSOLUTE_POSITION_AND_STEPS_IS_NEGATIVE = 0x0008000D,
    SUM_ABSOLUTE_POSITION_AND_STEPS_IS_VERY_BIG                = 0x0008000E,
    SFUNC_CAN_NOT_START_WHEN_WORKING_NOW                       = 0x00020041,
    SFUNC_CAN_NOT_START_FROM_UNDEFINED_POSITION                = 0x00020042,
    SFUNC_CAN_NOT_START_WHEN_MOVING_TO_ZERO                    = 0x00020043,
    SRVINT_RECEIVED_UNKNOWN_CMD                                = 0x00030050,
    SRVINT_EXCEEDED_MAXIMUM_TIME_LATENCY_BETWEEN_PACKET_CONTROL_BYTES =
      0x00030051,
    SRVINT_RECEIVED_HASH_NOT_EQUAL_CALCULATING_HASH           = 0x00030052,
    SRVINT_CAN_NOT_RECIEVE_OPERANDS_MORE_THEN_BUFFER_CAPACITY = 0x00030053,
    SRVINT_EXCEEDED_MAXIMUM_TIME_LATENCY_BETWEEN_PACKET_OPERANDS_BYTES =
      0x00030054,
    SRVINT_RECEIVED_DATA_HASH_NOT_EQUAL_CALCULATING_HASH       = 0x00030055,
    SRVINT_RECEIVED_PACKET_WITH_ZERO_MSB_BIT_IN_PACKET_ID      = 0x00030056,
    SRVINT_SAME_CMD_WITH_SAME_PACKETID_CAN_NOT_GO_SUCCESSIVELY = 0x00030057,
    SRVINT_UART_INPUT_BUFFER_OVERFLOW                          = 0x00030058,
    SRVINT_UNKNOWN_OPERANDS_VALUE                              = 0x00030059,
    FRAM_WRITE_INTO_STATUS_REGISTER_FAILED                     = 0x000D0061,
    SRVINTM_INCORRECT_OPERAND_INDEX                            = 0x000E0091,
    SRVINTM_UART_INPUT_BUFFER_OVERFLOW                         = 0x000E0092,
    SRVINTM_EXCEED_MAXIMUM_TIME_LATENCY_BETWEEN_PACKET_CONTROL_BYTES =
      0x000E0093,
    SRVINTM_RECEIVED_HASH_NOT_EQUAL_CALCULATING_HASH           = 0x000E0094,
    SRVINTM_CAN_NOT_RECIEVE_OPERANDS_MORE_THEN_BUFFER_CAPACITY = 0x000E0095,
    SRVINTM_EXCEEDED_MAX_TIME_LATENCY_BETWEEN_PACKET_OPERANDS_BYTES =
      0x000E0096,
    SRVINTM_RECEIVED_DATA_HASH_NOT_EQUAL_CALCULATING_HASH       = 0x000E0097,
    SRVINTM_RECEIVED_PACKET_WITH_ZERO_MSB_BIT_IN_PACKET_ID      = 0x000E0098,
    SRVINTM_SAME_CMD_WITH_SAME_PACKETID_CAN_NOT_GO_SUCCESSIVELY = 0x000E0099,
    SRVINMT_UNKNOWN_OPERANDS_VALUE                              = 0x000E009A,
    SRVINTM_RECEIVED_UNKNOWN_CMD                                = 0x000E009B,
    VM_INCORRECT_ADCFILTER_INDEX                                = 0x000A00D0,
    VM_INCORRECT_RAIL_INDEX                                     = 0x000A00D1,
    MAIN_STORE_FAILED                                           = 0x000900F0,
    STIL_IS_DEAD                                                = 0x000900F1,
  };
#endif // error_32bit_enabled

  enum Error : uint8_t
  {
    NoError                                                    = 0x00,
    SMMSIncorrectPosition                                      = 0x01,
    SmmsIncorrectSmmNumber                                     = 0xA1,
    SmmsIncorrectDataToWriteToTMC2660                          = 0xA2,
    SmmsCannotWriteIntoTMC2660WhenWritingIsNotComplete         = 0xA3,
    SmmsIncorrectEngineNumber                                  = 0xA4,
    SmmsUndefinedInterruptRoutineTimer5                        = 0xA6,
    SmmsUndefinedInterruptRoutineTimer7                        = 0xA7,
    SmmsUndefinedInterruptRoutineTimer9                        = 0xA8,
    EngineNoFreeTimersNow                                      = 0xb0,
    EngineNoFreeTimersNow1                                     = 0xb1,
    EngineNoFreeTimersNow2                                     = 0xb2,
    EngineNoFreeTimersNow3                                     = 0xb3,
    EngineNoFreeTimersNow4                                     = 0xb4,
    EngineNoFreeTimersNow5                                     = 0xb5,
    EngineNoFreeTimersNow6                                     = 0xb6,
    EngineCanNotFindZeroSwitch1                                = 0x01,
    EngineCanNotFindZeroSwitch2                                = 0x03,
    EngineCanNotMoveWhenUndefinedPosition                      = 0x05,
    EngineCanNotMoveWhenMovingToZero                           = 0x06,
    EngineCrossUndefinedSwitch                                 = 0x07,
    EngineCrossSwitch1                                         = 0x08,
    EngineCrossSwitch2                                         = 0x09,
    EngineDifferenceBetweenAbsolutePositionAndStepsIsNegative  = 0x0D,
    EngineSumAbsolutePositionAndStepsIsVeryBig                 = 0x0E,
    SfuncCanNotStartWhenWorkingNow                             = 0x41,
    SfuncCanNotStartFromUndefinedPosition                      = 0x42,
    SfuncCanNotStartWhenMovingToZero                           = 0x43,
    SrvintRecievedUnknownCmd                                   = 0x50,
    SrvintExceededMaximumTimeLatencyBetweenPacketControlBytes  = 0x51,
    SrvintRecievedHashNotEqualCalculatedHash                   = 0x52,
    SrvintCanNotRecieveOperandsMoreThanBufferCapacity          = 0x53,
    SrvintExceededMaximumTimeLatencyBetweenPacketOperandsBytes = 0x54,
    SrvintRecievedDataHashNotEquealCalulatedHash               = 0x55,
    SrvintRecievedPacketWithZeroMbsBitInPacketId               = 0x56,
    SrvintSameCmdWithSamePacketIdCanNotGoSuccessively          = 0x57,
    SrvintUartInputBufferOverflow                              = 0x58,
    SrvintUnknownOperandsValue                                 = 0x59,
    FramWriteIntoStatusRegisterFailed                          = 0x61,
    SrvintmIncorrectOperandIndex                               = 0x91,
    SrvintmUartInputBufferOverflow                             = 0x92,
    SrvintmExceededMaximumTimeLatencyBetweenPacketControlBytes = 0x93,
    SrvintmRecievedHashNotEqualCalculatedHash                  = 0x94,
    SrvintmCanNotRecieveOperandsMoreThanBufferCapacity         = 0x95,
    SrvintmExceededMaxTimeLatencyBetweenOperandsBytes          = 0x96,
    SrvintmRecievedDataHashNotEqualCalculatingHash             = 0x97,
    SrvintmRecievedPacketWithZeroMsbBitInPacketId              = 0x98,
    SrvintmSameCmdWithSamePacketIdCanNotGoSuccessively         = 0x99,
    SrvintmUnknownOperandsValue                                = 0x9A,
    SrvintmRecievedUnknownCmd                                  = 0x9B,
    RepeatabilityCannotBeZero                                  = 0xCA,
    RepeatabilityCannotBeLessThan250                           = 0xCB,
    UnknownParameterNumber                                     = 0xCE,
    RequestedSpeedCanNotBeMoreThanMaxSpeed                     = 0xCF,
    CanNotMoveToNStepsWhenMovingNow                            = 0xD1,
    CanNotSetSpeedWhenMovingToNSteps                           = 0xD2,
    CanNotSetSpeedWhenBreakingIsOn                             = 0xD3,
    SetDataLargeCalMaxSpeed                                    = 0xD4,
    AccelCurrentCannotBeZero                                   = 0xD5,
    MainStorageFailed                                          = 0xF0,
    StillIsDead                                                = 0xF1,
  };
  Q_ENUM(Error)

private:
  Q_GADGET
};

} // namespace protocol::motherboard

#endif // PROTOCOL_MOTHERBOARDERROR_H
