#ifndef MOTHERBOARD_RESPONSE_TYPE_H
#define MOTHERBOARD_RESPONSE_TYPE_H

#include "motherboard/command/export.h"

namespace protocol::motherboard {

enum MOTHERBOARD_COMMAND_EXPORT ResponseType : int
{
  Unknown,
  Empty,

  DeviceSetId,
  DeviceGetId,
  DeviceGetFwRev,

  StatusPing,
  SystemSoftReboot,
  SystemHardReboot,
  SystemGoToBootloader,
  SystemGetError,
  SystemResetError,

  GotoCmd,
  GotoNTicks,
  GotoAbsPos,
  GotoZeroPos,
  GotoEmStop,
  GotoUncontrolled,
  GotoSpeedSet,
  GotoSmoothStop,
  GotoUncontrolledWOpts,
  GotoMixCycles,
  GotoMixModeOff,
  GotoWriteCurrents,
  GotoMixTime,

  EnginesGetMaxSpeed,
  EnginesGetRepeatability,
  EnginesGetMicrostep,
  EnginesGetHomePosition,
  EnginesGetF0,
  EnginesGetFMax,
  EnginesGetDfMax,
  EnginesGetPosition,
  EnginesGetCurrents,
  EnginesGetMixModeState,

  EnginesSetParam,
  EnginesSetMaxSpeed,
  EnginesSetRepeatability,
  EnginesSetMicrostep,
  EnginesSetHomePosition,
  EnginesSetF0,
  EnginesSetFMax,
  EnginesSetDfMax,
  EnginesSetPosition,
  EnginesEnable,
  EnginesDriverRawWrite,
  EnginesDriverFactoryDefaults,

  PowerSetCooler,
  PowerSetLeds,
  PowerSetUvLamp,
  PowerSetBuzzer,
  PowerSetShdn,
  PowerTurnOff,
  PowerSetWdtValue,

  VoltageGetRail,
  VoltageGet24VRail,
  VoltageGetUvLampRail,
  VoltageGetR2RdacRail,
  VoltageGetBtns,

  RgbSetOnOff,

  GerconGetState,
  GerconClearTrigger,

  SnsState,

  OptDiagState,
  OptDiagClearTrigger,

  PwrBtnGetState,
  PwrBtnClearTrigger
};

} // namespace protocol::motherboard

#endif // MOTHERBOARD_RESPONSE_TYPE_H
