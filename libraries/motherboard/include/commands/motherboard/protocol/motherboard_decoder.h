#ifndef PROTOCOL_MOTHERBOARD_MOTHERBOARDDECODER_H
#define PROTOCOL_MOTHERBOARD_MOTHERBOARDDECODER_H

#include "motherboard/command/export.h"
#include "serial/protocol/decoder_interface.h"

namespace protocol::srvint {
struct SrvIntResponseFactory;
} // namespace protocol::srvint

namespace protocol::motherboard {

struct MOTHERBOARD_COMMAND_EXPORT MotherboardDecoder : DecoderInterface
{
  MotherboardDecoder();
  ~MotherboardDecoder() override;

  MotherboardDecoder(const MotherboardDecoder&)     = delete;
  MotherboardDecoder(MotherboardDecoder&&) noexcept = default;

  auto operator=(const MotherboardDecoder&) -> MotherboardDecoder& = delete;
  auto operator=(MotherboardDecoder&&) noexcept
    -> MotherboardDecoder& = default;

  [[nodiscard]] auto createResponse(const QByteArray& data, int type) const
    -> ResponseOrError override;

private:
  std::unique_ptr<protocol::srvint::SrvIntResponseFactory> factory{ nullptr };
};

} // namespace protocol::motherboard

#endif // PROTOCOL_MOTHERBOARD_MOTHERBOARDDECODER_H
