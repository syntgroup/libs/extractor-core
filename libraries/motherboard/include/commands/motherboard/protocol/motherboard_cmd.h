/*!
 * @file motherboard_cmd.h
 * @author tmayzenberg
 * @details описание кодов команд, используемых в реализации протокола SrvInt,
 * адаптированного для данного проекта.
 * Ссылаемся на ревизию 5 от 11.05.2023
 */

#ifndef PROTOCOL_MOTHERBOARDCMD_H
#define PROTOCOL_MOTHERBOARDCMD_H

#include "motherboard/command/export.h"

#include <cstdint>

namespace protocol::motherboard {

//! Firmware version number, saved in program flash of microcontroller
static constexpr uint8_t FIRMWARE_VERSION{ 0x8 };

enum class MOTHERBOARD_COMMAND_EXPORT DeviceIdAccessKey : uint8_t
{
  MSB  = 0x75,
  MIDB = 0xF3,
  LSB  = 0x19
};

// -----------------------------------------------------------------------------

//! list of areas avaliable for write access
enum class MOTHERBOARD_COMMAND_EXPORT SetParam : uint8_t
{
  Device  = 0x00,
  Hbit    = 0xB0,
  Goto    = 0xB1,
  Engines = 0xB2,
  Power   = 0xB4,
  RgbLeds = 0xB5,
  Sns     = 0xB6,
  Gercon  = 0xB7,
  OptDiag = 0xB8,
  PwrBtn  = 0xB9
};

//! list of areas avaliable for read access
enum class MOTHERBOARD_COMMAND_EXPORT GetParam : uint8_t
{
  Device   = static_cast<uint8_t>(SetParam::Device),
  Engines  = static_cast<uint8_t>(SetParam::Engines),
  Voltages = 0xB3,
  Buttons  = 0xB5,
  Sns      = static_cast<uint8_t>(SetParam::Sns),
  Gercon   = static_cast<uint8_t>(SetParam::Gercon),
  OptDiag  = static_cast<uint8_t>(SetParam::OptDiag),
  PwrBtn   = static_cast<uint8_t>(SetParam::PwrBtn)
};

// -----------------------------------------------------------------------------

//! parameters to be set in DEVICE section
enum class MOTHERBOARD_COMMAND_EXPORT DeviceSet : uint8_t
{
  Id = 0x01
};

//! parameters to be read from DEVICE section
enum class MOTHERBOARD_COMMAND_EXPORT DeviceGet : uint8_t
{
  Id               = static_cast<int>(DeviceSet::Id),
  FirmwareRevision = 0x02,
};

// -----------------------------------------------------------------------------

enum class MOTHERBOARD_COMMAND_EXPORT HbitSet : uint8_t
{
  Mode    = 0x01,
  RepFact = 0x02
};

// -----------------------------------------------------------------------------

enum class MOTHERBOARD_COMMAND_EXPORT GotoSet : uint8_t
{
  GotoNTicks                 = 0x00, //!< @note can be dual mode
  GotoAbsolutePosition       = 0x01, //!< @note can be dual mode
  GotoZeroPosition           = 0x02,
  EmergencyStop              = 0x03, //!< @note can be dual mode
  GotoNUncontrolled          = 0x04, //!< @note can be dual mode
  SpeedSet                   = 0x05, //!< @note can be dual mode
  SmoothStop                 = 0x06, //!< @note can be dual mode
  MixModeOn                  = 0x56,
  MixModeTime                = 0x57,
  MixModeOff                 = 0x58,
  MixModeSmoothStop          = 0x59,
  AccelStandbyCurrents       = 0x5A,
  SpeedSetWoOptions          = 0xF5, //!< @note can be dual mode
  GotoNUncontrolledWoSensors = 0xF7  //!< @note can be dual mode
};

// -----------------------------------------------------------------------------

enum class MOTHERBOARD_COMMAND_EXPORT EnginesSet : uint8_t
{
  MaximumSpeed    = 0x00,
  Repeatability   = 0x03,
  Microstep       = 0x04,
  HomePosition    = 0x05,
  F0              = 0x06,
  FMAX            = 0x07,
  DFMAX           = 0x08,
  CurrentPosition = 0x1C,
  SavePosition    = 0x0C, //! ????????????????
  LoadPosition    = 0x0D,
  EngineOn        = 0x1A,
  EngineOff       = 0x1B,
  DriverWrite     = 0x30,
  FactoryDefault  = 0xF0,
};

enum class MOTHERBOARD_COMMAND_EXPORT EnginesGet : uint8_t
{
  MaximumSpeed         = static_cast<uint8_t>(EnginesSet::MaximumSpeed),
  Repeatability        = static_cast<uint8_t>(EnginesSet::Repeatability),
  Microstep            = static_cast<uint8_t>(EnginesSet::Microstep),
  HomePosition         = static_cast<uint8_t>(EnginesSet::HomePosition),
  F0                   = static_cast<uint8_t>(EnginesSet::F0),
  FMAX                 = static_cast<uint8_t>(EnginesSet::FMAX),
  DFMAX                = static_cast<uint8_t>(EnginesSet::DFMAX),
  CurrentPosition      = static_cast<uint8_t>(EnginesSet::CurrentPosition),
  PulseCounter         = 0x1D,
  PositionAfterCross   = 0x20,
  MixModeState         = 0x21,
  AccelStandbyCurrents = 0x22,
  DriverReadBuffer     = 0x30,
};

// -----------------------------------------------------------------------------

enum class MOTHERBOARD_COMMAND_EXPORT PowerSet : uint8_t
{
  Cooler   = 0x01,
  Leds     = 0x02,
  UvLamp   = 0x03,
  Buzzer   = 0x04,
  SHDN     = 0x05,
  WdtValue = 0x06,
  TurnOff  = 0x07
};

// -----------------------------------------------------------------------------

enum class MOTHERBOARD_COMMAND_EXPORT VoltagesGet : uint8_t
{
  Rail24V    = 0x01,
  RailUvLamp = 0x02,
  RailR2RDac = 0x03,
};

// -----------------------------------------------------------------------------

enum class MOTHERBOARD_COMMAND_EXPORT ButtonsGet : uint8_t
{
  All = 0x01
};

// -----------------------------------------------------------------------------

enum class MOTHERBOARD_COMMAND_EXPORT SnsGet : uint8_t
{
  All = 0x01,
};

// -----------------------------------------------------------------------------

enum class MOTHERBOARD_COMMAND_EXPORT RgbSet
{
  OnOff = 0x01
};

// -----------------------------------------------------------------------------

enum class MOTHERBOARD_COMMAND_EXPORT SnsSet : uint8_t
{
  TransFunct = 0x01,
  DebugMode  = 0x02,
};

// -----------------------------------------------------------------------------

enum class MOTHERBOARD_COMMAND_EXPORT GerconSet : uint8_t
{
  ClearTrigger = 0x01,
};

enum class MOTHERBOARD_COMMAND_EXPORT GerconGet : uint8_t
{
  State = 0x01,
};

// -----------------------------------------------------------------------------

//! parameters of OptDiag with write access
enum class MOTHERBOARD_COMMAND_EXPORT OptDiagSet : uint8_t
{
  ClearTrigger = 0x01
};

//! parameters of OptDiag with read access
enum class MOTHERBOARD_COMMAND_EXPORT OptDiagGet : uint8_t
{
  State = 0x01
};

// -----------------------------------------------------------------------------

enum class MOTHERBOARD_COMMAND_EXPORT PwrBtnSet : uint8_t
{
  ClearTrigger = 0x01
};

enum class MOTHERBOARD_COMMAND_EXPORT PwrBtnGet : uint8_t
{
  State = 0x01
};

} // namespace protocol::motherboard

#endif // PROTOCOL_MOTHERBOARDCMD_H
