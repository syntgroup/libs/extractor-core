#ifndef GCODE_PARSERBUZZER_H
#define GCODE_PARSERBUZZER_H

#include "motherboard/gcode/export.h"
#include "parser/gcode/gcode_parser_interface.h"

namespace gcode {

/*!
 * @brief The ParserBuzzerOn class
 * @details M501
 */
class MOTHERBOARD_GCODE_EXPORT ParserBuzzerOn final
  : public GCodeParserInterface<ParserBuzzerOn>
{
  friend class GCodeParserInterface<ParserBuzzerOn>;
  [[nodiscard]] auto generateImpl(const QString& block) const -> ParseResult;
  [[nodiscard]] static auto allowEmptyInput() { return true; }
};

/*!
 * @brief The ParserBuzzerOff class
 * @details M500
 */
class MOTHERBOARD_GCODE_EXPORT ParserBuzzerOff final
  : public GCodeParserInterface<ParserBuzzerOff>
{
  friend class GCodeParserInterface<ParserBuzzerOff>;
  [[nodiscard]] auto generateImpl(const QString& block) const -> ParseResult;
  [[nodiscard]] static auto allowEmptyInput() { return true; }
};

} // namespace gcode

#endif // GCODE_PARSERBUZZER_H
