#ifndef GCODE_PARSERGRABBEROPEN_H
#define GCODE_PARSERGRABBEROPEN_H

#include "motherboard/gcode/export.h"
#include "parser/gcode/gcode_parser_interface.h"

namespace device {
class Engines;
class DiscreteOutput;
} // namespace device

namespace gcode {

/*!
 * @brief The ParserGrabberOpen class
 * @details M10 or M11
 */
class MOTHERBOARD_GCODE_EXPORT ParserGrabberOpen final
  : public GCodeParserInterface<ParserGrabberOpen>
{
  friend class GCodeParserInterface<ParserGrabberOpen>;

public:
  ParserGrabberOpen(std::weak_ptr<device::Engines> engines,
                    double target,
                    std::weak_ptr<device::DiscreteOutput> diag_sensor)
    : m_engines{ std::move(engines) }
    , m_diagSensor{ std::move(diag_sensor) }
    , m_target{ target }
  {
  }

private:
  [[nodiscard]] auto generateImpl(const QString& block) const -> ParseResult;
  [[nodiscard]] static auto allowEmptyInput() { return true; }
  std::weak_ptr<device::Engines> m_engines;
  std::weak_ptr<device::DiscreteOutput> m_diagSensor;
  double m_target;
};

} // namespace gcode

#endif // GCODE_PARSERGRABBEROPEN_H
