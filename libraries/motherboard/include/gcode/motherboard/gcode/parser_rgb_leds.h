#ifndef GCODE_PARSERRGBLEDS_H
#define GCODE_PARSERRGBLEDS_H

#include "motherboard/gcode/export.h"
#include "parser/gcode/gcode_parser_interface.h"

namespace device {
class RgbLeds;
} // namespace device

namespace gcode {

/*!
 * @brief The ParserRgbDisableLeds class
 * @details M300
 */
class MOTHERBOARD_GCODE_EXPORT ParserRgbDisableLeds final
  : public GCodeParserInterface<ParserRgbDisableLeds>
{
  friend class GCodeParserInterface<ParserRgbDisableLeds>;

public:
  explicit ParserRgbDisableLeds(std::weak_ptr<device::RgbLeds> leds)
    : m_leds{ std::move(leds) }
  {
  }

private:
  [[nodiscard]] auto generateImpl(const QString& block) const -> ParseResult;
  [[nodiscard]] static auto allowEmptyInput() { return true; }
  std::weak_ptr<device::RgbLeds> m_leds;
};

/*!
 * @brief The ParserRgbEnableLeds class
 * @details M301
 */
class MOTHERBOARD_GCODE_EXPORT ParserRgbEnableLeds final
  : public GCodeParserInterface<ParserRgbEnableLeds>
{
  friend class GCodeParserInterface<ParserRgbEnableLeds>;

public:
  explicit ParserRgbEnableLeds(std::weak_ptr<device::RgbLeds> leds)
    : m_leds{ std::move(leds) }
  {
  }

private:
  [[nodiscard]] auto generateImpl(const QString& block) const -> ParseResult;
  [[nodiscard]] static auto allowEmptyInput() { return true; }
  std::weak_ptr<device::RgbLeds> m_leds;
};

/*!
 * @brief The ParserRgbSetLeds class
 * @details M302
 */
class MOTHERBOARD_GCODE_EXPORT ParserRgbSetLeds final
  : public GCodeParserInterface<ParserRgbSetLeds>
{
  friend class GCodeParserInterface<ParserRgbSetLeds>;

public:
  explicit ParserRgbSetLeds(std::weak_ptr<device::RgbLeds> leds)
    : m_leds{ std::move(leds) }
  {
  }

private:
  [[nodiscard]] auto generateImpl(const QString& block) const -> ParseResult;
  [[nodiscard]] static auto allowEmptyInput() { return false; }
  std::weak_ptr<device::RgbLeds> m_leds;
};

} // namespace gcode

#endif // GCODE_PARSERRGBLEDS_H
