#ifndef GCODE_PARSERRELEASESLEEVES_H
#define GCODE_PARSERRELEASESLEEVES_H

#include "motherboard/gcode/export.h"
#include "parser/gcode/gcode_parser_interface.h"

namespace device {
class Engines;
} // namespace device

namespace gcode {

/*!
 * @brief The ParserReleaseSleeves class
 * @details M401
 */
class MOTHERBOARD_GCODE_EXPORT ParserReleaseSleeves final
  : public GCodeParserInterface<ParserReleaseSleeves>
{
  friend class GCodeParserInterface<ParserReleaseSleeves>;

public:
  explicit ParserReleaseSleeves(std::weak_ptr<device::Engines> engines)
    : m_engines{ std::move(engines) }
  {
  }

private:
  [[nodiscard]] auto generateImpl(const QString& block) const -> ParseResult;
  [[nodiscard]] static auto allowEmptyInput() { return true; }
  std::weak_ptr<device::Engines> m_engines;
};

/*!
 * @brief The ParserCollectSleeves class
 * @details M402
 */
class MOTHERBOARD_GCODE_EXPORT ParserCollectSleeves final
  : public GCodeParserInterface<ParserCollectSleeves>
{
  friend class GCodeParserInterface<ParserCollectSleeves>;

public:
  explicit ParserCollectSleeves(std::weak_ptr<device::Engines> engines)
    : m_engines{ std::move(engines) }
  {
  }

private:
  [[nodiscard]] auto generateImpl(const QString& block) const -> ParseResult;
  [[nodiscard]] static auto allowEmptyInput() { return true; }
  std::weak_ptr<device::Engines> m_engines;
};

} // namespace gcode

#endif // GCODE_PARSERRELEASESLEEVES_H
