#ifndef GCODE_PARSERMIX_H
#define GCODE_PARSERMIX_H

#include "motherboard/device/engine_values.h"
#include "motherboard/gcode/export.h"
#include "parser/gcode/gcode_parser_interface.h"

namespace device {
class Engines;
} // namespace device

namespace gcode {

/*!
 * @brief The ParserMixCycles class
 * @details G56
 */
class MOTHERBOARD_GCODE_EXPORT ParserMixCycles final
  : public GCodeParserInterface<ParserMixCycles>
{
  friend class GCodeParserInterface<ParserMixCycles>;

public:
  explicit ParserMixCycles(
    std::weak_ptr<device::Engines> engines,
    device::SpeedMode legacy_mode = device::SpeedMode::Modern)
    : _engines{ std::move(engines) }
    , _legacyMode{ legacy_mode }
  {
  }

private:
  [[nodiscard]] auto generateImpl(const QString& block) const -> ParseResult;
  [[nodiscard]] static auto allowEmptyInput() { return false; }
  std::weak_ptr<device::Engines> _engines;
  device::SpeedMode _legacyMode;
};

/*!
 * @brief The ParserMixTime class
 * @details G57
 */
class MOTHERBOARD_GCODE_EXPORT ParserMixTime final
  : public GCodeParserInterface<ParserMixTime>
{
  friend class GCodeParserInterface<ParserMixTime>;

public:
  explicit ParserMixTime(
    std::weak_ptr<device::Engines> engines,
    device::SpeedMode legacy_mode = device::SpeedMode::Modern)
    : _engines{ std::move(engines) }
    , _legacyMode{ legacy_mode }
  {
  }

private:
  [[nodiscard]] auto generateImpl(const QString& block) const -> ParseResult;
  [[nodiscard]] static auto allowEmptyInput() { return false; }
  std::weak_ptr<device::Engines> _engines;
  device::SpeedMode _legacyMode;
};

} // namespace gcode

#endif // GCODE_PARSERMIX_H
