#ifndef GCODE_PARSERCHECKSLEEVES_H
#define GCODE_PARSERCHECKSLEEVES_H

#include "motherboard/gcode/export.h"
#include "parser/gcode/gcode_parser_interface.h"

namespace device {
class Engines;
class DiscreteOutput;
} // namespace device

namespace gcode {

/*!
 * @brief The ParserCheckSleeves class
 * @details M400
 */
class MOTHERBOARD_GCODE_EXPORT ParserCheckSleeves final
  : public GCodeParserInterface<ParserCheckSleeves>
{
  friend class GCodeParserInterface<ParserCheckSleeves>;

public:
  ParserCheckSleeves(std::weak_ptr<device::Engines> engines,
                     std::weak_ptr<device::DiscreteOutput> sensors)
    : m_engines{ std::move(engines) }
    , m_diag_sensor{ std::move(sensors) }
  {
  }

private:
  [[nodiscard]] auto generateImpl(const QString& block) const -> ParseResult;
  [[nodiscard]] static auto allowEmptyInput() { return true; }
  std::weak_ptr<device::Engines> m_engines;
  std::weak_ptr<device::DiscreteOutput> m_diag_sensor;
};

/*!
 * @brief The ParserJustCheck class
 * @details M410
 */
class MOTHERBOARD_GCODE_EXPORT ParserJustCheck final
  : public GCodeParserInterface<ParserJustCheck>
{
  friend class GCodeParserInterface<ParserJustCheck>;

public:
  ParserJustCheck(std::weak_ptr<device::DiscreteOutput> sensors)
    : m_diag_button{ std::move(sensors) }
  {
  }

private:
  [[nodiscard]] auto generateImpl(const QString& block) const -> ParseResult;
  [[nodiscard]] static auto allowEmptyInput() { return true; }
  std::weak_ptr<device::DiscreteOutput> m_diag_button;
};

/*!
 * @brief The ParserSleevesCache class
 * @details M411
 */
class MOTHERBOARD_GCODE_EXPORT ParserSleevesCache final
  : public GCodeParserInterface<ParserSleevesCache>
{
  friend class GCodeParserInterface<ParserSleevesCache>;

public:
  ParserSleevesCache(std::weak_ptr<device::DiscreteOutput> diagSensor)
    : m_diagButton{ std::move(diagSensor) }
  {
  }

private:
  [[nodiscard]] auto generateImpl(const QString& block) const -> ParseResult;
  [[nodiscard]] static auto allowEmptyInput() { return true; }
  std::weak_ptr<device::DiscreteOutput> m_diagButton;
};

} // namespace gcode

#endif // GCODE_PARSERCHECKSLEEVES_H
