/*!
 * @file parser_linear_move.h
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 * @brief
 */

#ifndef GCODE_PARSERLINEARMOVE_H
#define GCODE_PARSERLINEARMOVE_H

#include "motherboard/device/engine_values.h"
#include "motherboard/gcode/export.h"
#include "parser/gcode/gcode_parser_interface.h"

namespace device {
class Engines;
class DiscreteOutput;
} // namespace device

namespace gcode {

/*!
 * @brief The ParserLinearMove class
 * @details G1
 */
class MOTHERBOARD_GCODE_EXPORT ParserLinearMove final
  : public GCodeParserInterface<ParserLinearMove>
{
  friend class GCodeParserInterface<ParserLinearMove>;

public:
  explicit ParserLinearMove(
    std::weak_ptr<device::Engines> engines,
    std::weak_ptr<device::DiscreteOutput> diag_sensor,
    device::SpeedMode legacy_mode = device::SpeedMode::Modern)
    : m_engines{ std::move(engines) }
    , m_diagSensor{ std::move(diag_sensor) }
    , m_legacyMode{ legacy_mode }
  {
  }

private:
  std::weak_ptr<device::Engines> m_engines;
  std::weak_ptr<device::DiscreteOutput> m_diagSensor;
  device::SpeedMode m_legacyMode;

  using Targets = std::vector<std::pair<QString, double>>;
  [[nodiscard]] static auto allowEmptyInput() { return false; }
  [[nodiscard]] auto generateImpl(const QString& block) const -> ParseResult;
  [[nodiscard]] auto parseSingle(const Targets& targets,
                                 const QString& block) const -> ParseResult;
  [[nodiscard]] auto parseMulti(const Targets& targets,
                                const QString& block) const -> ParseResult;
};

} // namespace gcode

#endif // GCODE_PARSERLINEARMOVE_H
