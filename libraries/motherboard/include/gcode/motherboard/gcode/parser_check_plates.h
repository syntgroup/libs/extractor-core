#ifndef GCODE_PARSERCHECKPLATES_H
#define GCODE_PARSERCHECKPLATES_H

#include "motherboard/gcode/export.h"
#include "parser/gcode/gcode_parser_interface.h"

namespace device {
class IrSensors;
} // namespace device

namespace gcode {

/*!
 * @brief The ParserCheckPlates class
 * @details M600
 */
class MOTHERBOARD_GCODE_EXPORT ParserCheckPlates final
  : public GCodeParserInterface<ParserCheckPlates>
{
  friend class GCodeParserInterface<ParserCheckPlates>;

public:
  explicit ParserCheckPlates(std::weak_ptr<device::IrSensors> sensors)
    : m_sensors{ std::move(sensors) }
  {
  }

private:
  [[nodiscard]] auto generateImpl(const QString& block) const -> ParseResult;
  [[nodiscard]] static auto allowEmptyInput() { return false; }
  std::weak_ptr<device::IrSensors> m_sensors;
};

} // namespace gcode

#endif // GCODE_PARSERCHECKPLATES_H
