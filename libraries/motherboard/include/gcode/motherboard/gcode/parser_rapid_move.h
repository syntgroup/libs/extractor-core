/*!
 * @file parser_rapid_move.h
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 */

#ifndef GCODE_PARSERRAPIDMOVE_H
#define GCODE_PARSERRAPIDMOVE_H

#include "motherboard/device/engine_speed.h"
#include "motherboard/gcode/export.h"
#include "parser/gcode/gcode_parser_interface.h"

namespace device {
class Engines;
class DiscreteOutput;
} // namespace device

namespace gcode {

/*!
 * @brief The ParserRapidMove class
 * @details G0
 */
class MOTHERBOARD_GCODE_EXPORT ParserRapidMove final
  : public GCodeParserInterface<ParserRapidMove>
{
  friend class GCodeParserInterface<ParserRapidMove>;

public:
  explicit ParserRapidMove(
    std::weak_ptr<device::Engines> engines,
    std::weak_ptr<device::DiscreteOutput> diag_sensor,
    device::SpeedMode legacy_mode = device::SpeedMode::Modern)
    : m_engines{ std::move(engines) }
    , m_diagSensor{ std::move(diag_sensor) }
    , m_legacyMode{ legacy_mode }
    , m_accelerationCurve{ m_legacyMode == device::SpeedMode::Modern
                             ? device::AccelerationCurve::Fast
                             : device::AccelerationCurve::Normal }
  {
  }

private:
  std::weak_ptr<device::Engines> m_engines;
  std::weak_ptr<device::DiscreteOutput> m_diagSensor;
  device::SpeedMode m_legacyMode;
  device::AccelerationCurve m_accelerationCurve;

  using Targets = std::vector<std::pair<QString, double>>;
  [[nodiscard]] static auto allowEmptyInput() { return false; }
  [[nodiscard]] auto generateImpl(const QString& block) const -> ParseResult;
  [[nodiscard]] auto ParseSingle(const Targets& targets,
                                 const QString& block) const -> ParseResult;
  [[nodiscard]] auto ParseMulti(const Targets& targets,
                                const QString& block) const -> ParseResult;
};

} // namespace gcode

#endif // GCODE_PARSERRAPIDMOVE_H
