/*!
 * @file parser_auto_home.h
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 */

#ifndef GCODE_PARSERAUTOHOME_H
#define GCODE_PARSERAUTOHOME_H

#include "motherboard/gcode/export.h"
#include "parser/gcode/gcode_parser_interface.h"

namespace device {
class Engines;
} // namespace device

namespace gcode {

/*!
 * @brief The ParserAutoHome class
 * @details is called by G54 code
 * Expected syntax:
 * 1. G54 -- no other parameters, homing all axises in particular order;
 * 2. G54 <axis>[ <more axises>] -- homing mentioned axises one by one in order
 * of appearance. Allowing up to 5 axises. No duplicates are allowed.
 */
class MOTHERBOARD_GCODE_EXPORT ParserAutoHome final
  : public GCodeParserInterface<ParserAutoHome>
{
  friend class GCodeParserInterface<ParserAutoHome>;

public:
  explicit ParserAutoHome(std::weak_ptr<device::Engines> engines)
    : m_engines{ std::move(engines) }
  {
  }

private:
  [[nodiscard]] auto generateImpl(const QString& block) const -> ParseResult;
  [[nodiscard]] static auto allowEmptyInput() { return true; }

  std::weak_ptr<device::Engines> m_engines;
};

} // namespace gcode

#endif // GCODE_PARSERAUTOHOME_H
