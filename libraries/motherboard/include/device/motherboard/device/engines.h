#ifndef DEVICE_ENGINES_H
#define DEVICE_ENGINES_H

#include "engine_defaults.h"
#include <QString>
#include <gsl/gsl-lite.hpp>
#include <memory>
#include <unordered_map>

namespace device {

class Engine;
class MOTHERBOARD_DEVICE_EXPORT Engines
{
public:
  using Container = std::unordered_map<AxisAddress, std::unique_ptr<Engine>>;

  explicit Engines(Container&& engines);
  ~Engines();

  Engines(const Engines&)                    = delete;
  auto operator=(const Engines&) -> Engines& = delete;
  Engines(Engines&&)                         = default;
  auto operator=(Engines&&) -> Engines&      = default;

  [[nodiscard]] auto engineByName(AxisAddress axis) const
    -> gsl::not_null<Engine*>;
  [[nodiscard]] auto engineByAxis(const QString& axis) const -> Engine*;

private:
  Container m_engines;
};

} // namespace device

#endif // DEVICE_ENGINES_H
