#ifndef MOTHERBOARD_PARAMS_H
#define MOTHERBOARD_PARAMS_H

#include "motherboard/device/export.h"
#include <cstdint>
#include <optional>

namespace device {

struct MOTHERBOARD_DEVICE_EXPORT MotherboardParameters
{
  std::optional<uint32_t> motherboardId{ std::nullopt };
  std::optional<uint8_t> fwRev{ std::nullopt };
};

} // namespace device

#endif // MOTHERBOARD_PARAMS_H
