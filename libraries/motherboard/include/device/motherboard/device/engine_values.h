#ifndef DEVICE_ENGINEVALUES_H
#define DEVICE_ENGINEVALUES_H

#include "motherboard/device/export.h"
#include <QMetaType>
#include <strong_type/strong_type.hpp>

namespace device {

using SpeedIndex = strong::
  type<uint32_t, struct SpeedIndexTag, strong::regular, strong::ordered>;

using Coordinate = strong::type<qint64,
                                struct CoordinateTag,
                                strong::regular,
                                strong::arithmetic,
                                strong::ordered>;

using Cycles = strong::type<uint32_t,
                            struct CyclesTag,
                            strong::regular,
                            strong::arithmetic,
                            strong::ordered>;

using Repeatability = strong::
  type<uint8_t, struct RepeatabilityTag, strong::regular, strong::ordered>;

//! frequency [Hz], [1/s]
using Frequency = strong::type<double,
                               struct FrequencyTag,
                               strong::semiregular,
                               strong::arithmetic,
                               strong::ordered,
                               strong::scalable_with<double>>;
enum class MOTHERBOARD_DEVICE_EXPORT FrequencyType
{
  F0,
  Fmax,
  dFmax
};
using Acceleration = std::pair<Frequency, FrequencyType>;

//! speed in [mm/min]
using Speed = strong::type<double,
                           struct SpeedTag,
                           strong::semiregular,
                           strong::arithmetic,
                           strong::ordered,
                           strong::scalable_with<double>>;
enum class SpeedMode : int
{
  Legacy,
  Modern
};

struct MOTHERBOARD_DEVICE_EXPORT EngineValues
{
  // NOLINTBEGIN
  enum class Microstep : uint8_t
  {
    Step_1_1   = 0x00,
    Step_1_2   = 0x01,
    Step_1_4   = 0x02,
    Step_1_8   = 0x03,
    Step_1_16  = 0x04,
    Step_1_32  = 0x05,
    Step_1_64  = 0x06,
    Step_1_128 = 0x07,
    Step_1_256 = 0x08,
    Undefined  = 0xff
  };
  Q_ENUM(Microstep)
  // NOLINTEND

  enum class ZeroSwitch : uint8_t
  {
    ZeroIsSw1 = 0x00,
    ZeroIsSw2 = 0x01
  };
  Q_ENUM(ZeroSwitch)

  // NOLINTBEGIN
  enum class Current : uint8_t
  {
    Current_1_32  = 0x00,
    Current_2_32  = 0x01,
    Current_3_32  = 0x02,
    Current_4_32  = 0x03,
    Current_5_32  = 0x04,
    Current_6_32  = 0x05,
    Current_7_32  = 0x06,
    Current_8_32  = 0x07,
    Current_9_32  = 0x08,
    Current_10_32 = 0x09,
    Current_11_32 = 0x0a,
    Current_12_32 = 0x0b,
    Current_13_32 = 0x0c,
    Current_14_32 = 0x0d,
    Current_15_32 = 0x0e,
    Current_16_32 = 0x0f,
    Current_17_32 = 0x10,
    Current_18_32 = 0x11,
    Current_19_32 = 0x12,
    Current_20_32 = 0x13,
    Current_21_32 = 0x14,
    Current_22_32 = 0x15,
    Current_23_32 = 0x16,
    Current_24_32 = 0x17,
    Current_25_32 = 0x18,
    Current_26_32 = 0x19,
    Current_27_32 = 0x1a,
    Current_28_32 = 0x1b,
    Current_29_32 = 0x1c,
    Current_30_32 = 0x1d,
    Current_31_32 = 0x1e,
    Current_32_32 = 0x1f,
    Undefined     = 0xff
  };
  Q_ENUM(Current)
  // NOLINTEND

private:
  Q_GADGET
};

using Microstep  = EngineValues::Microstep;
using Current    = EngineValues::Current;
using ZeroSwitch = EngineValues::ZeroSwitch;

} // namespace device

// NOLINTBEGIN
Q_DECLARE_METATYPE(device::Microstep)
Q_DECLARE_METATYPE(device::Current)
Q_DECLARE_METATYPE(device::ZeroSwitch)
Q_DECLARE_METATYPE(device::Acceleration);
Q_DECLARE_METATYPE(device::Frequency);
// NOLINTEND

#endif // DEVICE_ENGINEVALUES_H
