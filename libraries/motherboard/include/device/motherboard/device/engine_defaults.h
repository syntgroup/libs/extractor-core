#ifndef DEFAULTS_ENGINEPARAMS_H
#define DEFAULTS_ENGINEPARAMS_H

#include "motherboard/device/export.h"
#include <cstddef>

namespace device {

enum class MOTHERBOARD_DEVICE_EXPORT AxisAddress : size_t
{
  Column  = 0,
  Shield  = 1,
  Magnets = 2,
  Sleeves = 3,
  Grabber = 4
};

} // namespace device

#endif // DEFAULTS_ENGINEPARAMS_H
