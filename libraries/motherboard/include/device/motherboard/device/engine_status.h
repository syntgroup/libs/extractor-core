#ifndef MOTHERBOARD_ENGINE_STATUS_H
#define MOTHERBOARD_ENGINE_STATUS_H

#include "motherboard/device/export.h"
#include <cstdint>

class QDebug;

namespace device {

/*!
 * @brief decoding engine state from status_byte
 * @details in motherboard project this status looks like this:
 * ```c
 * struct bits
 * {
 *   uint8_t undefined_pos : 1;
 *   uint8_t switch_select : 1;
 *   uint8_t moving_to_zero : 1;
 *   uint8_t switch_find : 1;
 *   uint8_t engine_moving : 1;
 *   uint8_t engine_go_wo_opts : 1;
 *   uint8_t engine_error_saved : 1;
 * };
 * ```
 */
struct MOTHERBOARD_DEVICE_EXPORT EngineStatus
{
  EngineStatus() = default;
  explicit EngineStatus(uint8_t status_byte);

  //! false - position is undefined;
  //! true - position in defined, current engine position is stored in
  //! absolute_postition variable
  bool undefinedPos{ false };

  //! false - SW0 used as zero position switch;
  //! true - SW1 userd as zero position switch
  bool switchSelect{ false };

  //! false - engine is not moving to zero position;
  //! true - engine is moving to zero position
  bool engineGoToZero{ false };

  //! false - SW not finding, when moving to zero
  bool switchFound{ false };

  //! false - engine not moving;
  //! true - engine is moving
  bool engineGo{ false };

  //! false - engine not moving;
  //! true - engine is moving without switches state control
  bool engineGoWoOpts{ false };

  //! false - error is not translate to error buffer;
  //! true - error is translated to error buffer
  bool engineErrorSaved{ false };

  friend QDebug operator<<(QDebug debug, const device::EngineStatus& status);

  uint8_t byte{ 0 };

  [[nodiscard]] auto operator==(const EngineStatus& other) const
  {
    return other.byte == byte;
  }

private:
  enum BitsPos
  {
    BUndefinedPos     = 0,
    BSwitchSelect     = 1,
    BEngineGoToZero   = 2,
    BSwitchFind       = 3,
    BEngineGo         = 4,
    BEngineGoWoOpts   = 5,
    BEngineErrorSaved = 6
  };
};

} // namespace device

#endif // MOTHERBOARD_ENGINE_STATUS_H
