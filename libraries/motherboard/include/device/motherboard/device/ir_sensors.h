#ifndef DEVICE_IRSENSORS_H
#define DEVICE_IRSENSORS_H

#include "common/analog_input.h"
#include "motherboard/device/export.h"
#include <QObject>
#include <memory>
#include <vector>

namespace device {

class MOTHERBOARD_DEVICE_EXPORT IrSensor : public DiscreteInput
{
public:
  IrSensor(int idx)
    : _idx{ idx }
  {
  }
  ~IrSensor() override = default;

  [[nodiscard]] auto idx() const { return _idx; }

private:
  const int _idx = 0;
};

class MOTHERBOARD_DEVICE_EXPORT IrSensors : public QObject
{
  Q_OBJECT
  Q_DISABLE_COPY(IrSensors)

public:
  using Container = std::vector<std::unique_ptr<device::IrSensor>>;

  IrSensors(size_t count, QObject* parent = nullptr);
  IrSensors(Container&& sensors, QObject* parent = nullptr);

  [[nodiscard]] auto count() const { return m_sensors.size(); }
  [[nodiscard]] auto sensorState(int idx) const
  {
    return m_sensors.at(idx)->state();
  }
  void setSensor(int idx, bool value)
  {
    if (idx <= count()) {
      m_sensors.at(idx)->setState(value);
    }
  }

private:
  Container m_sensors;

signals:
  void notFound(int idx);
};

} // namespace device

#endif // DEVICE_IRSENSORS_H
