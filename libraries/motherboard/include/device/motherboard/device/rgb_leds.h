#ifndef EXTRACTOR_TOUCHPANEL_RGB_LEDS_H
#define EXTRACTOR_TOUCHPANEL_RGB_LEDS_H

#include "common/analog_input.h"
#include "motherboard/device/export.h"
#include "motherboard_values.h"
#include <QObject>
#include <memory>
#include <vector>

namespace device {

class RgbLed;

class MOTHERBOARD_DEVICE_EXPORT RgbLeds
{
public:
  using Container = std::vector<std::unique_ptr<device::RgbLed>>;

  RgbLeds(size_t count);
  RgbLeds(Container&& leds);

  [[nodiscard]] auto count() const { return m_leds.size(); }
  bool setLedColors(const std::vector<Qt::GlobalColor>& color_scheme);

private:
  Container m_leds;
};

class MOTHERBOARD_DEVICE_EXPORT RgbLed final
  : public QObject
  , public AnalogInput<Qt::GlobalColor>
{
  Q_OBJECT
  Q_DISABLE_COPY(RgbLed)

public:
  explicit RgbLed(uint8_t idx,
                  Qt::GlobalColor = Qt::black,
                  QObject* parent = nullptr);
  ~RgbLed() override = default;

  [[nodiscard]] auto idx() const { return m_idx; }

  void setValue(Qt::GlobalColor new_color) override;

private:
  uint8_t m_idx{ 0 };

signals:
  void colorChanged(Qt::GlobalColor color);
};

} // namespace device

#endif // EXTRACTOR_TOUCHPANEL_RGB_LEDS_H
