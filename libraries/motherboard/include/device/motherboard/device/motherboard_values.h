#ifndef DEVICE_MOTHERBOARDVALUES_H
#define DEVICE_MOTHERBOARDVALUES_H

#include <array>
#include <cstdint>
#include <strong_type/strong_type.hpp>

namespace device {

using PwmLevel =
  strong::type<uint16_t, struct PwmLevelTag, strong::regular, strong::ordered>;

using Voltage = strong::type<double,
                             struct VoltageTag,
                             strong::semiregular,
                             strong::arithmetic,
                             strong::ordered>;

constexpr const std::array lights_levels{ PwmLevel{ 0 },
                                          PwmLevel{ 100 },
                                          PwmLevel{ 500 },
                                          PwmLevel{ 1500 },
                                          PwmLevel{ 10000 } };
constexpr auto lights_max_level = lights_levels.size();
constexpr auto lights_off       = lights_levels.front();
constexpr auto lights_full      = lights_levels.back();

} // namespace device

#endif // DEVICE_MOTHERBOARDVALUES_H
