#ifndef DEVICE_ENGINESPEED_H
#define DEVICE_ENGINESPEED_H

#include "engine_values.h"

class QDebug;

namespace device {

enum class MOTHERBOARD_DEVICE_EXPORT AccelerationCurve : int
{
  Slow,
  Normal,
  Fast
};

class MOTHERBOARD_DEVICE_EXPORT AccelerationProfile
{
public:
  AccelerationProfile() = default;
  AccelerationProfile(Frequency f0, Frequency fmax, Frequency dfmax);

  [[nodiscard]] auto f0() const { return m_f0; }
  [[nodiscard]] auto fmax() const { return m_fmax; }
  [[nodiscard]] auto dfmax() const { return m_dfmax; }

  [[nodiscard]] auto maxSpeedIndex() const { return m_index; }

  void setF0(Frequency value)
  {
    m_f0.first = value;
    m_index    = calculateMaxSpeedIndex();
  }
  void setFmax(Frequency value)
  {
    m_fmax.first = value;
    m_index      = calculateMaxSpeedIndex();
  }
  void setdFmax(Frequency value)
  {
    m_fmax.first = value;
    m_index      = calculateMaxSpeedIndex();
  }

  /*!
   * \brief calculateMaxSpeedIndex
   * \return Индекс скорости (округлённо), при котором достигается значение fmax
   */
  [[nodiscard]] auto calculateMaxSpeedIndex() const -> SpeedIndex;

  /*!
   * \brief indexToFrequency
   * \param index
   * \return
   */
  [[nodiscard]] auto indexToFrequency(SpeedIndex index) const -> Frequency;

  /*!
   * \brief frequencyToIndex
   * \param f
   * \return на каком индексе будет достигнута заданная частота
   */
  [[nodiscard]] auto frequencyToIndex(Frequency f) const -> SpeedIndex;

  friend auto operator<<(QDebug debug, const AccelerationProfile& profile)
    -> QDebug;

private:
  Acceleration m_f0;
  Acceleration m_fmax;
  Acceleration m_dfmax;
  SpeedIndex m_index{ 0 };
};

} // namespace device

#endif // DEVICE_ENGINESPEED_H
