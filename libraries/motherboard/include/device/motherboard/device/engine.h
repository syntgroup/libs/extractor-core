#ifndef DEVICE_ENGINE_H
#define DEVICE_ENGINE_H

#include "engine_params.h"
#include "engine_speed.h"
#include "engine_status.h"
#include <QObject>
#include <memory>
#include <optional>

namespace device {

class MOTHERBOARD_DEVICE_EXPORT Engine final : public QObject
{
  Q_OBJECT

public:
  using ParamsContainer = std::unique_ptr<EngineParams>;
  using SpeedProfileContainer =
    std::unordered_map<AccelerationCurve, std::unique_ptr<AccelerationProfile>>;

  explicit Engine(uint8_t adr, QChar axis = 'X', QObject* parent = nullptr);
  [[deprecated]] explicit Engine(std::unique_ptr<EngineParams>&& params,
                                 QObject* parent = nullptr);
  [[deprecated]] Engine(std::unique_ptr<EngineParams>&& params,
                        std::unique_ptr<AccelerationProfile>&& profile_slow,
                        std::unique_ptr<AccelerationProfile>&& profile_normal,
                        std::unique_ptr<AccelerationProfile>&& profile_fast,
                        QObject* parent = nullptr);
  /*!
   * @brief Engine
   * @param params
   * @param profiles
   * @param parent
   * @note preferrable one for real use
   */
  Engine(ParamsContainer&& params,
         SpeedProfileContainer&& profiles,
         QObject* parent = nullptr);

  ~Engine() override;
  Engine(const Engine&)         = delete;
  Engine(Engine&&)              = delete;
  auto operator=(const Engine&) = delete;
  auto operator=(Engine&&)      = delete;

private:
  std::unique_ptr<EngineParams> m_params{ nullptr };
  SpeedProfileContainer m_profiles;

  [[deprecated]] SpeedIndex m_maxSpeed{ 0 }; //!< @deprecated
  Coordinate m_currentPosition{ 0 };
  Coordinate m_targetPosition{ 0 };
  Cycles m_currentMixCycle{ 0 };
  std::chrono::seconds m_currentMixTime{ 0 };

  EngineStatus m_status;
  bool m_enabled{ false };

  double m_tempAxisOffset{ 0.0 };

public:
  [[nodiscard]] auto address() const -> uint8_t;
  [[nodiscard]] auto axis() const -> QChar;
  [[nodiscard]] auto name() const -> QString;

  [[nodiscard]] auto currentPosition() const { return m_currentPosition; }
  [[nodiscard]] auto targetPosition() const { return m_targetPosition; }
  [[nodiscard]] auto currentMixCycle() const { return m_currentMixCycle; }
  [[nodiscard]] auto currentMixTime() const { return m_currentMixTime; }

  [[nodiscard]] auto repeatability() const -> Repeatability;
  [[nodiscard]] auto microstep() const -> Microstep;

  [[nodiscard]] auto homePosition() const -> Coordinate;
  [[nodiscard]] auto zeroSwitch() const -> ZeroSwitch;

  [[nodiscard]] auto homingSpeed() const -> SpeedIndex;
  [[nodiscard]] auto travelSpeed() const -> SpeedIndex;
  [[nodiscard]] auto actionSpeed() const -> SpeedIndex;
  [[nodiscard]] [[deprecated]] auto maxSpeed() const -> SpeedIndex;

  [[nodiscard]] auto status() const { return m_status; }
  [[nodiscard]] auto statusByte() const { return m_status.byte; }
  [[nodiscard]] auto enabled() const { return m_enabled; }

  [[nodiscard]] auto mmToSteps(double mms) const -> Coordinate;
  [[nodiscard]] auto stepsToMm(qint64 steps) const -> double;
  [[nodiscard]] auto stepsToMm(Coordinate steps) const -> double;

  /*!
   * @brief speedToFrequency
   * @param speed -- in mm/min
   * @return frequency of steps (microsteps) in steps/sec
   */
  [[nodiscard]] auto speedToFrequency(Speed speed) const -> Frequency;

  [[nodiscard]] auto currentStdby() const -> Current;
  [[nodiscard]] auto currentAccel() const -> Current;
  [[deprecated]] [[nodiscard]] auto axisOffset() const -> double;
  [[nodiscard]] auto tempOffset() const { return m_tempAxisOffset; }
  [[nodiscard]] auto stepsPerMmRatio() const -> double;

  [[nodiscard]] auto saveSettings() const
    -> std::reference_wrapper<EngineParams>;
  void loadSettings(std::unique_ptr<EngineParams>&& params);

  [[nodiscard]] auto accelerationProfile(AccelerationCurve mode) const
    -> std::optional<std::reference_wrapper<AccelerationProfile>>;

signals:
  // signals uses underlying types of enums and strong types so that sent values
  // can be set into widgets without additional extractions/casting

  void repeatabilityChanged(
    strong::underlying_type_t<device::Repeatability> repeatability);
  void microstepChanged(std::underlying_type_t<device::Microstep> microstep);
  void maxSpeedChanged(strong::underlying_type_t<device::SpeedIndex> max_speed);
  void currentPositionChanged(
    strong::underlying_type_t<device::Coordinate> current_position);
  void homePositionChanged(
    strong::underlying_type_t<device::Coordinate> home_position);
  void targetPositionChanged(
    strong::underlying_type_t<device::Coordinate> target_position);
  void currentMixCycleChanged(
    strong::underlying_type_t<device::Cycles> current_mix_cycle);
  void currentMixTimeChanged(std::chrono::seconds current_mix_time);
  void enabledChanged(bool enabled);
  void axisOffsetChanged(double axis_offset);
  void stepsPerMmRatioChanged(double new_value);
  void currentScalingAccelChanged(
    std::underlying_type_t<device::Current> accel_current);
  void currentScalingStdbyChanged(
    std::underlying_type_t<device::Current> stdby_current);

  void startMixingTime(std::chrono::seconds time);
  void stopMixing();

  // --

  void requestMicrostepPush(QChar axis, device::Microstep microstep);
  void requestHomePositionPush(QChar axis, device::Coordinate position);
  void requestCurrentScalingPush(QChar axis,
                                 device::Current accel_current,
                                 device::Current accel_standby);

  void requestOffsetSave(QChar axis, double offset);

  void requestFrequencyPush(QChar axis,
                            device::AccelerationCurve curve,
                            device::Acceleration value);
  void frequencyChanged(device::AccelerationCurve curve,
                        device::Acceleration value);

public slots:
  void setActionSpeed(device::SpeedIndex new_speed);
  void setTravelSpeed(device::SpeedIndex new_speed);
  void setHomingSpeed(device::SpeedIndex new_speed);
  [[deprecated]] void setMaxSpeed(device::SpeedIndex new_max_speed);

  void setCurrentHolding(device::Current new_current_holding);
  void setCurrentAccel(device::Current new_current_accel);

  void setMicrostep(device::Microstep new_microstep);
  void setRepeatability(device::Repeatability new_repeatability);

  [[deprecated]] void setAxisOffset(double new_axis_offset);
  void setTempOffset(double new_value);
  void resetTempOffset();
  void setStepsPerMmRatio(double new_value);
  void setHomePosition(device::Coordinate new_home_position);

  void setEnabled(bool new_enabled);
  void setStatus(device::EngineStatus new_status) { m_status = new_status; }
  void setStatus(uint8_t new_status);

  void setCurrentPosition(device::Coordinate new_current_position);
  void setTargetPosition(device::Coordinate new_target_position);
  void setCurrentMixCycle(device::Cycles new_current_mix_cycle);
  void setCurrentMixTime(std::chrono::seconds new_current_mix_time);
};

} // namespace device

#endif // DEVICE_ENGINE_H
