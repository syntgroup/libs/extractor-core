#ifndef ENGINE_PARAMS_H
#define ENGINE_PARAMS_H

#include "engine_values.h"
#include <QString>

namespace device {

struct MOTHERBOARD_DEVICE_EXPORT EngineParams
{
  uint8_t address{ 1 };
  QChar axis;
  QString name;

  Coordinate homePosition{ 0 };

  SpeedIndex travelSpeed{ 0.0 };
  SpeedIndex homingSpeed{ 0.0 };
  SpeedIndex actionSpeed{ 0.0 };

  Repeatability repeatability{ 1 };

  double axisOffset{ 0.0 };
  double stepsPerMmRatio{ 0.0 };

  Microstep microstep{ Microstep::Step_1_1 };
  ZeroSwitch zeroSwitch{ ZeroSwitch::ZeroIsSw1 };
  Current currentStandby{ Current::Current_7_32 };
  Current currentMoving{ Current::Current_13_32 };
};

} // namespace device

#endif // ENGINE_PARAMS_H
