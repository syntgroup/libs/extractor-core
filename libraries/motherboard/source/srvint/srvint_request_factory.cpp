#include "srvint/srvint_request_factory.h"

#include "srvint/srvint_packet.h"

namespace protocol::srvint {

QByteArray
SrvIntRequestFactory::create(PacketID pid,
                             Command cmd,
                             const Payload& payload) const
{
  const SrvIntPacket packet{ pid, address_device, cmd, payload };
  return packet.encode();
}

QByteArray
SrvIntRequestFactory::getParam(PacketID pid, const Payload& payload) const
{
  const SrvIntPacket packet{ pid, address_device, Command::GetParam, payload };
  return packet.encode();
}

QByteArray
SrvIntRequestFactory::setParam(PacketID pid, const Payload& payload) const
{
  const SrvIntPacket packet{ pid, address_device, Command::SetParam, payload };
  return packet.encode();
}

} // namespace protocol::srvint
