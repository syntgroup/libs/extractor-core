#include "srvint/srvint_response_factory.h"

#include "srvint/srvint_packet.h"

namespace protocol::srvint {

nonstd::expected<std::unique_ptr<SrvIntPacket>, Error>
SrvIntResponseFactory::create(const QByteArray& data) const
{
  if (data.isEmpty() || data.length() < FPPacketHash + 1) {
    return nonstd::make_unexpected(Error::NoData);
  }

  const auto start = static_cast<uint8_t>(data.at(FPStartByte));
  if (start != start_byte) {
    return nonstd::make_unexpected(Error::StartByteMismatch);
  }

  const Address address{ data.at(FPSAdr) };
  if (address != address_master) {
    return nonstd::make_unexpected(Error::AddressMismatch);
  }

  auto result = std::make_unique<SrvIntPacket>();
  result->setMasterAddress(address);
  result->setPacketId(PacketID{ data.at(FPPacketId) });
  result->setCommandCode(static_cast<Command>(data.at(FPCommand)));
  result->setDataCount(static_cast<uint8_t>(data.at(FPDataCount)));

  // check for preamble hash
  const Hash preamble_hash{ data.at(FPPacketHash) };
  if (!result->setPreambleHash(preamble_hash)) {
    return nonstd::make_unexpected(Error::PacketHashMismatch);
  }

  // no payload but hashes are all ok
  if (data.size() < FPPacketData + 1 && result->dataCount() == 0) {
    return result;
  }

  // check if actual payload bytes count equals promised
  const auto observed_payload_size =
    std::distance(data.begin() + FPPacketData, data.end());
  if (observed_payload_size < result->dataCount()) {
    return nonstd::make_unexpected(Error::NoExpectedPayload);
  }

  // bad check
#if 1
  if (observed_payload_size > result->dataCount() + 1) {
    return nonstd::make_unexpected(Error::PromisedNoPayload);
  }
#endif

  std::transform(data.cbegin() + FPPacketData,
                 data.cbegin() + FPPacketData + result->dataCount(),
                 std::back_inserter(result->payload()),
                 [](auto byte) { return byte; });

  const Hash data_hash{ data.at(FPPacketData + result->dataCount()) };
  if (!result->setDataHash(data_hash)) {
    return nonstd::make_unexpected(Error::DataHashMismatch);
  }

  return result;
}

} // namespace protocol::srvint
