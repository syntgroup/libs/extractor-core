#include "srvint/srvint_packet.h"

#include <QDataStream>
#include <algorithm>
#include <bitset>
#include <utility>

namespace protocol::srvint {

SrvIntPacket::SrvIntPacket(PacketID pid,
                           Address adr,
                           Command cmd,
                           const Payload& payload)
  : m_address{ adr }
  , m_pid{ pid }
  , m_command{ cmd }
  , m_dataCount{ static_cast<uint8_t>(payload.empty() ? 0 : payload.size()) }
  , m_cmdHash{ calculateCmdHash() }
  , m_data{ payload }
  , m_dataHash{ calculateDataHash() }
  , m_async{ m_command == Command::AsyncDataTransfer }
{
}

QByteArray
SrvIntPacket::encode() const
{
  QByteArray result;
  QDataStream stream{ &result, QIODevice::WriteOnly };
  stream << start_byte << m_address.value_of() << createPacketId().value_of()
         << static_cast<std::underlying_type_t<Command>>(m_command)
         << m_dataCount << calculateCmdHash().value_of();

  if (!m_data.empty()) {
    result += payloadBytes();
    result += calculateDataHash().value_of();
  }

  return result;
}

QByteArray
SrvIntPacket::payloadBytes() const
{
  if (m_data.empty()) {
    return {};
  }

  QByteArray result;
  QDataStream stream{ &result, QIODevice::WriteOnly };
  for (auto iter : m_data) {
    stream << iter;
  }

  return result;
}

bool
SrvIntPacket::setPreambleHash(Hash hash)
{
  if (hash != calculateCmdHash()) {
    return false;
  }

  m_cmdHash = hash;
  return true;
}

bool
SrvIntPacket::setDataHash(Hash hash)
{
  if (hash != calculateDataHash()) {
    return false;
  }

  m_dataHash = hash;
  return true;
}

Hash
SrvIntPacket::calculateCmdHash() const
{
  const std::array result{ m_address.value_of(),
                           m_pid.value_of(),
                           static_cast<std::underlying_type_t<Command>>(
                             m_command),
                           m_dataCount };
  return Hash{ std::accumulate(
    result.cbegin(), result.cend(), (uint8_t)0, std::bit_xor<>()) };
}

Hash
SrvIntPacket::calculateDataHash() const
{
  return Hash{ std::accumulate(
    m_data.cbegin(), m_data.cend(), (uint8_t)0, std::bit_xor<>()) };
}

PacketID
SrvIntPacket::createPacketId() const
{
  std::bitset<std::numeric_limits<uint8_t>::digits> pid{ m_pid.value_of() };
  pid.set(PIPAsync, m_async);
  return PacketID{ pid.to_ulong() };
}

} // namespace protocol::srvint
