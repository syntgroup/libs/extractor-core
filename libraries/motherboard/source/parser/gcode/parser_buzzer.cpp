#include "motherboard/gcode/parser_buzzer.h"

#include "motherboard/cmd/power_set_buzzer_operation.h"
#include "serial/utils/counter.h"

namespace gcode {

ParseResult
ParserBuzzerOn::generateImpl(const QString&) const
{
  // syntax:
  // M501

  auto operation = std::make_unique<cmd::motherboard::PowerSetBuzzerOperation>(
    utils::Counter::tick(), /*enable=*/true);

  ResultType result;
  result.reserve(1);
  result.push_back(operation.release());

  return result;
}

// -----------------------------------------------------------------------------

ParseResult
ParserBuzzerOff::generateImpl(const QString&) const
{
  // syntax:
  // M500

  auto operation = std::make_unique<cmd::motherboard::PowerSetBuzzerOperation>(
    utils::Counter::tick(), /*enable=*/false);

  ResultType result;
  result.reserve(1);
  result.push_back(operation.release());

  return result;
}

} // namespace gcode
