/*!
 * @file parser_linear_move.cpp
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 * @brief
 */

#include "motherboard/gcode/parser_linear_move.h"

#include "engine_process_support.h"
#include "engine_speed_support.h"
#include "motherboard/cmd/button_reset_cached.h"
#include "motherboard/cmd/multi_goto_absolute_operation.h"
#include "motherboard/cmd/multi_goto_two_axis_operation.h"
#include "motherboard/device/engine.h"
#include "motherboard/device/engine_values.h"
#include "motherboard/device/engines.h"
#include "utils/constexpr_latin_string.h"
#include <QRegularExpression>

namespace {
constexpr auto max_allowed{ 2 };
constexpr auto all_engines{ static_cast<size_t>(device::AxisAddress::Grabber) +
                            1 };

constexpr auto axis_pattern{ R"(([a-eg-zA-EG-Z])(-?\d{1,9}(\.\d{1,3})?))" };
constexpr auto feedrate_pattern{ R"(([fF])(\d*)([\-\+]?))" };

const QRegularExpression find_axis{ axis_pattern };
const QRegularExpression find_feedrate{ feedrate_pattern };
} // namespace

namespace gcode {

auto
ParserLinearMove::generateImpl(const QString& block) const -> ParseResult
{
  auto axis_mentioned = find_axis.globalMatch(block);
  if (!axis_mentioned.hasNext()) {
    return nonstd::make_unexpected(
      QStringLiteral("incorrect syntax of G1 command -- no "
                     "operands provided or incorrect axis name"));
  }

  Targets targets;
  targets.reserve(all_engines);
  while (axis_mentioned.hasNext()) {
    const auto param = axis_mentioned.next();
    targets.emplace_back(param.captured(BPAddres),
                         param.captured(BPValue).toDouble());
  }

  // check for upper bound
  if (targets.size() > max_allowed) {
    const auto targets_size = QString::number(targets.size());
    return nonstd::make_unexpected(
      QStringLiteral("cant move more than 2 axis simultaneously, requested: %2")
        .arg(targets_size));
  }

  // check for negative targets
  if (std::any_of(targets.begin(), targets.end(), utils::is_negative)) {
    return nonstd::make_unexpected(
      QStringLiteral("requested negative absolute position: %2").arg(block));
  }

  // check for duplicates
  const auto dup =
    std::adjacent_find(targets.begin(), targets.end(), utils::are_duplicates);
  if (dup != targets.end()) {
    return nonstd::make_unexpected(
      QStringLiteral("two identical axises requested"));
  }

  // process axises
  auto result = targets.size() == 1 ? parseSingle(targets, block)
                                    : parseMulti(targets, block);

  // add Reset Diag Sensor operation if grabber axis was mentioned
  if (result.has_value() &&
      std::any_of(targets.begin(), targets.end(), utils::is_grabber)) {
    if (auto sensor = m_diagSensor.lock()) {
      auto reset_cache = std::make_unique<cmd::meta::ResetCachedButton>(
        sensor.get(), /*state=*/false);
      result->push_back(reset_cache.release());
    } else {
      return nonstd::make_unexpected("error accessing sensor");
    }
  }
  return result;
}

auto
ParserLinearMove::parseSingle(const Targets& targets,
                              const QString& block) const -> ParseResult
{
  const auto engines = m_engines.lock();
  if (engines == nullptr) {
    return nonstd::make_unexpected(QStringLiteral("error accessing engines"));
  }

  auto axis    = std::get<QString>(targets.front());
  auto* engine = engines->engineByAxis(axis);
  if (engine == nullptr) {
    return nonstd::make_unexpected(
      QStringLiteral("unknown axis requested: %2\n%3").arg(axis, block));
  }

  const auto feedrate_match = find_feedrate.match(block);
  const auto curve          = utils::profile_matcher(feedrate_match);
  const auto feedrate =
    utils::feedrate_matcher(feedrate_match, *engine, curve, m_legacyMode);
  if (!feedrate.has_value()) {
    return nonstd::make_unexpected(QStringLiteral("no feedrate mentioned"));
  }

  auto operation = std::make_unique<cmd::multi::GotoAbsoluteOperation>(
    engine,
    engine->mmToSteps(std::get<double>(targets.front())),
    *feedrate,
    curve);

  ResultType result;
  result.reserve(1);
  result.push_back(operation.release());
  return result;
}

auto
ParserLinearMove::parseMulti(const Targets& targets, const QString& block) const
  -> ParseResult
{
  const auto engines = m_engines.lock();
  if (!engines) {
    return nonstd::make_unexpected(QStringLiteral("error accessing engines"));
  }

  if (std::any_of(targets.cbegin(),
                  targets.cend(),
                  [engines](const Targets::value_type& target) {
                    auto* engine = engines->engineByAxis(target.first);
                    return engine == nullptr;
                  })) {
    return nonstd::make_unexpected(
      QStringLiteral("Error accessing internal memory"));
  }

  using Operation = cmd::multi::GotoTwoAxisOperation;
  Operation::Params params;
  params.reserve(targets.size());

  const auto feedrate_match = find_feedrate.match(block);
  const auto curve_mode     = utils::profile_matcher(feedrate_match);

  for (const auto& target : targets) {
    auto* engine  = engines->engineByAxis(std::get<QString>(target));
    auto feedrate = utils::feedrate_matcher(
      feedrate_match, *engine, curve_mode, m_legacyMode);
    if (!feedrate.has_value()) {
      return nonstd::make_unexpected(
        QStringLiteral("Error calculating speed profile for engine: %1")
          .arg(engine->name()));
    }

    params.emplace_back(
      engine, engine->mmToSteps(std::get<double>(target)), *feedrate);
  }

  auto operation = std::make_unique<Operation>(std::move(params), curve_mode);

  ResultType result;
  result.reserve(1);
  result.push_back(operation.release());
  return result;
}

} // namespace gcode
