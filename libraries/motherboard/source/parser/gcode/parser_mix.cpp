#include "motherboard/gcode/parser_mix.h"

#include "engine_speed_support.h"
#include "motherboard/cmd/multi_mix_n_seconds_operation.h"
#include "motherboard/cmd/multi_mix_n_times_operation.h"
#include "motherboard/device/engine.h"
#include "motherboard/device/engine_values.h"
#include "motherboard/device/engines.h"
#include <QRegularExpression>
#include <QRegularExpressionMatch>

using namespace std::literals;

namespace gcode {

auto
ParserMixCycles::generateImpl(const QString& block) const -> ParseResult
{
  // G56 [<axis><amplitude>] [P<cycles>] [F<feedrate>]
  // параметр F опциональный, если его нет, то фигачим на action_speed
  if (block.isEmpty()) {
    return nonstd::make_unexpected(QStringLiteral(
      "incorrect syntax of G56 command // no operands provided"));
  }

  const QRegularExpression find_axis{ R"(([a-eg-or-zA-EG-OR-Z])(\d*\.?\d+))" };
  const QRegularExpression find_cycles{ R"(([pP])(\d*))" };
  const QRegularExpression find_feedrate{ R"(([fF])(\d*))" };

  const auto axis_mentioned     = find_axis.match(block);
  const auto cycles_mentioned   = find_cycles.match(block);
  const auto feedrate_mentioned = find_feedrate.match(block);

  if (!cycles_mentioned.hasMatch()) {
    return nonstd::make_unexpected(
      QStringLiteral("incorrect syntax of G56 command // no cycles mentioned"));
  }

  if (!axis_mentioned.hasMatch()) {
    return nonstd::make_unexpected(
      QStringLiteral("incorrect syntax of G56 command // no axis mentioned"));
  }

  const auto engines = _engines.lock();
  if (!engines) {
    return nonstd::make_unexpected(QStringLiteral("error accessing engines"));
  }

  auto axis    = axis_mentioned.captured(BPAddres);
  auto* engine = engines->engineByAxis(axis);
  if (engine == nullptr) {
    return nonstd::make_unexpected(
      QStringLiteral("unknown axis requested: %1").arg(axis));
  }

  device::Cycles cycles{ cycles_mentioned.captured(BPValue).toUInt() };
  device::Coordinate amplitude{ engine->mmToSteps(
    axis_mentioned.captured(BPValue).toDouble()) };

  auto curve_mode = utils::profile_matcher(feedrate_mentioned);
  auto feedrate   = utils::feedrate_matcher(
                    feedrate_mentioned, *engine, curve_mode, _legacyMode)
                    .value_or(engine->actionSpeed());
  auto operation = std::make_unique<cmd::multi::MixNCyclesOperation>(
    engine, std::move(cycles), std::move(amplitude), std::move(feedrate));

  ResultType result;
  result.reserve(1);
  result.push_back(operation.release());

  return result;
}

auto
ParserMixTime::generateImpl(const QString& block) const -> ParseResult
{
  // G57 [<axis><amplitude>] [S<time in seconds>] [P<time in milliseconds>]
  // [F<feedrate>] параметр F опциональный, если его нет, то фигачим на
  // action_speed

  const QRegularExpression find_axis{ R"(([a-eg-or-zA-EG-OR-Z])(\d*\.?\d+))" };
  const QRegularExpression find_seconds{ R"(([sS])(\d*))" };
  const QRegularExpression find_milliseconds{ R"(([pP])(\d*))" };
  const QRegularExpression find_feedrate{ R"(([fF])(\d*))" };

  const auto axis_mentioned         = find_axis.match(block);
  const auto feedrate_mentioned     = find_feedrate.match(block);
  const auto seconds_mentioned      = find_seconds.match(block);
  const auto milliseconds_mentioned = find_milliseconds.match(block);

  if (!axis_mentioned.hasMatch()) {
    return nonstd::make_unexpected(
      QStringLiteral("incorrect syntax of G57 command // no axis mentioned"));
  }

  if (!(seconds_mentioned.hasMatch() || milliseconds_mentioned.hasMatch())) {
    return nonstd::make_unexpected(
      QStringLiteral("incorrect syntax of G57 command // no time mentioned"));
  }

  const auto engines = _engines.lock();
  if (!engines) {
    return nonstd::make_unexpected(QStringLiteral("error accessing engines"));
  }

  auto axis    = axis_mentioned.captured(BPAddres);
  auto* engine = engines->engineByAxis(axis);
  if (engine == nullptr) {
    return nonstd::make_unexpected(
      QStringLiteral("unknown axis requested: %1").arg(axis));
  }

  bool conversion_ok = true;
  auto time = [&conversion_ok](const auto& seconds, const auto& milliseconds)
    -> std::optional<std::chrono::seconds> {
    if (seconds.hasMatch()) {
      const auto value = seconds.captured(BPValue).toDouble(&conversion_ok);
      if (!conversion_ok) {
        return std::nullopt;
      }
      return std::chrono::duration_cast<std::chrono::seconds>(
        std::chrono::duration<double>(value));
    }
    if (milliseconds.hasMatch()) {
      const std::chrono::milliseconds value{
        milliseconds.captured(BPValue).toUInt(&conversion_ok)
      };
      if (!conversion_ok) {
        return std::nullopt;
      }
      return std::chrono::duration_cast<std::chrono::seconds>(value);
    }
    return std::nullopt;
  }(seconds_mentioned, milliseconds_mentioned);

  if (!time.has_value()) {
    return nonstd::make_unexpected(
      QStringLiteral("incorrect syntax of G57 command // no time mentioned"));
  }

  device::Coordinate amplitude{ engine->mmToSteps(
    axis_mentioned.captured(BPValue).toDouble()) };

  auto curve_mode = utils::profile_matcher(feedrate_mentioned);
  auto feedrate   = utils::feedrate_matcher(
                    feedrate_mentioned, *engine, curve_mode, _legacyMode)
                    .value_or(engine->actionSpeed());
  auto operation = std::make_unique<cmd::multi::MixNSecondsOperation>(
    engine, std::move(time.value()), std::move(amplitude), std::move(feedrate));

  ResultType result;
  result.reserve(1);
  result.push_back(operation.release());
  return result;
}

} // namespace gcode
