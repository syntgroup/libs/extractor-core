/*!
 * @file parser_auto_home.cpp
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 * @brief
 */

#include "motherboard/gcode/parser_auto_home.h"

#include "motherboard/cmd/multi_goto_home_position_operation.h"
#include "motherboard/device/engine.h"
#include "motherboard/device/engines.h"
#include <QRegularExpression>
#include <QRegularExpressionMatch>

namespace {
constexpr std::array axises{ device::AxisAddress::Magnets,
                             device::AxisAddress::Sleeves,
                             device::AxisAddress::Column,
                             device::AxisAddress::Shield,
                             device::AxisAddress::Grabber };
} // namespace

namespace gcode {

auto
ParserAutoHome::generateImpl(const QString& block) const -> ParseResult
{
  const QRegularExpression find_axis{ "([a-zA-Z])" };
  auto axis_mentioned = find_axis.globalMatch(block);

  const auto engines = m_engines.lock();
  if (!engines) {
    return nonstd::make_unexpected(QStringLiteral("error accessing engines"));
  }

  ResultType result;
  result.reserve(axises.size());

  if (!axis_mentioned.hasNext()) {
    // по идее, эти имена осей проверять не нужно
    std::transform(std::cbegin(axises),
                   std::cend(axises),
                   std::back_inserter(result),
                   [&engines](auto axis) {
                     auto operation =
                       std::make_unique<cmd::multi::GotoHomeOperation>(
                         engines->engineByName(axis));
                     return operation.release();
                   });

    return result;
  }

  while (axis_mentioned.hasNext()) {
    const auto param     = axis_mentioned.next();
    const auto axis_name = param.captured(BPAddres);

    if (auto* engine = engines->engineByAxis(axis_name); engine != nullptr) {
      auto operation = std::make_unique<cmd::multi::GotoHomeOperation>(engine);
      result.push_back(operation.release());
    } else {
      return nonstd::make_unexpected(
        QStringLiteral("unknown axis requested: %2").arg(axis_name));
    }
  }

  return result;
}

} // namespace gcode
