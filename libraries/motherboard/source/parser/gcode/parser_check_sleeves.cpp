#include "motherboard/gcode/parser_check_sleeves.h"

#include "common/discrete_input.h"
#include "motherboard/cmd/button_report_cached.h"
#include "motherboard/cmd/button_request_diag_opt.h"
#include "motherboard/cmd/multi_goto_absolute_operation.h"
#include "motherboard/device/engine.h"
#include "motherboard/device/engines.h"
#include "serial/utils/counter.h"

namespace {
constexpr auto reference_height{ 3.0 };
constexpr device::Coordinate magnets_zero{ 0 };
} // namespace

namespace gcode {

auto
ParserCheckSleeves::generateImpl(const QString& /*block*/) const -> ParseResult
{
  auto engines = m_engines.lock();
  auto sensors = m_diag_sensor.lock();
  if (!engines || !sensors) {
    return nonstd::make_unexpected(QStringLiteral("internal error"));
  }

  std::unique_ptr<cmd::AbstractOperation> operation;

  ResultType result;
  result.reserve(3);

  // if magnets are below DiagOpt sensing point -- move them up forcefully
  auto magnets = engines->engineByName(device::AxisAddress::Magnets);
  if (magnets->stepsToMm(magnets->currentPosition()) > reference_height) {
    operation = std::make_unique<cmd::multi::GotoAbsoluteOperation>(
      magnets, magnets_zero, magnets->travelSpeed());
    result.push_back(operation.release());
  }

  auto sleeves = engines->engineByName(device::AxisAddress::Sleeves);

  operation = std::make_unique<cmd::multi::GotoAbsoluteOperation>(
    sleeves, sleeves->mmToSteps(reference_height), sleeves->travelSpeed());
  result.push_back(operation.release());

  operation = std::make_unique<cmd::motherboard::DiagOptStateOperation>(
    utils::Counter::tick(), sensors.get());
  result.push_back(operation.release());

  return result;
}

auto
ParserJustCheck::generateImpl([[maybe_unused]] const QString& block) const
  -> ParseResult
{
  return ResultType{ new cmd::motherboard::DiagOptStateOperation{
    utils::Counter::tick(), m_diag_button.lock().get() } };
}

auto
ParserSleevesCache::generateImpl([[maybe_unused]] const QString& block) const
  -> ParseResult
{
  return ResultType{ new cmd::meta::ReportCachedButton{
    m_diagButton.lock().get() } };
}

} // namespace gcode
