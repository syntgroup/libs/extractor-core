/*!
 * @file engine_process_support.h
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 * @brief
 */

#ifndef ENGINE_PROCESS_SUPPORT_H
#define ENGINE_PROCESS_SUPPORT_H

#include <QString>
#include <vector>

namespace gcode::utils {

/*!
 * @brief checks if presented chunk is referring to C axix
 * @details is to be used with ParserLinearMove and ParserRapidMove
 */
constexpr auto is_grabber = [](const std::pair<QString, double>& target) {
  return target.first.toUpper() == 'C';
};

using Targets = std::vector<std::pair<QString, double>>;

/*!
 * @brief checks if presented chunk is requesting movement to negative position
 */
constexpr auto is_negative = [](const Targets::value_type& target) {
  return std::get<double>(target) < 0;
};

/*!
 * @brief checks if two chunks are requesting same axis movement
 */
constexpr auto are_duplicates = [](const Targets::value_type& target1,
                                   const Targets::value_type& target2) {
  return std::get<QString>(target1) == std::get<QString>(target2);
};

} // namespace gcode::utils

#endif // ENGINE_PROCESS_SUPPORT_H