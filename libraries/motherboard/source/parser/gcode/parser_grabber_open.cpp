#include "motherboard/gcode/parser_grabber_open.h"

#include "motherboard/cmd/button_reset_cached.h"
#include "motherboard/cmd/multi_goto_absolute_operation.h"
#include "motherboard/device/engine.h"
#include "motherboard/device/engines.h"

namespace gcode {

auto
ParserGrabberOpen::generateImpl(const QString& block) const -> ParseResult
{
  if (m_target < 0) {
    return nonstd::make_unexpected(
      QStringLiteral("Invalid target value: %1").arg(m_target));
  }

  const auto engines = m_engines.lock();
  const auto sensor  = m_diagSensor.lock();
  if (!engines || !sensor) {
    return nonstd::make_unexpected(
      QStringLiteral("Error accessing internal memory"));
  }

  std::unique_ptr<cmd::AbstractOperation> operation;

  ResultType result;
  result.reserve(2);

  auto sleeves = engines->engineByName(device::AxisAddress::Sleeves);

  operation = std::make_unique<cmd::multi::GotoAbsoluteOperation>(
    sleeves, sleeves->mmToSteps(m_target), sleeves->travelSpeed());
  result.push_back(operation.release());

  operation = std::make_unique<cmd::meta::ResetCachedButton>(sensor.get());
  result.push_back(operation.release());

  return result;
}

} // namespace gcode
