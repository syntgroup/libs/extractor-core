/*!
 * @file engine_speed_support.h
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 * @brief This file contains functions that are used to support both modern and
 * legacy mode of speed index calculation from parsed protocol string
 */

#ifndef ENGINE_SPEED_SUPPORT_H
#define ENGINE_SPEED_SUPPORT_H

#include "motherboard/device/engine_values.h"
#include <optional>

class QRegularExpressionMatch;

namespace device {
class Engine;
enum class AccelerationCurve : int;
} // namespace device

namespace gcode::utils {

/*!
 * @brief Maps speed suffix symbols to kind of AccelerationCurve:
 *  * '+' == fast;
 *  * '-' == slow;
 *  * ' ' (empty, no suffix) == normal
 * @param match
 * @return
 */
[[nodiscard]] auto
profile_matcher(const QRegularExpressionMatch& match)
  -> device::AccelerationCurve;

/*!
 * @brief Create movement feedrate
 * @param match
 * @param engine
 * @param curve
 * @param legacy
 */
[[nodiscard]] auto
feedrate_matcher(const QRegularExpressionMatch& match,
                 const device::Engine& engine,
                 device::AccelerationCurve curve,
                 device::SpeedMode legacy) -> std::optional<device::SpeedIndex>;

} // namespace gcode::utils

#endif // ENGINE_SPEED_SUPPORT_H
