/*!
 * @file parser_rapid_move.cpp
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 * @brief
 */

#include "motherboard/gcode/parser_rapid_move.h"

#include "engine_process_support.h"
#include "motherboard/cmd/button_reset_cached.h"
#include "motherboard/cmd/multi_goto_absolute_operation.h"
#include "motherboard/cmd/multi_goto_two_axis_operation.h"
#include "motherboard/device/engine.h"
#include "motherboard/device/engine_values.h"
#include "motherboard/device/engines.h"
#include <QRegularExpression>
#include <QRegularExpressionMatch>

namespace {
constexpr auto max_allowed{ 2 };
constexpr auto all_engines{ static_cast<size_t>(device::AxisAddress::Grabber) +
                            1 };
} // namespace

namespace gcode {

auto
ParserRapidMove::generateImpl(const QString& block) const -> ParseResult
{
  const QRegularExpression find_axis{ R"(([a-zA-Z])(\d*\.?\d+))" };
  auto axis_mentioned = find_axis.globalMatch(block);

  if (!axis_mentioned.hasNext()) {
    return nonstd::make_unexpected(
      QStringLiteral("incorrect syntax of G0 command -- no "
                     "operands provided or incorrect axis name\n%2")
        .arg(block));
  }

  Targets requests;
  requests.reserve(all_engines);
  while (axis_mentioned.hasNext()) {
    const auto param      = axis_mentioned.next();
    const auto axis       = param.captured(BPAddres);
    const auto coordinate = param.captured(BPValue).toDouble();

    requests.emplace_back(axis, coordinate);
  }

  // check for upper bound
  if (requests.size() > max_allowed) {
    return nonstd::make_unexpected(
      QStringLiteral("cant move more than 2 axis simultaneously, requested: %2")
        .arg(requests.size()));
  }

  // check for negative targets
  if (std::any_of(requests.begin(), requests.end(), utils::is_negative)) {
    return nonstd::make_unexpected(
      QStringLiteral("requested negative absolute position: %2").arg(block));
  }

  if (std::adjacent_find(requests.begin(),
                         requests.end(),
                         utils::are_duplicates) != requests.end()) {
    return nonstd::make_unexpected(
      QStringLiteral("two identical axises requested"));
  }

  auto result = requests.size() == 1 ? ParseSingle(requests, block)
                                     : ParseMulti(requests, block);

  if (result.has_value() &&
      std::any_of(requests.begin(), requests.end(), utils::is_grabber)) {
    if (auto sensor = m_diagSensor.lock()) {
      auto reset_cache = std::make_unique<cmd::meta::ResetCachedButton>(
        sensor.get(), /*state=*/false);
      result->push_back(reset_cache.release());
    } else {
      return nonstd::make_unexpected("error accessing sensor");
    }
  }
  return result;
}

auto
ParserRapidMove::ParseSingle(const Targets& targets,
                             [[maybe_unused]] const QString& block) const
  -> ParseResult
{
  const auto engines = m_engines.lock();
  if (engines == nullptr) {
    return nonstd::make_unexpected(QStringLiteral("error accessing engines"));
  }

  auto axis    = targets.front().first;
  auto* engine = engines->engineByAxis(axis);

  if (engine == nullptr) {
    return nonstd::make_unexpected(
      QStringLiteral("unknown axis requested: %2\n%3").arg(axis, block));
  }

  auto operation = std::make_unique<cmd::multi::GotoAbsoluteOperation>(
    engine,
    engine->mmToSteps(targets.cbegin()->second),
    engine->travelSpeed(),
    m_accelerationCurve,
    m_legacyMode);

  ResultType result;
  result.reserve(1);
  result.push_back(operation.release());

  return result;
}

auto
ParserRapidMove::ParseMulti(const Targets& targets,
                            [[maybe_unused]] const QString& block) const
  -> ParseResult
{
  using Operation = cmd::multi::GotoTwoAxisOperation;

  const auto engines = m_engines.lock();
  if (!engines) {
    return nonstd::make_unexpected(QStringLiteral("error accessing engines"));
  }
  const auto check_null = [engines](const Targets::value_type& request) {
    return engines->engineByAxis(request.first) == nullptr;
  };

  Operation::Params params;
  params.reserve(targets.size());

  if (std::any_of(targets.begin(), targets.end(), check_null)) {
    return nonstd::make_unexpected(
      QStringLiteral("Error accessing internal memory"));
  }

  std::transform(targets.begin(),
                 targets.end(),
                 std::back_inserter(params),
                 [engines](const Targets::value_type& request) {
                   auto* engine =
                     engines->engineByAxis(std::get<QString>(request));
                   return std::make_tuple(engine,
                                          engine->mmToSteps(request.second),
                                          engine->travelSpeed());
                 });
  auto operation =
    std::make_unique<Operation>(std::move(params), m_accelerationCurve);

  ResultType result;
  result.reserve(1);
  result.push_back(operation.release());
  return result;
}

} // namespace gcode
