#include "motherboard/gcode/parser_rgb_leds.h"

#include "motherboard/cmd/rgb_led_set_operation.h"
#include "motherboard/device/motherboard_values.h"
#include "motherboard/device/rgb_leds.h"
#include "serial/utils/counter.h"
#include <QRegularExpression>

namespace {
constexpr auto default_color = Qt::red;
constexpr auto map_color     = [](uint32_t v) -> Qt::GlobalColor {
  switch (v) {
    case 0:
      return Qt::black;
    case 1:
      return Qt::blue;
    case 2:
      return Qt::green;
    case 3:
      return Qt::cyan;
    case 4:
      return Qt::red;
    case 5:
      return Qt::magenta;
    case 6:
      return Qt::yellow;
    default:
      return Qt::white;
  }
};
} // namespace

namespace gcode {
auto
ParserRgbDisableLeds::generateImpl(const QString&) const -> ParseResult
{
  //  syntax
  //  M300
  if (auto leds = m_leds.lock()) {
    auto operation = std::make_unique<cmd::motherboard::RgbLedSetOperation>(
      utils::Counter::tick(), leds.get(), std::vector{ 5, Qt::black });

    ResultType result;
    result.reserve(1);
    result.push_back(operation.release());

    return result;
  }
  return nonstd::make_unexpected("");
}

// -----------------------------------------------------------------------------

auto
ParserRgbEnableLeds::generateImpl(const QString& block) const -> ParseResult
{
  //  syntax
  //  M301 [C<color>]
  const QRegularExpression find_color{ QStringLiteral("([cC])([1-8]{1})") };
  const auto color_mentioned = find_color.match(block);
  const auto casted_color =
    map_color(color_mentioned.captured(BPValue).toUInt());
  if (auto leds = m_leds.lock()) {
    auto operation = std::make_unique<cmd::motherboard::RgbLedSetOperation>(
      utils::Counter::tick(),
      leds.get(),
      std::vector{ 5,
                   color_mentioned.hasMatch() ? casted_color : default_color });

    ResultType result;
    result.reserve(1);
    result.push_back(operation.release());
    return result;
  }
  return nonstd::make_unexpected("");
}

// -----------------------------------------------------------------------------

auto
ParserRgbSetLeds::generateImpl(const QString& block) const -> ParseResult
{
  // syntax
  // M302 [C<color>] ...
  const QRegularExpression find_color{ QStringLiteral("([cC])([0-7]{1})") };
  auto color_mentioned = find_color.globalMatch(block);

  if (!color_mentioned.hasNext()) {
    return nonstd::make_unexpected(
      QStringLiteral("line: %1\nCommand M302 invalid syntax"));
  }

  auto leds = m_leds.lock();
  if (!leds) {
    return nonstd::make_unexpected("");
  }

  std::vector<Qt::GlobalColor> colors;
  colors.reserve(leds->count());

  while (color_mentioned.hasNext()) {
    const auto param = color_mentioned.next();
    colors.push_back(map_color(param.captured(BPValue).toUInt()));
  }
  if (colors.size() != leds->count()) {
    return nonstd::make_unexpected(
      QStringLiteral("mentioned less than 5 colors"));
  }
  auto operation = std::make_unique<cmd::motherboard::RgbLedSetOperation>(
    utils::Counter::tick(), leds.get(), std::move(colors));

  ResultType result;
  result.reserve(1);
  result.push_back(operation.release());

  return result;
}

} // namespace gcode
