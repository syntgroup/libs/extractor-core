#include "motherboard/gcode/parser_release_sleeves.h"

#include "motherboard/cmd/multi_goto_absolute_operation.h"
#include "motherboard/device/engine.h"
#include "motherboard/device/engine_defaults.h"
#include "motherboard/device/engine_values.h"
#include "motherboard/device/engines.h"

/*!
 * @todo добавить доступ к настройкам, куда положить координаты
 */

namespace {
constexpr double grabber_opened{ 2400 };    // steps
constexpr double grabber_closed{ 100 };     // steps
constexpr double sleeves_grab_height{ 75 }; // mm
constexpr double reference_height{ 3 };     // mm
constexpr double zero_height{ 0 };          // mm

const auto error_message = QStringLiteral("Error accessing internal memory");
} // namespace

namespace gcode {

auto
ParserReleaseSleeves::generateImpl(const QString& /*block*/) const
  -> ParseResult
{
  static constexpr auto expected_operations_count{ 5 };
  const auto engines = m_engines.lock();
  if (!engines) {
    return nonstd::make_unexpected(error_message);
  }

  auto grabber = engines->engineByName(device::AxisAddress::Grabber);
  auto sleeves = engines->engineByName(device::AxisAddress::Sleeves);
  auto magnets = engines->engineByName(device::AxisAddress::Magnets);

  ResultType result;
  result.reserve(expected_operations_count);

  std::unique_ptr<cmd::AbstractOperation> operation;

  const auto current_magnets_position = magnets->currentPosition();
  const auto current_sleeves_position = sleeves->currentPosition();

  const bool magnets_down =
    magnets->stepsToMm(current_magnets_position) > reference_height;
  if (magnets_down) {
    operation = std::make_unique<cmd::multi::GotoAbsoluteOperation>(
      magnets, magnets->mmToSteps(zero_height), magnets->travelSpeed());
    result.push_back(operation.release()); // 0
  }

  // 1
  operation = std::make_unique<cmd::multi::GotoAbsoluteOperation>(
    sleeves, sleeves->mmToSteps(sleeves_grab_height), sleeves->travelSpeed());
  result.push_back(operation.release());

  // 2
  operation = std::make_unique<cmd::multi::GotoAbsoluteOperation>(
    grabber, grabber->mmToSteps(grabber_opened), grabber->travelSpeed());
  result.push_back(operation.release());

  // 3
  operation = std::make_unique<cmd::multi::GotoAbsoluteOperation>(
    sleeves,
    sleeves->mmToSteps(sleeves_grab_height - 2.0),
    sleeves->travelSpeed());
  result.push_back(operation.release());

  // 4
  operation = std::make_unique<cmd::multi::GotoAbsoluteOperation>(
    grabber, grabber->mmToSteps(grabber_closed), grabber->travelSpeed());
  result.push_back(operation.release());

  // 5
  operation = std::make_unique<cmd::multi::GotoAbsoluteOperation>(
    sleeves, current_sleeves_position, sleeves->travelSpeed());
  result.push_back(operation.release());

  return result;
}

auto
ParserCollectSleeves::generateImpl(const QString& /*block*/) const
  -> ParseResult
{
  static constexpr auto expected_operations_count{ 4 };
  const auto engines = m_engines.lock();
  if (!engines) {
    return nonstd::make_unexpected(error_message);
  }

  auto grabber = engines->engineByName(device::AxisAddress::Grabber);
  auto sleeves = engines->engineByName(device::AxisAddress::Sleeves);

  const auto current_sleeves_position = sleeves->currentPosition();

  ResultType result;
  result.reserve(expected_operations_count);

  std::unique_ptr<cmd::AbstractOperation> operation;

  // 1
  operation = std::make_unique<cmd::multi::GotoAbsoluteOperation>(
    grabber, grabber->mmToSteps(grabber_opened), grabber->travelSpeed());
  result.push_back(operation.release());

  // 2
  operation = std::make_unique<cmd::multi::GotoAbsoluteOperation>(
    sleeves, sleeves->mmToSteps(sleeves_grab_height), sleeves->travelSpeed());
  result.push_back(operation.release());

  // 3
  operation = std::make_unique<cmd::multi::GotoAbsoluteOperation>(
    grabber, grabber->mmToSteps(grabber_closed), grabber->travelSpeed());
  result.push_back(operation.release());

  // 4
  operation = std::make_unique<cmd::multi::GotoAbsoluteOperation>(
    sleeves, current_sleeves_position, grabber->travelSpeed());
  result.push_back(operation.release());

  return result;
}

} // namespace gcode
