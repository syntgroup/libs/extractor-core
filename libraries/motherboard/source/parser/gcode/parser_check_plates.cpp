#include "motherboard/gcode/parser_check_plates.h"

#include "motherboard/cmd/sns_state_operation.h"
#include "motherboard/device/ir_sensors.h"
#include "serial/utils/counter.h"
#include <QRegularExpression>
#include <QRegularExpressionMatch>

namespace gcode {

auto
ParserCheckPlates::generateImpl(const QString& block) const -> ParseResult
{
  // syntax M600 S<idx>?
  auto sensors = m_sensors.lock();
  if (!sensors) {
    return nonstd::make_unexpected(
      QStringLiteral("error accessing internal memory"));
  }

  const QRegularExpression find_sensor_idx{ R"(([Ss])(\d+))" };
  const auto sensor_idx_mentioned = find_sensor_idx.match(block);

  if (!sensor_idx_mentioned.hasMatch()) {
    return nonstd::make_unexpected(QStringLiteral(
      "incorrect syntax of M600 command: no sensor index mentioned"));
  }

  auto conversion_ok = true;
  const auto sensor_idx =
    sensor_idx_mentioned.captured(BPValue).toUInt(&conversion_ok);
  if (!conversion_ok || sensor_idx < 1 || sensor_idx > sensors->count()) {
    return nonstd::make_unexpected(QStringLiteral(
      "incorrect syntax of M600 command: invalid index of sensor"));
  }

  auto operation = std::make_unique<cmd::motherboard::SnsStateOperation>(
    utils::Counter::tick(), sensors.get(), sensor_idx - 1);

  ResultType result;
  result.reserve(1);
  result.push_back(operation.release());
  return result;
}

} // namespace gcode
