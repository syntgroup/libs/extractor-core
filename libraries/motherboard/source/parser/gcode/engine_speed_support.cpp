/*!
 * @file engine_speed_support.cpp
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 */

#include "engine_speed_support.h"

#include "motherboard/device/engine.h"
#include "utils/constexpr_latin_string.h"
#include <QRegularExpressionMatch>

namespace {

constexpr utils::ConstLatin1String minus{ "-" };
constexpr utils::ConstLatin1String plus{ "+" };
constexpr device::SpeedIndex zero{ 0 };

[[nodiscard]] auto
create_speed_index(const device::Engine& engine,
                   const device::Speed& speed,
                   device::AccelerationCurve mode)
  -> std::optional<device::SpeedIndex>
{
  const auto frequency = engine.speedToFrequency(speed);
  const auto profile   = engine.accelerationProfile(mode);
  if (!profile.has_value()) {
    return std::nullopt;
  }

  std::optional<device::SpeedIndex> result;
  if (frequency > std::get<device::Frequency>(profile->get().fmax())) {
    result.emplace(profile->get().maxSpeedIndex());
  } else if (frequency < std::get<device::Frequency>(profile->get().f0())) {
    result.emplace(zero);
  } else {
    result.emplace(profile->get().frequencyToIndex(frequency));
  }
  return result;
};

} // namespace

namespace gcode::utils {

auto
profile_matcher(const QRegularExpressionMatch& match)
  -> device::AccelerationCurve
{
  if (!match.hasMatch()) {
    return device::AccelerationCurve::Normal;
  }

  const QStringView mode{ match.captured(3) };
  if (mode == minus) {
    return device::AccelerationCurve::Slow;
  }

  if (mode == plus) {
    return device::AccelerationCurve::Fast;
  }

  return device::AccelerationCurve::Normal;
}

[[nodiscard]] auto
feedrate_matcher(const QRegularExpressionMatch& match,
                 const device::Engine& engine,
                 device::AccelerationCurve curve,
                 device::SpeedMode legacy) -> std::optional<device::SpeedIndex>
{
  std::optional<device::SpeedIndex> result;
  // best case -- no feedrate mentioned, no processing required, cruise on
  // travel speed
  if (!match.hasMatch()) {
    result.emplace(engine.travelSpeed());
    return result;
  }

  switch (legacy) {
    case device::SpeedMode::Legacy: {
      // legacy mode -- expecting feedrate as raw speed index
      result.emplace(match.captured(2).toUInt());
      break;
    }
    case device::SpeedMode::Modern: {
      // modern mode -- getting feedrate as mm/min, need to convert to speed
      // index
      device::Speed speed{ match.captured(2).toDouble() };
      result = create_speed_index(engine, speed, curve);
      break;
    }
    default:
      return std::nullopt;
  }
  return result;
}

} // namespace gcode::utils
