#include "motherboard/cmd/device_get_fw_operation.h"

#include "motherboard/device/motherboard_params.h"
#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/device_request.h"
#include "protocol/device_response.h"

namespace cmd::motherboard {

DeviceGetFWOperation::DeviceGetFWOperation(
  protocol::PacketID cmd_idx,
  gsl::not_null<device::MotherboardParameters*> motherboard_parameters,
  QObject* parent)
  : AbstractMotherboardOperation{ cmd_idx, parent }
  , m_motherboardParameters{ motherboard_parameters }
{
}

QString
DeviceGetFWOperation::description() const
{
  return QStringLiteral("Requesting frimware revision from MB");
}

int
DeviceGetFWOperation::responseType() const
{
  return protocol::motherboard::ResponseType::DeviceGetFwRev;
}

AbstractSerialOperation::Request
DeviceGetFWOperation::createRequest() const
{
  return std::make_unique<TRequest>(id());
}

bool
DeviceGetFWOperation::processResponse(const Response& t_response)
{
  const auto* get_fw_response{ dynamic_cast<TResponse*>(t_response.get()) };
  Q_ASSERT(get_fw_response);
  if (get_fw_response->isError()) {
    return false;
  }
  m_motherboardParameters->fwRev.emplace(get_fw_response->fwRev);
  return true;
}

} // namespace cmd::motherboard
