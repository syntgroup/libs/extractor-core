#include "motherboard/cmd/multi_goto_two_axis_operation.h"

#include "motherboard/cmd/engine_get_max_speed.h"
#include "motherboard/cmd/engine_get_position_current.h"
#include "motherboard/cmd/engine_write_acceleration_operation.h"
#include "motherboard/cmd/engine_write_max_speed_operation.h"
#include "motherboard/cmd/goto_absolute_pos_operation.h"
#include "motherboard/cmd/goto_sm_stop_operation.h"
#include "motherboard/cmd/system_zeroize_error_operation.h"
#include "motherboard/device/engine.h"
#include "utils/constexpr_latin_string.h"

namespace {

template<typename Iter>
[[nodiscard]] inline auto
all_reached(Iter begin, Iter end)
{
  return std::accumulate(begin, end, true, [](bool result, auto&& target) {
    const auto engine = std::get<gsl::not_null<device::Engine*>>(target);
    return result && (engine->currentPosition() == engine->targetPosition());
  });
}

} // namespace

namespace cmd::multi {

GotoTwoAxisOperation::GotoTwoAxisOperation(
  Params&& params,
  device::AccelerationCurve acceleration_profile,
  QObject* parent)
  : cmd::AbstractMultiStageOperation{ parent }
  , _params{ std::move(params) }
  , _currentEngine{ _params.begin() }
  , _accelerationProfile{ acceleration_profile }
{
}

GotoTwoAxisOperation::~GotoTwoAxisOperation() = default;

auto
GotoTwoAxisOperation::description() const -> QString
{
  static constexpr utils::ConstLatin1String header{ "MULTI GOTO TWO AXISES: " };
  static constexpr utils::ConstLatin1String engine_param{
    "[AXIS %1: target %2]"
  };

  QStringList result{ header };
  std::transform(_params.cbegin(),
                 _params.cend(),
                 std::back_inserter(result),
                 [](const auto& param) {
                   const auto axis   = std::get<Engine>(param)->axis();
                   const auto target = QString::number(
                     std::get<device::Coordinate>(param).value_of());
                   return engine_param.arg(axis, target);
                 });
  return result.join(' ');
}

void
GotoTwoAxisOperation::resetError()
{
  registerSubOperation<motherboard::SystemZeroizeErrorOperation>();
}

void
GotoTwoAxisOperation::zeroizeError()
{
  registerSubOperation<motherboard::SystemZeroizeErrorOperation>();
}

void
GotoTwoAxisOperation::gotoEngines()
{
  motherboard::GotoAbsolutePosOperation::Params params;
  std::transform(_params.begin(),
                 _params.end(),
                 std::back_inserter(params),
                 [](auto&& target) {
                   return std::make_pair(std::get<Engine>(target),
                                         std::get<device::Coordinate>(target));
                 });
  registerSubOperation<motherboard::GotoAbsolutePosOperation>(
    std::move(params));
}

void
GotoTwoAxisOperation::checkEngine(Engine engine)
{
  registerSubOperation<motherboard::EngineGetPositionCurrentOperation>(engine);
}

void
GotoTwoAxisOperation::setEngineSpeed(Engine engine, device::SpeedIndex speed)
{
  registerSubOperation<motherboard::EngineWriteMaxSpeedOperation>(engine,
                                                                  speed);
}

void
GotoTwoAxisOperation::checkEngineSpeed(Engine engine)
{
  registerSubOperation<motherboard::EngineGetMaxSpeedOperation>(engine);
}

void
GotoTwoAxisOperation::smoothStops()
{
  motherboard::GotoSmStopOperation::Param params;
  std::transform(_params.begin(),
                 _params.end(),
                 std::back_inserter(params),
                 [](const auto& target) { return std::get<Engine>(target); });
  registerSubOperation<motherboard::GotoSmStopOperation, execution::DeadPolicy>(
    std::move(params));
}

void
GotoTwoAxisOperation::writeFrequency(Engine engine,
                                     device::Acceleration frequency)
{
  registerSubOperation<motherboard::EngineWriteAccelerationOperation>(
    engine, frequency);
}

auto
GotoTwoAxisOperation::allReached() const -> bool
{
  return std::accumulate(
    _params.cbegin(), _params.cend(), true, [](bool result, auto&& target) {
      const auto engine = std::get<Engine>(target);
      return result && (engine->currentPosition() == engine->targetPosition());
    });
}

void
GotoTwoAxisOperation::switcher(OperationState new_state)
{
  if (std::next(_currentEngine) == std::end(_params)) {
    setOperationState(new_state);
    _currentEngine = _params.begin();
  } else {
    _currentEngine = std::next(_currentEngine);
  }
}

void
GotoTwoAxisOperation::nextStateLogic()
{
  switch (operationState()) {
    case Ready:
      setOperationState(ResettingError);
      _currentEngine = _params.begin();
      resetError();
      break;

    case ResettingError: {
      auto engine  = std::get<Engine>(*_currentEngine);
      auto profile = engine->accelerationProfile(_accelerationProfile);
      if (profile.has_value()) {
        writeFrequency(engine, profile->get().f0());
        switcher(WriteFrequencyStart);
      } else {
        finishWithError(BackendError::UnknownError,
                        QString{ "WRITE_FREQ_START: cant access acceleration "
                                 "profile to check movement speed: %1" }
                          .arg(engine->name()));
      }
      break;
    }

    case WriteFrequencyStart: {
      auto engine  = std::get<Engine>(*_currentEngine);
      auto profile = engine->accelerationProfile(_accelerationProfile);
      if (profile.has_value()) {
        writeFrequency(engine, profile->get().fmax());
        switcher(WriteFrequencyStop);
      } else {
        finishWithError(BackendError::UnknownError,
                        QString{ "WRITE_FREQ_STOP: cant access acceleration "
                                 "profile to check movement speed: %1" }
                          .arg(engine->name()));
      }
      break;
    }

    case WriteFrequencyStop: {
      auto engine  = std::get<Engine>(*_currentEngine);
      auto profile = engine->accelerationProfile(_accelerationProfile);
      if (profile.has_value()) {
        writeFrequency(engine, profile->get().dfmax());
        switcher(WriteFrequencyStep);
      } else {
        finishWithError(BackendError::UnknownError,
                        QString{ "WRITE_FREQ_STEP: cant access acceleration "
                                 "profile to check movement speed: %1" }
                          .arg(engine->name()));
      }
      break;
    }

    case WriteFrequencyStep: {
      setEngineSpeed(std::get<Engine>(*_currentEngine),
                     std::get<device::SpeedIndex>(*_currentEngine));
      switcher(SettingNewSpeedEngine);
      break;
    }

    case SettingNewSpeedEngine: {
      checkEngineSpeed(std::get<Engine>(*_currentEngine));
      switcher(CheckingNewSpeedEngine);
      break;
    }

    case CheckingNewSpeedEngine: {
      gotoEngines();
      setOperationState(ExecGotoEngines);
      break;
    }

    case ExecGotoEngines: {
      checkEngine(std::get<Engine>(*_currentEngine));
      if (std::next(_currentEngine) == std::end(_params)) {
        _currentEngine = _params.begin();
        if (all_reached(_params.begin(), _params.end())) {
          setOperationState(CheckEngines);
        }
      } else {
        _currentEngine = std::next(_currentEngine);
      }
      break;
    }

    case CheckEngines: {
      //! @todo тут может проверять на предмет равенства текущей установленной
      //! скорости и travelSpeed? и если они равны -- то прыгать сразу на пару
      //! шагов вперед?
      auto engine = std::get<Engine>(*_currentEngine);
      setEngineSpeed(engine, engine->travelSpeed());
      switcher(SettingTravelSpeedEngine);
      break;
    }

    case SettingTravelSpeedEngine: {
      checkEngineSpeed(std::get<Engine>(*_currentEngine));
      if (std::next(_currentEngine) == std::end(_params)) {
        finish();
      } else {
        _currentEngine = std::next(_currentEngine);
      }
      break;
    }

    case ZeroizingError:
      setOperationState(_buffer);
      advanceOperationState();
      break;

    case SmoothStopEngine: {
      setOperationState(Idle);
      smoothStops();
      break;
    }

    case Idle:
      break;

    default:
      finishWithError(
        BackendError::UnknownError,
        QStringLiteral(
          "Operation %1 got to unknown state, have to terminate here")
          .arg(description()));
      break;
  }
}

void
GotoTwoAxisOperation::onSubOperationError(
  const AbstractSerialOperation& operation)
{
  if (operation.error() == BackendError::TimeoutError) {
    finishWithError(operation.error(), operation.errorString());
    return;
  }
  switch (auto state{ static_cast<OperationState>(operationState()) }) {
    case GotoTwoAxisOperation::ResettingError:
    case GotoTwoAxisOperation::ZeroizingError:
    case GotoTwoAxisOperation::SmoothStopEngine:
    case GotoTwoAxisOperation::Idle:
      break;
    default:
      _buffer = state;
      setOperationState(GotoTwoAxisOperation::ZeroizingError);
      zeroizeError();
      emit stepRetry();
      break;
  }
}

void
GotoTwoAxisOperation::pause()
{
  _buffer = static_cast<OperationState>(operationState());
  setOperationState(SmoothStopEngine);
}

void
GotoTwoAxisOperation::resume()
{
  switch (static_cast<OperationState>(_buffer)) {
    case CheckEngines: {
      if (!all_reached(_params.begin(), _params.end())) {
        _buffer = OperationState::CheckingNewSpeedEngine;
      }
      break;
    }
    default:
      break;
  }
  setOperationState(_buffer);
  advanceOperationState();
}

} // namespace cmd::multi
