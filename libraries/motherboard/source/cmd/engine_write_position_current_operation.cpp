#include "motherboard/cmd/engine_write_position_current_operation.h"

#include "motherboard/device/engine.h"
#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/engine_request.h"
#include "protocol/engine_response.h"

namespace cmd::motherboard {

EngineWritePositionCurrentOperation::EngineWritePositionCurrentOperation(
  protocol::PacketID pid,
  gsl::not_null<device::Engine*> engine,
  device::Coordinate value,
  QObject* parent)
  : AbstractMotherboardOperation{ pid, parent }
  , m_engine{ engine }
  , m_newValue{ value }
{
}

QString
EngineWritePositionCurrentOperation::description() const
{
  const auto value{ QString::number(m_newValue.value_of()) };
  const auto address{ QString::number(m_engine->address()) };
  return QStringLiteral("Setting new current position %1 for engine %2")
    .arg(value, address);
}

auto
EngineWritePositionCurrentOperation::responseType() const -> int
{
  return protocol::motherboard::ResponseType::EnginesSetPosition;
}

AbstractSerialOperation::Request
EngineWritePositionCurrentOperation::createRequest() const
{
  return std::make_unique<TRequest>(id(), m_engine->address(), m_newValue);
}

bool
EngineWritePositionCurrentOperation::processResponse(const Response& t_response)
{
  const auto* response{ dynamic_cast<TResponse*>(t_response.get()) };
  m_engine->setStatus(response->engineState);

  return !response->isError();
}

} // namespace cmd::motherboard
