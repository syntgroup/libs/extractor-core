#include "motherboard/cmd/button_request_door_power.h"

#include "common/discrete_input.h"
#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/power_request.h"
#include "protocol/power_response.h"

namespace cmd::motherboard {

PowerGetButtonsOperation::PowerGetButtonsOperation(protocol::PacketID pid,
                                                   DoorSensor* door,
                                                   PowerButton* power,
                                                   QObject* parent)
  : AbstractMotherboardOperation{ pid, parent }
  , m_doorButton{ door }
  , m_powerButton{ power }
{
}

auto
PowerGetButtonsOperation::description() const -> QString
{
  return QStringLiteral("Requesting buttons state");
}

auto
PowerGetButtonsOperation::responseType() const -> int
{
  return protocol::motherboard::ResponseType::VoltageGetBtns;
}

auto
PowerGetButtonsOperation::createRequest() const -> Request
{
  return std::make_unique<TRequest>(id());
}

auto
PowerGetButtonsOperation::processResponse(const Response& t_response) -> bool
{
  const auto* response = dynamic_cast<TResponse*>(t_response.get());
  if (response == nullptr) {
    setError(BackendError::DataError, QStringLiteral(""));
    return false;
  }

  m_doorButton->setState(!response->stateGercon);
  m_powerButton->setState(!response->statePushButon);
  return !response->isError();
}

} // namespace cmd::motherboard
