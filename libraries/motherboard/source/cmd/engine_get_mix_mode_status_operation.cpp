#include "motherboard/cmd/engine_get_mix_mode_status_operation.h"

#include "motherboard/device/engine.h"
#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/engine_request.h"
#include "protocol/engine_response.h"

namespace cmd::motherboard {

EngineGetMixModeStatusOperation::EngineGetMixModeStatusOperation(
  protocol::PacketID pid,
  gsl::not_null<device::Engine*> engine,
  QObject* parent)
  : AbstractMotherboardOperation{ pid, parent }
  , m_engine{ engine }
{
}

QString
EngineGetMixModeStatusOperation::description() const
{
  return QStringLiteral("Requesting engine %1 mix mode status")
    .arg(m_engine->address());
}

int
EngineGetMixModeStatusOperation::responseType() const
{
  return protocol::motherboard::ResponseType::EnginesGetMixModeState;
}

AbstractSerialOperation::Request
EngineGetMixModeStatusOperation::createRequest() const
{
  return std::make_unique<TRequest>(id(), m_engine->address());
}

bool
EngineGetMixModeStatusOperation::processResponse(const Response& t_response)
{
  const auto* response{ dynamic_cast<TResponse*>(t_response.get()) };
  Q_ASSERT(response);

  m_engine->setStatus(response->engineState);
  m_engine->setCurrentMixCycle(response->currentCycle);
  m_engine->setCurrentMixTime(response->currentTime);

  return !response->isError();
}

} // namespace cmd::motherboard
