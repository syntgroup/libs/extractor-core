#include "motherboard/cmd/multi_mix_n_times_operation.h"

#include "motherboard/cmd/engine_get_mix_mode_status_operation.h"
#include "motherboard/cmd/engine_write_max_speed_operation.h"
#include "motherboard/cmd/engine_write_repeatability_operation.h"
#include "motherboard/cmd/goto_em_stop_operation.h"
#include "motherboard/cmd/goto_mix_cycles_operation.h"
#include "motherboard/cmd/goto_mix_stop_operation.h"
#include "motherboard/cmd/system_zeroize_error_operation.h"
#include "motherboard/device/engine.h"

namespace {
constexpr device::Repeatability mix_repeatability{ 1 };
constexpr device::Repeatability move_repeatability{ 2 };

constexpr device::SpeedIndex zero_speed{ 0 };
constexpr device::Cycles zero_cycles{ 0 };
} // namespace

namespace cmd::multi {

MixNCyclesOperation::MixNCyclesOperation(gsl::not_null<device::Engine*> engine,
                                         device::Cycles&& cycles,
                                         device::Coordinate&& amplitude,
                                         device::SpeedIndex&& mixing_speed,
                                         QObject* parent)
  : cmd::AbstractMultiStageOperation{ parent }
  , m_engine{ engine }
  , m_cycles{ std::move(cycles) }
  , m_amplitude{ std::move(amplitude) }
  , m_new_speed{ mixing_speed == zero_speed ? m_engine->actionSpeed()
                                            : std::move(mixing_speed) }
{
}

QString
MixNCyclesOperation::description() const
{
  const auto address = QString::number(m_engine->address());
  const auto cycles  = QString::number(m_cycles.value_of());
  const auto message =
    QStringLiteral("MULTI Axis %1 Mix %2 times").arg(address, cycles);
  return (operationState() == Ready)
           ? message
           : message + " // " + currentOperation()->description();
}

void
MixNCyclesOperation::writeSpeed(device::SpeedIndex speed)
{
  registerSubOperation<motherboard::EngineWriteMaxSpeedOperation>(m_engine,
                                                                  speed);
}

void
MixNCyclesOperation::writeRepeatability(device::Repeatability repeatability)
{
  registerSubOperation<motherboard::EngineWriteRepeatabilityOperation>(
    m_engine, repeatability);
}

void
MixNCyclesOperation::execStartMixing()
{
  m_engine->setCurrentMixCycle(zero_cycles);

  registerSubOperation<motherboard::GotoMixCyclesOperation>(
    m_engine, m_cycles, m_amplitude);
}

void
MixNCyclesOperation::checkMixingCycle()
{
  registerSubOperation<motherboard::EngineGetMixModeStatusOperation>(m_engine);
}

void
MixNCyclesOperation::zeroizeError()
{
  registerSubOperation<motherboard::SystemZeroizeErrorOperation>();
}

void
MixNCyclesOperation::stopMixing()
{
  registerSubOperation<motherboard::GotoMixStopOperation,
                       execution::DeadPolicy>(m_engine);
}

void
MixNCyclesOperation::emergencyStop()
{
  motherboard::GotoEmStopOperation::Param params{ m_engine };
  registerSubOperation<motherboard::GotoEmStopOperation, execution::DeadPolicy>(
    std::move(params));
}

void
MixNCyclesOperation::nextStateLogic()
{
  switch (operationState()) {
    case Ready: {
      setOperationState(WritingMixingSpeed);
      const bool to_update = m_new_speed != zero_speed;
      writeSpeed(to_update ? m_new_speed : m_engine->actionSpeed());
      break;
    }

    case WritingMixingSpeed:
      setOperationState(WritingMixingRepeatability);
      writeRepeatability(mix_repeatability);
      break;

    case WritingMixingRepeatability:
      setOperationState(ExecStartMixingNCycles);
      execStartMixing();
      break;

    case ExecStartMixingNCycles:
      setOperationState(CheckMixingState);
      checkMixingCycle();
      break;

    case CheckMixingState: {
      const auto cycles_elapsed = m_engine->currentMixCycle() == m_cycles;
      const auto engine_moving  = m_engine->status().engineGo;
      if (cycles_elapsed && !engine_moving) {
        setOperationState(WritingTravelMaxSpeed);
        writeSpeed(m_engine->travelSpeed());
      } else {
        checkMixingCycle();
      }
      break;
    }

    case WritingTravelMaxSpeed:
      setOperationState(WritingTravelRepeatability);
      writeRepeatability(move_repeatability);
      break;

    case WritingTravelRepeatability:
      finish();
      break;

    case ZeroizeError:
      setOperationState(m_buffer);
      advanceOperationState();
      break;

    case EmergencyStop:
      setOperationState(Idle);
      emergencyStop();
      break;

    case SmoothStop:
      setOperationState(Idle);
      stopMixing();
      break;

    case Idle:
      break;

    default:
      finishWithError(
        BackendError::UnknownError,
        QStringLiteral(
          "Operation %1 got to unknown state, have to terminate here")
          .arg(description()));
      break;
  }
}

void
MixNCyclesOperation::onSubOperationError(const AbstractSerialOperation&)
{
  switch (auto state = static_cast<OperationState>(operationState())) {
    case MixNCyclesOperation::WritingMixingSpeed:
    case MixNCyclesOperation::WritingMixingRepeatability:
    case MixNCyclesOperation::ExecStartMixingNCycles:
    case MixNCyclesOperation::CheckMixingState:
    case MixNCyclesOperation::WritingTravelMaxSpeed:
    case MixNCyclesOperation::WritingTravelRepeatability:
      m_buffer = state;
      setOperationState(ZeroizeError);
      zeroizeError();
      emit stepRetry();
      break;
    default:
      break;
  }
}

void
MixNCyclesOperation::pause()
{
  m_buffer = static_cast<OperationState>(operationState());
  setOperationState(SmoothStop);
}

void
MixNCyclesOperation::resume()
{
  if (m_buffer == OperationState::CheckMixingState &&
      m_engine->currentMixCycle() != m_cycles) {
    m_cycles -= m_engine->currentMixCycle();
    setOperationState(OperationState::ExecStartMixingNCycles);
    execStartMixing();
    return;
  }

  setOperationState(m_buffer);
  advanceOperationState();
}

} // namespace cmd::multi
