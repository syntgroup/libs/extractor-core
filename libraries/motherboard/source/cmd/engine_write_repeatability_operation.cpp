#include "motherboard/cmd/engine_write_repeatability_operation.h"

#include "motherboard/device/engine.h"
#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/engine_request.h"
#include "protocol/engine_response.h"

namespace cmd::motherboard {

EngineWriteRepeatabilityOperation::EngineWriteRepeatabilityOperation(
  protocol::PacketID pid,
  gsl::not_null<device::Engine*> engine,
  device::Repeatability repeatability,
  QObject* parent)
  : AbstractMotherboardOperation{ pid, parent }
  , m_engine{ engine }
  , m_repeatability{ repeatability }
{
}

QString
EngineWriteRepeatabilityOperation::description() const
{
  const auto address{ QString::number(m_engine->address()) };
  const auto value{ QString::number(m_repeatability.value_of()) };
  return QStringLiteral("Setting repeatability %1 for engine %2")
    .arg(value, address);
}

auto
EngineWriteRepeatabilityOperation::responseType() const -> int
{
  return protocol::motherboard::ResponseType::EnginesSetRepeatability;
}

AbstractSerialOperation::Request
EngineWriteRepeatabilityOperation::createRequest() const
{
  return std::make_unique<TRequest>(id(), m_engine->address(), m_repeatability);
}

bool
EngineWriteRepeatabilityOperation::processResponse(const Response& t_response)
{
  const auto* response{ dynamic_cast<TResponse*>(t_response.get()) };
  m_engine->setStatus(response->engineState);

  return !response->isError();
}

} // namespace cmd::motherboard
