#include "motherboard/cmd/power_set_uv_lamp_operation.h"

#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/power_request.h"
#include "serial/protocol/response_interface.h"

namespace cmd::motherboard {

PowerSetUvLampOperation::PowerSetUvLampOperation(protocol::PacketID pid,
                                                 gsl::not_null<TDevice*> lamp,
                                                 bool enable,
                                                 QObject* parent)
  : AbstractMotherboardOperation{ pid, parent }
  , m_lamp{ lamp }
  , m_enable{ enable }
{
}

QString
PowerSetUvLampOperation::description() const
{
  return QStringLiteral("Setting UV Lamp %1").arg(m_enable ? "On" : "Off");
}

auto
PowerSetUvLampOperation::responseType() const -> int
{
  return protocol::motherboard::ResponseType::PowerSetUvLamp;
}

AbstractSerialOperation::Request
PowerSetUvLampOperation::createRequest() const
{
  return std::make_unique<TRequest>(id(), m_enable);
}

bool
PowerSetUvLampOperation::processResponse(const Response& t_response)
{
  if (!t_response->isError()) {
    m_lamp->setState(m_enable);
  }
  return !t_response->isError();
}

} // namespace cmd::motherboard
