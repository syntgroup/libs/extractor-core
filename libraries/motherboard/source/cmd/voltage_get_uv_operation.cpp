#include "motherboard/cmd/voltage_get_uv_operation.h"

#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/power_request.h"
#include "protocol/power_response.h"

namespace cmd::motherboard {

VoltageGetUvOperation::VoltageGetUvOperation(protocol::PacketID pid,
                                             gsl::not_null<TDevice*> lamp,
                                             QObject* parent)
  : AbstractMotherboardOperation{ pid, parent }
  , m_lamp{ std::move(lamp) }
{
}

QString
VoltageGetUvOperation::description() const
{
  return QStringLiteral("Requesting UV lamp voltage");
}

auto
VoltageGetUvOperation::responseType() const -> int
{
  return protocol::motherboard::ResponseType::VoltageGetUvLampRail;
}

AbstractSerialOperation::Request
VoltageGetUvOperation::createRequest() const
{
  return std::make_unique<TRequest>(id());
}

bool
VoltageGetUvOperation::processResponse(const Response& t_response)
{
  const auto* response{ dynamic_cast<TResponse*>(t_response.get()) };
  m_lamp->setValue(response->voltage);

  return !response->isError();
}

} // namespace cmd::motherboard
