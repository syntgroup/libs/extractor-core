#include "motherboard/cmd/button_clear_trigger_power.h"

#include "common/discrete_input.h"
#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/buttons_request.h"
#include "serial/protocol/response_interface.h"

namespace cmd::motherboard {

PowerButtonClearTrigger::PowerButtonClearTrigger(
  protocol::PacketID pid,
  gsl::not_null<device::DiscreteOutput*> button,
  QObject* parent)
  : AbstractMotherboardOperation{ pid, parent }
  , m_powerButton{ button }
{
}

auto
PowerButtonClearTrigger::description() const -> QString
{
  return QStringLiteral("clear power button trigger state");
}

auto
PowerButtonClearTrigger::responseType() const -> int
{
  return protocol::motherboard::ResponseType::PwrBtnClearTrigger;
}

auto
PowerButtonClearTrigger::createRequest() const -> Request
{
  return std::make_unique<TRequest>(id());
}

auto
PowerButtonClearTrigger::processResponse(const Response& response) -> bool
{
  m_powerButton->resetTrigger();
  return !response->isError();
}

} // namespace cmd::motherboard
