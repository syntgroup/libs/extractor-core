#include "motherboard/cmd/power_set_wdg_value.h"

#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/power_request.h"

namespace cmd::motherboard {

PowerSetWdgValue::PowerSetWdgValue(protocol::PacketID pid,
                                   uint32_t timeout,
                                   QObject* parent)
  : cmd::motherboard::AbstractMotherboardOperation{ pid, parent }
  , m_timeout{ timeout }
{
}

QString
PowerSetWdgValue::description() const
{
  return QStringLiteral("set poweroff timeout to %1").arg(m_timeout);
}

int
PowerSetWdgValue::responseType() const
{
  return protocol::motherboard::ResponseType::PowerSetWdtValue;
}

AbstractSerialOperation::Request
PowerSetWdgValue::createRequest() const
{
  return std::make_unique<TRequset>(id(), m_timeout);
}

} // namespace cmd::motherboard
