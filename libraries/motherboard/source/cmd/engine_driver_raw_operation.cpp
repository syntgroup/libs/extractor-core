#include "motherboard/cmd/engine_driver_raw_operation.h"

#include "motherboard/device/engine.h"
#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/engine_request.h"
#include "protocol/engine_response.h"

namespace cmd::motherboard {

EngineDriverRawWriteOperation::EngineDriverRawWriteOperation(
  protocol::PacketID idx,
  gsl::not_null<device::Engine*> engine,
  QByteArray bytes,
  QObject* parent)
  : AbstractMotherboardOperation{ idx, parent }
  , m_engine{ engine }
  , m_data{ std::move(bytes) }
{
}

QString
EngineDriverRawWriteOperation::description() const
{
  const auto address{ QString::number(m_engine->address()) };
  const auto bytes{ QString::fromUtf8(m_data.toHex(':').toUpper()) };
  return QStringLiteral("Writing raw command to stepper driver #%1: %2")
    .arg(address, bytes);
}

int
EngineDriverRawWriteOperation::responseType() const
{
  return protocol::motherboard::ResponseType::EnginesDriverRawWrite;
}

AbstractSerialOperation::Request
EngineDriverRawWriteOperation::createRequest() const
{
  return std::make_unique<TRequest>(id(), m_engine->address(), m_data);
}

bool
EngineDriverRawWriteOperation::processResponse(const Response& t_response)
{
  const auto* response_write{ dynamic_cast<TResponse*>(t_response.get()) };
  Q_ASSERT(response_write);

  m_engine->setStatus(response_write->engineState);

  return !response_write->isError();
}

} // namespace cmd::motherboard
