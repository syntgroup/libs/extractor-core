#include "motherboard/cmd/system_soft_reboot_operation.h"

#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/system_request.h"

namespace cmd::motherboard {

QString
SystemSoftRebootOperation::description() const
{
  return QStringLiteral("Soft reset motherboard");
}

auto
SystemSoftRebootOperation::responseType() const -> int
{
  return protocol::motherboard::ResponseType::SystemSoftReboot;
}

AbstractSerialOperation::Request
SystemSoftRebootOperation::createRequest() const
{
  return std::make_unique<TRequest>(id());
}

} // namespace cmd::motherboard
