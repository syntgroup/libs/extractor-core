#include "motherboard/cmd/power_get_24v_operation.h"

#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/power_request.h"
#include "protocol/power_response.h"

namespace cmd::motherboard {

VoltageGet24VOperation::VoltageGet24VOperation(protocol::PacketID pid,
                                               gsl::not_null<TDevice*> powers,
                                               QObject* parent)
  : AbstractMotherboardOperation{ pid, parent }
  , m_voltages{ std::move(powers) }
{
}

QString
VoltageGet24VOperation::description() const
{
  return QStringLiteral("Requesting 24V Rail voltage");
}

auto
VoltageGet24VOperation::responseType() const -> int
{
  return protocol::motherboard::ResponseType::VoltageGet24VRail;
}

AbstractSerialOperation::Request
VoltageGet24VOperation::createRequest() const
{
  return std::make_unique<TRequest>(id());
}

bool
VoltageGet24VOperation::processResponse(const Response& t_response)
{
  const auto* response{ dynamic_cast<TResponse*>(t_response.get()) };
  m_voltages->setValue(response->voltage);

  return !response->isError();
}

} // namespace cmd::motherboard
