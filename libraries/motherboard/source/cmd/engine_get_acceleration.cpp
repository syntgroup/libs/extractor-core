#include "motherboard/cmd/engine_get_acceleration.h"

#include "motherboard/device/engine.h"
#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/engine_request.h"
#include "protocol/engine_response.h"

namespace {
[[nodiscard]] constexpr auto
mode_to_name(device::FrequencyType type)
{
  switch (type) {
    case device::FrequencyType::F0:
      return QLatin1String{ "F0" };
    case device::FrequencyType::Fmax:
      return QLatin1String{ "Fmax" };
    default:
      break;
  }
  return QLatin1String{ "dFmax" };
}
} // namespace

namespace cmd::motherboard {

EngineGetAccelerationOperation::EngineGetAccelerationOperation(
  protocol::PacketID pid,
  gsl::not_null<device::Engine*> engine,
  device::AccelerationCurve curve,
  device::FrequencyType mode,
  QObject* parent)
  : AbstractMotherboardOperation{ pid, parent }
  , m_engine{ engine }
  , m_mode{ mode }
  , m_curve{ curve }
{
}

auto
EngineGetAccelerationOperation::description() const -> QString
{
  return QStringLiteral("Requesting engine %1 acceleration %2")
    .arg(m_engine->axis(), mode_to_name(m_mode));
}

auto
EngineGetAccelerationOperation::responseType() const -> int
{
  switch (m_mode) {
    case device::FrequencyType::F0:
      return protocol::motherboard::ResponseType::EnginesGetF0;
    case device::FrequencyType::Fmax:
      return protocol::motherboard::ResponseType::EnginesGetFMax;
    case device::FrequencyType::dFmax:
      return protocol::motherboard::ResponseType::EnginesGetDfMax;
  }
}

auto
EngineGetAccelerationOperation::createRequest() const -> Request
{
  switch (m_mode) {
    case device::FrequencyType::F0:
      return std::make_unique<protocol::motherboard::EngineRequestF0>(
        id(), m_engine->address());
    case device::FrequencyType::Fmax:
      return std::make_unique<protocol::motherboard::EngineRequestFMax>(
        id(), m_engine->address());
    case device::FrequencyType::dFmax:
      return std::make_unique<protocol::motherboard::EngineRequestDFMax>(
        id(), m_engine->address());
  }
}

auto
EngineGetAccelerationOperation::processResponse(const Response& t_response)
  -> bool
{
  const auto* response{ dynamic_cast<TResponse*>(t_response.get()) };
  Q_ASSERT(response);

  m_engine->setStatus(response->engineState);

  auto profile{ m_engine->accelerationProfile(m_curve) };
  if (!profile.has_value()) {
    setError(BackendError::DataError, "");
    return false;
  }

  switch (m_mode) {
    case device::FrequencyType::F0: {
      profile->get().setF0(response->acceleration);
      emit m_engine->frequencyChanged(m_curve, profile->get().f0());
      break;
    }
    case device::FrequencyType::Fmax: {
      profile->get().setFmax(response->acceleration);
      emit m_engine->frequencyChanged(m_curve, profile->get().fmax());
      break;
    }
    case device::FrequencyType::dFmax: {
      profile->get().setdFmax(response->acceleration);
      emit m_engine->frequencyChanged(m_curve, profile->get().dfmax());
      break;
    }
  }

  return !response->isError();
}

} // namespace cmd::motherboard
