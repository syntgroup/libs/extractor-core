#include "motherboard/cmd/button_report_cached.h"

#include "common/discrete_input.h"

namespace cmd::meta {

ReportCachedButton::ReportCachedButton(
  gsl::not_null<device::DiscreteOutput*> button,
  QObject* parent)
  : AbstractMetaOperation{ parent }
  , _button{ button }
{
}

ReportCachedButton::~ReportCachedButton() = default;

auto
ReportCachedButton::description() const -> QString
{
  return "REPORT CACHED BUTTON: " + _button->objectName();
}

void
ReportCachedButton::start()
{
  _button->setState(_button->state());
  finish();
}

} // namespace cmd::meta
