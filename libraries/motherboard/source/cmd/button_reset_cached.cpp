#include "motherboard/cmd/button_reset_cached.h"

#include "common/discrete_input.h"

namespace cmd::meta {

ResetCachedButton::ResetCachedButton(
  gsl::not_null<device::DiscreteOutput*> button,
  std::optional<bool> modifier,
  QObject* parent)
  : AbstractMetaOperation{ parent }
  , _button{ std::move(button) }
  , _modifier{ std::move(modifier) }
{
}

ResetCachedButton::~ResetCachedButton() = default;

auto
ResetCachedButton::description() const -> QString
{
  return "RESET CACHED BUTTON: " + _button->objectName();
}

void
ResetCachedButton::start()
{
  if (_modifier.has_value()) {
    _button->setState(*_modifier, /*notify=*/false);
  } else {
    _button->resetState(/*notify=*/false);
  }
  finish();
}

} // namespace cmd::meta
