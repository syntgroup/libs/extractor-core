#include "motherboard/cmd/status_ping_operation.h"

#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/ping_request.h"

namespace cmd::motherboard {

QString
StatusPingOperation::description() const
{
  return QStringLiteral("Sending Ping signal to MB");
}

auto
StatusPingOperation::responseType() const -> int
{
  return protocol::motherboard::ResponseType::StatusPing;
}

AbstractSerialOperation::Request
StatusPingOperation::createRequest() const
{
  return std::make_unique<protocol::motherboard::StatusPingRequest>(id());
}

} // namespace cmd::motherboard
