#include "motherboard/cmd/system_get_error_operation.h"

#include "motherboard/protocol/motherboard_error.h"
#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/system_request.h"
#include "protocol/system_response.h"

namespace cmd::motherboard {

SystemGetErrorOperation::SystemGetErrorOperation(protocol::PacketID pid,
                                                 uint8_t error_pos,
                                                 QObject* parent)
  : AbstractMotherboardOperation{ pid, parent }
  , m_errorPos{ error_pos }
{
}

QString
SystemGetErrorOperation::description() const
{
  return QStringLiteral("Requesting error at position %1").arg(m_errorPos);
}

auto
SystemGetErrorOperation::responseType() const -> int
{
  return protocol::motherboard::ResponseType::SystemGetError;
}

AbstractSerialOperation::Request
SystemGetErrorOperation::createRequest() const
{
  return std::make_unique<TRequest>(id(), m_errorPos);
}

bool
SystemGetErrorOperation::processResponse(const Response& t_response)
{
  const auto* response{ dynamic_cast<TResponse*>(t_response.get()) };
  return !response->isError();
}

} // namespace cmd::motherboard
