#include "motherboard/cmd/multi_mix_n_seconds_operation.h"

#include "motherboard/cmd/engine_get_mix_mode_status_operation.h"
#include "motherboard/cmd/engine_write_max_speed_operation.h"
#include "motherboard/cmd/engine_write_repeatability_operation.h"
#include "motherboard/cmd/goto_em_stop_operation.h"
#include "motherboard/cmd/goto_mix_stop_operation.h"
#include "motherboard/cmd/goto_mix_time_operation.h"
#include "motherboard/cmd/system_zeroize_error_operation.h"
#include "motherboard/device/engine.h"

namespace {
constexpr device::Repeatability repeatability_mixing{ 1 };
constexpr device::Repeatability repeatability_travel{ 2 };
constexpr device::SpeedIndex zero_speed{ 0 };
constexpr device::Cycles zero_cycles{ 0 };
constexpr std::chrono::seconds zero_time{ 0 };
} // namespace

namespace cmd::multi {

MixNSecondsOperation::MixNSecondsOperation(
  gsl::not_null<device::Engine*> engine,
  std::chrono::seconds&& time,
  device::Coordinate&& amplitude,
  device::SpeedIndex&& mixing_speed,
  QObject* parent)
  : cmd::AbstractMultiStageOperation{ parent }
  , m_engine{ engine }
  , m_seconds{ std::move(time) }
  , m_amplitude{ std::move(amplitude) }
  , m_newSpeed{ mixing_speed == zero_speed ? m_engine->actionSpeed()
                                           : std::move(mixing_speed) }
  , m_countdownTimer{ std::make_unique<QTimer>() }
  , m_timeBuffer{ m_seconds }
{
  m_countdownTimer->setInterval(m_countdown_interval);
  m_countdownTimer->setSingleShot(false);
  connect(m_countdownTimer.get(),
          &QTimer::timeout,
          this,
          &MixNSecondsOperation::countdownRoutine);
}

QString
MixNSecondsOperation::description() const
{
  const auto address  = QString::number(m_engine->address());
  const auto duration = QString::number(m_seconds.count());
  const auto message =
    QStringLiteral("MULTI Axis %1 Mix %2 seconds").arg(address, duration);
  return (operationState() == Ready)
           ? message
           : message + " // " + currentOperation()->description();
}

void
MixNSecondsOperation::writeSpeed(device::SpeedIndex speed)
{
  registerSubOperation<motherboard::EngineWriteMaxSpeedOperation>(m_engine,
                                                                  speed);
}

void
MixNSecondsOperation::writeRepeatability(device::Repeatability repeatability)
{
  registerSubOperation<motherboard::EngineWriteRepeatabilityOperation>(
    m_engine, repeatability);
}

void
MixNSecondsOperation::execStartMixing()
{
  m_engine->setCurrentMixCycle(zero_cycles);
  m_engine->setCurrentMixTime(zero_time);

  registerSubOperation<motherboard::GotoMixTimeOperation>(
    m_engine, m_seconds, m_amplitude);

  m_countdownTimer->start();
  emit m_engine->startMixingTime(m_seconds);
}

void
MixNSecondsOperation::checkMixingTime()
{
  registerSubOperation<motherboard::EngineGetMixModeStatusOperation>(m_engine);
}

void
MixNSecondsOperation::zeroizeError()
{
  registerSubOperation<motherboard::SystemZeroizeErrorOperation>();
}

void
MixNSecondsOperation::stopMixing()
{
  registerSubOperation<motherboard::GotoMixStopOperation,
                       execution::DeadPolicy>(m_engine);
}

void
MixNSecondsOperation::emergencyStop()
{
  motherboard::GotoEmStopOperation::Param params{ m_engine };
  registerSubOperation<motherboard::GotoEmStopOperation, execution::DeadPolicy>(
    std::move(params));
}

void
MixNSecondsOperation::nextStateLogic()
{
  switch (operationState()) {
    case Ready: {
      setOperationState(WritingMixingSpeed);
      writeSpeed(m_newSpeed != zero_speed ? m_newSpeed
                                          : m_engine->actionSpeed());
      break;
    }

    case WritingMixingSpeed:
      setOperationState(WritingMixingRepeatability);
      writeRepeatability(repeatability_mixing);
      break;

    case WritingMixingRepeatability:
      setOperationState(ExecStartMixingNSeconds);
      execStartMixing();
      break;

    case ExecStartMixingNSeconds:
      setOperationState(CheckMixingTime);
      checkMixingTime();
      break;

    case CheckMixingTime: {
      if (timeElapsed() && !m_engine->status().engineGo) {
        setOperationState(WritingTravelMaxSpeed);
        writeSpeed(m_engine->travelSpeed());
      } else {
        checkMixingTime();
      }
      break;
    }

    case WritingTravelMaxSpeed:
      setOperationState(WritingTravelRepeatability);
      writeRepeatability(repeatability_travel);
      break;

    case WritingTravelRepeatability:
      emit m_engine->stopMixing();
      finish();
      break;

    case ZeroizeError:
      setOperationState(m_buffer);
      advanceOperationState();
      break;

    case EmergencyStop:
      setOperationState(Idle);
      emergencyStop();
      break;

    case SmoothStop:
      setOperationState(Idle);
      stopMixing();
      break;

    case Idle:
      break;

    default:
      finishWithError(
        BackendError::UnknownError,
        QStringLiteral(
          "Operation %1 got to unknown state, have to terminate here")
          .arg(description()));
      break;
  }
}

void
MixNSecondsOperation::countdownRoutine()
{
  m_timeBuffer -= m_countdown_interval;
  if (timeElapsed()) {
    m_countdownTimer->stop();
  }
}

void
MixNSecondsOperation::onSubOperationError(const AbstractSerialOperation&)
{
  switch (auto state{ static_cast<OperationState>(operationState()) }) {
    case MixNSecondsOperation::WritingMixingSpeed:
    case MixNSecondsOperation::WritingMixingRepeatability:
    case MixNSecondsOperation::ExecStartMixingNSeconds:
    case MixNSecondsOperation::CheckMixingTime:
    case MixNSecondsOperation::WritingTravelMaxSpeed:
    case MixNSecondsOperation::WritingTravelRepeatability:
      m_buffer = state;
      setOperationState(ZeroizeError);
      zeroizeError();
      emit stepRetry();
      break;
    default:
      break;
  }
}

void
MixNSecondsOperation::pause()
{
  m_buffer = static_cast<OperationState>(operationState());
  setOperationState(SmoothStop);
}

void
MixNSecondsOperation::resume()
{
  if (m_buffer == OperationState::CheckMixingTime &&
      m_engine->currentMixTime() != m_seconds) {
    m_seconds -= m_engine->currentMixTime();
    setOperationState(OperationState::ExecStartMixingNSeconds);
    execStartMixing();
    return;
  }

  setOperationState(m_buffer);
  advanceOperationState();
}

} // namespace cmd::multi
