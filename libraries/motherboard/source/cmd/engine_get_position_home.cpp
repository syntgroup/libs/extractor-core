#include "motherboard/cmd/engine_get_position_home.h"

#include "motherboard/device/engine.h"
#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/engine_request.h"
#include "protocol/engine_response.h"
#include "utils/converter.h"

namespace cmd::motherboard {

EngineGetPositionHomeOperation::EngineGetPositionHomeOperation(
  protocol::PacketID pid,
  gsl::not_null<device::Engine*> engine,
  QObject* parent)
  : AbstractMotherboardOperation{ pid, parent }
  , m_engine{ engine }
{
}

QString
EngineGetPositionHomeOperation::description() const
{
  return QStringLiteral("Requesting engine %1 home position")
    .arg(m_engine->address());
}

int
EngineGetPositionHomeOperation::responseType() const
{
  return protocol::motherboard::ResponseType::EnginesGetHomePosition;
}

AbstractSerialOperation::Request
EngineGetPositionHomeOperation::createRequest() const
{
  return std::make_unique<TRequest>(id(), m_engine->address());
}

bool
EngineGetPositionHomeOperation::processResponse(const Response& t_response)
{
  const auto* response{ dynamic_cast<TResponse*>(t_response.get()) };
  Q_ASSERT(response);

  m_engine->setStatus(response->engineState);
  m_engine->setHomePosition(response->homePosition);

  if (response->errorCode() !=
      protocol::motherboard::MotherboardError::NoError) {
    setError(BackendError::OperationError,
             utils::to_string(response->errorCode()));
  }

  return !response->isError();
}

} // namespace cmd::motherboard
