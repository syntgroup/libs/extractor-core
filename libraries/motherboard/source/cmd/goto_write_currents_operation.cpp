#include "motherboard/cmd/goto_write_currents_operation.h"

#include "motherboard/device/engine.h"
#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/goto_request.h"
#include "protocol/goto_response.h"
#include "utils/converter.h"

namespace cmd::motherboard {

GotoWriteCurrentsOperation::GotoWriteCurrentsOperation(
  protocol::PacketID pid,
  gsl::not_null<device::Engine*> engine,
  device::Current current_accel,
  device::Current current_stdby,
  QObject* parent)
  : AbstractMotherboardOperation{ pid, parent }
  , m_engine{ engine }
  , m_currentAccel{ current_accel }
  , m_currentStdby{ current_stdby }
{
}

QString
GotoWriteCurrentsOperation::description() const
{
  return QStringLiteral(
           "WRITING ENGINE %1 ACCEL CURRENT %2 AND STANDBY CURRENT %3")
    .arg(m_engine->axis(),
         utils::to_string(m_currentAccel),
         utils::to_string(m_currentStdby));
}

auto
GotoWriteCurrentsOperation::responseType() const -> int
{
  return protocol::motherboard::ResponseType::GotoWriteCurrents;
}

AbstractSerialOperation::Request
GotoWriteCurrentsOperation::createRequest() const
{
  return std::make_unique<TRequest>(
    id(), m_engine->address(), m_currentAccel, m_currentStdby);
}

bool
GotoWriteCurrentsOperation::processResponse(const Response& t_response)
{
  const auto* response{ dynamic_cast<TResponse*>(t_response.get()) };
  Q_ASSERT(response);

  m_engine->setStatus(response->engineState);

  return !response->isError();
}

} // namespace cmd::motherboard
