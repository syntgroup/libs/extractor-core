#include "motherboard/cmd/sns_state_operation.h"

#include "motherboard/device/ir_sensors.h"
#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/sns_state_request.h"
#include "protocol/sns_state_response.h"
#include <bitset>

namespace cmd::motherboard {

SnsStateOperation::SnsStateOperation(protocol::PacketID pid,
                                     gsl::not_null<device::IrSensors*> sensors,
                                     unsigned int index_to_check,
                                     QObject* parent)
  : AbstractMotherboardOperation{ pid, parent }
  , m_sensors{ sensors }
  , m_indexToCheck{ index_to_check }
{
}

QString
SnsStateOperation::description() const
{
  return QStringLiteral("Getting sns states");
}

AbstractSerialOperation::Request
SnsStateOperation::createRequest() const
{
  return std::make_unique<TRequest>(id());
}

auto
SnsStateOperation::responseType() const -> int
{
  return protocol::motherboard::ResponseType::SnsState;
}

bool
SnsStateOperation::processResponse(const Response& t_response)
{
  const auto* casted_response{ dynamic_cast<TResponse*>(t_response.get()) };
  const std::bitset<
    std::numeric_limits<decltype(casted_response->snsStates)>::digits>
    sensors_states{ casted_response->snsStates };

  for (auto idx = 0; idx < m_sensors->count(); ++idx) {
    m_sensors->setSensor(idx, sensors_states.test(idx));
  }

  if (!m_sensors->sensorState(m_indexToCheck)) {
    emit m_sensors->notFound(m_indexToCheck + 1);
  }

  return !casted_response->isError();
}

} // namespace cmd::motherboard
