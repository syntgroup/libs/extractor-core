#include "motherboard/cmd/multi_goto_home_position_operation.h"

#include "motherboard/cmd/engine_enable_operation.h"
#include "motherboard/cmd/engine_get_max_speed.h"
#include "motherboard/cmd/engine_get_position_current.h"
#include "motherboard/cmd/engine_get_position_home.h"
#include "motherboard/cmd/engine_write_acceleration_operation.h"
#include "motherboard/cmd/engine_write_max_speed_operation.h"
#include "motherboard/cmd/engine_write_position_home_operation.h"
#include "motherboard/cmd/goto_em_stop_operation.h"
#include "motherboard/cmd/goto_sm_stop_operation.h"
#include "motherboard/cmd/goto_zero_operation.h"
#include "motherboard/cmd/system_zeroize_error_operation.h"
#include "motherboard/device/engine.h"
#include <QTimer>

namespace {
constexpr auto homing_curve{ device::AccelerationCurve::Normal };
}

namespace cmd::multi {

GotoHomeOperation::GotoHomeOperation(gsl::not_null<device::Engine*> engine,
                                     QObject* parent)
  : cmd::AbstractMultiStageOperation{ parent }
  , m_engine{ engine }
{
}

auto
GotoHomeOperation::description() const -> QString
{
  const auto message = QStringLiteral("MULTI Moving Axis %1 to home position")
                         .arg(m_engine->address());
  return message;
}

void
GotoHomeOperation::execGotoZero()
{
  registerSubOperation<motherboard::GotoZeroOperation>(m_engine);
}

void
GotoHomeOperation::writeFrequency(device::Acceleration frequency)
{
  registerSubOperation<motherboard::EngineWriteAccelerationOperation>(
    m_engine, frequency);
}

void
GotoHomeOperation::writeSpeed(device::SpeedIndex speed)
{
  registerSubOperation<motherboard::EngineWriteMaxSpeedOperation>(m_engine,
                                                                  speed);
}

void
GotoHomeOperation::checkSpeed()
{
  registerSubOperation<motherboard::EngineGetMaxSpeedOperation>(m_engine);
}

void
GotoHomeOperation::checkCompleteMovement()
{
  registerSubOperation<motherboard::EngineGetPositionCurrentOperation>(
    m_engine);
}

void
GotoHomeOperation::writeHomePosition(device::Coordinate pos)
{
  registerSubOperation<motherboard::EngineWritePositionHomeOperation>(m_engine,
                                                                      pos);
}

void
GotoHomeOperation::checkHomePosition()
{
  registerSubOperation<motherboard::EngineGetPositionHomeOperation>(m_engine);
}

void
GotoHomeOperation::smoothStop()
{
  motherboard::GotoSmStopOperation::Param params{ m_engine };
  registerSubOperation<motherboard::GotoSmStopOperation, execution::DeadPolicy>(
    std::move(params));
}

void
GotoHomeOperation::emergencyStop()
{
  motherboard::GotoEmStopOperation::Param params{ m_engine };
  registerSubOperation<motherboard::GotoEmStopOperation, execution::DeadPolicy>(
    std::move(params));
}

void
GotoHomeOperation::resetError()
{
  registerSubOperation<motherboard::SystemZeroizeErrorOperation>();
}

void
GotoHomeOperation::turnEngineOn()
{
  registerSubOperation<motherboard::EngineEnableOperation>(m_engine, true);
}

void
GotoHomeOperation::nextStateLogic()
{
  switch (operationState()) {
    case Ready:
      setOperationState(ResettingError);
      resetError();
      break;

    case ResettingError:
      setOperationState(TurningEngineOn);
      turnEngineOn();
      break;

    case TurningEngineOn: {
      const auto profile = m_engine->accelerationProfile(homing_curve);
      if (profile.has_value()) {
        setOperationState(WriteFrequencyStart);
        writeFrequency(profile->get().f0());
      } else {
        finishWithError(BackendError::UnknownError, "");
      }
      break;
    }

    case WriteFrequencyStart: {
      const auto profile = m_engine->accelerationProfile(homing_curve);
      if (profile.has_value()) {
        setOperationState(WriteFrequencyStop);
        writeFrequency(profile->get().fmax());
      } else {
        finishWithError(BackendError::UnknownError, "");
      }
      break;
    }

    case WriteFrequencyStop: {
      const auto profile = m_engine->accelerationProfile(homing_curve);
      if (profile.has_value()) {
        setOperationState(WriteFrequencyStep);
        writeFrequency(profile->get().dfmax());
      } else {
        finishWithError(BackendError::UnknownError, "");
      }
      break;
    }

    case WriteFrequencyStep:
      setOperationState(WritingHomingSpeed);
      writeSpeed(m_engine->homingSpeed());
      break;

    case WritingHomingSpeed:
      setOperationState(CheckingHomingSpeed);
      checkSpeed();
      break;

    case CheckingHomingSpeed:
      setOperationState(WritingHomePosition);
      writeHomePosition(m_engine->homePosition());
      break;

    case WritingHomePosition:
      setOperationState(CheckingHomePosition);
      checkHomePosition();
      break;

    case CheckingHomePosition:
      setOperationState(ExecGotoZero);
      execGotoZero();
      break;

    case ExecGotoZero:
      setOperationState(CheckingZeroSuccess);
      checkCompleteMovement();
      break;

    case CheckingZeroSuccess: {
      const auto engine_moving      = m_engine->status().engineGoToZero;
      const auto position_undefined = !m_engine->status().undefinedPos;

      if (engine_moving && position_undefined) {
        checkCompleteMovement();
        break;
      }

      if (!engine_moving && position_undefined) {
        finishWithError(
          BackendError::OperationError,
          QStringLiteral("Engine is not moving but position isn't defined?"));
        break;
      }

      setOperationState(WritingTravelSpeed);
      writeSpeed(m_engine->travelSpeed());
      break;
    }

    case WritingTravelSpeed:
      setOperationState(CheckingTravelSpeed);
      checkSpeed();
      break;

    case CheckingTravelSpeed:
      finish();
      break;

    case ZeroizingError:
      setOperationState(m_buffer);
      advanceOperationState();
      break;

    case EmergencyStop:
      setOperationState(Idle);
      emergencyStop();
      break;

    case SmoothStop:
      setOperationState(Idle);
      smoothStop();
      break;

    case Idle:
      break;

    default:
      finishWithError(
        BackendError::UnknownError,
        QStringLiteral(
          "Operation %1 got to unknown state, have to terminate here")
          .arg(description()));
      break;
  }
}

void
GotoHomeOperation::onSubOperationError(const AbstractSerialOperation& operation)
{
  const bool abort = operation.error() == BackendError::OperationError ||
                     operation.error() == BackendError::TimeoutError;
  if (abort) {
    finishWithError(operation.error(), operation.errorString());
    return;
  }

  switch (auto state{ static_cast<OperationState>(operationState()) }) {
    case GotoHomeOperation::TurningEngineOn:
    case GotoHomeOperation::WritingHomingSpeed:
    case GotoHomeOperation::CheckingHomingSpeed:
    case GotoHomeOperation::WritingHomePosition:
    case GotoHomeOperation::CheckingHomePosition:
    case GotoHomeOperation::ExecGotoZero:
    case GotoHomeOperation::CheckingZeroSuccess:
    case GotoHomeOperation::WritingTravelSpeed:
    case GotoHomeOperation::CheckingTravelSpeed: {
      m_buffer = state;
      setOperationState(GotoHomeOperation::ZeroizingError);
      resetError();
      emit stepRetry();
      break;
    }
    default:
      break;
  }
}

void
GotoHomeOperation::pause()
{
  m_buffer = static_cast<OperationState>(operationState());
  setOperationState(SmoothStop);
}

void
GotoHomeOperation::resume()
{
  if (m_buffer == OperationState::CheckingZeroSuccess &&
      !m_engine->status().undefinedPos) {
    m_buffer = OperationState::CheckingHomePosition;
  }

  if (m_buffer == OperationState::ZeroizingError) {
    m_buffer = OperationState::ResettingError;
  }

  setOperationState(m_buffer);
  advanceOperationState();
}

} // namespace cmd::multi
