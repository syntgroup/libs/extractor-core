#include "motherboard/cmd/goto_zero_operation.h"

#include "motherboard/device/engine.h"
#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/goto_request.h"
#include "protocol/goto_response.h"
#include "utils/converter.h"

namespace cmd::motherboard {

GotoZeroOperation::GotoZeroOperation(protocol::PacketID pid,
                                     gsl::not_null<device::Engine*> engine,
                                     QObject* parent)
  : AbstractMotherboardOperation{ pid, parent }
  , m_engine{ engine }
{
}

QString
GotoZeroOperation::description() const
{
  return QStringLiteral("Zeroing axis %1").arg(m_engine->address());
}

auto
GotoZeroOperation::responseType() const -> int
{
  return protocol::motherboard::ResponseType::GotoZeroPos;
}

AbstractSerialOperation::Request
GotoZeroOperation::createRequest() const
{
  return std::make_unique<TRequest>(
    id(), m_engine->address(), m_engine->zeroSwitch());
}

bool
GotoZeroOperation::processResponse(const Response& t_response)
{
  const auto* response{ dynamic_cast<TResponse*>(t_response.get()) };

  constexpr device::Coordinate zero{ 0 };
  m_engine->setTargetPosition(zero);
  m_engine->setStatus(response->engineState);

  using Error = protocol::motherboard::MotherboardError::Error;
  switch (response->errorCode()) {
    case Error::NoError:
      return true;

    case Error::EngineNoFreeTimersNow:
    case Error::EngineNoFreeTimersNow1:
    case Error::EngineNoFreeTimersNow2:
    case Error::EngineNoFreeTimersNow3:
    case Error::EngineNoFreeTimersNow4:
    case Error::EngineNoFreeTimersNow5:
    case Error::EngineNoFreeTimersNow6:
    case Error::EngineCanNotFindZeroSwitch2:
    case Error::EngineCanNotMoveWhenMovingToZero:
    case Error::EngineCrossUndefinedSwitch:
    case Error::EngineCrossSwitch1:
    case Error::EngineCrossSwitch2:
      setError(BackendError::OperationError,
               utils::to_string(response->errorCode()));
      return false;

    default:
      setError(BackendError::ProtocolError,
               utils::to_string(response->errorCode()));
      return false;
  }
}

} // namespace cmd::motherboard
