#include "motherboard/cmd/power_set_led_operation.h"

#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/power_request.h"
#include "serial/protocol/response_interface.h"

namespace cmd::motherboard {

PowerSetLedOperation::PowerSetLedOperation(protocol::PacketID pid,
                                           gsl::not_null<TDevice*> lamp,
                                           device::PwmLevel pwm,
                                           QObject* parent)
  : AbstractMotherboardOperation{ pid, parent }
  , m_lamp{ lamp }
  , m_pwm{ pwm }
{
}

QString
PowerSetLedOperation::description() const
{
  return QStringLiteral("Setting leds brightness %1").arg(m_pwm.value_of());
}

auto
PowerSetLedOperation::responseType() const -> int
{
  return protocol::motherboard::ResponseType::PowerSetLeds;
}

AbstractSerialOperation::Request
PowerSetLedOperation::createRequest() const
{
  return std::make_unique<TRequest>(id(), m_pwm);
}

bool
PowerSetLedOperation::processResponse(const Response& t_response)
{
  m_lamp->setValue(m_pwm);
  return !t_response->isError();
}

} // namespace cmd::motherboard
