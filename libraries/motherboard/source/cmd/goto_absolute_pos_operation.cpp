#include "motherboard/cmd/goto_absolute_pos_operation.h"

#include "motherboard/device/engine.h"
#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/goto_request.h"
#include "protocol/goto_response.h"
#include "utils/converter.h"

namespace {

const auto create_message{ [](const auto& param) -> QString {
  const auto address{ QString::number(
    std::get<device::Engine*>(param)->address()) };
  const auto target{ QString::number(
    std::get<device::Coordinate>(param).value_of()) };
  return QStringLiteral("%1 @ %2").arg(address, target);
} };

const auto create_request{
  [](const auto& param) -> std::pair<uint8_t, device::Coordinate> {
    return std::make_pair(std::get<device::Engine*>(param)->address(),
                          std::get<device::Coordinate>(param));
  }
};

const auto update_position{ [](auto& param) -> void {
  auto* engine{ std::get<device::Engine*>(param) };
  auto target{ std::get<device::Coordinate>(param) };
  engine->setTargetPosition(target);
} };

} // namespace

namespace cmd::motherboard {

GotoAbsolutePosOperation::GotoAbsolutePosOperation(protocol::PacketID id,
                                                   Params&& params,
                                                   QObject* parent)
  : AbstractMotherboardOperation{ id, parent }
  , m_params{ std::move(params) }
{
}

QString
GotoAbsolutePosOperation::description() const
{
  QStringList infos;
  std::transform(m_params.cbegin(),
                 m_params.cend(),
                 std::back_inserter(infos),
                 create_message);
  return QStringLiteral("MOVING AXISES: %1").arg(infos.join(" && "));
}

auto
GotoAbsolutePosOperation::responseType() const -> int
{
  return protocol::motherboard::ResponseType::GotoAbsPos;
}

AbstractSerialOperation::Request
GotoAbsolutePosOperation::createRequest() const
{
  TRequest::Params request_params;
  std::transform(m_params.cbegin(),
                 m_params.cend(),
                 std::back_inserter(request_params),
                 create_request);
  return std::make_unique<TRequest>(id(), request_params);
}

bool
GotoAbsolutePosOperation::processResponse(const Response& t_response)
{
  const auto* response{ dynamic_cast<TResponse*>(t_response.get()) };
  Q_ASSERT(response);

  std::for_each(m_params.begin(), m_params.end(), update_position);

  using Error = protocol::motherboard::MotherboardError;
  switch (response->errorCode()) {
    case Error::NoError:
      return true;

    case Error::EngineNoFreeTimersNow:
    case Error::EngineNoFreeTimersNow1:
    case Error::EngineNoFreeTimersNow2:
    case Error::EngineNoFreeTimersNow3:
    case Error::EngineNoFreeTimersNow4:
    case Error::EngineNoFreeTimersNow5:
    case Error::EngineNoFreeTimersNow6:
    case Error::EngineCanNotFindZeroSwitch2:
    case Error::EngineCanNotMoveWhenMovingToZero:
    case Error::EngineCrossUndefinedSwitch:
    case Error::EngineCrossSwitch1:
    case Error::EngineCrossSwitch2:
      setError(BackendError::OperationError,
               utils::to_string(response->errorCode()));
      return false;

    default:
      setError(BackendError::ProtocolError,
               utils::to_string(response->errorCode()));
      return false;
  }
}

bool
GotoAbsolutePosOperation::begin()
{
  if (m_params.empty()) {
    setError(BackendError::DataError, QStringLiteral("no engines requested"));
  }

  if (m_params.size() > 2) {
    setError(BackendError::DataError,
             QStringLiteral("too many engines requested"));
  }

  if (m_params.size() > 1 &&
      std::get<device::Engine*>(m_params.at(0))->address() ==
        std::get<device::Engine*>(m_params.at(1))->address()) {
    setError(BackendError::DataError, QStringLiteral("same engines requested"));
  }
  return isError();
}

} // namespace cmd::motherboard
