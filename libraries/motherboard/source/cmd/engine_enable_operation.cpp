#include "motherboard/cmd/engine_enable_operation.h"

#include "motherboard/device/engine.h"
#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/engine_request.h"
#include "protocol/engine_response.h"
#include "utils/converter.h"

namespace cmd::motherboard {

EngineEnableOperation::EngineEnableOperation(
  protocol::PacketID cmd_idx,
  gsl::not_null<device::Engine*> engine,
  bool enable,
  QObject* parent)
  : AbstractMotherboardOperation{ cmd_idx, parent }
  , m_engine{ engine }
  , m_enable{ m_engine->address() == 1 ? !enable : enable }
{
}

QString
EngineEnableOperation::description() const
{
  const auto address{ QString::number(m_engine->address()) };
  return QStringLiteral("Turning engine %1 %2")
    .arg(address, m_enable ? "On" : "Off");
}

int
EngineEnableOperation::responseType() const
{
  return protocol::motherboard::ResponseType::EnginesEnable;
}

AbstractSerialOperation::Request
EngineEnableOperation::createRequest() const
{
  return std::make_unique<TRequest>(id(), m_engine->address(), m_enable);
}

bool
EngineEnableOperation::processResponse(const Response& t_response)
{
  const auto* response{ dynamic_cast<TResponse*>(t_response.get()) };
  Q_ASSERT(response);

  m_engine->setStatus(response->engineState);
  m_engine->setEnabled(m_enable);

  if (response->errorCode() !=
      protocol::motherboard::MotherboardError::NoError) {
    setError(BackendError::OperationError,
             utils::to_string(response->errorCode()));
  }

  return !response->isError();
}

} // namespace cmd::motherboard
