#include "motherboard/cmd/goto_n_ticks_operation.h"

#include "motherboard/device/engine.h"
#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/goto_request.h"
#include "protocol/goto_response.h"

namespace cmd::motherboard {

GotoNTicksOperation::GotoNTicksOperation(protocol::PacketID pid,
                                         gsl::not_null<device::Engine*> engine,
                                         device::Coordinate ticks,
                                         QObject* parent)
  : AbstractMotherboardOperation{ pid, parent }
  , m_engine{ engine }
  , m_ticks{ ticks }
{
}

QString
GotoNTicksOperation::description() const
{
  const auto address{ QString::number(m_engine->address()) };
  const auto current{ QString::number(m_engine->currentPosition().value_of()) };
  const auto target{ QString::number(
    (m_engine->currentPosition() + m_ticks).value_of()) };

  return QStringLiteral("Axis %1 goto from %2 to %3")
    .arg(address, current, target);
}

auto
GotoNTicksOperation::responseType() const -> int
{
  return protocol::motherboard::ResponseType::GotoNTicks;
}

AbstractSerialOperation::Request
GotoNTicksOperation::createRequest() const
{
  //  return std::make_unique<TRequest>(id(), m_engine->address(), m_ticks);
  const TRequest::Params request_params{ std::make_pair(m_engine->address(),
                                                        m_ticks) };
  return std::make_unique<TRequest>(id(), request_params);
}

bool
GotoNTicksOperation::processResponse(const Response& t_response)
{
  const auto* response{ dynamic_cast<TResponse*>(t_response.get()) };
  Q_ASSERT(response);

  if (response->errorCode() ==
      protocol::motherboard::MotherboardError::NoError) {
    m_engine->setTargetPosition(m_engine->targetPosition() + m_ticks);
    m_engine->setStatus(response->engineState);
    return !response->isError();
  }

  setError(BackendError::OperationError, response->errorString());
  return false;
}

} // namespace cmd::motherboard
