#include "motherboard/cmd/button_request_diag_opt.h"

#include "common/discrete_input.h"
#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/buttons_request.h"
#include "protocol/buttons_response.h"

namespace cmd::motherboard {

DiagOptStateOperation::DiagOptStateOperation(
  protocol::PacketID pid,
  gsl::not_null<device::DiscreteOutput*> sensors,
  QObject* parent)
  : AbstractMotherboardOperation{ pid, parent }
  , m_sensors{ std::move(sensors) }
{
}

auto
DiagOptStateOperation::description() const -> QString
{
  return QStringLiteral("Getting Diagnostic optical sensor state");
}

auto
DiagOptStateOperation::responseType() const -> int
{
  return protocol::motherboard::ResponseType::OptDiagState;
}

auto
DiagOptStateOperation::createRequest() const -> AbstractSerialOperation::Request
{
  return std::make_unique<TRequest>(id());
}

auto
DiagOptStateOperation::processResponse(const Response& t_response) -> bool
{
  const auto* response = dynamic_cast<TResponse*>(t_response.get());
  if (response == nullptr) {
    setError(BackendError::ProtocolError);
    return false;
  }

  m_sensors->setState(!response->state);
  m_sensors->setTrigger(response->trigger);
  return !response->isError();
}

} // namespace cmd::motherboard
