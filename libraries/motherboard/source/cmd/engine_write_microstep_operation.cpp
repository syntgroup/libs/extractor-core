#include "motherboard/cmd/engine_write_microstep_operation.h"

#include "motherboard/device/engine.h"
#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/engine_request.h"
#include "protocol/engine_response.h"
#include "utils/converter.h"

namespace cmd::motherboard {

EngineWriteMicrostepOperation::EngineWriteMicrostepOperation(
  protocol::PacketID pid,
  gsl::not_null<device::Engine*> engine,
  device::Microstep new_value,
  QObject* parent)
  : AbstractMotherboardOperation{ pid, parent }
  , m_engine{ engine }
  , m_newMicrostep{ new_value }
{
}

QString
EngineWriteMicrostepOperation::description() const
{
  return QStringLiteral("Setting microstep %1 for engine %2")
    .arg(utils::to_string(m_newMicrostep),
         QString::number(m_engine->address()));
}

auto
EngineWriteMicrostepOperation::responseType() const -> int
{
  return protocol::motherboard::ResponseType::EnginesSetMicrostep;
}

AbstractSerialOperation::Request
EngineWriteMicrostepOperation::createRequest() const
{
  return std::make_unique<TRequest>(id(), m_engine->address(), m_newMicrostep);
}

bool
EngineWriteMicrostepOperation::processResponse(const Response& t_response)
{
  const auto* response{ dynamic_cast<TResponse*>(t_response.get()) };
  m_engine->setStatus(response->engineState);

  return !response->isError();
}

} // namespace cmd::motherboard
