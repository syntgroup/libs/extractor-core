#include "motherboard/cmd/button_clear_trigger_diag_opt.h"

#include "common/discrete_input.h"
#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/buttons_request.h"
#include "protocol/buttons_response.h"

namespace cmd::motherboard {

DiagOptResetTriggerOperation::DiagOptResetTriggerOperation(
  protocol::PacketID pid,
  gsl::not_null<device::DiscreteOutput*> sensors,
  QObject* parent)
  : cmd::motherboard::AbstractMotherboardOperation{ pid, parent }
  , m_sensors{ std::move(sensors) }
{
}

auto
DiagOptResetTriggerOperation::description() const -> QString
{
  return QStringLiteral("RESET Diagnostic optical sensor trigger");
}

auto
DiagOptResetTriggerOperation::responseType() const -> int
{
  return protocol::motherboard::ResponseType::OptDiagClearTrigger;
}

auto
DiagOptResetTriggerOperation::createRequest() const -> Request
{
  return std::make_unique<TRequest>(id());
}

auto
DiagOptResetTriggerOperation::processResponse(const Response& response) -> bool
{
  m_sensors->resetTrigger();
  return !response->isError();
}

} // namespace cmd::motherboard
