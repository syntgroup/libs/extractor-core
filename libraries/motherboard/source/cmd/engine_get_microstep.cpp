#include "motherboard/cmd/engine_get_microstep.h"

#include "motherboard/device/engine.h"
#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/engine_request.h"
#include "protocol/engine_response.h"

namespace cmd::motherboard {

EngineGetMicrostepOperation::EngineGetMicrostepOperation(
  protocol::PacketID pid,
  gsl::not_null<device::Engine*> engine,
  QObject* parent)
  : AbstractMotherboardOperation{ pid, parent }
  , m_engine{ engine }
{
}

QString
EngineGetMicrostepOperation::description() const
{
  return QStringLiteral("Requesting engine %1 microstep")
    .arg(m_engine->address());
}

int
EngineGetMicrostepOperation::responseType() const
{
  return protocol::motherboard::ResponseType::EnginesGetMicrostep;
}

AbstractSerialOperation::Request
EngineGetMicrostepOperation::createRequest() const
{
  return std::make_unique<TRequest>(id(), m_engine->address());
}

bool
EngineGetMicrostepOperation::processResponse(const Response& t_response)
{
  const auto* response{ dynamic_cast<TResponse*>(t_response.get()) };
  Q_ASSERT(response);

  m_engine->setStatus(response->engineState);
  m_engine->setMicrostep(response->microstep);

  return !response->isError();
}

} // namespace cmd::motherboard
