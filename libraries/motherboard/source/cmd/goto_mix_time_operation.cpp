#include "motherboard/cmd/goto_mix_time_operation.h"

#include "motherboard/device/engine.h"
#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/goto_request.h"

namespace cmd::motherboard {

GotoMixTimeOperation::GotoMixTimeOperation(
  protocol::PacketID pid,
  gsl::not_null<device::Engine*> engine,
  std::chrono::seconds seconds,
  device::Coordinate amplitude,
  QObject* parent)
  : AbstractMotherboardOperation{ pid, parent }
  , m_engine{ engine }
  , m_seconds{ seconds }
  , m_amplitude{ amplitude }
{
}

QString
GotoMixTimeOperation::description() const
{
  const auto address{ QString::number(m_engine->address()) };
  const auto time{ QString::number(m_seconds.count()) };
  const auto ampliture{ QString::number(m_amplitude.value_of()) };
  return QStringLiteral("Moving engine %1 for %2 seconds with %3 amplitude")
    .arg(address, time, ampliture);
}

int
GotoMixTimeOperation::responseType() const
{
  return protocol::motherboard::ResponseType::GotoMixTime;
}

AbstractSerialOperation::Request
GotoMixTimeOperation::createRequest() const
{
  return std::make_unique<TRequest>(
    id(), m_engine->address(), m_seconds, m_amplitude);
}

bool
GotoMixTimeOperation::begin()
{
  constexpr static auto maximum_path{ 75.0 };
  const auto maximum_steps{ m_engine->mmToSteps(maximum_path) };
  if (m_engine->currentPosition() + m_amplitude >= maximum_steps) {
    setError(
      BackendError::OperationError,
      QStringLiteral("Engine %1 is too low to start mixing, check protocol")
        .arg(m_engine->axis()));
  }
  return isError();
}

} // namespace cmd::motherboard
