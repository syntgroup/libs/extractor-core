#include "motherboard/cmd/goto_mix_cycles_operation.h"

#include "motherboard/device/engine.h"
#include "motherboard/device/engine_values.h"
#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/goto_request.h"
#include "protocol/goto_response.h"
#include "utils/converter.h"

namespace cmd::motherboard {

GotoMixCyclesOperation::GotoMixCyclesOperation(
  protocol::PacketID pid,
  gsl::not_null<device::Engine*> engine,
  device::Cycles cycles,
  device::Coordinate amplitude,
  QObject* parent)
  : AbstractMotherboardOperation{ pid, parent }
  , m_engine{ engine }
  , m_cycles{ cycles }
  , m_amplitude{ amplitude }
{
}

QString
GotoMixCyclesOperation::description() const
{
  const auto address{ QString::number(m_engine->address()) };
  const auto cycles{ QString::number(m_cycles.value_of()) };
  const auto ampliture{ QString::number(m_amplitude.value_of()) };
  return QStringLiteral(
           "Requesting MIX on engine %1 for %2 times at %3 amplitude")
    .arg(address, cycles, ampliture);
}

auto
GotoMixCyclesOperation::responseType() const -> int
{
  return protocol::motherboard::ResponseType::GotoMixCycles;
}

AbstractSerialOperation::Request
GotoMixCyclesOperation::createRequest() const
{
  return std::make_unique<TRequest>(
    id(), m_engine->address(), m_cycles, m_amplitude);
}

bool
GotoMixCyclesOperation::begin()
{
  constexpr auto maximum_path{ 75.0 };
  const auto maximum_steps{ m_engine->mmToSteps(maximum_path) };
  if (m_engine->currentPosition() + m_amplitude >= maximum_steps) {
    setError(
      BackendError::OperationError,
      QStringLiteral("Engine %1 is too low to start mixing, check protocol")
        .arg(m_engine->axis()));
  }
  return isError();
}

bool
GotoMixCyclesOperation::processResponse(const Response& t_response)
{
  const auto* response{ static_cast<TResponse*>(t_response.get()) };
  Q_ASSERT(response);

  m_engine->setStatus(response->engineState);

  if (response->errorCode() !=
      protocol::motherboard::MotherboardError::NoError) {
    setError(BackendError::OperationError,
             utils::to_string(response->errorCode()));
  }

  return !response->isError();
}

} // namespace cmd::motherboard
