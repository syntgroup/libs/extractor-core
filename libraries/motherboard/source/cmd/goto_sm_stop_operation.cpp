#include "motherboard/cmd/goto_sm_stop_operation.h"

#include "motherboard/device/engine.h"
#include "motherboard/protocol/motherboard_encoder.h"
#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/goto_request.h"
#include "protocol/goto_response.h"

namespace cmd::motherboard {

GotoSmStopOperation::GotoSmStopOperation(protocol::PacketID pid,
                                         Param&& param,
                                         QObject* parent)
  : AbstractMotherboardOperation{ pid, parent }
  , m_engines{ std::move(param) }
{
}

bool
GotoSmStopOperation::begin()
{
  if (m_engines.empty()) {
    setError(BackendError::DataError, QStringLiteral("no engines requested"));
  }

  if (m_engines.size() > 2) {
    setError(BackendError::DataError,
             QStringLiteral("too many engines requested"));
  }

  if (m_engines.size() > 1 &&
      m_engines.at(0)->address() == m_engines.at(1)->address()) {
    setError(BackendError::DataError, QStringLiteral("same engines requested"));
  }

  return isError();
}

QString
GotoSmStopOperation::description() const
{
  QStringList infos;
  std::transform(
    m_engines.cbegin(),
    m_engines.cend(),
    std::back_inserter(infos),
    [](auto&& engine) { return QString::number(engine->address()); });
  return QStringLiteral("Gracefully stopping axises %1").arg(infos.join(" & "));
}

auto
GotoSmStopOperation::responseType() const -> int
{
  return protocol::motherboard::ResponseType::GotoSmoothStop;
}

AbstractSerialOperation::Request
GotoSmStopOperation::createRequest() const
{
  TRequest::Params request_params;
  std::transform(m_engines.cbegin(),
                 m_engines.cend(),
                 std::back_inserter(request_params),
                 [](auto&& engine) { return engine->address(); });
  return std::make_unique<TRequest>(id(), request_params);
}

bool
GotoSmStopOperation::processResponse(const Response& t_response)
{
  const auto* response{ dynamic_cast<TResponse*>(t_response.get()) };
  if (response == nullptr) {
    return false;
  }

  for (auto&& engine : m_engines) {
    engine->setStatus(response->engineState);
  }

  return !response->isError();
}

} // namespace cmd::motherboard
