#include "motherboard/cmd/buttons_request_gercon.h"

#include "motherboard/device/discrete_input.h"
#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/buttons_request.h"
#include "protocol/buttons_response.h"

namespace cmd::motherboard {

PowerGetGerconOperation::PowerGetGerconOperation(
  protocol::PacketID pid,
  gsl::not_null<device::DiscreteInput> button,
  QObject* parent)
  : cmd::motherboard::AbstractMotherboardOperation{ pid, parent }
  , m_doorButton{ std::move(button) }
{
}

QString
PowerGetGerconOperation::description() const
{
  return QStringLiteral("Requesting door sensor");
}

auto
PowerGetGerconOperation::responseType() const -> int
{
  return protocol::motherboard::ResponseType::GerconGetState;
}

AbstractSerialOperation::Request
PowerGetGerconOperation::createRequest() const
{
  return std::make_unique<TRequest>(id());
}

bool
PowerGetGerconOperation::processResponse(const Response& t_response)
{
  if (auto button = m_doorButton.lock()) {
    const auto* response{ dynamic_cast<TResponse*>(t_response.get()) };
    button->setState(!response->state);

    return !response->isError();
  }

  setError(BackendError::DataError, QStringLiteral(""));
  return false;
}

} // namespace cmd::motherboard
