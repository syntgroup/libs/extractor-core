#include "motherboard/cmd/goto_n_ticks_unc_operation.h"

#include "motherboard/device/engine.h"
#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/goto_request.h"
#include "protocol/goto_response.h"
#include "utils/converter.h"

namespace cmd::motherboard {

GotoNTicksUncOperation::GotoNTicksUncOperation(
  protocol::PacketID pid,
  gsl::not_null<device::Engine*> engine,
  device::Coordinate ticks,
  QObject* parent)
  : AbstractMotherboardOperation{ pid, parent }
  , m_engine{ engine }
  , m_ticks{ ticks }
{
}

QString
GotoNTicksUncOperation::description() const
{
  const auto address{ QString::number(m_engine->address()) };
  const auto value{ QString::number(m_ticks.value_of()) };
  return QStringLiteral("Axis %1 goto UNC to %2").arg(address, value);
}

auto
GotoNTicksUncOperation::responseType() const -> int
{
  return protocol::motherboard::ResponseType::GotoCmd;
}

AbstractSerialOperation::Request
GotoNTicksUncOperation::createRequest() const
{
  return std::make_unique<TRequest>(id(), m_engine->address(), m_ticks);
}

bool
GotoNTicksUncOperation::processResponse(const Response& t_response)
{
  const auto* goto_response{ dynamic_cast<TResponse*>(t_response.get()) };
  Q_ASSERT(goto_response);

  if (goto_response->errorCode() !=
      protocol::motherboard::MotherboardError::NoError) {
    setError(BackendError::OperationError,
             utils::to_string(goto_response->errorCode()));
  }

  m_engine->setStatus(goto_response->engineState);
  return !goto_response->isError();
}

} // namespace cmd::motherboard
