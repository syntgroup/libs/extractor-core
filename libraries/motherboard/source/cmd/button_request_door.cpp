#include "motherboard/cmd/button_request_door.h"

#include "common/discrete_input.h"
#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/buttons_request.h"
#include "protocol/buttons_response.h"

namespace cmd::motherboard {

ButtonsRequestDoor::ButtonsRequestDoor(protocol::PacketID pid,
                                       device::DiscreteOutput* door_button,
                                       QObject* parent)
  : AbstractMotherboardOperation{ pid, parent }
  , m_doorButton{ door_button }
{
}

auto
ButtonsRequestDoor::description() const -> QString
{
  return QStringLiteral("requesting door sensor");
}

auto
ButtonsRequestDoor::responseType() const -> int
{
  return protocol::motherboard::ResponseType::GerconGetState;
}

auto
ButtonsRequestDoor::createRequest() const -> Request
{
  return std::make_unique<TRequest>(id());
}

auto
ButtonsRequestDoor::processResponse(const Response& t_response) -> bool
{
  const auto* response = dynamic_cast<TResponse*>(t_response.get());
  if (response == nullptr) {
    setError(BackendError::ProtocolError);
    return false;
  }

  m_doorButton->setState(!response->state);
  m_doorButton->setTrigger(response->trigger);
  return !response->isError();
}

} // namespace cmd::motherboard
