#include "motherboard/cmd/engine_get_currents_operation.h"

#include "motherboard/device/engine.h"
#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/engine_request.h"
#include "protocol/engine_response.h"

namespace cmd::motherboard {

EngineGetCurrentsOperation::EngineGetCurrentsOperation(
  protocol::PacketID pid,
  gsl::not_null<device::Engine*> engine,
  QObject* parent)
  : AbstractMotherboardOperation{ pid, parent }
  , m_engine{ engine }
{
}

QString
EngineGetCurrentsOperation::description() const
{
  return QStringLiteral("REQUESTING ENGINE %1 CURRENTS")
    .arg(m_engine->address());
}

auto
EngineGetCurrentsOperation::responseType() const -> int
{
  return protocol::motherboard::ResponseType::EnginesGetCurrents;
}

AbstractSerialOperation::Request
EngineGetCurrentsOperation::createRequest() const
{
  return std::make_unique<TRequest>(id(), m_engine->address());
}

bool
EngineGetCurrentsOperation::processResponse(const Response& t_response)
{
  const auto* response{ dynamic_cast<TResponse*>(t_response.get()) };
  Q_ASSERT(response);

  m_engine->setStatus(response->engineState);
  m_engine->setCurrentAccel(response->currentAccel);
  m_engine->setCurrentHolding(response->currentStdby);

  return !response->isError();
}

} // namespace cmd::motherboard
