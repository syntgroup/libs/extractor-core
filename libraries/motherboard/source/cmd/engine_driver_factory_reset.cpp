#include "motherboard/cmd/engine_driver_factory_reset.h"

#include "motherboard/device/engine.h"
#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/engine_request.h"
#include "protocol/engine_response.h"

namespace cmd::motherboard {

EngineDriverFactoryReset::EngineDriverFactoryReset(
  protocol::PacketID pid,
  gsl::not_null<device::Engine*> engine,
  QObject* parent)
  : AbstractMotherboardOperation{ pid, parent }
  , m_engine{ engine }
{
}

QString
EngineDriverFactoryReset::description() const
{
  return QStringLiteral("Resetting engine %1 parameters to factory defaults")
    .arg(m_engine->address());
}

int
EngineDriverFactoryReset::responseType() const
{
  return protocol::motherboard::ResponseType::EnginesDriverFactoryDefaults;
}

AbstractSerialOperation::Request
EngineDriverFactoryReset::createRequest() const
{
  return std::make_unique<TRequest>(id(), m_engine->address());
}

bool
EngineDriverFactoryReset::processResponse(const Response& t_response)
{
  const auto* response{ dynamic_cast<TResponse*>(t_response.get()) };
  Q_ASSERT(response);
  m_engine->setStatus(response->engineState);
  return !response->isError();
}

} // namespace cmd::motherboard
