#include "motherboard/cmd/engine_write_acceleration_operation.h"

#include "motherboard/device/engine.h"
#include "motherboard/protocol/motherboard_encoder.h"
#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/engine_request.h"
#include "protocol/engine_response.h"
#include <QVariant>

namespace {
[[nodiscard]] constexpr auto
mode_to_name(device::FrequencyType type)
{
  switch (type) {
    case device::FrequencyType::F0:
      return QLatin1String{ "F0" };
    case device::FrequencyType::Fmax:
      return QLatin1String{ "Fmax" };
    default:
      break;
  }
  return QLatin1String{ "dFmax" };
}
} // namespace

namespace cmd::motherboard {

EngineWriteAccelerationOperation::EngineWriteAccelerationOperation(
  protocol::PacketID pid,
  gsl::not_null<device::Engine*> engine,
  device::Acceleration value,
  QObject* parent)
  : AbstractMotherboardOperation{ pid, parent }
  , _engine{ engine }
  , _newValue{ std::move(value) }
{
}

auto
EngineWriteAccelerationOperation::description() const -> QString
{
  const auto type{ mode_to_name(std::get<device::FrequencyType>(_newValue)) };
  const auto value{ QString::number(
    std::get<device::Frequency>(_newValue).value_of()) };
  const auto address{ QString::number(_engine->address()) };
  return QStringLiteral("Writing new acceleration %1 value %2 to engine %3")
    .arg(type, value, address);
}

auto
EngineWriteAccelerationOperation::responseType() const -> int
{
  switch (std::get<device::FrequencyType>(_newValue)) {
    case device::FrequencyType::F0:
      return protocol::motherboard::ResponseType::EnginesSetF0;
    case device::FrequencyType::Fmax:
      return protocol::motherboard::ResponseType::EnginesSetFMax;
    case device::FrequencyType::dFmax:
      return protocol::motherboard::ResponseType::EnginesSetDfMax;
    default:
      return -1;
  }
}

auto
EngineWriteAccelerationOperation::createRequest() const -> Request
{
  switch (_newValue.second) {
    case device::FrequencyType::F0:
      return std::make_unique<protocol::motherboard::EngineWriteF0>(
        id(), _engine->address(), std::get<device::Frequency>(_newValue));
    case device::FrequencyType::Fmax:
      return std::make_unique<protocol::motherboard::EngineWriteFMax>(
        id(), _engine->address(), std::get<device::Frequency>(_newValue));
    case device::FrequencyType::dFmax:
      return std::make_unique<protocol::motherboard::EngineWriteDFMax>(
        id(), _engine->address(), std::get<device::Frequency>(_newValue));
  }
}

auto
EngineWriteAccelerationOperation::processResponse(const Response& t_response)
  -> bool
{
  const auto* response{ dynamic_cast<TResponse*>(t_response.get()) };
  if (response == nullptr) {
    return false;
  }

  _engine->setStatus(response->engineState);

  return !response->isError();
}

} // namespace cmd::motherboard
