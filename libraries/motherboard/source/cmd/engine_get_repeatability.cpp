#include "motherboard/cmd/engine_get_repeatability.h"

#include "motherboard/device/engine.h"
#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/engine_request.h"
#include "protocol/engine_response.h"

namespace cmd::motherboard {

EngineGetRepeatabilityOperation::EngineGetRepeatabilityOperation(
  protocol::PacketID pid,
  gsl::not_null<device::Engine*> engine,
  QObject* parent)
  : AbstractMotherboardOperation{ pid, parent }
  , m_engine{ engine }
{
}

QString
EngineGetRepeatabilityOperation::description() const
{
  return QStringLiteral("Requesting engine #%1 repeatability")
    .arg(m_engine->address());
}

int
EngineGetRepeatabilityOperation::responseType() const
{
  return protocol::motherboard::ResponseType::EnginesGetRepeatability;
}

AbstractSerialOperation::Request
EngineGetRepeatabilityOperation::createRequest() const
{
  return std::make_unique<TRequest>(id(), m_engine->address());
}

bool
EngineGetRepeatabilityOperation::processResponse(const Response& t_response)
{
  const auto* response{ dynamic_cast<TResponse*>(t_response.get()) };
  Q_ASSERT(response);

  m_engine->setRepeatability(response->repeatability);
  m_engine->setStatus(response->engineState);

  return !response->isError();
}

} // namespace cmd::motherboard
