#include "motherboard/cmd/engine_get_position_current.h"

#include "motherboard/device/engine.h"
#include "motherboard/protocol/motherboard_error.h"
#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/engine_request.h"
#include "protocol/engine_response.h"
#include "utils/converter.h"

namespace cmd::motherboard {

EngineGetPositionCurrentOperation::EngineGetPositionCurrentOperation(
  protocol::PacketID pid,
  gsl::not_null<device::Engine*> engine,
  QObject* parent)
  : AbstractMotherboardOperation{ pid, parent }
  , m_engine{ engine }
{
}

QString
EngineGetPositionCurrentOperation::description() const
{
  return QStringLiteral("Requesting engine %1 current position")
    .arg(m_engine->address());
}

int
EngineGetPositionCurrentOperation::responseType() const
{
  return protocol::motherboard::ResponseType::EnginesGetPosition;
}

AbstractSerialOperation::Request
EngineGetPositionCurrentOperation::createRequest() const
{
  return std::make_unique<TRequest>(id(), m_engine->address());
}

bool
EngineGetPositionCurrentOperation::processResponse(const Response& t_response)
{
  const auto* response{ dynamic_cast<TResponse*>(t_response.get()) };
  if (response == nullptr) {
    return false;
  }

  m_engine->setStatus(response->engineState);

  if (m_engine->status().undefinedPos) {
    m_engine->setCurrentPosition(response->position);
  }

  using Error = protocol::motherboard::MotherboardError::Error;
  switch (response->errorCode()) {
    case Error::NoError:
      return true;

    case Error::EngineNoFreeTimersNow:
    case Error::EngineNoFreeTimersNow1:
    case Error::EngineNoFreeTimersNow2:
    case Error::EngineNoFreeTimersNow3:
    case Error::EngineNoFreeTimersNow4:
    case Error::EngineNoFreeTimersNow5:
    case Error::EngineNoFreeTimersNow6:
    case Error::EngineCanNotFindZeroSwitch2:
    case Error::EngineCanNotMoveWhenMovingToZero:
    case Error::EngineCrossUndefinedSwitch:
    case Error::EngineCrossSwitch1:
    case Error::EngineCrossSwitch2:
      setError(BackendError::OperationError,
               utils::to_string(response->errorCode()));
      return false;

    default:
      setError(BackendError::ProtocolError,
               utils::to_string(response->errorCode()));
      return false;
  }

  return !response->isError();
}

} // namespace cmd::motherboard
