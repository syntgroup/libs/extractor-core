#include "motherboard/cmd/system_zeroize_error_operation.h"

#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/system_request.h"

namespace cmd::motherboard {

QString
SystemZeroizeErrorOperation::description() const
{
  return QStringLiteral("Clearing last error");
}

auto
SystemZeroizeErrorOperation::responseType() const -> int
{
  return protocol::motherboard::ResponseType::SystemResetError;
}

AbstractSerialOperation::Request
SystemZeroizeErrorOperation::createRequest() const
{
  return std::make_unique<TRequest>(id());
}

} // namespace cmd::motherboard
