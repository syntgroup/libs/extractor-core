#include "motherboard/cmd/goto_em_stop_operation.h"

#include "motherboard/device/engine.h"
#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/goto_request.h"
#include "protocol/goto_response.h"

namespace cmd::motherboard {

GotoEmStopOperation::GotoEmStopOperation(protocol::PacketID pid,
                                         Param&& param,
                                         QObject* parent)
  : AbstractMotherboardOperation{ pid, parent }
  , m_params{ std::move(param) }
{
}

bool
GotoEmStopOperation::begin()
{
  if (m_params.empty()) {
    setError(BackendError::DataError, QStringLiteral("no engines requested"));
  }

  if (m_params.size() > 2) {
    setError(BackendError::DataError,
             QStringLiteral("too many engines requested"));
  }

  if (m_params.size() > 1 &&
      m_params.at(0)->address() == m_params.at(1)->address()) {
    setError(BackendError::DataError, QStringLiteral("same engines requested"));
  }

  return isError();
}

QString
GotoEmStopOperation::description() const
{
  QStringList infos;
  std::transform(
    m_params.cbegin(),
    m_params.cend(),
    std::back_inserter(infos),
    [](auto&& engine) { return QString::number(engine->address()); });
  return QStringLiteral("Emergency stopping axis %1").arg(infos.join(" & "));
}

auto
GotoEmStopOperation::responseType() const -> int
{
  return protocol::motherboard::ResponseType::GotoEmStop;
}

AbstractSerialOperation::Request
GotoEmStopOperation::createRequest() const
{
  TRequest::Params params;
  std::transform(m_params.begin(),
                 m_params.end(),
                 std::back_inserter(params),
                 [](auto&& engine) { return engine->address(); });
  return std::make_unique<TRequest>(id(), params);
}

bool
GotoEmStopOperation::processResponse(const Response& t_response)
{
  const auto* response{ dynamic_cast<TResponse*>(t_response.get()) };
  if (response == nullptr) {
    return false;
  }

  for (auto&& engine : m_params) {
    engine->setStatus(response->engineState);
  }

  return !response->isError();
}

} // namespace cmd::motherboard
