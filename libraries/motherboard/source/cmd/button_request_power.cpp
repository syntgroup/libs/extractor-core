#include "motherboard/cmd/button_request_power.h"

#include "common/discrete_input.h"
#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/buttons_request.h"
#include "protocol/buttons_response.h"

namespace cmd::motherboard {

ButtonsRequestPowerButton::ButtonsRequestPowerButton(
  protocol::PacketID pid,
  gsl::not_null<device::DiscreteOutput*> button,
  QObject* parent)
  : AbstractMotherboardOperation{ pid, parent }
  , m_powerButton{ button }
{
}

auto
ButtonsRequestPowerButton::description() const -> QString
{
  return QStringLiteral("request power button state");
}

auto
ButtonsRequestPowerButton::responseType() const -> int
{
  return protocol::motherboard::ResponseType::PwrBtnGetState;
}

auto
ButtonsRequestPowerButton::createRequest() const -> Request
{
  return std::make_unique<TRequest>(id());
}

auto
ButtonsRequestPowerButton::processResponse(const Response& t_response) -> bool
{
  const auto* response = dynamic_cast<TResponse*>(t_response.get());
  if (response == nullptr) {
    setError(BackendError::DataError);
    return false;
  }

  m_powerButton->setState(response->state);
  m_powerButton->setTrigger(response->trigger);
  return !response->isError();
}

} // namespace cmd::motherboard
