#include "motherboard/cmd/rgb_led_set_operation.h"

#include "motherboard/device/rgb_leds.h"
#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/rgb_request.h"
#include "serial/protocol/response_interface.h"

namespace cmd::motherboard {

RgbLedSetOperation::RgbLedSetOperation(protocol::PacketID pid,
                                       gsl::not_null<device::RgbLeds*> leds,
                                       std::vector<Qt::GlobalColor>&& colors,
                                       QObject* parent)
  : AbstractMotherboardOperation{ pid, parent }
  , m_leds{ std::move(leds) }
  , m_colors{ std::move(colors) }
{
}

QString
RgbLedSetOperation::description() const
{
  return QStringLiteral("updating rgb leds");
}

auto
RgbLedSetOperation::responseType() const -> int
{
  return protocol::motherboard::ResponseType::RgbSetOnOff;
}

AbstractSerialOperation::Request
RgbLedSetOperation::createRequest() const
{
  return std::make_unique<TRequest>(id(), m_colors);
}

bool
RgbLedSetOperation::processResponse(const Response& response)
{
  m_leds->setLedColors(m_colors);
  return !response->isError();
}

} // namespace cmd::motherboard
