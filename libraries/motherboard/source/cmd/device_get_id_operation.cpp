#include "motherboard/cmd/device_get_id_operation.h"

#include "motherboard/device/motherboard_params.h"
#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/device_request.h"
#include "protocol/device_response.h"

namespace cmd::motherboard {

DeviceGetIdOperation::DeviceGetIdOperation(
  protocol::PacketID pid,
  gsl::not_null<device::MotherboardParameters*> motherboard_parameters,
  QObject* parent)
  : AbstractMotherboardOperation{ pid, parent }
  , m_motherboardParameters{ motherboard_parameters }
{
}

QString
DeviceGetIdOperation::description() const
{
  return QStringLiteral("Requesting ID from MB");
}

int
DeviceGetIdOperation::responseType() const
{
  return protocol::motherboard::ResponseType::DeviceGetId;
}

AbstractSerialOperation::Request
DeviceGetIdOperation::createRequest() const
{
  return std::make_unique<TRequest>(id());
}

bool
DeviceGetIdOperation::processResponse(const Response& t_response)
{
  const auto* response{ dynamic_cast<TResponse*>(t_response.get()) };
  if (response->isError()) {
    return false;
  }
  m_motherboardParameters->motherboardId.emplace(response->deviceId);
  return true;
}

} // namespace cmd::motherboard
