#include "motherboard/cmd/power_set_buzzer_operation.h"

#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/power_request.h"

namespace cmd::motherboard {

PowerSetBuzzerOperation::PowerSetBuzzerOperation(protocol::PacketID pid,
                                                 bool enable,
                                                 QObject* parent)
  : AbstractMotherboardOperation{ pid, parent }
  , _enable{ enable }
{
}

QString
PowerSetBuzzerOperation::description() const
{
  return QStringLiteral("Setting Buzzer %1").arg(_enable ? "ON" : "OFF");
}

auto
PowerSetBuzzerOperation::responseType() const -> int
{
  return protocol::motherboard::ResponseType::PowerSetBuzzer;
}

AbstractSerialOperation::Request
PowerSetBuzzerOperation::createRequest() const
{
  return std::make_unique<TRequest>(id(), _enable);
}

} // namespace cmd::motherboard
