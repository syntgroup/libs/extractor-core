#include "motherboard/cmd/multi_goto_absolute_operation.h"

#include "motherboard/cmd/engine_get_max_speed.h"
#include "motherboard/cmd/engine_get_position_current.h"
#include "motherboard/cmd/engine_write_acceleration_operation.h"
#include "motherboard/cmd/engine_write_max_speed_operation.h"
#include "motherboard/cmd/goto_absolute_pos_operation.h"
#include "motherboard/cmd/goto_em_stop_operation.h"
#include "motherboard/cmd/goto_sm_stop_operation.h"
#include "motherboard/cmd/system_zeroize_error_operation.h"
#include "motherboard/device/engine.h"

namespace {
constexpr device::SpeedIndex zero_speed{ 0 };
} // namespace

namespace cmd::multi {

GotoAbsoluteOperation::GotoAbsoluteOperation(
  gsl::not_null<device::Engine*> engine,
  device::Coordinate target_pos,
  device::SpeedIndex new_speed,
  device::AccelerationCurve curve,
  device::SpeedMode legacy_mode,
  QObject* parent)
  : cmd::AbstractMultiStageOperation{ parent }
  , _engine{ engine }
  , _target_position{ std::move(target_pos) }
  , _travel_speed{ std::move(new_speed) }
  , _curve{ curve }
  , _legacyMode{ legacy_mode }
{
}

void
GotoAbsoluteOperation::start()
{
  if (_legacyMode == device::SpeedMode::Legacy &&
      _curve != device::AccelerationCurve::Normal) {
    finishWithError(BackendError::DataError,
                    "Requested fast speed profile on legacy machine");
    return;
  }

  if (operationState() != AbstractOperation::Ready) {
    finishWithError(BackendError::UnknownError, "Not ready to start");
    return;
  }

  advanceOperationState();
}

auto
GotoAbsoluteOperation::description() const -> QString
{
  const auto address{ QString::number(_engine->address()) };
  const auto target{ QString::number(_target_position.value_of()) };
  return QStringLiteral("MULTI Moving Axis %1 to absolute position %2")
    .arg(address, target);
}

void
GotoAbsoluteOperation::execGotoAbsolutePos()
{
  using Operation = motherboard::GotoAbsolutePosOperation;
  Operation::Params params{ std::make_pair(_engine, _target_position) };
  registerSubOperation<Operation>(std::move(params));
}

void
GotoAbsoluteOperation::checkGotoAbsolutePos()
{
  if (!_engine->status().engineGo &&
      _engine->currentPosition() == _engine->targetPosition()) {
    finish();
    return;
  }

  if (!_engine->status().undefinedPos) {
    finishWithError(
      BackendError::OperationError,
      QStringLiteral("Engine is moving but position isn't defined?"));
    return;
  }

  registerSubOperation<motherboard::EngineGetPositionCurrentOperation>(_engine);
}

void
GotoAbsoluteOperation::resetLastError()
{
  registerSubOperation<motherboard::SystemZeroizeErrorOperation>();
}

void
GotoAbsoluteOperation::writeMaxSpeed(device::SpeedIndex value)
{
  registerSubOperation<motherboard::EngineWriteMaxSpeedOperation>(_engine,
                                                                  value);
}

void
GotoAbsoluteOperation::checkMaxSpeed()
{
  registerSubOperation<motherboard::EngineGetMaxSpeedOperation>(_engine);
}

void
GotoAbsoluteOperation::smoothStop()
{
  using Operation = motherboard::GotoSmStopOperation;
  Operation::Param params{ _engine };
  registerSubOperation<Operation, execution::DeadPolicy>(std::move(params));
}

void
GotoAbsoluteOperation::emergencyStop()
{
  using Operation = motherboard::GotoEmStopOperation;
  Operation::Param params{ _engine };
  registerSubOperation<Operation, execution::DeadPolicy>(std::move(params));
}

void
GotoAbsoluteOperation::execWriteAcceleration(device::Acceleration frequency)
{
  registerSubOperation<motherboard::EngineWriteAccelerationOperation>(
    _engine, frequency);
}

void
GotoAbsoluteOperation::nextStateLogic()
{
  switch (operationState()) {
    case Ready: {
      setOperationState(WriteFrequencyStart);
      const auto profile = _engine->accelerationProfile(_curve);
      if (profile.has_value()) {
        execWriteAcceleration(profile->get().f0());
      } else {
        finishWithError(BackendError::UnknownError,
                        QString{ "WRITE_FREQ_START: cant access acceleration "
                                 "profile to check movement speed: %1" }
                          .arg(_engine->name()));
      }
      break;
    }

    case WriteFrequencyStart: {
      setOperationState(WriteFrequencyStop);
      const auto profile = _engine->accelerationProfile(_curve);
      if (profile.has_value()) {
        execWriteAcceleration(profile->get().fmax());
      } else {
        finishWithError(BackendError::UnknownError,
                        QString{ "WRITE_FREQ_STOP: cant access acceleration "
                                 "profile to check movement speed: %1" }
                          .arg(_engine->name()));
      }
      break;
    }

    case WriteFrequencyStop: {
      setOperationState(WriteFrequencyStep);
      const auto profile = _engine->accelerationProfile(_curve);
      if (profile.has_value()) {
        execWriteAcceleration(profile->get().dfmax());
      } else {
        finishWithError(BackendError::UnknownError,
                        QString{ "WRITE_FREQ_STEP: cant access acceleration "
                                 "profile to check movement speed: %1" }
                          .arg(_engine->name()));
      }
      break;
    }

    case WriteFrequencyStep:
      setOperationState(CheckingMaxSpeed);
      checkMaxSpeed();
      break;

    case CheckingMaxSpeed:
      // по идее, нам из порта приехало некое значение, которое мы тут можем
      // сравнить.
      // Если оно не сойдется с расчетным значением из профиля, то можно зайти
      // на второй круг записи, или просто дропнуть все к чертям.
      //      if (m_engine->maxSpeed() !=
      //          m_engine->accelerationProfile(m_curve)->maxSpeedIndex()) {
      //      }
      // Но пока не будем этого делать, просто записываем новое значение
      setOperationState(WriteTravelSpeed);
      writeMaxSpeed(_travel_speed);
      break;

    case WriteTravelSpeed:
      setOperationState(ExecGotoAbsolutePos);
      execGotoAbsolutePos();
      break;

    case ExecGotoAbsolutePos:
      setOperationState(CheckGotoAbsolutePos);
      checkGotoAbsolutePos();
      break;

    case CheckGotoAbsolutePos:
      checkGotoAbsolutePos();
      break;

    case ZeroingError:
      setOperationState(_buffer);
      advanceOperationState();
      break;

    case EmergencyStop:
      setOperationState(Idle);
      emergencyStop();
      break;

    case SmoothStop:
      setOperationState(Idle);
      smoothStop();
      break;

    case Idle:
      break;

    default:
      finishWithError(
        BackendError::UnknownError,
        QStringLiteral(
          "Operation %1 got to unknown state, have to terminate here")
          .arg(description()));
      break;
  }
}

void
GotoAbsoluteOperation::onSubOperationError(
  const AbstractSerialOperation& operation)
{
  if (operation.error() == BackendError::OperationError) {
    finishWithError(BackendError::OperationError, operation.errorString());
  } else if (operation.error() == BackendError::TimeoutError) {
    finishWithError(BackendError::TimeoutError, operation.errorString());
  } else {
    switch (auto state = static_cast<OperationState>(operationState())) {
      case GotoAbsoluteOperation::ExecGotoAbsolutePos:
      case GotoAbsoluteOperation::CheckGotoAbsolutePos:
        _buffer = state;
        setOperationState(GotoAbsoluteOperation::ZeroingError);
        resetLastError();
        emit stepRetry();
        break;

      default:
        break;
    }
  }
}

void
GotoAbsoluteOperation::pause()
{
  if (operationState() == AbstractOperation::Finished) {
    return;
  }

  _buffer = static_cast<OperationState>(operationState());
  setOperationState(SmoothStop);
}

void
GotoAbsoluteOperation::resume()
{
  if (operationState() == AbstractOperation::Finished) {
    return;
  }

  if (_buffer == OperationState::CheckGotoAbsolutePos &&
      _engine->currentPosition() != _engine->targetPosition()) {
    _buffer = OperationState::CheckingMaxSpeed;
  }

  setOperationState(_buffer);
  advanceOperationState();
}

} // namespace cmd::multi
