#include "motherboard/cmd/engine_write_max_speed_operation.h"

#include "motherboard/device/engine.h"
#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/engine_request.h"
#include "protocol/engine_response.h"
#include "utils/converter.h"

namespace cmd::motherboard {

EngineWriteMaxSpeedOperation::EngineWriteMaxSpeedOperation(
  protocol::PacketID pid,
  gsl::not_null<device::Engine*> engine,
  device::SpeedIndex value,
  QObject* parent)
  : AbstractMotherboardOperation{ pid, parent }
  , m_engine{ engine }
  , m_newSpeed{ std::move(value) }
{
}

QString
EngineWriteMaxSpeedOperation::description() const
{
  const auto new_speed{ QString::number(m_newSpeed.value_of()) };
  const auto address{ QString::number(m_engine->address(), 16) };
  return QStringLiteral("Setting max speed %1 for engine %2")
    .arg(new_speed, address);
}

AbstractSerialOperation::Request
EngineWriteMaxSpeedOperation::createRequest() const
{
  return std::make_unique<TRequest>(id(), m_engine->address(), m_newSpeed);
}

int
EngineWriteMaxSpeedOperation::responseType() const
{
  return protocol::motherboard::ResponseType::EnginesSetMaxSpeed;
}

bool
EngineWriteMaxSpeedOperation::processResponse(const Response& t_response)
{
  const auto* response{ dynamic_cast<TResponse*>(t_response.get()) };
  m_engine->setStatus(response->engineState);

  if (response->errorCode() !=
      protocol::motherboard::MotherboardError::NoError) {
    setError(BackendError::OperationError,
             utils::to_string(response->errorCode()));
  }

  return !response->isError();
}

} // namespace cmd::motherboard
