#include "motherboard/cmd/goto_n_ticks_unc_wo_operation.h"

#include "motherboard/device/engine.h"
#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/goto_request.h"
#include "protocol/goto_response.h"
#include "utils/converter.h"

namespace cmd::motherboard {

GotoNTicksUncWoOperation::GotoNTicksUncWoOperation(
  protocol::PacketID pid,
  gsl::not_null<device::Engine*> engine,
  device::Coordinate ticks,
  QObject* parent)
  : AbstractMotherboardOperation{ pid, parent }
  , m_engine{ engine }
  , m_ticks{ ticks }
{
}

QString
GotoNTicksUncWoOperation::description() const
{
  const auto address{ QString::number(m_engine->address()) };
  const auto value{ QString::number(m_ticks.value_of()) };
  return QStringLiteral("Axis %1 goto UNC w/o opts to %2 steps")
    .arg(address, value);
}

auto
GotoNTicksUncWoOperation::responseType() const -> int
{
  return protocol::motherboard::ResponseType::GotoCmd;
}

AbstractSerialOperation::Request
GotoNTicksUncWoOperation::createRequest() const
{
  return std::make_unique<TRequest>(id(), m_engine->address(), m_ticks);
}

bool
GotoNTicksUncWoOperation::processResponse(const Response& t_response)
{
  const auto* response{ dynamic_cast<TResponse*>(t_response.get()) };
  Q_ASSERT(response);

  if (response->errorCode() !=
      protocol::motherboard::MotherboardError::NoError) {
    setError(BackendError::OperationError,
             utils::to_string(response->errorCode()));
  }

  m_engine->setStatus(response->engineState);
  return !response->isError();
}

} // namespace cmd::motherboard
