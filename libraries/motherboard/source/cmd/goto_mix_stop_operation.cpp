#include "motherboard/cmd/goto_mix_stop_operation.h"

#include "motherboard/device/engine.h"
#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/goto_request.h"
#include "protocol/goto_response.h"
#include "utils/converter.h"

namespace cmd::motherboard {

GotoMixStopOperation::GotoMixStopOperation(
  protocol::PacketID pid,
  gsl::not_null<device::Engine*> engine,
  QObject* parent)
  : AbstractMotherboardOperation{ pid, parent }
  , m_engine{ engine }
{
}

QString
GotoMixStopOperation::description() const
{
  return QStringLiteral("Requesting MIX STOP for engine %1")
    .arg(m_engine->address());
}

auto
GotoMixStopOperation::responseType() const -> int
{
  return protocol::motherboard::ResponseType::GotoMixModeOff;
}

AbstractSerialOperation::Request
GotoMixStopOperation::createRequest() const
{
  return std::make_unique<TRequest>(id(), m_engine->address());
}

bool
GotoMixStopOperation::processResponse(const Response& t_response)
{
  const auto* response{ dynamic_cast<TResponse*>(t_response.get()) };
  Q_ASSERT(response);

  if (response->errorCode() !=
      protocol::motherboard::MotherboardError::NoError) {
    setError(BackendError::OperationError,
             utils::to_string(response->errorCode()));
  }

  m_engine->setStatus(response->engineState);
  emit m_engine->stopMixing();

  return !response->isError();
}

} // namespace cmd::motherboard
