#include "motherboard/cmd/device_write_id_operation.h"

#include "motherboard/device/motherboard_params.h"
#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/device_request.h"
#include "protocol/device_response.h"

namespace cmd::motherboard {

DeviceWriteIdOperation::DeviceWriteIdOperation(
  protocol::PacketID pid,
  gsl::not_null<device::MotherboardParameters*> parameters,
  uint32_t new_device_id,
  QObject* parent)
  : AbstractMotherboardOperation{ pid, parent }
  , m_newDeviceId{ new_device_id }
  , m_motherboardParameters{ parameters }
{
}

QString
DeviceWriteIdOperation::description() const
{
  return QStringLiteral("Setting to MB new ID: %1").arg(m_newDeviceId);
}

int
DeviceWriteIdOperation::responseType() const
{
  return protocol::motherboard::ResponseType::DeviceSetId;
}

AbstractSerialOperation::Request
DeviceWriteIdOperation::createRequest() const
{
  return std::make_unique<TRequest>(id(), m_newDeviceId);
}

bool
DeviceWriteIdOperation::processResponse(const Response& t_response)
{
  const auto* response{ dynamic_cast<TResponse*>(t_response.get()) };
  m_motherboardParameters->motherboardId = m_newDeviceId;

  return !response->isError();
}

} // namespace cmd::motherboard
