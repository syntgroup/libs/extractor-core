#include "motherboard/cmd/power_request_turn_off.h"

#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/power_request.h"

namespace cmd::motherboard {

QString
cmd::motherboard::PowerRequestTurnOff::description() const
{
  return QStringLiteral("requesting poweroff");
}

int
cmd::motherboard::PowerRequestTurnOff::responseType() const
{
  return protocol::motherboard::ResponseType::PowerTurnOff;
}

AbstractSerialOperation::Request
PowerRequestTurnOff::createRequest() const
{
  return std::make_unique<TRequest>(id());
}

} // namespace cmd::motherboard
