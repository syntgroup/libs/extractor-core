#include "motherboard/cmd/engine_get_max_speed.h"

#include "motherboard/device/engine.h"
#include "motherboard/protocol/motherboard_response_type.h"
#include "protocol/engine_request.h"
#include "protocol/engine_response.h"
#include "utils/converter.h"

namespace cmd::motherboard {

EngineGetMaxSpeedOperation::EngineGetMaxSpeedOperation(
  protocol::PacketID pid,
  gsl::not_null<device::Engine*> engine,
  QObject* parent)
  : AbstractMotherboardOperation{ pid, parent }
  , m_engine{ engine }
{
}

QString
EngineGetMaxSpeedOperation::description() const
{
  return QStringLiteral("Requesting engine %1 max speed")
    .arg(m_engine->address());
}

auto
EngineGetMaxSpeedOperation::responseType() const -> int
{
  return protocol::motherboard::ResponseType::EnginesGetMaxSpeed;
}

AbstractSerialOperation::Request
EngineGetMaxSpeedOperation::createRequest() const
{
  return std::make_unique<TRequest>(id(), m_engine->address());
}

bool
EngineGetMaxSpeedOperation::processResponse(const Response& t_response)
{
  using MotherboardError = protocol::motherboard::MotherboardError;

  const auto* response{ dynamic_cast<TResponse*>(t_response.get()) };
  Q_ASSERT(response);

  m_engine->setStatus(response->engineState);
  m_engine->setMaxSpeed(response->maxSpeed);

  if (response->errorCode() != MotherboardError::NoError) {
    setError(BackendError::OperationError,
             utils::to_string(response->errorCode()));
  }

  return !response->isError();
}

} // namespace cmd::motherboard
