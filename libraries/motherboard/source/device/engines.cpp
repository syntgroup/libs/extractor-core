#include "motherboard/device/engines.h"

#include "motherboard/device/engine.h"

namespace device {

Engines::Engines(Container&& engines)
  : m_engines{ std::move(engines) }
{
}

Engines::~Engines() = default;

auto
Engines::engineByName(AxisAddress axis) const -> gsl::not_null<Engine*>
{
  return m_engines.at(axis).get();
}

auto
Engines::engineByAxis(const QString& axis) const -> Engine*
{
  const auto iter = std::find_if(
    m_engines.cbegin(), m_engines.cend(), [axis](const auto& engine) {
      return engine.second->axis() == axis.toUpper();
    });

  if (iter == m_engines.end()) {
    return nullptr;
  }

  return iter->second.get();
}

} // namespace device
