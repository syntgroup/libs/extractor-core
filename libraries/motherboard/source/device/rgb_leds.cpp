#include "motherboard/device/rgb_leds.h"

namespace device {

RgbLeds::RgbLeds(size_t count)
{
  for (size_t idx = 0; idx < count; ++idx) {
    m_leds.push_back(std::make_unique<RgbLed>(idx));
  }
}

RgbLeds::RgbLeds(Container&& leds)
  : m_leds{ std::move(leds) }
{
}

auto
RgbLeds::setLedColors(const std::vector<Qt::GlobalColor>& color_scheme) -> bool
{
  if (color_scheme.size() != m_leds.size()) {
    return false;
  }

  for (size_t idx = 0, max = color_scheme.size(); idx < max; ++idx) {
    m_leds.at(idx)->setValue(color_scheme.at(idx));
  }

  return true;
}

} // namespace device

namespace device {

RgbLed::RgbLed(uint8_t idx, Qt::GlobalColor color, QObject* parent)
  : QObject{ parent }
  , AnalogInput<Qt::GlobalColor>{ color }
  , m_idx{ idx }
{
}

void
RgbLed::setValue(Qt::GlobalColor new_color)
{
  if (_value != new_color) {
    _value = new_color;
    emit colorChanged(_value);
  }
}

} // namespace device
