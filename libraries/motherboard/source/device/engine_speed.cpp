#include "motherboard/device/engine_speed.h"

#include <QDebugStateSaver>
#include <cmath>

namespace {

//! @brief коэффициент приближения k
//! @details типичное значение 0,95 … 0,99, показывает, с какой точностью fmax
//! приближается к асимптотическому значению, [б/р]
//! @default 0.95
constexpr auto approximation_coeff{ 0.95 };

[[nodiscard]] auto
approx_max_freq(const device::AccelerationProfile& profile)
{
  return profile.fmax().first / approximation_coeff;
}

[[nodiscard]] auto
speed_coefficients(const device::AccelerationProfile& profile)
{
  const auto f0   = std::get<device::Frequency>(profile.f0()).value_of();
  const auto fmax = std::get<device::Frequency>(profile.fmax()).value_of();
  const auto dfmax =
    std::get<device::Frequency>(profile.dfmax()).value_of() / 1000.0;

  const auto alpha = std::log(1.0 - approximation_coeff * (f0 / fmax));
  const auto beta  = dfmax / ((fmax / approximation_coeff) - f0);

  return std::make_pair(alpha, beta);
}

} // namespace

namespace device {

AccelerationProfile::AccelerationProfile(Frequency f0,
                                         Frequency fmax,
                                         Frequency dfmax)
  : m_f0{ std::make_pair(std::move(f0), FrequencyType::F0) }
  , m_fmax{ std::make_pair(std::move(fmax), FrequencyType::Fmax) }
  , m_dfmax{ std::make_pair(std::move(dfmax), FrequencyType::dFmax) }
  , m_index{ calculateMaxSpeedIndex() }
{
}

SpeedIndex
AccelerationProfile::calculateMaxSpeedIndex() const
{
  const auto [alpha, beta] = speed_coefficients(*this);
  const auto tau = (alpha - std::log(1.0 - approximation_coeff)) / beta;

  return SpeedIndex{ std::lround(tau) };
}

Frequency
AccelerationProfile::indexToFrequency(SpeedIndex index) const
{
  const auto amp           = approx_max_freq(*this).value_of();
  const auto [alpha, beta] = speed_coefficients(*this);
  const auto speed_index   = index.value_of();

  return Frequency{ amp * (1.0 - std::exp(alpha - beta * speed_index)) };
}

SpeedIndex
AccelerationProfile::frequencyToIndex(Frequency f) const
{
  const auto f0    = std::get<Frequency>(m_f0).value_of();
  const auto fmax  = std::get<Frequency>(m_fmax).value_of();
  const auto dfmax = std::get<Frequency>(m_dfmax).value_of() / 1000.0;
  const auto freq  = f.value_of();

  const auto alpha = std::log((fmax - approximation_coeff * f0) /
                              (fmax - approximation_coeff * freq));
  const auto beta  = (fmax / approximation_coeff) - f0;

  return SpeedIndex{ std::lround(alpha * beta / dfmax) };
}

auto
operator<<(QDebug debug, const AccelerationProfile& profile) -> QDebug
{
  const QDebugStateSaver saver{ debug };

  debug.noquote() << "ACCELERATION PROFILE:";
  debug << "/ F0:" << profile.m_f0.first.value_of();
  debug << "/ Fmax:" << profile.m_fmax.first.value_of();
  debug << "/ dFmax:" << profile.m_dfmax.first.value_of();
  debug << "/ MaxSpeedIndex:" << profile.m_index.value_of();

  return debug;
}

} // namespace device
