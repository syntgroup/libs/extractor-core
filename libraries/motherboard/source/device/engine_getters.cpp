#include "motherboard/device/engine.h"

namespace device {

auto
Engine::axisOffset() const -> double
{
  return m_params->axisOffset;
}

auto
Engine::currentAccel() const -> Current
{
  return m_params->currentMoving;
}

auto
Engine::axis() const -> QChar
{
  return m_params->axis;
}

auto
Engine::microstep() const -> Microstep
{
  return m_params->microstep;
}

auto
Engine::zeroSwitch() const -> ZeroSwitch
{
  return m_params->zeroSwitch;
}

auto
Engine::address() const -> uint8_t
{
  return m_params->address;
}

auto
Engine::repeatability() const -> Repeatability
{
  return m_params->repeatability;
}

auto
Engine::maxSpeed() const -> SpeedIndex
{
  return m_maxSpeed;
}

auto
Engine::homePosition() const -> Coordinate
{
  return m_params->homePosition;
}

auto
Engine::name() const -> QString
{
  return m_params->name;
}

auto
Engine::homingSpeed() const -> SpeedIndex
{
  return m_params->homingSpeed;
}

auto
Engine::travelSpeed() const -> SpeedIndex
{
  return m_params->travelSpeed;
}

auto
Engine::actionSpeed() const -> SpeedIndex
{
  return m_params->actionSpeed;
}

auto
Engine::currentStdby() const -> Current
{
  return m_params->currentStandby;
}

auto
Engine::stepsPerMmRatio() const -> double
{
  return m_params->stepsPerMmRatio;
}

} // namespace device
