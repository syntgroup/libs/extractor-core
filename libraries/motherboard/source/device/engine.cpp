#include "motherboard/device/engine.h"

#include "motherboard/device/engine_defaults.h"

namespace {

[[nodiscard]] constexpr auto
microstep(device::Microstep key)
{
  using Microstep = device::Microstep;
  switch (key) {
    case Microstep::Step_1_1:
      return 1;
    case Microstep::Step_1_2:
      return 2;
    case Microstep::Step_1_4:
      return 4;
    case Microstep::Step_1_8:
      return 8;
    case Microstep::Step_1_16:
      return 16;
    case Microstep::Step_1_32:
      return 32;
    case Microstep::Step_1_64:
      return 64;
    case Microstep::Step_1_128:
      return 128;
    case Microstep::Step_1_256:
      return 256;
    case Microstep::Undefined:
      break;
  }
  return -1;
}

constexpr std::array curves{ device::AccelerationCurve::Slow,
                             device::AccelerationCurve::Normal,
                             device::AccelerationCurve::Fast };

} // namespace

namespace device {

Engine::Engine(uint8_t adr, QChar axis, QObject* parent)
  : QObject{ parent }
  , m_params{ std::make_unique<EngineParams>() }
{
  m_params->address = adr;
  m_params->axis    = axis;

  setObjectName(m_params->axis + ' ' + QString::number(m_params->address));

  for (auto curve : curves) {
    m_profiles.emplace(curve, std::make_unique<AccelerationProfile>());
  }
}

Engine::Engine(std::unique_ptr<EngineParams>&& params, QObject* parent)
  : QObject{ parent }
  , m_params{ std::move(params) }
{
  setObjectName(m_params->axis + ' ' + QString::number(m_params->address));
  for (auto curve : curves) {
    m_profiles.emplace(curve, std::make_unique<AccelerationProfile>());
  }
}

Engine::Engine(std::unique_ptr<EngineParams>&& params,
               std::unique_ptr<AccelerationProfile>&& profile_slow,
               std::unique_ptr<AccelerationProfile>&& profile_normal,
               std::unique_ptr<AccelerationProfile>&& profile_fast,
               QObject* parent)
  : QObject{ parent }
  , m_params{ std::move(params) }
{
  m_profiles.emplace(AccelerationCurve::Slow, std::move(profile_slow));
  m_profiles.emplace(AccelerationCurve::Normal, std::move(profile_normal));
  m_profiles.emplace(AccelerationCurve::Fast, std::move(profile_fast));

  setObjectName(m_params->axis + ' ' + QString::number(m_params->address));
}

Engine::Engine(ParamsContainer&& params,
               SpeedProfileContainer&& profiles,
               QObject* parent)
  : QObject{ parent }
  , m_params{ std::move(params) }
{
  if (profiles.empty() || profiles.size() > 3) {
    return;
  }
  m_profiles = std::move(profiles);
}

Engine::~Engine() = default;

void
Engine::loadSettings(std::unique_ptr<EngineParams>&& params)
{
  m_params = std::move(params);
}

auto
Engine::accelerationProfile(AccelerationCurve mode) const
  -> std::optional<std::reference_wrapper<AccelerationProfile>>
{
  auto profile = m_profiles.find(mode);
  if (profile == m_profiles.end()) {
    return std::nullopt;
  }
  return *profile->second;
}

auto
Engine::saveSettings() const -> std::reference_wrapper<EngineParams>
{
  return *m_params;
}

auto
Engine::mmToSteps(double mms) const -> Coordinate
{
  return Coordinate{ static_cast<qint64>(
    (mms + m_params->axisOffset + m_tempAxisOffset) *
    m_params->stepsPerMmRatio * ::microstep(m_params->microstep)) };
}

auto
Engine::stepsToMm(qint64 steps) const -> double
{
  const auto steps_d{ static_cast<double>(steps) };
  const auto microstep{ static_cast<double>(::microstep(m_params->microstep)) };

  return (steps_d / (m_params->stepsPerMmRatio * microstep)) -
         m_params->axisOffset - m_tempAxisOffset;
}

auto
Engine::stepsToMm(Coordinate steps) const -> double
{
  return stepsToMm(steps.value_of());
}

auto
Engine::speedToFrequency(Speed speed) const -> Frequency
{
  static constexpr auto denom{ 1 / 60.0 };
  return Frequency{ (speed.value_of() * ::microstep(m_params->microstep) *
                     m_params->stepsPerMmRatio) *
                    denom };
}

} // namespace device
