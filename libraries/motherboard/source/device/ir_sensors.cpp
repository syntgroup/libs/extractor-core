#include "motherboard/device/ir_sensors.h"

namespace device {

device::IrSensors::IrSensors(size_t count, QObject* parent)
  : QObject{ parent }
{
  for (size_t idx = 0; idx < count; ++idx) {
    m_sensors.push_back(std::make_unique<IrSensor>(idx));
  }
}

device::IrSensors::IrSensors(Container&& sensors, QObject* parent)
  : QObject{ parent }
  , m_sensors{ std::move(sensors) }
{
}

} // namespace device
