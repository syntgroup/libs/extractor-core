#include "motherboard/device/engine.h"

#include <cmath>

namespace {

constexpr auto epsilon{ 0.1 };
template<typename T>
auto
equals(T rhs, T lhs)
{
  return std::fabs(rhs.value_of() - lhs.value_of()) < epsilon;
}

} // namespace

namespace device {

void
Engine::setActionSpeed(SpeedIndex new_speed)
{
  if (m_params->actionSpeed == new_speed) {
    return;
  }
  m_params->actionSpeed = new_speed;
}

void
Engine::setTravelSpeed(SpeedIndex new_speed)
{
  if (m_params->travelSpeed == new_speed) {
    return;
  }
  m_params->travelSpeed = new_speed;
}

void
Engine::setHomingSpeed(SpeedIndex new_speed)
{
  if (m_params->homingSpeed == new_speed) {
    return;
  }
  m_params->homingSpeed = new_speed;
}

void
Engine::setCurrentAccel(Current new_current_accel)
{
  m_params->currentMoving = new_current_accel;

  emit currentScalingAccelChanged(
    static_cast<std::underlying_type_t<Current>>(m_params->currentMoving));
}

void
Engine::setCurrentHolding(Current new_current_holding)
{
  m_params->currentStandby = new_current_holding;

  emit currentScalingStdbyChanged(
    static_cast<std::underlying_type_t<Current>>(m_params->currentStandby));
}

void
Engine::setEnabled(bool new_enabled)
{
  if (m_enabled == new_enabled) {
    return;
  }
  m_enabled = new_enabled;
  emit enabledChanged(m_enabled);
}

void
Engine::setCurrentMixTime(std::chrono::seconds new_current_mix_time)
{
  if (m_currentMixTime == new_current_mix_time) {
    return;
  }
  m_currentMixTime = new_current_mix_time;
  emit currentMixTimeChanged(m_currentMixTime);
}

void
Engine::setAxisOffset(double new_axis_offset)
{
  if (qFuzzyCompare(m_params->axisOffset, new_axis_offset)) {
    return;
  }
  m_params->axisOffset = new_axis_offset;
  emit axisOffsetChanged(m_params->axisOffset);
}

void
Engine::setTempOffset(double new_value)
{
  if (qFuzzyCompare(m_tempAxisOffset, new_value)) {
    return;
  }
  m_tempAxisOffset = new_value;
}

void
Engine::resetTempOffset()
{
  m_tempAxisOffset = 0;
}

void
Engine::setMaxSpeed(SpeedIndex new_max_speed)
{
  if (m_maxSpeed == new_max_speed) {
    return;
  }
  m_maxSpeed = new_max_speed;
  emit maxSpeedChanged(m_maxSpeed.value_of());
}

void
Engine::setCurrentPosition(Coordinate new_current_position)
{
  if (m_currentPosition == new_current_position) {
    return;
  }
  m_currentPosition = new_current_position;
  emit currentPositionChanged(m_currentPosition.value_of());
}

void
Engine::setTargetPosition(Coordinate new_target_position)
{
  if (m_targetPosition == new_target_position) {
    return;
  }
  m_targetPosition = new_target_position;
  emit targetPositionChanged(m_targetPosition.value_of());
}

void
Engine::setCurrentMixCycle(Cycles new_current_mix_cycle)
{
  if (m_currentMixCycle == new_current_mix_cycle) {
    return;
  }
  m_currentMixCycle = new_current_mix_cycle;
  emit currentMixCycleChanged(m_currentMixCycle.value_of());
}

void
Engine::setHomePosition(Coordinate new_home_position)
{
  if (m_params->homePosition == new_home_position) {
    return;
  }
  m_params->homePosition = new_home_position;
  emit homePositionChanged(m_params->homePosition.value_of());
}

void
Engine::setMicrostep(Microstep new_microstep)
{
  if (m_params->microstep == new_microstep) {
    return;
  }
  m_params->microstep = new_microstep;
  emit microstepChanged(static_cast<uint8_t>(m_params->microstep));
}

void
Engine::setRepeatability(Repeatability new_repeatability)
{
  if (m_params->repeatability == new_repeatability) {
    return;
  }
  m_params->repeatability = new_repeatability;
  emit repeatabilityChanged(m_params->repeatability.value_of());
}

void
Engine::setStatus(uint8_t new_status)
{
  m_status = EngineStatus{ new_status };
}

void
Engine::setStepsPerMmRatio(double new_value)
{
  if (qFuzzyCompare(m_params->stepsPerMmRatio, new_value)) {
    return;
  }
  m_params->stepsPerMmRatio = new_value;
  emit stepsPerMmRatioChanged(m_params->stepsPerMmRatio);
}

} // namespace device
