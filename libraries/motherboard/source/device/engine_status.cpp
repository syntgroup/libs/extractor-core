#include "motherboard/device/engine_status.h"

#include <QDebugStateSaver>
#include <bitset>

namespace device {

EngineStatus::EngineStatus(uint8_t status_byte)
  : byte{ status_byte }
{
  const std::bitset<std::numeric_limits<uint8_t>::digits> bits{ byte };

  undefinedPos     = bits.test(BUndefinedPos);
  switchSelect     = bits.test(BSwitchSelect);
  switchFound      = bits.test(BSwitchFind);
  engineGo         = bits.test(BEngineGo);
  engineGoToZero   = bits.test(BEngineGoToZero);
  engineGoWoOpts   = bits.test(BEngineGoWoOpts);
  engineErrorSaved = bits.test(BEngineErrorSaved);
}

auto
operator<<(QDebug debug, const EngineStatus& status) -> QDebug
{
  QDebugStateSaver saver{ debug };

  debug.noquote() << "status" << Qt::showbase << Qt::hex << status.byte
                  << Qt::bin << status.byte;
  debug << "/ position defined?" << status.undefinedPos;
  debug << "/ SW:"
        << (status.switchSelect ? QStringLiteral("SW1")
                                : QStringLiteral("SW2"));
  debug << "/ SW found?" << status.switchFound;
  debug << "/ is moving?" << status.engineGo;
  debug << "/ is moving to zero?" << status.engineGoToZero;
  debug << "/ is moving w/o state control?" << status.engineGoWoOpts;
  debug << "/ is error saved?" << status.engineErrorSaved;

  return debug;
}

} // namespace device
