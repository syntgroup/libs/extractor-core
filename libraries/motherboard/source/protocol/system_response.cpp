#include "system_response.h"

#include "srvint/srvint_packet.h"

namespace protocol::motherboard {

GetErrorResponse::GetErrorResponse(
  std::unique_ptr<srvint::SrvIntPacket>& packet)
  : MotherboardResponse{ packet }
{
  const auto payload{ getPacket()->payload() };
  errorPosition = payload.at(PErrorPosition);
  error         = payload.at(PErrorValue);
}

} // namespace protocol::motherboard
