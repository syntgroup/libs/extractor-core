#include "buttons_response.h"

#include "srvint/srvint_packet.h"

namespace protocol::motherboard {

ButtonsResponse::ButtonsResponse(SrvIntPacket& packet)
  : MotherboardResponse{ packet }
{
  const auto payload{ getPacket()->payload() };
  state   = payload.at(State) == 1;
  trigger = payload.at(Trigger) == 1;
}

} // namespace protocol::motherboard
