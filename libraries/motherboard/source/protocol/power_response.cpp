#include "power_response.h"

#include "srvint/srvint_packet.h"
#include "utils/byte_converter.h"

namespace protocol::motherboard {

GetRailResponse::GetRailResponse(std::unique_ptr<srvint::SrvIntPacket>& packet)
  : MotherboardResponse{ packet }
{
  const auto voltage_encoded{ getPacket()->payloadBytes().mid(
    Double1, Double8 - Pcode) };
  voltage = device::Voltage{ utils::byteArrayToType<double>(voltage_encoded) };
}

GetBtnsResponse::GetBtnsResponse(std::unique_ptr<srvint::SrvIntPacket>& packet)
  : MotherboardResponse{ packet }
{
  statePushButon = getPacket()->payload().at(PushButton) == 1;
  stateGercon    = getPacket()->payload().at(Gercon) == 1;
}

} // namespace protocol::motherboard
