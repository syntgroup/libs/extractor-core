#ifndef GOTO_RESPONSE_H
#define GOTO_RESPONSE_H

#include "motherboard/protocol/motherboard_response.h"

namespace protocol::motherboard {

enum class GotoSet : uint8_t;

struct GotoResponse final : MotherboardResponse
{
  explicit GotoResponse(std::unique_ptr<srvint::SrvIntPacket>& packet);

  uint8_t engineState{ 0 };
  uint8_t engineAddress{ 0 };
  GotoSet opCode;

private:
  enum FieldPos
  {
    Command      = 0,
    EngineNumber = 1,
    Opcode       = 2,
    EngineStatus = 3,
    LastError    = 4
  };
};

} // namespace protocol::motherboard

#endif // GOTO_RESPONSE_H
