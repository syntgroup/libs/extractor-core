#include "engine_response.h"

#include "motherboard/protocol/motherboard_cmd.h"
#include "srvint/srvint_packet.h"
#include "utils/byte_converter.h"

namespace protocol::motherboard {

EnginesSetParamResponse::EnginesSetParamResponse(
  std::unique_ptr<srvint::SrvIntPacket>& packet)
  : MotherboardResponse{ packet }
{
  const auto payload{ getPacket()->payload() };
  engineState = payload.at(EngineStatus);
  engineId    = payload.at(EngineNumber);
  pcode       = static_cast<EnginesSet>(payload.at(Pcode));
}

// -----------------------------------------------------------------------------

EnginesGetMaxSpeedResponse::EnginesGetMaxSpeedResponse(
  std::unique_ptr<srvint::SrvIntPacket>& packet)
  : MotherboardResponse{ packet }
{
  const auto payload{ getPacket()->payloadBytes() };

  engineAdr   = payload.at(EngineIdx);
  engineState = payload.at(EngineStatus);
  pcode       = static_cast<EnginesGet>(payload.at(Pcode));

  const auto bytes{ payload.mid(Speed0, Speed3 - Pcode) };
  const auto encoded{ utils::byteArrayToType<uint32_t>(bytes) };
  const device::SpeedIndex value{ encoded };
  maxSpeed = value;
}

// -----------------------------------------------------------------------------

EnginesGetRepeatabilityResponse::EnginesGetRepeatabilityResponse(
  std::unique_ptr<srvint::SrvIntPacket>& packet)
  : MotherboardResponse{ packet }
{
  const auto payload{ getPacket()->payload() };

  engineAdr     = payload.at(EngineIdx);
  pCode         = static_cast<EnginesGet>(payload.at(Pcode));
  engineState   = device::EngineStatus{ payload.at(EngineStatus) };
  repeatability = device::Repeatability{ payload.at(Value) };
}

// -----------------------------------------------------------------------------

EnginesGetMicrostepResponse::EnginesGetMicrostepResponse(
  std::unique_ptr<srvint::SrvIntPacket>& packet)
  : MotherboardResponse{ packet }
{
  const auto payload{ getPacket()->payload() };

  engineAdr   = payload.at(EngineIdx);
  engineState = payload.at(EngineStatus);
  microstep   = static_cast<device::Microstep>(payload.at(Value));
  opCode      = static_cast<EnginesGet>(payload.at(Pcode));
}

// -----------------------------------------------------------------------------

EnginesGetHomePositionResponse::EnginesGetHomePositionResponse(
  std::unique_ptr<srvint::SrvIntPacket>& packet)
  : MotherboardResponse{ packet }
{
  const auto payload{ getPacket()->payloadBytes() };

  engineAdr   = payload.at(EngineIdx);
  engineState = payload.at(EngineStatus);
  opCode      = static_cast<EnginesGet>(payload.at(Pcode));

  const auto bytes{ payload.mid(Value0, Value7 - Pcode) };
  const auto value{ utils::byteArrayToType<qint64>(bytes) };

  homePosition = device::Coordinate{ value };
}

// -----------------------------------------------------------------------------

EnginesGetFResponse::EnginesGetFResponse(
  std::unique_ptr<srvint::SrvIntPacket>& packet)
  : MotherboardResponse{ packet }
{
  const auto payload{ getPacket()->payloadBytes() };
  const auto f_encoded{ payload.mid(Double1, Double8 - Pcode) };
  const auto value{ utils::byteArrayToType<double>(f_encoded) };

  acceleration = device::Frequency{ value };
  engineAdr    = payload.at(EngineIdx);
  engineState  = payload.at(EngineStatus);
  opCcode      = static_cast<EnginesGet>(payload.at(Pcode));
}

// -----------------------------------------------------------------------------

EnginesGetPositionResponse::EnginesGetPositionResponse(
  std::unique_ptr<srvint::SrvIntPacket>& packet)
  : MotherboardResponse{ packet }
{
  const auto payload{ getPacket()->payloadBytes() };

  engineAdr   = payload.at(EngineIdx);
  engineState = payload.at(EngineStatus);
  opCode      = static_cast<EnginesGet>(payload.at(Pcode));

  const auto value{ utils::byteArrayToType<qint64>(
    payload.mid(Value0, Value7 - Pcode)) };
  position = device::Coordinate{ value };
}

// -----------------------------------------------------------------------------

EngineGetCurrentsResponse::EngineGetCurrentsResponse(
  std::unique_ptr<srvint::SrvIntPacket>& packet)
  : MotherboardResponse{ packet }
{
  const auto payload{ getPacket()->payload() };

  engineAdr    = payload.at(EngineIdx);
  engineState  = payload.at(EngineStatus);
  opCode       = static_cast<EnginesGet>(payload.at(Pcode));
  currentAccel = static_cast<device::Current>(payload.at(CurrentAccel));
  currentStdby = static_cast<device::Current>(payload.at(CurrentStdby));
}

// -----------------------------------------------------------------------------

EngineGetMixModeStateResponse::EngineGetMixModeStateResponse(
  std::unique_ptr<srvint::SrvIntPacket>& packet)
  : MotherboardResponse{ packet }
{
  const auto payload{ getPacket()->payloadBytes() };

  engineAdr    = payload.at(EngineIdx);
  engineState  = payload.at(EngineStatus);
  opCode       = static_cast<EnginesGet>(payload.at(Pcode));
  mixmodeState = payload.at(CurrentState);

  const auto time_encoded{ payload.mid(Time0, Time3 - Cycle3) };
  currentTime =
    std::chrono::seconds{ utils::byteArrayToType<uint32_t>(time_encoded) };

  const auto cycle_encoded{ payload.mid(Cycle0, Cycle3 - CurrentState) };
  currentCycle =
    device::Cycles{ utils::byteArrayToType<uint32_t>(cycle_encoded) };
}

} // namespace protocol::motherboard
