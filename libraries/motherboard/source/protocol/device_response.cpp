#include "device_response.h"

#include "srvint/srvint_packet.h"
#include "utils/byte_converter.h"

namespace protocol::motherboard {

DeviceGetIdResponse::DeviceGetIdResponse(SrvIntPacket& packet)
  : MotherboardResponse{ packet }
{
  const auto id_encoded{ getPacket()->payloadBytes().mid(IdMsb, IdLsb - 1) };
  deviceId = utils::byteArrayToType<decltype(deviceId)>(id_encoded);
}

DeviceGetFWResponse::DeviceGetFWResponse(SrvIntPacket& packet)
  : MotherboardResponse{ packet }
{
  fwRev = getPacket()->payload().at(Fw);
}

} // namespace protocol::motherboard
