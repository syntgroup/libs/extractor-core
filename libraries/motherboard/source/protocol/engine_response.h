#ifndef ENGINE_RESPONSE_H
#define ENGINE_RESPONSE_H

#include "motherboard/device/engine_status.h"
#include "motherboard/device/engine_values.h"
#include "motherboard/protocol/motherboard_response.h"

namespace protocol::motherboard {

enum class EnginesSet : uint8_t;
enum class EnginesGet : uint8_t;

struct EnginesSetParamResponse final : MotherboardResponse
{
  explicit EnginesSetParamResponse(
    std::unique_ptr<srvint::SrvIntPacket>& packet);

  uint8_t engineState{ 0 };
  uint8_t engineId{ 0 };
  EnginesSet pcode;

private:
  enum Pos
  {
    SetParamEngines = 0, // op1
    EngineNumber    = 1, // op2
    Pcode           = 2, // op3
    EngineStatus    = 3, // op4
    LastError       = 4  // op5
  };
};

struct EnginesGetMaxSpeedResponse final : MotherboardResponse
{
  explicit EnginesGetMaxSpeedResponse(
    std::unique_ptr<srvint::SrvIntPacket>& packet);

  uint8_t engineAdr{ 0 };
  uint8_t engineState{ 0 };
  device::SpeedIndex maxSpeed{ 0 };
  EnginesGet pcode;

private:
  enum Pos
  {
    Op1          = 0,
    EngineIdx    = 1,
    Pcode        = 2,
    Speed0       = 3,
    Speed1       = 4,
    Speed2       = 5,
    Speed3       = 6,
    EngineStatus = 7,
    ErrorCode    = 8
  };
};

struct EnginesGetRepeatabilityResponse final : MotherboardResponse
{
  explicit EnginesGetRepeatabilityResponse(
    std::unique_ptr<srvint::SrvIntPacket>& packet);

  uint8_t engineAdr{ 0 };
  device::EngineStatus engineState{ 0 };
  device::Repeatability repeatability{ 0 };
  EnginesGet pCode;

private:
  enum Pos
  {
    Op1          = 0,
    EngineIdx    = 1,
    Pcode        = 2,
    Value        = 3,
    EngineStatus = 4,
    LastError    = 5
  };
};

struct EnginesGetMicrostepResponse final : MotherboardResponse
{
  explicit EnginesGetMicrostepResponse(
    std::unique_ptr<srvint::SrvIntPacket>& packet);

  uint8_t engineAdr{ 0 };
  uint8_t engineState{ 0 };
  device::Microstep microstep{ 0 };
  EnginesGet opCode;

private:
  enum Pos
  {
    Op1          = 0,
    EngineIdx    = 1,
    Pcode        = 2,
    Value        = 3,
    EngineStatus = 4,
    LastError    = 5
  };
};

struct EnginesGetHomePositionResponse final : MotherboardResponse
{
  explicit EnginesGetHomePositionResponse(
    std::unique_ptr<srvint::SrvIntPacket>& packet);

  uint8_t engineAdr{ 0 };
  uint8_t engineState{ 0 };
  device::Coordinate homePosition{ 0 };
  EnginesGet opCode;

private:
  enum Pos
  {
    Op1          = 0,
    EngineIdx    = 1,
    Pcode        = 2,
    Value0       = 3,
    Value1       = 4,
    Value2       = 5,
    Value3       = 6,
    Value4       = 7,
    Value5       = 8,
    Value6       = 9,
    Value7       = 10,
    EngineStatus = 11,
    ErrorCode    = 12
  };
};

struct EnginesGetFResponse final : MotherboardResponse
{
  explicit EnginesGetFResponse(std::unique_ptr<srvint::SrvIntPacket>& packet);

  device::Frequency acceleration{ 0 };
  uint8_t engineAdr{ 0 };
  uint8_t engineState{ 0 };
  EnginesGet opCcode;

private:
  enum Pos
  {
    Param        = 0,  // op1
    EngineIdx    = 1,  // op2
    Pcode        = 2,  // op3
    Double1      = 3,  // op4
    Double2      = 4,  // op5
    Double3      = 5,  // op6
    Double4      = 6,  // op7
    Double5      = 7,  // op8
    Double6      = 8,  // op9
    Double7      = 9,  // op10
    Double8      = 10, // op11
    EngineStatus = 11, // op12
    LastError    = 12  // op13
  };
};

struct EnginesGetPositionResponse final : MotherboardResponse
{
  explicit EnginesGetPositionResponse(
    std::unique_ptr<srvint::SrvIntPacket>& packet);

  uint8_t engineAdr{ 0 };
  uint8_t engineState{ 0 };
  device::Coordinate position{ 0 };
  EnginesGet opCode;

private:
  enum Pos
  {
    Op1          = 0,
    EngineIdx    = 1,
    Pcode        = 2,
    Value0       = 3,
    Value1       = 4,
    Value2       = 5,
    Value3       = 6,
    Value4       = 7,
    Value5       = 8,
    Value6       = 9,
    Value7       = 10,
    EngineStatus = 11,
    ErrorCode    = 12
  };
};

struct EngineGetCurrentsResponse final : MotherboardResponse
{
  explicit EngineGetCurrentsResponse(
    std::unique_ptr<srvint::SrvIntPacket>& packet);

  uint8_t engineAdr{ 0 };
  uint8_t engineState{ 0 };
  device::Current currentAccel{ 0 };
  device::Current currentStdby{ 0 };
  EnginesGet opCode;

private:
  enum Pos
  {
    Op1          = 0,
    EngineIdx    = 1,
    Pcode        = 2,
    CurrentAccel = 3,
    CurrentStdby = 4,
    EngineStatus = 5,
    ErrorCode    = 6
  };
};

struct EngineGetMixModeStateResponse final : MotherboardResponse
{
  explicit EngineGetMixModeStateResponse(
    std::unique_ptr<srvint::SrvIntPacket>& packet);

  uint8_t engineAdr{ 0 };
  uint8_t engineState{ 0 };
  uint8_t mixmodeState{ 0 };
  std::chrono::seconds currentTime{ 0 };
  device::Cycles currentCycle{ 0 };
  EnginesGet opCode;

private:
  enum Pos
  {
    Op1          = 0,
    EngineIdx    = 1,
    Pcode        = 2,
    CurrentState = 3,
    Cycle0       = 4,
    Cycle3       = 7,
    Time0        = 8,
    Time3        = 11,
    EngineStatus = 12,
    LastError    = 13
  };
};

} // namespace protocol::motherboard

#endif // ENGINE_RESPONSE_H
