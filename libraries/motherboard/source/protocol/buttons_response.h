/*!
 * @file buttons_response.h
 * @author tmayzenberg
 */

#ifndef PROTOCOL_MOTHERBOARD_BUTTONSRESPONSE_H
#define PROTOCOL_MOTHERBOARD_BUTTONSRESPONSE_H

#include "motherboard/protocol/motherboard_response.h"

namespace protocol::motherboard {

/*!
 * @brief The ButtonsResponse class
 * @details this class is to be used with reponse to:
 *  - SET_PARAM_GERCON_CLEAR_TRIGGER,
 *  - SET_PARAM_OPT_DIAG_CLEAR_TRIGGER,
 *  - SET_PARAM_PBTN_CLEAR_TRIGGER,
 *  - GET_PARAM_GERCON_STATE,
 *  - GET_PARAM_OPT_DIAG_STATE,
 *  - GET_PARAM_PBTN_STATE
 */
struct ButtonsResponse final : protocol::motherboard::MotherboardResponse
{
  explicit ButtonsResponse(SrvIntPacket& packet);

  bool state{ false };
  bool trigger{ false };

private:
  enum FieldPos
  {
    Command   = 0, // op1
    Opcode    = 1, // op2
    State     = 2, // op3
    Trigger   = 3, // op4
    LastError = 4  // op5
  };
};

} // namespace protocol::motherboard

#endif // PROTOCOL_MOTHERBOARD_BUTTONSRESPONSE_H
