#ifndef PROTOCOL_MOTHERBOARD_ENGINEREQUEST_H
#define PROTOCOL_MOTHERBOARD_ENGINEREQUEST_H

#include "motherboard/device/engine_values.h"
#include "serial/protocol/protocol_values.h"
#include "serial/protocol/request_interface.h"
#include "srvint/srvint_defs.h"
#include <array>

namespace protocol::motherboard {

struct EngineRequestMaxSpeed final : protocol::RequestInterface
{
  explicit EngineRequestMaxSpeed(PacketID pid_, uint8_t engine_);
  const PacketID pid;
  const srvint::Payload payload;
};

struct EngineRequestRepeatability final : protocol::RequestInterface
{
  explicit EngineRequestRepeatability(PacketID pid, uint8_t engine);
  const PacketID pid;
  const srvint::Payload payload;
};

struct EngineRequestMicrostep final : protocol::RequestInterface
{
  explicit EngineRequestMicrostep(PacketID pid, uint8_t engine);
  const PacketID pid;
  const srvint::Payload payload;
};

struct EngineRequestHomePosition final : protocol::RequestInterface
{
  explicit EngineRequestHomePosition(PacketID pid, uint8_t engine);
  const PacketID pid;
  const srvint::Payload payload;
};

struct EngineRequestF0 final : protocol::RequestInterface
{
  explicit EngineRequestF0(PacketID pid, uint8_t engine);
  const PacketID pid;
  const srvint::Payload payload;
};

struct EngineRequestFMax final : protocol::RequestInterface
{
  explicit EngineRequestFMax(PacketID pid, uint8_t engine);
  const PacketID pid;
  const srvint::Payload payload;
};

struct EngineRequestDFMax final : protocol::RequestInterface
{
  explicit EngineRequestDFMax(PacketID pid, uint8_t engine);
  const PacketID pid;
  const srvint::Payload payload;
};

struct EngineRequestPosition final : protocol::RequestInterface
{
  explicit EngineRequestPosition(PacketID pid, uint8_t engine);
  const PacketID pid;
  const srvint::Payload payload;
};

struct EnginesCurrentsGetRequest final : protocol::RequestInterface
{
  explicit EnginesCurrentsGetRequest(PacketID pid, uint8_t engine);
  const PacketID pid;
  const srvint::Payload payload;
};

struct EngineRequestMixModeState final : protocol::RequestInterface
{
  explicit EngineRequestMixModeState(PacketID pid, uint8_t engine);
  const PacketID pid;
  const srvint::Payload payload;
};

} // namespace protocol::motherboard

namespace protocol::motherboard {

struct EngineWriteMaxSpeed final : protocol::RequestInterface
{
  explicit EngineWriteMaxSpeed(PacketID pid,
                               uint8_t engine,
                               device::SpeedIndex speed_index);
  const PacketID pid;
  srvint::Payload payload;
};

struct EngineWriteRepeatability final : protocol::RequestInterface
{
  explicit EngineWriteRepeatability(PacketID pid,
                                    uint8_t engine,
                                    device::Repeatability repeatability);
  const PacketID pid;
  const srvint::Payload payload;
};

struct EngineWriteMicrostep final : protocol::RequestInterface
{
  explicit EngineWriteMicrostep(PacketID pid,
                                uint8_t engine,
                                device::Microstep microstep);
  const PacketID pid;
  const srvint::Payload payload;
};

struct EngineWriteHomePosition final : protocol::RequestInterface
{
  explicit EngineWriteHomePosition(PacketID pid,
                                   uint8_t engine,
                                   device::Coordinate home_position);
  const PacketID pid;
  srvint::Payload payload;
};

struct EngineWriteF0 final : protocol::RequestInterface
{
  explicit EngineWriteF0(PacketID pid, uint8_t engine, device::Frequency value);
  const PacketID pid;
  srvint::Payload payload;
};

struct EngineWriteFMax final : protocol::RequestInterface
{
  explicit EngineWriteFMax(PacketID pid,
                           uint8_t engine,
                           device::Frequency value);
  const PacketID pid;
  srvint::Payload payload;
};

struct EngineWriteDFMax final : protocol::RequestInterface
{
  explicit EngineWriteDFMax(PacketID pid,
                            uint8_t engine,
                            device::Frequency value);
  const PacketID pid;
  srvint::Payload payload;
};

struct EngineWritePosition final : protocol::RequestInterface
{
  explicit EngineWritePosition(PacketID pid,
                               uint8_t engine,
                               device::Coordinate position);
  const PacketID pid;
  srvint::Payload payload;
};

struct EngineSavePosition final : protocol::RequestInterface
{
  explicit EngineSavePosition(PacketID pid, uint8_t engine);
  const PacketID pid;
  const srvint::Payload payload;
};

struct EngineLoadPosition final : protocol::RequestInterface
{
  explicit EngineLoadPosition(PacketID pid, uint8_t engine);
  const PacketID pid;
  const srvint::Payload payload;
};

struct EngineEnableRequest final : protocol::RequestInterface
{
  explicit EngineEnableRequest(PacketID pid, uint8_t engine, bool enable);
  const PacketID pid;
  const srvint::Payload payload;
};

struct EngineDriverRawWrite final : protocol::RequestInterface
{
  explicit EngineDriverRawWrite(PacketID pid,
                                uint8_t engine,
                                const QByteArray& bytes);
  const PacketID pid;
  srvint::Payload payload;
};

struct EngineDriverFactoryDefault final : protocol::RequestInterface
{
  explicit EngineDriverFactoryDefault(PacketID pid, uint8_t engine);
  const PacketID pid;
  srvint::Payload payload;
};

} // namespace protocol::motherboard

#endif // PROTOCOL_MOTHERBOARD_ENGINEREQUEST_H
