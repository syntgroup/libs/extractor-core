#ifndef MB_SYSTEM_RESPONSE_H
#define MB_SYSTEM_RESPONSE_H

#include "motherboard/protocol/motherboard_response.h"

namespace protocol::motherboard {

struct GetErrorResponse final : MotherboardResponse
{
  explicit GetErrorResponse(std::unique_ptr<srvint::SrvIntPacket>& packet);

  uint8_t errorPosition{ 0 };
  uint8_t error{ 0 };

private:
  enum Pos
  {
    PErrorPosition = 0,
    PErrorValue,
    PLastError
  };
};

} // namespace protocol::motherboard

#endif // MB_SYSTEM_RESPONSE_H
