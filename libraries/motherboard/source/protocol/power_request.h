#ifndef POWER_REQUEST_H
#define POWER_REQUEST_H

#include "motherboard/device/motherboard_values.h"
#include "motherboard/protocol/motherboard_cmd.h"
#include "serial/protocol/protocol_values.h"
#include "serial/protocol/request_interface.h"
#include "srvint/srvint_defs.h"

namespace protocol::motherboard {

struct Voltage24VRailRequest final : protocol::RequestInterface
{
  explicit Voltage24VRailRequest(PacketID pid)
    : pid{ pid }
  {
  }
  const PacketID pid;
  const srvint::Payload payload{
    static_cast<srvint::PayloadType>(GetParam::Voltages),  // op1
    static_cast<srvint::PayloadType>(VoltagesGet::Rail24V) // op2
  };
};

struct VoltageUVLampRequest final : protocol::RequestInterface
{
  explicit VoltageUVLampRequest(PacketID pid)
    : pid{ pid }
  {
  }
  const PacketID pid;
  const srvint::Payload payload{
    static_cast<srvint::PayloadType>(GetParam::Voltages),     // op1
    static_cast<srvint::PayloadType>(VoltagesGet::RailUvLamp) // op2
  };
};

struct VoltageR2RDACRequest final : protocol::RequestInterface
{
  explicit VoltageR2RDACRequest(PacketID pid)
    : pid{ pid }
  {
  }
  const PacketID pid;
  srvint::Payload payload{
    static_cast<srvint::PayloadType>(GetParam::Voltages),     // op1
    static_cast<srvint::PayloadType>(VoltagesGet::RailR2RDac) // op2
  };
};

struct VoltageBtnsRequest final : protocol::RequestInterface
{
  explicit VoltageBtnsRequest(PacketID pid)
    : pid{ pid }
  {
  }
  const PacketID pid;
  const srvint::Payload payload{
    static_cast<srvint::PayloadType>(GetParam::Buttons), // op1
    static_cast<srvint::PayloadType>(ButtonsGet::All)    // op2
  };
};

struct PowerSetCoolerRequest final : protocol::RequestInterface
{
  explicit PowerSetCoolerRequest(PacketID pid, device::PwmLevel new_pwm);
  const PacketID pid;
  srvint::Payload payload;
};

struct PowerSetLedsRequest final : protocol::RequestInterface
{
  explicit PowerSetLedsRequest(PacketID pid, device::PwmLevel new_pwm);
  const PacketID pid;
  srvint::Payload payload;
};

struct PowerSetUVLampRequest final : protocol::RequestInterface
{
  explicit PowerSetUVLampRequest(PacketID pid, bool new_state);
  const PacketID pid;
  const srvint::Payload payload;
};

struct PowerSetBuzzerRequest final : protocol::RequestInterface
{
  explicit PowerSetBuzzerRequest(PacketID pid, bool new_state);
  const PacketID pid;
  const srvint::Payload payload;
};

struct PowerSetSHDNRequest final : protocol::RequestInterface
{
  explicit PowerSetSHDNRequest(PacketID pid, bool new_state);
  const PacketID pid;
  const srvint::Payload payload;
};

struct PowerSetTurnOff final : protocol::RequestInterface
{
  explicit PowerSetTurnOff(PacketID pid)
    : pid{ pid }
  {
  }
  const PacketID pid;
  const srvint::Payload payload{
    static_cast<srvint::PayloadType>(SetParam::Power),  // op1
    static_cast<srvint::PayloadType>(PowerSet::TurnOff) // op2
  };
};

struct PowerSetWdtTimeout final : protocol::RequestInterface
{
  explicit PowerSetWdtTimeout(PacketID pid, uint32_t wdt_value);
  const PacketID pid;
  srvint::Payload payload;
};

} // namespace protocol::motherboard

#endif // POWER_REQUEST_H
