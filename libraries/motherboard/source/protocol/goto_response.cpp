#include "goto_response.h"

#include "motherboard/protocol/motherboard_cmd.h"
#include "srvint/srvint_packet.h"

namespace protocol::motherboard {

GotoResponse::GotoResponse(std::unique_ptr<srvint::SrvIntPacket>& packet)
  : MotherboardResponse{ packet }
{
  const auto payload{ getPacket()->payload() };

  engineAddress = payload.at(EngineNumber);
  engineState   = payload.at(EngineStatus);
  opCode        = static_cast<GotoSet>(payload.at(Opcode));
}

} // namespace protocol::motherboard
