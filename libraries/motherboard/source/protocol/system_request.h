#ifndef SYSTEM_REQUEST_H
#define SYSTEM_REQUEST_H

#include "serial/protocol/protocol_values.h"
#include "serial/protocol/request_interface.h"
#include "srvint/srvint_defs.h"

namespace protocol::motherboard {

struct SystemSoftRebootRequest final : protocol::RequestInterface
{
  explicit SystemSoftRebootRequest(PacketID pid)
    : pid{ pid }
  {
  }
  const PacketID pid;
  const srvint::Command command{ srvint::Command::SwReset };
};

struct SystemHardRebootRequest final : protocol::RequestInterface
{
  explicit SystemHardRebootRequest(PacketID pid)
    : pid{ pid }
  {
  }
  const PacketID pid;
  const srvint::Command command{ srvint::Command::HwReset };
};

struct SystemGoToBootloaderRequest final : protocol::RequestInterface
{
  explicit SystemGoToBootloaderRequest(PacketID pid)
    : pid{ pid }
  {
  }
  const PacketID pid;
  const srvint::Command command{ srvint::Command::GoToBootMode };
};

struct SystemGetErrorRequest final : protocol::RequestInterface
{
  explicit SystemGetErrorRequest(PacketID pid, uint8_t error_pos)
    : pid{ pid }
    , payload{ error_pos }
  {
  }
  const PacketID pid;
  const srvint::Command command{ srvint::Command::GetError };
  const srvint::Payload payload;
};

struct SystemZeroizeErrorRequest final : protocol::RequestInterface
{
  explicit SystemZeroizeErrorRequest(PacketID pid)
    : pid{ pid }
  {
  }
  const PacketID pid;
  const srvint::Command command{ srvint::Command::ResetError };
};

} // namespace protocol::motherboard

#endif // SYSTEM_REQUEST_H
