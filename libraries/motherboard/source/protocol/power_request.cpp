#include "power_request.h"

#include "utils/byte_converter.h"

namespace protocol::motherboard {

PowerSetCoolerRequest::PowerSetCoolerRequest(PacketID pid,
                                             device::PwmLevel new_pwm)
  : pid{ pid }
  , payload{
    static_cast<srvint::PayloadType>(SetParam::Power), // op1
    static_cast<srvint::PayloadType>(PowerSet::Cooler) // op2
  }
{
  const auto converted{ utils::type_to_bytes(new_pwm.value_of()) };
  std::copy(converted.begin(), converted.end(), std::back_inserter(payload));
}

PowerSetLedsRequest::PowerSetLedsRequest(PacketID pid, device::PwmLevel new_pwm)
  : pid{ pid }
  , payload{
    static_cast<srvint::PayloadType>(SetParam::Power), // op1
    static_cast<srvint::PayloadType>(PowerSet::Leds)   // op2
  }
{
  const auto converted{ utils::type_to_bytes(new_pwm.value_of()) };
  std::copy(converted.begin(), converted.end(), std::back_inserter(payload));
}

PowerSetUVLampRequest::PowerSetUVLampRequest(PacketID pid, bool new_state)
  : pid{ pid }
  , payload{
    static_cast<srvint::PayloadType>(SetParam::Power),  // op1
    static_cast<srvint::PayloadType>(PowerSet::UvLamp), // op2
    static_cast<srvint::PayloadType>(new_state)         // op3
  }
{
}

PowerSetBuzzerRequest::PowerSetBuzzerRequest(PacketID pid, bool new_state)
  : pid{ pid }
  , payload{
    static_cast<srvint::PayloadType>(SetParam::Power),  // op1
    static_cast<srvint::PayloadType>(PowerSet::Buzzer), // op2
    static_cast<srvint::PayloadType>(new_state)         // op3
  }
{
}

PowerSetSHDNRequest::PowerSetSHDNRequest(PacketID pid, bool new_state)
  : pid{ pid }
  , payload{
    static_cast<srvint::PayloadType>(SetParam::Power), // op1
    static_cast<srvint::PayloadType>(PowerSet::SHDN),  // op2
    static_cast<srvint::PayloadType>(new_state)        // op3
  }
{
}

PowerSetWdtTimeout::PowerSetWdtTimeout(PacketID pid, uint32_t wdt_value)
  : pid{ pid }
  , payload{
    static_cast<srvint::PayloadType>(SetParam::Power),   // op1
    static_cast<srvint::PayloadType>(PowerSet::WdtValue) // op2
  }
{
  const auto wdt_encoded{ utils::type_to_bytes(wdt_value) };
  std::copy(
    wdt_encoded.cbegin(), wdt_encoded.cend(), std::back_inserter(payload));
}

} // namespace protocol::motherboard
