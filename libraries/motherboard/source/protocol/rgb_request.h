#ifndef PROTOCOL_MOTHERBOARD_RGB_REQUEST_H
#define PROTOCOL_MOTHERBOARD_RGB_REQUEST_H

#include "motherboard/device/motherboard_values.h"
#include "qnamespace.h"
#include "serial/protocol/protocol_values.h"
#include "serial/protocol/request_interface.h"
#include "srvint/srvint_defs.h"

namespace protocol::motherboard {

/*!
 * \brief The RgbSetOnOff class
 *
 * \details
 * согласно описанию протокола, в качестве параметров команды нужно передать
 * 32-битное число (или 4 байта) информации о цветах 5 светодиодов сразу.
 *
 * цвет каждого светодиода кодируется 3-битной палитрой
 * unsigned L1_B	:1; - младший бит
 *
 * образец структуры такого закодированного числа, как себе это понимают в Srv:
 *
 * ```c
 * typedef union _RGBLinesType
 * {
 *   struct
 *   {
 *     unsigned L1_B : 1; // LED1 Blue color
 *     unsigned L1_G : 1; // LED1 Green color
 *     unsigned L1_R : 1; // LED1 Red color
 *
 *     unsigned L2_B : 1; // LED2 Blue color
 *     unsigned L2_G : 1; // LED2 Green color
 *     unsigned L2_R : 1; // LED2 Red color
 *
 *     unsigned L3_B : 1; // LED3 Blue color
 *     unsigned L3_G : 1; // LED3 Green color
 *     unsigned L3_R : 1; // LED3 Red color
 *
 *     unsigned L4_B : 1; // LED4 Blue color
 *     unsigned L4_G : 1; // LED4 Green color
 *     unsigned L4_R : 1; // LED4 Red color
 *
 *     unsigned L5_B : 1; // LED5 Blue color
 *     unsigned L5_G : 1; // LED5 Green color
 *     unsigned L5_R : 1; // LED5 Red color
 *   };
 *   unsigned int UInt32;
 * } RGBLinesType;
 * ```
 */
struct RgbSetOnOffRequest final : protocol::RequestInterface
{
  explicit RgbSetOnOffRequest(PacketID pid,
                              const std::vector<Qt::GlobalColor>& colors);
  const PacketID pid;
  srvint::Payload payload;
};

} // namespace protocol::motherboard

#endif // PROTOCOL_MOTHERBOARD_RGB_REQUEST_H
