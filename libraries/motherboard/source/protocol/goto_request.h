#ifndef GOTO_REQUEST_H
#define GOTO_REQUEST_H

#include "motherboard/device/engine_values.h"
#include "serial/protocol/protocol_values.h"
#include "serial/protocol/request_interface.h"
#include "srvint/srvint_defs.h"
#include <array>
#include <chrono>

namespace protocol::motherboard {

struct GotoNTicksRequest final : protocol::RequestInterface
{
  using Params = std::vector<std::pair<uint8_t, device::Coordinate>>;
  explicit GotoNTicksRequest(PacketID pid, const Params& params);
  explicit GotoNTicksRequest(PacketID pid,
                             uint8_t engine,
                             device::Coordinate target);
  const PacketID pid;
  srvint::Payload payload;
};

struct GotoAbsPosRequest final : protocol::RequestInterface
{
  using Params = std::vector<std::pair<uint8_t, device::Coordinate>>;
  explicit GotoAbsPosRequest(PacketID pid, const Params& params);
  explicit GotoAbsPosRequest(PacketID pid,
                             uint8_t engine,
                             device::Coordinate target);

  const PacketID pid;
  srvint::Payload payload;
};

struct GotoZeroRequest final : protocol::RequestInterface
{
  explicit GotoZeroRequest(PacketID pid,
                           uint8_t engine,
                           device::ZeroSwitch zero_switch);
  const PacketID pid;
  const srvint::Payload payload;
};

struct GotoSpeedSetRequest final : protocol::RequestInterface
{
  using Params = std::vector<std::pair<uint8_t, device::SpeedIndex>>;
  explicit GotoSpeedSetRequest(PacketID pid, const Params& params);

  const PacketID pid;
  srvint::Payload payload;
};

struct GotoEmStopRequest final : protocol::RequestInterface
{
  using Params = std::vector<uint8_t>;
  //! for multiple engines
  explicit GotoEmStopRequest(PacketID pid, const Params& params);
  //! for one engine
  explicit GotoEmStopRequest(PacketID pid, uint8_t engine);

  const PacketID pid;
  srvint::Payload payload;
};

struct GotoSmoothStopRequest final : protocol::RequestInterface
{
  using Params = std::vector<uint8_t>;
  explicit GotoSmoothStopRequest(PacketID pid, const Params& params);
  explicit GotoSmoothStopRequest(PacketID pid, uint8_t engine);

  const PacketID pid;
  srvint::Payload payload;
};

struct GotoMixCyclesRequest final : protocol::RequestInterface
{
  explicit GotoMixCyclesRequest(PacketID pid,
                                uint8_t engine,
                                device::Cycles ticks,
                                device::Coordinate amplitude);
  const PacketID pid;
  srvint::Payload payload;
};

struct GotoMixStop final : protocol::RequestInterface
{
  explicit GotoMixStop(PacketID pid, uint8_t engine);
  const PacketID pid;
  const srvint::Payload payload;
};

struct GotoCurrentsSetRequest final : protocol::RequestInterface
{
  explicit GotoCurrentsSetRequest(PacketID pid,
                                  uint8_t engine,
                                  device::Current current_accel,
                                  device::Current current_stdby);
  const PacketID pid;
  const srvint::Payload payload;
};

struct GotoNTicksUNCRequest final : protocol::RequestInterface
{
  explicit GotoNTicksUNCRequest(PacketID pid,
                                uint8_t engine,
                                device::Coordinate ticks);
  const PacketID pid;
  srvint::Payload payload;
};

struct GotoNTicksUNCRequestWoOpts final : protocol::RequestInterface
{
  explicit GotoNTicksUNCRequestWoOpts(PacketID pid,
                                      uint8_t engine,
                                      device::Coordinate ticks);
  const PacketID pid;
  srvint::Payload payload;
};

struct GotoMixTimeRequest final : protocol::RequestInterface
{
  explicit GotoMixTimeRequest(PacketID pid,
                              uint8_t engine,
                              std::chrono::seconds seconds,
                              device::Coordinate amplitude);
  const PacketID pid;
  srvint::Payload payload;
};

} // namespace protocol::motherboard

#endif // GOTO_REQUEST_H
