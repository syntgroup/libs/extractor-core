#include "engine_request.h"

#include "motherboard/protocol/motherboard_cmd.h"
#include "utils/byte_converter.h"

namespace protocol::motherboard {

EngineRequestMaxSpeed::EngineRequestMaxSpeed(PacketID pid_, uint8_t engine_)
  : pid{ pid_ }
  , payload{
    static_cast<srvint::PayloadType>(GetParam::Engines),       // op1
    static_cast<srvint::PayloadType>(engine_),                 // op2
    static_cast<srvint::PayloadType>(EnginesGet::MaximumSpeed) // op3
  }
{
}

EngineRequestRepeatability::EngineRequestRepeatability(PacketID pid,
                                                       uint8_t engine)
  : pid{ pid }
  , payload{
    static_cast<srvint::PayloadType>(GetParam::Engines),        // op1
    static_cast<srvint::PayloadType>(engine),                   // op2
    static_cast<srvint::PayloadType>(EnginesGet::Repeatability) // op3
  }
{
}

EngineRequestMicrostep::EngineRequestMicrostep(PacketID pid, uint8_t engine)
  : pid{ pid }
  , payload{
    static_cast<srvint::PayloadType>(GetParam::Engines),    // op1
    static_cast<srvint::PayloadType>(engine),               // op2
    static_cast<srvint::PayloadType>(EnginesGet::Microstep) // op3
  }
{
}

EngineRequestHomePosition::EngineRequestHomePosition(PacketID pid,
                                                     uint8_t engine)
  : pid{ pid }
  , payload{
    static_cast<srvint::PayloadType>(GetParam::Engines),       // op1
    static_cast<srvint::PayloadType>(engine),                  // op2
    static_cast<srvint::PayloadType>(EnginesGet::HomePosition) // op3
  }
{
}

EngineRequestF0::EngineRequestF0(PacketID pid, uint8_t engine)
  : pid{ pid }
  , payload{
    static_cast<srvint::PayloadType>(GetParam::Engines), // op1
    static_cast<srvint::PayloadType>(engine),            // op2
    static_cast<srvint::PayloadType>(EnginesGet::F0)     // op3
  }
{
}

EngineRequestFMax::EngineRequestFMax(PacketID pid, uint8_t engine)
  : pid{ pid }
  , payload{
    static_cast<srvint::PayloadType>(GetParam::Engines), // op1
    static_cast<srvint::PayloadType>(engine),            // op2
    static_cast<srvint::PayloadType>(EnginesGet::FMAX)   // op3
  }
{
}

EngineRequestDFMax::EngineRequestDFMax(PacketID pid, uint8_t engine)
  : pid{ pid }
  , payload{
    static_cast<srvint::PayloadType>(GetParam::Engines), // op1
    static_cast<srvint::PayloadType>(engine),            // op2
    static_cast<srvint::PayloadType>(EnginesGet::DFMAX)  // op3
  }
{
}

EngineRequestPosition::EngineRequestPosition(PacketID pid, uint8_t engine)
  : pid{ pid }
  , payload{
    static_cast<srvint::PayloadType>(GetParam::Engines),          // op1
    static_cast<srvint::PayloadType>(engine),                     // op2
    static_cast<srvint::PayloadType>(EnginesGet::CurrentPosition) // op3
  }
{
}

EnginesCurrentsGetRequest::EnginesCurrentsGetRequest(PacketID pid,
                                                     uint8_t engine)
  : pid{ pid }
  , payload{
    static_cast<srvint::PayloadType>(GetParam::Engines),               // op1
    static_cast<srvint::PayloadType>(engine),                          // op2
    static_cast<srvint::PayloadType>(EnginesGet::AccelStandbyCurrents) // op3
  }
{
}

EngineRequestMixModeState::EngineRequestMixModeState(PacketID pid,
                                                     uint8_t engine)
  : pid{ pid }
  , payload{
    static_cast<srvint::PayloadType>(GetParam::Engines),       // op1
    static_cast<srvint::PayloadType>(engine),                  // op2
    static_cast<srvint::PayloadType>(EnginesGet::MixModeState) // op3
  }
{
}

} // namespace protocol::motherboard

namespace protocol::motherboard {

EngineWriteMaxSpeed::EngineWriteMaxSpeed(PacketID pid,
                                         uint8_t engine,
                                         device::SpeedIndex speed_index)
  : pid{ pid }
  , payload{
    static_cast<srvint::PayloadType>(SetParam::Engines),        // op1
    static_cast<srvint::PayloadType>(engine),                   // op2
    static_cast<srvint::PayloadType>(EnginesSet::MaximumSpeed), // op3
  }
{
  auto bytes_encoded{ utils::type_to_bytes(speed_index.value_of()) };
  std::move(
    bytes_encoded.begin(), bytes_encoded.end(), std::back_inserter(payload));
}

EngineWriteRepeatability::EngineWriteRepeatability(
  PacketID pid,
  uint8_t engine,
  device::Repeatability repeatability)
  : pid{ pid }
  , payload{
    static_cast<uint8_t>(SetParam::Engines),         // op1
    static_cast<uint8_t>(engine),                    // op2
    static_cast<uint8_t>(EnginesSet::Repeatability), // op3
    static_cast<uint8_t>(repeatability.value_of())   // op4
  }
{
}

EngineWriteMicrostep::EngineWriteMicrostep(PacketID pid,
                                           uint8_t engine,
                                           device::Microstep microstep)
  : pid{ pid }
  , payload{
    static_cast<srvint::PayloadType>(SetParam::Engines),     // op1
    static_cast<srvint::PayloadType>(engine),                // op2
    static_cast<srvint::PayloadType>(EnginesSet::Microstep), // op3
    static_cast<srvint::PayloadType>(microstep)              // op4
  }
{
}

EngineWriteHomePosition::EngineWriteHomePosition(
  PacketID pid,
  uint8_t engine,
  device::Coordinate home_position)
  : pid{ pid }
  , payload{
    static_cast<srvint::PayloadType>(SetParam::Engines),        // op1
    static_cast<srvint::PayloadType>(engine),                   // op2
    static_cast<srvint::PayloadType>(EnginesSet::HomePosition), // op3
  }
{
  auto const value_encoded{ utils::typeToByteArray<qint64>(
    home_position.value_of()) };
  std::copy(
    value_encoded.cbegin(), value_encoded.cend(), std::back_inserter(payload));
}

EngineWriteF0::EngineWriteF0(PacketID pid,
                             uint8_t engine,
                             device::Frequency value)
  : pid{ pid }
  , payload{ static_cast<srvint::PayloadType>(SetParam::Engines), // op1
             static_cast<srvint::PayloadType>(engine),            // op2
             static_cast<srvint::PayloadType>(EnginesSet::F0) }   // op3
{
  const auto value_encoded{ utils::typeToByteArray(value.value_of()) };
  std::copy(
    value_encoded.cbegin(), value_encoded.cend(), std::back_inserter(payload));
}

EngineWriteFMax::EngineWriteFMax(PacketID pid,
                                 uint8_t engine,
                                 device::Frequency value)
  : pid{ pid }
  , payload{ static_cast<srvint::PayloadType>(SetParam::Engines), // op1
             static_cast<srvint::PayloadType>(engine),            // op2
             static_cast<srvint::PayloadType>(EnginesSet::FMAX) } // op3
{
  const auto value_encoded{ utils::typeToByteArray(value.value_of()) };
  std::copy(
    value_encoded.cbegin(), value_encoded.cend(), std::back_inserter(payload));
}

EngineWriteDFMax::EngineWriteDFMax(PacketID pid,
                                   uint8_t engine,
                                   device::Frequency value)
  : pid{ pid }
  , payload{ static_cast<srvint::PayloadType>(SetParam::Engines),  // op1
             static_cast<srvint::PayloadType>(engine),             // op2
             static_cast<srvint::PayloadType>(EnginesSet::DFMAX) } // op3
{
  constexpr auto denominator{ 1e3 };
  const auto value_encoded{ utils::typeToByteArray(value.value_of() /
                                                   denominator) };
  std::copy(
    value_encoded.cbegin(), value_encoded.cend(), std::back_inserter(payload));
}

EngineWritePosition::EngineWritePosition(PacketID pid,
                                         uint8_t engine,
                                         device::Coordinate position)
  : pid{ pid }
  , payload{
    static_cast<srvint::PayloadType>(SetParam::Engines),          // op1
    static_cast<srvint::PayloadType>(engine),                     // op2
    static_cast<srvint::PayloadType>(EnginesSet::CurrentPosition) // op3
  }
{
  const auto bytes_encoded{ utils::typeToByteArray<qint64>(
    position.value_of()) };
  std::copy(
    bytes_encoded.cbegin(), bytes_encoded.cend(), std::back_inserter(payload));
}

EngineSavePosition::EngineSavePosition(PacketID pid, uint8_t engine)
  : pid{ pid }
  , payload{
    static_cast<srvint::PayloadType>(SetParam::Engines),       // op1
    static_cast<srvint::PayloadType>(engine),                  // op2
    static_cast<srvint::PayloadType>(EnginesSet::SavePosition) // op3
  }
{
}

EngineLoadPosition::EngineLoadPosition(PacketID pid, uint8_t engine)
  : pid{ pid }
  , payload{
    static_cast<srvint::PayloadType>(SetParam::Engines),       // op1
    static_cast<srvint::PayloadType>(engine),                  // op2
    static_cast<srvint::PayloadType>(EnginesSet::LoadPosition) // op3
  }
{
}

EngineEnableRequest::EngineEnableRequest(PacketID pid,
                                         uint8_t engine,
                                         bool enable)
  : pid{ pid }
  , payload{
    static_cast<srvint::PayloadType>(SetParam::Engines), // op1
    static_cast<srvint::PayloadType>(engine),            // op2
    static_cast<srvint::PayloadType>(enable ? EnginesSet::EngineOn
                                            : EnginesSet::EngineOff) // op3
  }
{
}

EngineDriverRawWrite::EngineDriverRawWrite(PacketID pid,
                                           uint8_t engine,
                                           const QByteArray& bytes)
  : pid{ pid }
  , payload{
    static_cast<srvint::PayloadType>(SetParam::Engines),      // op1
    static_cast<srvint::PayloadType>(engine),                 // op2
    static_cast<srvint::PayloadType>(EnginesSet::DriverWrite) // op3
  }
{
  std::copy(bytes.cbegin(), bytes.cend(), std::back_inserter(payload));
}

EngineDriverFactoryDefault::EngineDriverFactoryDefault(PacketID pid,
                                                       uint8_t engine)
  : pid{ pid }
  , payload{
    static_cast<srvint::PayloadType>(SetParam::Engines),         // op1
    static_cast<srvint::PayloadType>(engine),                    // op2
    static_cast<srvint::PayloadType>(EnginesSet::FactoryDefault) // op3
  }
{
}

} // namespace protocol::motherboard
