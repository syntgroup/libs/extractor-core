#ifndef PROTOCOL_MOTHERBOARD_DEVICEREQUESTID_H
#define PROTOCOL_MOTHERBOARD_DEVICEREQUESTID_H

#include "motherboard/protocol/motherboard_cmd.h"
#include "serial/protocol/protocol_values.h"
#include "serial/protocol/request_interface.h"
#include "srvint/srvint_defs.h"
#include <array>

namespace protocol::motherboard {

struct DeviceRequestId final : protocol::RequestInterface
{
  explicit DeviceRequestId(PacketID pid_)
    : pid{ pid_ }
  {
  }

  const PacketID pid;
  const srvint::Payload payload{
    static_cast<std::underlying_type_t<GetParam>>(GetParam::Device),
    static_cast<std::underlying_type_t<DeviceGet>>(DeviceGet::Id)
  };
};

struct DeviceSetIdRequest final : protocol::RequestInterface
{
  explicit DeviceSetIdRequest(PacketID pid, uint32_t new_device_id);

  const PacketID pid;
  srvint::Payload payload;
};

struct DeviceRequestFwRev : protocol::RequestInterface
{
  explicit DeviceRequestFwRev(PacketID pid_)
    : pid{ pid_ }
  {
  }

  const PacketID pid;
  const srvint::Payload payload{
    static_cast<std::underlying_type_t<GetParam>>(GetParam::Device),
    static_cast<std::underlying_type_t<DeviceGet>>(DeviceGet::FirmwareRevision)
  };
};

} // namespace protocol::motherboard

#endif // PROTOCOL_MOTHERBOARD_DEVICEREQUESTID_H
