#ifndef POWER_RESPONSE_H
#define POWER_RESPONSE_H

#include "motherboard/device/motherboard_values.h"
#include "motherboard/protocol/motherboard_response.h"

namespace protocol::motherboard {

struct GetRailResponse final : MotherboardResponse
{
  explicit GetRailResponse(std::unique_ptr<srvint::SrvIntPacket>& packet);

  device::Voltage voltage{ 0 };

private:
  enum Pos
  {
    Param = 0, // op1
    Pcode,     // op2
    Double1,   // op3
    Double2,   // op4
    Double3,   // op5
    Double4,   // op6
    Double5,   // op7
    Double6,   // op8
    Double7,   // op9
    Double8,   // op10
    LastError  // op11
  };
};

struct GetBtnsResponse final : MotherboardResponse
{
  explicit GetBtnsResponse(std::unique_ptr<srvint::SrvIntPacket>& packet);

  bool statePushButon{ false };
  bool stateGercon{ false };

private:
  enum Pos
  {
    Param = 0,  // op1
    Pcode,      // op2
    PushButton, // op3
    Gercon,     // op4
    LastError   // op5
  };
};

} // namespace protocol::motherboard

#endif // POWER_RESPONSE_H
