#include "motherboard/protocol/motherboard_encoder.h"

#include "motherboard/protocol/motherboard_response_type.h"
#include "srvint/srvint_request_factory.h"

#include "buttons_request.h"
#include "device_request.h"
#include "engine_request.h"
#include "goto_request.h"
#include "ping_request.h"
#include "power_request.h"
#include "rgb_request.h"
#include "sns_state_request.h"
#include "system_request.h"

namespace protocol::motherboard {

MotherboardEncoder::MotherboardEncoder()
  : m_factory{ std::make_unique<protocol::srvint::SrvIntRequestFactory>() }
{
}

MotherboardEncoder::~MotherboardEncoder() = default;

QByteArray
MotherboardEncoder::encode(const Request& request, int type) const
{
  switch (static_cast<ResponseType>(type)) {
    case Unknown:
    case Empty:
      break;

    case DeviceSetId: {
      const auto* casted{ static_cast<const DeviceSetIdRequest*>(
        request.get()) };
      return m_factory->setParam(casted->pid, casted->payload);
    }

    case DeviceGetId: {
      const auto* casted{ static_cast<const DeviceRequestId*>(request.get()) };
      return m_factory->getParam(casted->pid, casted->payload);
    }

    case DeviceGetFwRev: {
      const auto* casted{ static_cast<const DeviceRequestFwRev*>(
        request.get()) };
      return m_factory->getParam(casted->pid, casted->payload);
    }

    case StatusPing: {
      const auto* casted{ static_cast<const StatusPingRequest*>(
        request.get()) };
      return m_factory->create(casted->pid, casted->command);
    }
    case SystemSoftReboot: {
      const auto* casted{ static_cast<const SystemSoftRebootRequest*>(
        request.get()) };
      return m_factory->create(casted->pid, casted->command);
    }
    case SystemHardReboot: {
      const auto* casted{ static_cast<const SystemHardRebootRequest*>(
        request.get()) };
      return m_factory->create(casted->pid, casted->command);
    }
    case SystemGoToBootloader: {
      const auto* casted{ static_cast<const SystemGoToBootloaderRequest*>(
        request.get()) };
      return m_factory->create(casted->pid, casted->command);
    }
    case SystemGetError: {
      const auto* casted{ static_cast<const SystemGetErrorRequest*>(
        request.get()) };
      return m_factory->create(casted->pid, casted->command, casted->payload);
    }
    case SystemResetError: {
      const auto* casted{ static_cast<const SystemZeroizeErrorRequest*>(
        request.get()) };
      return m_factory->create(casted->pid, casted->command);
    }
    case GotoNTicks: {
      const auto* casted{ static_cast<const GotoNTicksRequest*>(
        request.get()) };
      return m_factory->setParam(casted->pid, casted->payload);
    }
    case GotoAbsPos: {
      const auto* casted{ static_cast<const GotoAbsPosRequest*>(
        request.get()) };
      return m_factory->setParam(casted->pid, casted->payload);
    }
    case GotoZeroPos: {
      const auto* casted{ static_cast<const GotoZeroRequest*>(request.get()) };
      return m_factory->setParam(casted->pid, casted->payload);
    }
    case GotoEmStop: {
      const auto* casted{ static_cast<const GotoEmStopRequest*>(
        request.get()) };
      return m_factory->setParam(casted->pid, casted->payload);
    }
    case GotoUncontrolled: {
      const auto* casted{ static_cast<const GotoNTicksUNCRequest*>(
        request.get()) };
      return m_factory->setParam(casted->pid, casted->payload);
    }
    case GotoSpeedSet: {
      const auto* casted{ static_cast<const GotoSpeedSetRequest*>(
        request.get()) };
      return m_factory->setParam(casted->pid, casted->payload);
    }
    case GotoSmoothStop: {
      const auto* casted{ static_cast<const GotoSmoothStopRequest*>(
        request.get()) };
      return m_factory->setParam(casted->pid, casted->payload);
    }
    case GotoUncontrolledWOpts: {
      const auto* casted{ static_cast<const GotoNTicksUNCRequestWoOpts*>(
        request.get()) };
      return m_factory->setParam(casted->pid, casted->payload);
    }
    case GotoMixCycles: {
      const auto* casted{ static_cast<const GotoMixCyclesRequest*>(
        request.get()) };
      return m_factory->setParam(casted->pid, casted->payload);
    }
    case GotoMixModeOff: {
      const auto* casted{ static_cast<const GotoMixStop*>(request.get()) };
      return m_factory->setParam(casted->pid, casted->payload);
    }
    case GotoWriteCurrents: {
      const auto* casted{ static_cast<const GotoCurrentsSetRequest*>(
        request.get()) };
      return m_factory->setParam(casted->pid, casted->payload);
    }
    case GotoMixTime: {
      const auto* casted{ static_cast<const GotoMixTimeRequest*>(
        request.get()) };
      return m_factory->setParam(casted->pid, casted->payload);
    }
    case EnginesGetMaxSpeed: {
      const auto* casted{ static_cast<const EngineRequestMaxSpeed*>(
        request.get()) };
      return m_factory->getParam(casted->pid, casted->payload);
    }
    case EnginesGetRepeatability: {
      const auto* casted{ static_cast<const EngineRequestRepeatability*>(
        request.get()) };
      return m_factory->getParam(casted->pid, casted->payload);
    }
    case EnginesGetMicrostep: {
      const auto* casted{ static_cast<const EngineRequestMicrostep*>(
        request.get()) };
      return m_factory->getParam(casted->pid, casted->payload);
    }
    case EnginesGetHomePosition: {
      const auto* casted{ static_cast<const EngineRequestHomePosition*>(
        request.get()) };
      return m_factory->getParam(casted->pid, casted->payload);
    }
    case EnginesGetF0: {
      const auto* casted{ static_cast<const EngineRequestF0*>(request.get()) };
      return m_factory->getParam(casted->pid, casted->payload);
    }
    case EnginesGetFMax: {
      const auto* casted{ static_cast<const EngineRequestFMax*>(
        request.get()) };
      return m_factory->getParam(casted->pid, casted->payload);
    }
    case EnginesGetDfMax: {
      const auto* casted{ static_cast<const EngineRequestDFMax*>(
        request.get()) };
      return m_factory->getParam(casted->pid, casted->payload);
    }
    case EnginesGetPosition: {
      const auto* casted{ static_cast<const EngineRequestPosition*>(
        request.get()) };
      return m_factory->getParam(casted->pid, casted->payload);
    }
    case EnginesGetCurrents: {
      const auto* casted{ static_cast<const EnginesCurrentsGetRequest*>(
        request.get()) };
      return m_factory->getParam(casted->pid, casted->payload);
    }
    case EnginesGetMixModeState: {
      const auto* casted{ static_cast<const EngineRequestMixModeState*>(
        request.get()) };
      return m_factory->getParam(casted->pid, casted->payload);
    }
    case EnginesSetMaxSpeed: {
      const auto* casted{ static_cast<const EngineWriteMaxSpeed*>(
        request.get()) };
      return m_factory->setParam(casted->pid, casted->payload);
    }
    case EnginesSetRepeatability: {
      const auto* casted{ static_cast<const EngineWriteRepeatability*>(
        request.get()) };
      return m_factory->setParam(casted->pid, casted->payload);
    }
    case EnginesSetMicrostep: {
      const auto* casted{ static_cast<const EngineWriteMicrostep*>(
        request.get()) };
      return m_factory->setParam(casted->pid, casted->payload);
    }
    case EnginesSetHomePosition: {
      const auto* casted{ static_cast<const EngineWriteHomePosition*>(
        request.get()) };
      return m_factory->setParam(casted->pid, casted->payload);
    }
    case EnginesSetF0: {
      const auto* casted{ static_cast<const EngineWriteF0*>(request.get()) };
      return m_factory->setParam(casted->pid, casted->payload);
    }
    case EnginesSetFMax: {
      const auto* casted{ static_cast<const EngineWriteFMax*>(request.get()) };
      return m_factory->setParam(casted->pid, casted->payload);
    }
    case EnginesSetDfMax: {
      const auto* casted{ static_cast<const EngineWriteDFMax*>(request.get()) };
      return m_factory->setParam(casted->pid, casted->payload);
    }
    case EnginesSetPosition: {
      const auto* casted{ static_cast<const EngineWritePosition*>(
        request.get()) };
      return m_factory->setParam(casted->pid, casted->payload);
    }
    case EnginesEnable: {
      const auto* casted{ static_cast<const EngineEnableRequest*>(
        request.get()) };
      return m_factory->setParam(casted->pid, casted->payload);
    }
    case EnginesDriverRawWrite: {
      const auto* casted{ static_cast<const EngineDriverRawWrite*>(
        request.get()) };
      return m_factory->setParam(casted->pid, casted->payload);
    }
    case EnginesDriverFactoryDefaults: {
      const auto* casted{ static_cast<const EngineDriverFactoryDefault*>(
        request.get()) };
      return m_factory->setParam(casted->pid, casted->payload);
    }
    case PowerSetCooler: {
      const auto* casted{ static_cast<const PowerSetCoolerRequest*>(
        request.get()) };
      return m_factory->setParam(casted->pid, casted->payload);
    }
    case PowerSetLeds: {
      const auto* casted{ static_cast<const PowerSetLedsRequest*>(
        request.get()) };
      return m_factory->setParam(casted->pid, casted->payload);
    }
    case PowerSetUvLamp: {
      const auto* casted{ static_cast<const PowerSetUVLampRequest*>(
        request.get()) };
      return m_factory->setParam(casted->pid, casted->payload);
    }
    case PowerSetBuzzer: {
      const auto* casted{ static_cast<const PowerSetBuzzerRequest*>(
        request.get()) };
      return m_factory->setParam(casted->pid, casted->payload);
    }
    case PowerSetShdn: {
      const auto* casted{ static_cast<const PowerSetSHDNRequest*>(
        request.get()) };
      return m_factory->setParam(casted->pid, casted->payload);
    }
    case PowerTurnOff: {
      const auto* casted{ static_cast<const PowerSetTurnOff*>(request.get()) };
      return m_factory->setParam(casted->pid, casted->payload);
    }
    case PowerSetWdtValue: {
      const auto* casted{ static_cast<const PowerSetWdtTimeout*>(
        request.get()) };
      return m_factory->setParam(casted->pid, casted->payload);
    }
    case VoltageGet24VRail: {
      const auto* casted{ static_cast<const Voltage24VRailRequest*>(
        request.get()) };
      return m_factory->getParam(casted->pid, casted->payload);
    }
    case VoltageGetUvLampRail: {
      const auto* casted{ static_cast<const VoltageUVLampRequest*>(
        request.get()) };
      return m_factory->getParam(casted->pid, casted->payload);
    }
    case VoltageGetR2RdacRail: {
      const auto* casted{ static_cast<const VoltageR2RDACRequest*>(
        request.get()) };
      return m_factory->getParam(casted->pid, casted->payload);
    }
    case VoltageGetBtns: {
      const auto* casted{ static_cast<const VoltageBtnsRequest*>(
        request.get()) };
      return m_factory->getParam(casted->pid, casted->payload);
    }
    case RgbSetOnOff: {
      const auto* casted{ static_cast<const RgbSetOnOffRequest*>(
        request.get()) };
      return m_factory->setParam(casted->pid, casted->payload);
    }
    case GerconGetState: {
      const auto* casted{ static_cast<const GerconGetStateRequest*>(
        request.get()) };
      return m_factory->getParam(casted->pid, casted->payload);
    }
    case GerconClearTrigger: {
      const auto* casted{ static_cast<const GerconClearTriggerRequest*>(
        request.get()) };
      return m_factory->getParam(casted->pid, casted->payload);
    }
    case SnsState: {
      const auto* casted{ static_cast<const SnsStateRequest*>(request.get()) };
      return m_factory->getParam(casted->pid, casted->payload);
    }
    case OptDiagState: {
      const auto* casted{ static_cast<const OptDiagRequest*>(request.get()) };
      return m_factory->getParam(casted->pid, casted->payload);
    }
    case OptDiagClearTrigger: {
      const auto* casted{ static_cast<const OptDiagClearTriggerRequest*>(
        request.get()) };
      return m_factory->setParam(casted->pid, casted->payload);
    }
    case PwrBtnGetState: {
      const auto* casted{ static_cast<const PowerButtonGetState*>(
        request.get()) };
      return m_factory->getParam(casted->pid, casted->payload);
    }
    case PwrBtnClearTrigger: {
      const auto* casted{ static_cast<const PowerButtonClearTrigger*>(
        request.get()) };
      return m_factory->setParam(casted->pid, casted->payload);
    }
  }
  return {};
}

} // namespace protocol::motherboard
