#ifndef CMD_MOTHERBOARD_PING_H
#define CMD_MOTHERBOARD_PING_H

#include "serial/protocol/protocol_values.h"
#include "serial/protocol/request_interface.h"
#include "srvint/srvint_defs.h"

namespace protocol::motherboard {

struct StatusPingRequest final : protocol::RequestInterface
{
  explicit StatusPingRequest(PacketID pid)
    : pid{ pid }
  {
  }

  const PacketID pid;
  const srvint::Command command{ srvint::Command::Ping };
};

} // namespace protocol::motherboard

#endif // CMD_MOTHERBOARD_PING_H
