#ifndef EXTRACTOR_TOUCHPANEL_SNS_STATE_RESPONSE_H
#define EXTRACTOR_TOUCHPANEL_SNS_STATE_RESPONSE_H

#include "motherboard/protocol/motherboard_response.h"

namespace protocol::motherboard {

struct SnsStateResponse final : MotherboardResponse
{
  explicit SnsStateResponse(SrvIntPacket& packet);

  uint8_t snsStates{ 0 };

private:
  enum Pos
  {
    GetParamSns,         // op1
    GetParamSnsStateAll, // op2
    SnsState,            // op3 (1 - object present, 0 - empty)
    LastError            // op4
  };
};

} // namespace protocol::motherboard

#endif // EXTRACTOR_TOUCHPANEL_SNS_STATE_RESPONSE_H
