#include "device_request.h"

#include "utils/byte_converter.h"

namespace protocol::motherboard {

DeviceSetIdRequest::DeviceSetIdRequest(PacketID pid_, uint32_t new_device_id)
  : pid{ pid_ }
  , payload{
    static_cast<srvint::PayloadType>(SetParam::Device),        // op1
    static_cast<srvint::PayloadType>(DeviceSet::Id),           // op2
    static_cast<srvint::PayloadType>(DeviceIdAccessKey::MSB),  // op3
    static_cast<srvint::PayloadType>(DeviceIdAccessKey::MIDB), // op4
    static_cast<srvint::PayloadType>(DeviceIdAccessKey::LSB)   // op5
  }
{
  auto value{ utils::type_to_bytes<uint32_t>(new_device_id) };
  std::move(value.begin(), value.end(), std::back_inserter(payload));
}

} // namespace protocol::motherboard
