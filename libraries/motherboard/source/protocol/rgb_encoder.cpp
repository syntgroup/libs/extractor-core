#include "rgb_encoder.h"

#include <bitset>

namespace protocol::motherboard {

auto
RgbEncoder::encode(const std::vector<Qt::GlobalColor>& leds) -> uint32_t
{
  std::bitset<std::numeric_limits<uint32_t>::digits> result;

  for (size_t i = 0; i < leds.size(); ++i) {
    const auto [low, mid, high] =
      [](Qt::GlobalColor color) -> std::tuple<bool, bool, bool> {
      switch (color) {
        case Qt::black:
          return std::make_tuple(false, false, false);
        case Qt::blue:
          return std::make_tuple(true, false, false);
        case Qt::green:
          return std::make_tuple(false, true, false);
        case Qt::cyan:
          return std::make_tuple(true, true, false);
        case Qt::red:
          return std::make_tuple(false, false, true);
        case Qt::magenta:
          return std::make_tuple(true, false, true);
        case Qt::yellow:
          return std::make_tuple(false, true, true);
        case Qt::white:
        default:
          return std::make_tuple(true, true, true);
      }
    }(leds.at(i));

    result.set(i * 3, low);
    result.set(i * 3 + 1, mid);
    result.set(i * 3 + 2, high);
  }

  return result.to_ulong();
}

} // namespace protocol::motherboard
