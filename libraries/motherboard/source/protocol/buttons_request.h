#ifndef PROTOCOL_MOTHERBOARD_BUTTONSREQUEST_H
#define PROTOCOL_MOTHERBOARD_BUTTONSREQUEST_H

#include "motherboard/protocol/motherboard_cmd.h"
#include "serial/protocol/protocol_values.h"
#include "serial/protocol/request_interface.h"
#include "srvint/srvint_defs.h"

namespace protocol::motherboard {

struct GerconClearTriggerRequest final : protocol::RequestInterface
{
  explicit GerconClearTriggerRequest(PacketID pid_)
    : pid{ pid_ }
  {
  }

  const PacketID pid;
  const srvint::Payload payload{
    static_cast<srvint::PayloadType>(SetParam::Gercon),       // op1
    static_cast<srvint::PayloadType>(GerconSet::ClearTrigger) // op2
  };
};

struct GerconGetStateRequest final : protocol::RequestInterface
{
  explicit GerconGetStateRequest(PacketID pid_)
    : pid{ pid_ }
  {
  }
  const PacketID pid;
  const srvint::Payload payload{
    static_cast<srvint::PayloadType>(GetParam::Gercon), // op1
    static_cast<srvint::PayloadType>(GerconGet::State)  // op2
  };
};

struct OptDiagRequest final : protocol::RequestInterface
{
  explicit OptDiagRequest(PacketID pid_)
    : pid{ pid_ }
  {
  }
  const PacketID pid;
  const srvint::Payload payload{
    static_cast<srvint::PayloadType>(GetParam::OptDiag), // op1
    static_cast<srvint::PayloadType>(OptDiagGet::State)  // op2
  };
};

struct OptDiagClearTriggerRequest final : protocol::RequestInterface
{
  explicit OptDiagClearTriggerRequest(PacketID pid_)
    : pid{ pid_ }
  {
  }
  const PacketID pid;
  const srvint::Payload payload{
    static_cast<srvint::PayloadType>(SetParam::OptDiag),       // op1
    static_cast<srvint::PayloadType>(OptDiagSet::ClearTrigger) // op2
  };
};

struct PowerButtonGetState final : protocol::RequestInterface
{
  explicit PowerButtonGetState(PacketID pid_)
    : pid{ pid_ }
  {
  }
  const PacketID pid;
  const srvint::Payload payload{
    static_cast<srvint::PayloadType>(GetParam::PwrBtn), // op1
    static_cast<srvint::PayloadType>(PwrBtnGet::State)  // op2
  };
};

struct PowerButtonClearTrigger final : protocol::RequestInterface
{
  explicit PowerButtonClearTrigger(PacketID pid_)
    : pid{ pid_ }
  {
  }
  const PacketID pid;
  const srvint::Payload payload{
    static_cast<srvint::PayloadType>(SetParam::PwrBtn),       // op1
    static_cast<srvint::PayloadType>(PwrBtnSet::ClearTrigger) // op2
  };
};

} // namespace protocol::motherboard

#endif // PROTOCOL_MOTHERBOARD_BUTTONSREQUEST_H
