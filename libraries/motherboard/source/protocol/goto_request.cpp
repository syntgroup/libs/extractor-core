#include "goto_request.h"

#include "motherboard/protocol/motherboard_cmd.h"
#include "utils/byte_converter.h"

namespace protocol::motherboard {

GotoNTicksRequest::GotoNTicksRequest(PacketID pid, const Params& params)
  : pid{ pid }
  , payload{
    static_cast<srvint::PayloadType>(SetParam::Goto) // op1
  }
{
  if (params.empty() || params.size() > 2) {
    payload.clear();
    return;
  }
  if (params.size() == 2 && params.at(0).first == params.at(1).first) {
    // error, not able to move more than 2 engines simanteniously!!!!!!!!!!
    payload.clear();
    return;
  }

  std::for_each(params.cbegin(), params.cend(), [this](const auto& param) {
    // add engine address
    payload.emplace_back(static_cast<srvint::PayloadType>(param.first));
    // add opcode
    payload.emplace_back(static_cast<srvint::PayloadType>(GotoSet::GotoNTicks));

    const auto position_encoded{ utils::typeToByteArray(
      std::get<device::Coordinate>(param).value_of()) };
    std::copy(position_encoded.cbegin(),
              position_encoded.cend(),
              std::back_inserter(payload));
  });
}

GotoNTicksRequest::GotoNTicksRequest(PacketID pid,
                                     uint8_t engine,
                                     device::Coordinate target)
  : pid{ pid }
  , payload{
    static_cast<srvint::PayloadType>(SetParam::Goto),     // op1
    static_cast<srvint::PayloadType>(engine),             // op2
    static_cast<srvint::PayloadType>(GotoSet::GotoNTicks) // op3
  }
{
  const auto position_encoded{ utils::typeToByteArray<qint64>(
    target.value_of()) };
  std::copy(position_encoded.cbegin(),
            position_encoded.cend(),
            std::back_inserter(payload));
}

GotoZeroRequest::GotoZeroRequest(PacketID pid,
                                 uint8_t engine,
                                 device::ZeroSwitch zero_switch)
  : pid{ pid }
  , payload{
    static_cast<srvint::PayloadType>(SetParam::Goto),            // op1
    static_cast<srvint::PayloadType>(engine),                    // op2
    static_cast<srvint::PayloadType>(GotoSet::GotoZeroPosition), // op3
    static_cast<srvint::PayloadType>(zero_switch)                // op4
  }
{
}

GotoEmStopRequest::GotoEmStopRequest(PacketID pid, const Params& params)
  : pid{ pid }
  , payload{
    static_cast<srvint::PayloadType>(SetParam::Goto) // op1
  }
{
  if (params.empty() || params.size() > 2) {
    payload.clear();
    return;
  }
  if (params.size() == 2 && params.at(0) == params.at(1)) {
    payload.clear();
    return;
  }

  for (auto&& param : params) {
    payload.emplace_back(static_cast<srvint::PayloadType>(param));
    payload.emplace_back(
      static_cast<srvint::PayloadType>(GotoSet::EmergencyStop));
  }
}

GotoEmStopRequest::GotoEmStopRequest(PacketID pid, uint8_t engine)
  : pid{ pid }
  , payload{
    static_cast<srvint::PayloadType>(SetParam::Goto),        // op1
    static_cast<srvint::PayloadType>(engine),                // op2
    static_cast<srvint::PayloadType>(GotoSet::EmergencyStop) // op3
  }
{
}

GotoSmoothStopRequest::GotoSmoothStopRequest(PacketID pid, const Params& params)
  : pid{ pid }
  , payload{
    static_cast<srvint::PayloadType>(SetParam::Goto) // op1
  }
{
  if (params.empty() || params.size() > 2) {
    payload.clear();
    return;
  }
  if (params.size() == 2 && params.at(0) == params.at(1)) {
    payload.clear();
    return;
  }
  for (auto&& param : params) {
    payload.emplace_back(static_cast<srvint::PayloadType>(param));
    payload.emplace_back(static_cast<srvint::PayloadType>(GotoSet::SmoothStop));
  }
}

GotoSmoothStopRequest::GotoSmoothStopRequest(PacketID pid, uint8_t engine)
  : pid{ pid }
  , payload{
    static_cast<srvint::PayloadType>(SetParam::Goto),     // op1
    static_cast<srvint::PayloadType>(engine),             // op2
    static_cast<srvint::PayloadType>(GotoSet::SmoothStop) // op3
  }
{
}

GotoAbsPosRequest::GotoAbsPosRequest(PacketID pid, const Params& params)
  : pid{ pid }
  , payload{ static_cast<srvint::PayloadType>(SetParam::Goto) }
{
  if (params.empty() || params.size() > 2) {
    payload.clear();
    return;
  }
  if (params.size() == 2 &&
      std::get<uint8_t>(params.at(0)) == std::get<uint8_t>(params.at(1))) {
    payload.clear();
    return;
  }

  for (auto&& param : params) {
    payload.emplace_back(
      static_cast<srvint::PayloadType>(std::get<uint8_t>(param)));
    payload.emplace_back(
      static_cast<srvint::PayloadType>(GotoSet::GotoAbsolutePosition));

    const auto position_encoded{ utils::typeToByteArray(
      std::get<device::Coordinate>(param).value_of()) };
    std::copy(position_encoded.cbegin(),
              position_encoded.cend(),
              std::back_inserter(payload));
  }
}

GotoAbsPosRequest::GotoAbsPosRequest(PacketID pid,
                                     uint8_t engine,
                                     device::Coordinate target)
  : pid{ pid }
  , payload{
    static_cast<srvint::PayloadType>(SetParam::Goto),               // op1
    static_cast<srvint::PayloadType>(engine),                       // op2
    static_cast<srvint::PayloadType>(GotoSet::GotoAbsolutePosition) // op3
  }
{
  const auto position_encoded{ utils::typeToByteArray<qint64>(
    target.value_of()) };
  std::copy(position_encoded.cbegin(),
            position_encoded.cend(),
            std::back_inserter(payload));
}

GotoSpeedSetRequest::GotoSpeedSetRequest(PacketID pid, const Params& params)
  : pid{ pid }
  , payload{
    static_cast<srvint::PayloadType>(SetParam::Goto) // op1
  }
{
  for (auto&& param : params) {
    payload.emplace_back(
      static_cast<srvint::PayloadType>(param.first)); // add engine idx
    payload.emplace_back(static_cast<srvint::PayloadType>(GotoSet::SpeedSet));

    const auto speed_encoded{ utils::typeToByteArray(
      std::get<device::SpeedIndex>(param).value_of()) };
    std::copy(speed_encoded.cbegin(),
              speed_encoded.cend(),
              std::back_inserter(payload));
  }
}

GotoMixCyclesRequest::GotoMixCyclesRequest(PacketID pid,
                                           uint8_t engine,
                                           device::Cycles ticks,
                                           device::Coordinate amplitude)
  : pid{ pid }
  , payload{
    static_cast<srvint::PayloadType>(SetParam::Goto),    // op1
    static_cast<srvint::PayloadType>(engine),            // op2
    static_cast<srvint::PayloadType>(GotoSet::MixModeOn) // op3
  }
{
  const auto ticks_encoded{ utils::type_to_bytes(ticks.value_of()) };
  std::copy(
    ticks_encoded.cbegin(), ticks_encoded.cend(), std::back_inserter(payload));

  //! @todo ПРОТЕСТИРОВАТЬ ВУСМЕРТЬ
  const auto amplitude_encoded{
    utils::type_to_bytes<uint32_t, srvint::PayloadType>(amplitude.value_of())
  };
  std::copy(amplitude_encoded.cbegin(),
            amplitude_encoded.cend(),
            std::back_inserter(payload));
}

GotoMixStop::GotoMixStop(PacketID pid, uint8_t engine)
  : pid{ pid }
  , payload{
    static_cast<srvint::PayloadType>(SetParam::Goto),     // op1
    static_cast<srvint::PayloadType>(engine),             // op2
    static_cast<srvint::PayloadType>(GotoSet::MixModeOff) // op3
  }
{
}

GotoCurrentsSetRequest::GotoCurrentsSetRequest(PacketID pid,
                                               uint8_t engine,
                                               device::Current current_accel,
                                               device::Current current_stdby)
  : pid{ pid }
  , payload{
    static_cast<srvint::PayloadType>(SetParam::Goto),                // op1
    static_cast<srvint::PayloadType>(engine),                        // op2
    static_cast<srvint::PayloadType>(GotoSet::AccelStandbyCurrents), // op3
    static_cast<srvint::PayloadType>(current_accel),                 // op4
    static_cast<srvint::PayloadType>(current_stdby)                  // op5
  }
{
}

GotoNTicksUNCRequest::GotoNTicksUNCRequest(PacketID pid,
                                           uint8_t engine,
                                           device::Coordinate ticks)
  : pid{ pid }
  , payload{
    static_cast<srvint::PayloadType>(SetParam::Goto),            // op1
    static_cast<srvint::PayloadType>(engine),                    // op2
    static_cast<srvint::PayloadType>(GotoSet::GotoNUncontrolled) // op3
  }
{
  auto const pos_encoded{
    utils::typeToByteArray<strong::underlying_type_t<device::Coordinate>>(
      ticks.value_of())
  };
  std::copy(
    pos_encoded.cbegin(), pos_encoded.cend(), std::back_inserter(payload));
}

GotoNTicksUNCRequestWoOpts::GotoNTicksUNCRequestWoOpts(PacketID pid,
                                                       uint8_t engine,
                                                       device::Coordinate ticks)
  : pid{ pid }
  , payload{
    static_cast<srvint::PayloadType>(SetParam::Goto),                     // op1
    static_cast<srvint::PayloadType>(engine),                             // op2
    static_cast<srvint::PayloadType>(GotoSet::GotoNUncontrolledWoSensors) // op3
  }
{
  auto const pos_encoded{
    utils::typeToByteArray<strong::underlying_type_t<device::Coordinate>>(
      ticks.value_of())
  };
  std::copy(
    pos_encoded.cbegin(), pos_encoded.cend(), std::back_inserter(payload));
}

protocol::motherboard::GotoMixTimeRequest::GotoMixTimeRequest(
  PacketID pid,
  uint8_t engine,
  std::chrono::seconds seconds,
  device::Coordinate amplitude)
  : pid{ pid }
  , payload{
    static_cast<srvint::PayloadType>(SetParam::Goto),      // op1
    static_cast<srvint::PayloadType>(engine),              // op2
    static_cast<srvint::PayloadType>(GotoSet::MixModeTime) // op3
  }
{
  const auto encoded_time{ utils::type_to_bytes<uint>(seconds.count()) };
  std::copy(
    encoded_time.cbegin(), encoded_time.cend(), std::back_inserter(payload));

  const auto encoded_amplitude{ utils::type_to_bytes<uint32_t>(
    (amplitude.value_of())) };
  std::copy(encoded_amplitude.cbegin(),
            encoded_amplitude.cend(),
            std::back_inserter(payload));
}

} // namespace protocol::motherboard
