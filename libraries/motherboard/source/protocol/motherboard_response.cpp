#include "motherboard/protocol/motherboard_response.h"

#include "motherboard/protocol/motherboard_response_type.h"
#include "srvint/srvint_packet.h"
#include "utils/converter.h"

namespace protocol::motherboard {

MotherboardResponse::MotherboardResponse(SrvIntPacket& packet)
  : m_packet{ std::move(packet) }
  , m_errorCode{ detectError() }
{
}

MotherboardResponse::~MotherboardResponse() = default;

auto
MotherboardResponse::id() const -> PacketID
{
  return m_packet->id();
}

auto
MotherboardResponse::encodedSize() const -> size_t
{
  return m_packet->encode().size();
}

auto
MotherboardResponse::hasNext() const -> bool
{
  return false;
}

auto
MotherboardResponse::isError() const -> bool
{
  return m_errorCode != MotherboardError::NoError;

  // ~~реагируем только на критические ошибки, остальное должно более-менее
  // рассасываться в обработчиках команд~~
  // реагируем на ошибки протокола? т.к. связанные с конкретными операциями
  // ошибки есть возможность (и лучше так и поступать) разбирать внутри,
  // собственно, операций
}

auto
MotherboardResponse::errorString() const -> QString
{
  return utils::to_string(m_errorCode);
}

int
MotherboardResponse::minimumSize(ResponseType type)
{
  constexpr auto preamble_size{ srvint::FieldPos::FPPacketHash + 1 };
  constexpr auto hash_size{ 1 };
  const auto payload_size{ [](auto type) {
    switch (type) {
      case ResponseType::SystemGoToBootloader:
        // no answer here actually
        return 0;
      case ResponseType::StatusPing:
      case ResponseType::SystemSoftReboot:
      case ResponseType::SystemHardReboot:
      case ResponseType::SystemResetError:
        return 1;
      case ResponseType::Unknown:
      case ResponseType::Empty:
      case ResponseType::DeviceSetId:
      case ResponseType::SystemGetError:
      case ResponseType::PowerSetCooler:
      case ResponseType::PowerSetLeds:
      case ResponseType::PowerSetUvLamp:
      case ResponseType::PowerSetBuzzer:
      case ResponseType::PowerSetShdn:
      case ResponseType::RgbSetOnOff:
      case ResponseType::PowerTurnOff:
      case ResponseType::PowerSetWdtValue:
        return 3;
      case ResponseType::DeviceGetFwRev:
      case ResponseType::SnsState:
        return 4;
      case ResponseType::GotoCmd:
      case ResponseType::GotoNTicks:
      case ResponseType::GotoAbsPos:
      case ResponseType::GotoZeroPos:
      case ResponseType::GotoEmStop:
      case ResponseType::GotoUncontrolled:
      case ResponseType::GotoSpeedSet:
      case ResponseType::GotoSmoothStop:
      case ResponseType::GotoUncontrolledWOpts:
      case ResponseType::GotoMixCycles:
      case ResponseType::GotoMixModeOff:
      case ResponseType::GotoWriteCurrents:
      case ResponseType::GotoMixTime:
      case ResponseType::EnginesSetParam:
      case ResponseType::EnginesSetMaxSpeed:
      case ResponseType::EnginesSetRepeatability:
      case ResponseType::EnginesSetMicrostep:
      case ResponseType::EnginesSetHomePosition:
      case ResponseType::EnginesSetF0:
      case ResponseType::EnginesSetFMax:
      case ResponseType::EnginesSetDfMax:
      case ResponseType::EnginesSetPosition:
      case ResponseType::EnginesEnable:
      case ResponseType::EnginesDriverRawWrite:
      case ResponseType::EnginesDriverFactoryDefaults:
      case ResponseType::VoltageGetBtns:
      case ResponseType::GerconGetState:
      case ResponseType::GerconClearTrigger:
      case ResponseType::OptDiagState:
      case ResponseType::OptDiagClearTrigger:
      case ResponseType::PwrBtnClearTrigger:
      case ResponseType::PwrBtnGetState:
        return 5;
      case ResponseType::EnginesGetRepeatability:
      case ResponseType::EnginesGetMicrostep:
        return 6;
      case ResponseType::DeviceGetId:
      case ResponseType::EnginesGetCurrents:
        return 7;
      case ResponseType::EnginesGetMaxSpeed:
        return 9;
      case ResponseType::VoltageGetRail:
      case ResponseType::VoltageGet24VRail:
      case ResponseType::VoltageGetUvLampRail:
      case ResponseType::VoltageGetR2RdacRail:
        return 11;
      case ResponseType::EnginesGetHomePosition:
      case ResponseType::EnginesGetF0:
      case ResponseType::EnginesGetFMax:
      case ResponseType::EnginesGetDfMax:
      case ResponseType::EnginesGetPosition:
        return 13;
      case ResponseType::EnginesGetMixModeState:
        return 14;
    }
  }(type) };
  return preamble_size + payload_size + hash_size;
}

MotherboardError::Error
MotherboardResponse::detectError() const
{
  // заглушка из-за возможного бага в прошивке -- иногда ответ на команду
  // ResetError приходил с поломанной структурой, хотя работало все корректно
  if (static_cast<srvint::Command>(m_packet->command()) ==
      srvint::Command::ResetError) {
    return MotherboardError::NoError;
  }

  const auto error_pos{ m_packet->payload().size() - 1 };
  const auto error{ static_cast<MotherboardError::Error>(
    m_packet->payload().at(error_pos)) };
  return error;
}

} // namespace protocol::motherboard
