#ifndef PROTOCOL_MOTHERBOARD_DEVICERESPONSE_H
#define PROTOCOL_MOTHERBOARD_DEVICERESPONSE_H

#include "motherboard/protocol/motherboard_response.h"

namespace protocol::motherboard {

struct DeviceGetIdResponse final : MotherboardResponse
{
  explicit DeviceGetIdResponse(SrvIntPacket& packet);
  uint32_t deviceId{ 0 };

private:
  enum Pos
  {
    IdMsb   = 2,
    IdMidb1 = 3,
    IdMidb0 = 4,
    IdLsb   = 5
  };
};

struct DeviceGetFWResponse final : MotherboardResponse
{
  explicit DeviceGetFWResponse(SrvIntPacket& packet);
  int fwRev{ 0 };

private:
  enum Pos
  {
    Op1 = 0,
    Op2 = 1,
    Fw  = 2
  };
};

} // namespace protocol::motherboard

#endif // PROTOCOL_MOTHERBOARD_DEVICERESPONSE_H
