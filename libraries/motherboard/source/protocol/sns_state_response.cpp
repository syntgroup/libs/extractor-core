#include "sns_state_response.h"

#include "srvint/srvint_packet.h"

protocol::motherboard::SnsStateResponse::SnsStateResponse(
  std::unique_ptr<srvint::SrvIntPacket>& packet)
  : MotherboardResponse{ packet }
{
  snsStates = getPacket()->payload().at(SnsState);
}
