#ifndef EXTRACTOR_TOUCHPANEL_SNS_STATE_REQUEST_H
#define EXTRACTOR_TOUCHPANEL_SNS_STATE_REQUEST_H

#include "motherboard/protocol/motherboard_cmd.h"
#include "serial/protocol/protocol_values.h"
#include "serial/protocol/request_interface.h"
#include "srvint/srvint_defs.h"

namespace protocol::motherboard {

struct SnsStateRequest final : protocol::RequestInterface
{
  explicit SnsStateRequest(PacketID pid_)
    : pid{ pid_ }
  {
  }
  const PacketID pid;
  const srvint::Payload payload{
    static_cast<std::underlying_type_t<GetParam>>(GetParam::Sns), // op1
    static_cast<std::underlying_type_t<SnsGet>>(SnsGet::All)      // op2
  };
};

} // namespace protocol::motherboard

#endif // EXTRACTOR_TOUCHPANEL_SNS_STATE_REQUEST_H
