#ifndef PROTOCOL_MOTHERBOARD_RGBENCODER_H
#define PROTOCOL_MOTHERBOARD_RGBENCODER_H

#include "motherboard/device/motherboard_values.h"
#include "qnamespace.h"
#include <vector>

namespace protocol::motherboard {

struct [[nodiscard]] RgbEncoder
{
  static auto encode(const std::vector<Qt::GlobalColor>& leds) -> uint32_t;
};

} // namespace protocol::motherboard

#endif // PROTOCOL_MOTHERBOARD_RGBENCODER_H
