#include "rgb_request.h"

#include "motherboard/protocol/motherboard_cmd.h"
#include "rgb_encoder.h"
#include "utils/byte_converter.h"

namespace protocol::motherboard {

RgbSetOnOffRequest::RgbSetOnOffRequest(
  PacketID pid,
  const std::vector<Qt::GlobalColor>& colors_)
  : pid{ pid }
  , payload{
    static_cast<srvint::PayloadType>(SetParam::RgbLeds), // op1
    static_cast<srvint::PayloadType>(RgbSet::OnOff)      // op2
  }
{
  const auto colors  = RgbEncoder::encode(colors_);
  const auto encoded = utils::type_to_bytes(colors);
  std::copy(encoded.cbegin(), encoded.cend(), std::back_inserter(payload));
}

} // namespace protocol::motherboard
