#include "motherboard/protocol/motherboard_decoder.h"

#include "buttons_response.h"
#include "device_response.h"
#include "engine_response.h"
#include "goto_response.h"
#include "motherboard/protocol/motherboard_response_type.h"
#include "power_response.h"
#include "sns_state_response.h"
#include "srvint/srvint_packet.h"
#include "srvint/srvint_response_factory.h"
#include "system_response.h"

namespace protocol::motherboard {

MotherboardDecoder::MotherboardDecoder()
  : factory{ std::make_unique<srvint::SrvIntResponseFactory>() }
{
}

MotherboardDecoder::~MotherboardDecoder() = default;

DecoderInterface::ResponseOrError
MotherboardDecoder::createResponse(const QByteArray& data, int type) const
{
  if (data.isEmpty()) {
    return nonstd::make_unexpected(CreateFailureReason::NotEnoughData);
  }

  auto packet{ factory->create(data) };

  if (!packet.has_value()) {
    switch (packet.error()) {
      case srvint::Error::NoError:
        break;
      case srvint::Error::NoData:
      case srvint::Error::NoExpectedPayload:
        return nonstd::make_unexpected(CreateFailureReason::NotEnoughData);
      default:
        return nonstd::make_unexpected(CreateFailureReason::GarbageData);
    }
  }

  // ?
  if (packet.value()->encode().size() <
      MotherboardResponse::minimumSize(static_cast<ResponseType>(type))) {
    return nonstd::make_unexpected(CreateFailureReason::NotEnoughData);
  }

  switch (static_cast<ResponseType>(type)) {
    case ResponseType::Unknown:
    case ResponseType::Empty:
      break;

    case ResponseType::DeviceGetId:
      return std::make_unique<DeviceGetIdResponse>(packet.value());

    case ResponseType::DeviceGetFwRev:
      return std::make_unique<DeviceGetFWResponse>(packet.value());

    case ResponseType::SystemGetError:
      return std::make_unique<GetErrorResponse>(packet.value());

    case ResponseType::GotoCmd:
    case ResponseType::GotoNTicks:
    case ResponseType::GotoAbsPos:
    case ResponseType::GotoZeroPos:
    case ResponseType::GotoEmStop:
    case ResponseType::GotoUncontrolled:
    case ResponseType::GotoSpeedSet:
    case ResponseType::GotoSmoothStop:
    case ResponseType::GotoUncontrolledWOpts:
    case ResponseType::GotoMixCycles:
    case ResponseType::GotoMixModeOff:
    case ResponseType::GotoWriteCurrents:
    case ResponseType::GotoMixTime:
      return std::make_unique<GotoResponse>(packet.value());

    case ResponseType::EnginesGetMaxSpeed:
      return std::make_unique<EnginesGetMaxSpeedResponse>(packet.value());

    case ResponseType::EnginesGetRepeatability:
      return std::make_unique<EnginesGetRepeatabilityResponse>(packet.value());

    case ResponseType::EnginesGetMicrostep:
      return std::make_unique<EnginesGetMicrostepResponse>(packet.value());

    case ResponseType::EnginesGetHomePosition:
      return std::make_unique<EnginesGetHomePositionResponse>(packet.value());

    case ResponseType::EnginesGetF0:
    case ResponseType::EnginesGetFMax:
    case ResponseType::EnginesGetDfMax:
      return std::make_unique<EnginesGetFResponse>(packet.value());

    case ResponseType::EnginesGetPosition:
      return std::make_unique<EnginesGetPositionResponse>(packet.value());

    case ResponseType::EnginesGetCurrents:
      return std::make_unique<EngineGetCurrentsResponse>(packet.value());

    case ResponseType::EnginesGetMixModeState:
      return std::make_unique<EngineGetMixModeStateResponse>(packet.value());

    case ResponseType::EnginesSetParam:
    case ResponseType::EnginesSetMaxSpeed:
    case ResponseType::EnginesSetRepeatability:
    case ResponseType::EnginesSetMicrostep:
    case ResponseType::EnginesSetHomePosition:
    case ResponseType::EnginesSetF0:
    case ResponseType::EnginesSetFMax:
    case ResponseType::EnginesSetDfMax:
    case ResponseType::EnginesSetPosition:
    case ResponseType::EnginesEnable:
    case ResponseType::EnginesDriverRawWrite:
    case ResponseType::EnginesDriverFactoryDefaults:
      return std::make_unique<EnginesSetParamResponse>(packet.value());

    case ResponseType::VoltageGetRail:
    case ResponseType::VoltageGet24VRail:
    case ResponseType::VoltageGetUvLampRail:
    case ResponseType::VoltageGetR2RdacRail:
      return std::make_unique<GetRailResponse>(packet.value());

    case ResponseType::VoltageGetBtns:
      return std::make_unique<GetBtnsResponse>(packet.value());

    case ResponseType::SnsState:
      return std::make_unique<SnsStateResponse>(packet.value());

    case ResponseType::GerconGetState:
    case ResponseType::GerconClearTrigger:
    case ResponseType::OptDiagState:
    case ResponseType::OptDiagClearTrigger:
    case ResponseType::PwrBtnGetState:
    case ResponseType::PwrBtnClearTrigger:
      return std::make_unique<ButtonsResponse>(packet.value());

    default:
      return std::make_unique<MotherboardResponse>(packet.value());
  }

  return nonstd::make_unexpected(CreateFailureReason::GarbageData);
}

} // namespace protocol::motherboard
