/*!
 * @file byte_converter.cpp
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 */

#include "byte_converter.h"

namespace utils {

uint32_t
byteArrayToUint32(const QByteArray& bytes)
{
  const auto count{ static_cast<uint32_t>(bytes.size()) };
  if (count == 0 || count > sizeof(uint32_t)) {
    return 0;
  }
  auto number{ 0U };
  for (auto i = 0U; i < count; ++i) {
    const auto pos{ static_cast<int32_t>(count - 1 - i) };
    const auto byte{ static_cast<uint8_t>(bytes.at(pos)) };
    number += static_cast<uint32_t>(byte << (8 * i));
  }
  return number;
}

} // namespace utils
