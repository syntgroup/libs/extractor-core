/*!
 * @file byte_converter.h
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 * @brief This file contains converters for Motherboard values
 */

#ifndef MOTHERBOARD_UTILS_BYTECONVERTER_H
#define MOTHERBOARD_UTILS_BYTECONVERTER_H

#include <QByteArray>
#include <QDataStream>
#include <cstdint>

namespace utils {

/*!
 * \brief converts QByteArray to unsingned long integer
 * \param bytes [QByteArray] - encoded int, typically from motherboard
 * \return [uint32_t] - decoded int
 * \deprecated
 * \details copied from  Burkhard Stubert's article Converting a QByteArray
 * into an Unsigned Integer
 * -
 * https://embeddeduse.com/2018/03/28/converting-a-qbytearray-into-an-unsigned-integer/
 * -
 * https://github.com/bstubert/embeddeduse/tree/master/BlogPosts/ByteArrayToUint32
 */
[[deprecated]] [[nodiscard]] static uint32_t
byteArrayToUint32(const QByteArray& bytes);

template<typename T>
[[nodiscard]] static auto
typeToByteArray(T data)
{
  QByteArray bytes;
  QDataStream stream{ &bytes, QIODevice::WriteOnly };
  stream.setByteOrder(QDataStream::BigEndian);
  stream.setVersion(QDataStream::Qt_5_12);

  stream << data;

  return bytes;
}

template<typename T>
[[nodiscard]] static auto
byteArrayToType(const QByteArray& bytes)
{
  QDataStream stream{ bytes };
  stream.setByteOrder(QDataStream::BigEndian);
  stream.setVersion(QDataStream::Qt_5_12);

  T result;
  stream >> result;

  return result;
}

template<
  typename ularge_type,
  typename ushort_type            = uint8_t,
  bool big_endian                 = true,
  typename std::enable_if_t<std::is_integral_v<ularge_type> &&
                              !std::is_floating_point_v<ularge_type> &&
                              std::is_integral_v<ushort_type> &&
                              std::numeric_limits<ularge_type>::digits %
                                  std::numeric_limits<ushort_type>::digits ==
                                0,
                            bool> = true>
[[nodiscard]] static constexpr auto
type_to_bytes(ularge_type value)
  -> std::array<ushort_type,
                std::numeric_limits<ularge_type>::digits /
                  std::numeric_limits<ushort_type>::digits>
{
  constexpr auto ushort_digits{ std::numeric_limits<ushort_type>::digits };
  constexpr auto ularge_digits{ std::numeric_limits<ularge_type>::digits };
  constexpr auto result_size{ ularge_digits / ushort_digits };

  std::array<ushort_type, result_size> result;

  for (std::size_t byte = 0; byte < result_size; ++byte) {
    result.at(byte) =
      static_cast<ushort_type>(value >> (byte * ushort_digits) & 0xff);
  }
  if (big_endian) {
    std::reverse(result.begin(), result.end());
  }
  return result;
}

} // namespace utils

#endif // MOTHERBOARD_UTILS_BYTECONVERTER_H
