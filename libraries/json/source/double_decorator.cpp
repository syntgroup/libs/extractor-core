/*!
 * @file double_decorator.cpp
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 * @brief This file contains json::DoubleDecorator class declaration
 */

#include "json/double_decorator.h"

namespace json {

class DoubleDecorator::Implementation
{
public:
  Implementation(double t_value, DoubleDecorator* double_decorator)
    : doubleDecorator{ double_decorator }
    , value{ t_value }
  {
  }

  DoubleDecorator* doubleDecorator{ nullptr };
  double value{ 0.0 };
};

DoubleDecorator::DoubleDecorator(const QString& key,
                                 const QString& label,
                                 double value,
                                 Entity* parent_entity)
  : DataDecorator{ key, label, parent_entity }
  , m_implementation{ std::make_unique<Implementation>(value, this) }
{
}

DoubleDecorator::~DoubleDecorator() = default;

DoubleDecorator&
DoubleDecorator::setValue(const double value)
{
  if (!qFuzzyCompare(value, m_implementation->value)) {
    m_implementation->value = value;
    emit valueChanged();
  }
  return *this;
}

double
DoubleDecorator::value() const
{
  return m_implementation->value;
}

QJsonValue
DoubleDecorator::jsonValue() const
{
  return QJsonValue::fromVariant(QVariant{ m_implementation->value });
}

void
DoubleDecorator::update(const QJsonObject& json_object)
{
  if (json_object.contains(key())) {
    setValue(json_object.value(key()).toDouble());
  } else {
    setValue(0.0);
  }
}

} // namespace json
