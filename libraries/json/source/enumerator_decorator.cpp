/*!
 * @file enumerator_decorator.cpp
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 * @brief This file contains json::EnumeratorDecorator class implementation
 */

#include "json/enumerator_decorator.h"

#include <QVariant>

namespace json {

class EnumeratorDecorator::Implementation
{
public:
  Implementation(int t_value,
                 std::unordered_map<int, QString>&& description_mapper,
                 EnumeratorDecorator* enumerator_decorator)
    : enumeratorDecorator{ enumerator_decorator }
    , value{ t_value }
    , descriptionMapper{ std::move(description_mapper) }
  {
  }

  EnumeratorDecorator* enumeratorDecorator{ nullptr };
  int value{ 0 };
  std::unordered_map<int, QString> descriptionMapper;
};

EnumeratorDecorator::EnumeratorDecorator(Entity* parent_entity)
  : DataDecorator{ QLatin1String{ "SomeItemKey" },
                   QLatin1String{},
                   parent_entity }
  , m_implementation{ std::make_unique<Implementation>(
      0,
      std::move(decltype(Implementation::descriptionMapper){}),
      this) }
{
}

EnumeratorDecorator::EnumeratorDecorator(
  const QString& key,
  const QString& label,
  int value,
  std::unordered_map<int, QString> description_mapper,
  Entity* parent_entity)
  : DataDecorator{ key, label, parent_entity }
  , m_implementation{
    std::make_unique<Implementation>(value, std::move(description_mapper), this)
  }
{
}

EnumeratorDecorator::~EnumeratorDecorator() = default;

EnumeratorDecorator&
EnumeratorDecorator::setValue(int value)
{
  if (value != m_implementation->value) {
    m_implementation->value = value;
    emit valueChanged();
  }
  return *this;
}

int
EnumeratorDecorator::value() const
{
  return m_implementation->value;
}

QString
EnumeratorDecorator::valueDescription() const
{
  if (m_implementation->descriptionMapper.find(m_implementation->value) !=
      m_implementation->descriptionMapper.end()) {
    return m_implementation->descriptionMapper.at(m_implementation->value);
  }

  return {};
}

QJsonValue
EnumeratorDecorator::jsonValue() const
{
  return QJsonValue::fromVariant(QVariant{ m_implementation->value });
}

void
EnumeratorDecorator::update(const QJsonObject& json_object)
{
  if (json_object.contains(key())) {
    const auto value_from_json{ json_object.value(key()).toInt() };
    setValue(value_from_json);
  } else {
    setValue(0);
  }
}

} // namespace json
