/*!
 * @file bool_decorator.cpp
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 * @brief This file contains json::DateTimeDecorator class implemntation
 */

#include "json/datetime_decorator.h"

#include <QVariant>

namespace json {

class DateTimeDecorator::Implementation
{
public:
  Implementation(const QDateTime& t_value,
                 DateTimeDecorator* date_time_decorator)
    : dateTimeDecorator{ date_time_decorator }
    , value{ t_value }
  {
  }

  DateTimeDecorator* dateTimeDecorator{ nullptr };
  QDateTime value;
};

DateTimeDecorator::DateTimeDecorator(const QString& key,
                                     const QString& label,
                                     const QDateTime& value,
                                     Entity* parent_entity)
  : DataDecorator{ key, label, parent_entity }
  , m_implementation{ std::make_unique<Implementation>(value, this) }
{
}

DateTimeDecorator::~DateTimeDecorator() = default;

DateTimeDecorator&
DateTimeDecorator::setValue(const QDateTime& date_time)
{
  if (date_time != m_implementation->value) {
    m_implementation->value = date_time;
    emit valueChanged();
  }
  return *this;
}

QDateTime
DateTimeDecorator::value() const
{
  return m_implementation->value;
}

QJsonValue
DateTimeDecorator::jsonValue() const
{
  return QJsonValue::fromVariant(
    QVariant{ m_implementation->value.toString(Qt::ISODate) });
}

void
DateTimeDecorator::update(const QJsonObject& json_object)
{
  if (json_object.contains(key())) {
    const auto value_as_string{ json_object.value(key()).toString() };
    const auto value_as_date{ QDateTime::fromString(value_as_string) };
    setValue(value_as_date);
  } else {
    setValue(QDateTime{});
  }
}

QString
DateTimeDecorator::toIso8601String() const
{
  if (m_implementation->value.isNull()) {
    return QLatin1String();
  }

  return m_implementation->value.toString(Qt::ISODate);
}

QString
DateTimeDecorator::toPrettyDateString() const
{
  if (m_implementation->value.isNull()) {
    return QStringLiteral("Not set");
  }

  static constexpr QStringView format{ u"dddd MMM yyyy @ HH:mm:ss" };
  return m_implementation->value.toString(format);
}

QString
DateTimeDecorator::toPrettyTimeString() const
{
  if (m_implementation->value.isNull()) {
    return QStringLiteral("not set");
  }

  static constexpr QStringView format{ u"d MMM yyyy" };
  return m_implementation->value.toString(format);
}

QString
DateTimeDecorator::toPrettyString() const
{
  if (m_implementation->value.isNull()) {
    return QStringLiteral("not set");
  }

  static constexpr QStringView format{ u"hh:mm ap" };
  return m_implementation->value.toString(format);
}

} // namespace json
