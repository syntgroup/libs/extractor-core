/*!
 * @file int_decorator.cpp
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 * @brief This file contains json::IntDecorator class implementation
 */

#include "json/int_decorator.h"

#include <QVariant>

namespace json {

struct IntDecorator::Implementation
{
  Implementation(int t_value, IntDecorator* int_decorator)
    : intDecorator{ int_decorator }
    , value{ t_value }
  {
  }

  IntDecorator* intDecorator;
  int value;
};

IntDecorator::IntDecorator(const QString& key,
                           const QString& label,
                           const int value,
                           Entity* parent_entity)
  : DataDecorator{ key, label, parent_entity }
  , m_implementation{ std::make_unique<Implementation>(value, this) }
{
}

IntDecorator::~IntDecorator() = default;

IntDecorator&
IntDecorator::setValue(const int value)
{
  if (value != m_implementation->value) {
    m_implementation->value = value;
    emit valueChanged();
  }
  return *this;
}

int
IntDecorator::value() const
{
  return m_implementation->value;
}

QJsonValue
IntDecorator::jsonValue() const
{
  return QJsonValue::fromVariant(QVariant{ m_implementation->value });
}

void
IntDecorator::update(const QJsonObject& json_object)
{
  if (json_object.contains(key())) {
    setValue(json_object.value(key()).toInt());
  } else {
    setValue(0);
  }
}

} // namespace json
