/*!
 * @file data_decorator.cpp
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 * @brief This file contains json::DataDecorator class implementation
 */

#include "json/data_decorator.h"

#include "json/entity.h"

namespace json {

class DataDecorator::Implementation
{
public:
  Implementation(const QString& t_key, const QString& t_label, Entity* parent)
    : parentEntity{ parent }
    , key{ t_key }
    , label{ t_label }
  {
  }

  Entity* parentEntity{ nullptr };
  QString key;
  QString label;
};

DataDecorator::DataDecorator(const QString& key,
                             const QString& label,
                             Entity* parent)
  : QObject{ qobject_cast<QObject*>(parent) }
  , m_implementation{ std::make_unique<Implementation>(key, label, parent) }
{
  setObjectName(key);
}

DataDecorator::~DataDecorator() = default;

QString
DataDecorator::key() const
{
  return m_implementation->key;
}

QString
DataDecorator::label() const
{
  return m_implementation->label;
}

Entity*
DataDecorator::parentEntity() const
{
  return m_implementation->parentEntity;
}

} // namespace json
