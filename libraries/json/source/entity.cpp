/*!
 * @file entity.cpp
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 * @brief This file contains json::Entity class implementation
 */

#include "json/entity.h"

#include "json/entity_collection.h"
#include "json/string_decorator.h"
#include <QUuid>

namespace json {

class Entity::Implementation
{
public:
  Implementation(const QString& t_key, Entity* t_entity)
    : entity{ t_entity }
    , key{ t_key }
    , id{ QUuid::createUuid().toString() }
  {
  }

  Entity* entity{ nullptr };
  QString key;
  QString id;
  StringDecorator* primaryKey{ nullptr };
  std::unordered_map<QString, EntityCollectionBase*> childCollections;
  std::unordered_map<QString, Entity*> childEntities;
  std::unordered_map<QString, DataDecorator*> dataDecorators;
};

Entity::Entity(const QString& key, QObject* parent)
  : QObject{ parent }
  , m_implementation{ std::make_unique<Implementation>(key, this) }
{
  setObjectName(key);
}

Entity::Entity(const QString& key,
               const QJsonObject& json_object,
               QObject* parent)
  : Entity{ key, parent }
{
  update(json_object);
}

Entity::~Entity() = default;

QString
Entity::id() const
{
  if (m_implementation->primaryKey != nullptr &&
      !m_implementation->primaryKey->value().isEmpty()) {
    return m_implementation->primaryKey->value();
  }

  return m_implementation->id;
}

QString
Entity::key() const
{
  return m_implementation->key;
}

void
Entity::setPrimaryKey(StringDecorator* primary_key)
{
  m_implementation->primaryKey = primary_key;
}

Entity*
Entity::addChild(const QString& key, Entity* entity)
{
  if (m_implementation->childEntities.find(key) ==
      m_implementation->childEntities.end()) {
    m_implementation->childEntities.emplace(key, entity);
    emit childEntitiesChanged();
  }
  return entity;
}

EntityCollectionBase*
Entity::addChildCollection(EntityCollectionBase* entity_collection)
{
  if (m_implementation->childCollections.find(entity_collection->getKey()) ==
      m_implementation->childCollections.end()) {
#if 0
    m_implementation->childCollections[entity_collection->getKey()] =
      entity_collection;
#else
    m_implementation->childCollections.emplace(entity_collection->getKey(),
                                               entity_collection);
#endif
    emit childCollectionsChanged(entity_collection->getKey());
  }

  return entity_collection;
}

DataDecorator*
Entity::addDataItem(DataDecorator* data_decorator)
{
  if (m_implementation->dataDecorators.find(data_decorator->key()) ==
      m_implementation->dataDecorators.end()) {
    m_implementation->dataDecorators.emplace(data_decorator->key(),
                                             data_decorator);
    emit dataDecoratorsChanged();
  }
  return data_decorator;
}

void
Entity::update(const QJsonObject& json_object)
{
  if (json_object.contains("id")) {
    const QUuid uuid{ json_object.value("id").toString() };
    m_implementation->id = uuid.toString();
  }

  // Update data decorators
  for (const auto& data_decorator_pair : m_implementation->dataDecorators) {
    data_decorator_pair.second->update(json_object);
  }

  // Update child entities
  for (const auto& child_entity_pair : m_implementation->childEntities) {
    child_entity_pair.second->update(
      json_object.value(child_entity_pair.first).toObject());
  }

  // Update child collections
  for (const auto& child_collection_pair : m_implementation->childCollections) {
    child_collection_pair.second->update(
      json_object.value(child_collection_pair.first).toArray());
  }
}

QJsonObject
Entity::toJson() const
{
  QJsonObject result{ { QStringLiteral("id"), m_implementation->id } };

  // add data decorator
  for (const auto& [key, data] : m_implementation->dataDecorators) {
    result.insert(key, data->jsonValue());
  }

  // Add child entities
  for (const auto& [key, entity] : m_implementation->childEntities) {
    result.insert(key, entity->toJson());
  }

  // Add child collections
  for (const auto& [key, collection] : m_implementation->childCollections) {
    QJsonArray entity_array;
    for (auto* entity : collection->baseEntities()) {
      entity_array.append(entity->toJson());
    }
    result.insert(key, entity_array);
  }

  return result;
}

} // namespace json
