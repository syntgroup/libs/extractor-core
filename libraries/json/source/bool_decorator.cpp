/*!
 * @file bool_decorator.cpp
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 * @brief This file contains implementation of json::BoolDecorator class
 */

#include "json/bool_decorator.h"

#include <QVariant>

namespace json {

class BoolDecorator::Implementation
{
public:
  Implementation(bool t_value, BoolDecorator* bool_decorator)
    : boolDecorator{ bool_decorator }
    , value{ t_value }
  {
  }

  BoolDecorator* boolDecorator{ nullptr };
  bool value{ false };
};

BoolDecorator::BoolDecorator(const QString& key,
                             const QString& label,
                             bool value,
                             Entity* parent_entity)
  : DataDecorator{ key, label, parent_entity }
  , m_implementation{ std::make_unique<Implementation>(value, this) }
{
}

BoolDecorator::~BoolDecorator() = default;

BoolDecorator&
BoolDecorator::setValue(bool value)
{
  if (value != m_implementation->value) {
    m_implementation->value = value;
    emit valueChanged();
  }
  return *this;
}

bool
BoolDecorator::value() const
{
  return m_implementation->value;
}

QJsonValue
BoolDecorator::jsonValue() const
{
  return QJsonValue::fromVariant(QVariant{ m_implementation->value });
}

void
BoolDecorator::update(const QJsonObject& json_object)
{
  if (json_object.contains(key())) {
    setValue(json_object.value(key()).toBool());
  } else {
    setValue(false);
  }
}

} // namespace json
