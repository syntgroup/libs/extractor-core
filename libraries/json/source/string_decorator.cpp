/*!
 * @file string_decorator.cpp
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 * @brief This file contains json::StringDecorator class implementation
 */

#include "json/string_decorator.h"

#include <utility>

namespace json {

struct StringDecorator::Implementation
{
  Implementation(const QString& t_value, StringDecorator* string_decorator)
    : stringDecorator{ string_decorator }
    , value{ t_value }
  {
  }

  StringDecorator* stringDecorator{ nullptr };
  QString value;
};

StringDecorator::StringDecorator(const QString& key,
                                 const QString& label,
                                 const QString& value,
                                 Entity* parent_entity)
  : DataDecorator{ key, label, parent_entity }
  , m_implementation{ std::make_unique<Implementation>(value, this) }
{
}

StringDecorator::~StringDecorator() = default;

StringDecorator&
StringDecorator::setValue(const QString& value)
{
  if (value != m_implementation->value) {
    // validation here if required
    m_implementation->value = value;
    emit valueChanged();
  }
  return *this;
}

QString
StringDecorator::value() const
{
  return m_implementation->value;
}

QJsonValue
StringDecorator::jsonValue() const
{
  return m_implementation->value.isEmpty()
           ? QJsonValue{}
           : QJsonValue::fromVariant(m_implementation->value);
}

void
StringDecorator::update(const QJsonObject& json_object)
{
  if (json_object.contains(key())) {
    setValue(json_object.value(key()).toString());
  } else {
    setValue(QLatin1String());
  }
}

} // namespace json
