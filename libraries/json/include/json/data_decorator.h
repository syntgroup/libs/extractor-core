/*!
 * @file data_decorator.h
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 * @brief This file contains json::DataDecorator class declaration
 */

#ifndef DATADECORATOR_H
#define DATADECORATOR_H

#include "json_export.h"

#include <QJsonObject>
#include <QJsonValue>
#include <QObject>
#include <memory>
#include <unordered_map>

namespace json {

class Entity;

/*!
 * @class DataDecorator data_decorator.h "json/data_decorator.h"
 * @brief The DataDecorator class is used as base for other data types
 * decorators
 */
class CORE_JSON_EXPORT DataDecorator : public QObject
{
  Q_OBJECT
  Q_PROPERTY(QString ui_label READ label CONSTANT)

public:
  DataDecorator(const QString& key   = QStringLiteral("SomeItemKey"),
                const QString& label = QLatin1String{},
                Entity* parent       = nullptr);
  ~DataDecorator() override;

  [[nodiscard]] auto key() const -> QString;
  [[nodiscard]] auto label() const -> QString;
  [[nodiscard]] auto parentEntity() const -> Entity*;

  [[nodiscard]] virtual QJsonValue jsonValue() const  = 0;
  virtual void update(const QJsonObject& json_object) = 0;

private:
  class Implementation;
  const std::unique_ptr<Implementation> m_implementation;
};

} // namespace json

#endif // DATADECORATOR_H
