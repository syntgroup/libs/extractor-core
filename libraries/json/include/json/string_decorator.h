/*!
 * @file string_decorator.h
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 * @brief This file contains json::StringDecorator class declaration
 */

#ifndef STRINGDECORATOR_H
#define STRINGDECORATOR_H

#include "data_decorator.h"
#include <QString>

namespace json {

/*!
 * @class StringDecorator string_decorator.h "json/string_decorator.h"
 * @brief The StringDecorator class
 */
class CORE_JSON_EXPORT StringDecorator : public DataDecorator
{
  Q_OBJECT
  Q_PROPERTY(QString ui_value READ value WRITE setValue NOTIFY valueChanged)

public:
  explicit StringDecorator(const QString& key   = QStringLiteral("SomeItemKey"),
                           const QString& label = QLatin1String{},
                           const QString& value = QLatin1String{},
                           Entity* parent_entity = nullptr);
  ~StringDecorator() override;

  auto setValue(const QString& value) -> StringDecorator&;
  [[nodiscard]] auto value() const -> QString;

  [[nodiscard]] QJsonValue jsonValue() const override;
  void update(const QJsonObject& json_object) override;

signals:
  void valueChanged();

private:
  struct Implementation;
  std::unique_ptr<Implementation> m_implementation;
};

} // namespace json

#endif // STRINGDECORATOR_H
