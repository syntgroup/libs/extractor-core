/*!
 * @file enumerator_decorator.h
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 * @brief This file contains json::EnumeratorDecorator class declaration
 */

#ifndef ENUMERATORDECORATOR_H
#define ENUMERATORDECORATOR_H

#include "data_decorator.h"

namespace json {

/*!
 * @class EnumeratorDecorator enumerator_decorator.h
 * "json/enumerator_decorator.h"
 * @brief The EnumeratorDecorator class
 */
class CORE_JSON_EXPORT EnumeratorDecorator : public DataDecorator
{
  Q_OBJECT
  Q_PROPERTY(int ui_value READ value WRITE setValue NOTIFY valueChanged)
  Q_PROPERTY(
    QString ui_valueDescription READ valueDescription NOTIFY valueChanged)
public:
  EnumeratorDecorator(Entity* parent_entity = nullptr);
  EnumeratorDecorator(const QString& key,
                      const QString& label,
                      int value,
                      std::unordered_map<int, QString> description_mapper,
                      Entity* parent_entity = nullptr);
  ~EnumeratorDecorator() override;

  auto setValue(int value) -> EnumeratorDecorator&;
  [[nodiscard]] auto value() const -> int;
  [[nodiscard]] auto valueDescription() const -> QString;

  [[nodiscard]] QJsonValue jsonValue() const override;
  void update(const QJsonObject& json_object) override;

signals:
  void valueChanged();

private:
  class Implementation;
  std::unique_ptr<Implementation> m_implementation;
};

} // namespace json

#endif // ENUMERATORDECORATOR_H
