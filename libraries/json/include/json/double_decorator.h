/*!
 * @file double_decorator.h
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 * @brief This file contains json::DoubleDecorator class declaration
 */

#ifndef DATA_DOUBLEDECORATOR_H
#define DATA_DOUBLEDECORATOR_H

#include "data_decorator.h"

namespace json {

/*!
 * @class DoubleDecorator double_decorator.h "json/double_decorator.h"
 * @brief The DoubleDecorator class
 */
class CORE_JSON_EXPORT DoubleDecorator : public DataDecorator
{
  Q_OBJECT
  Q_PROPERTY(double ui_value READ value WRITE setValue NOTIFY valueChanged)
public:
  explicit DoubleDecorator(const QString& key   = QStringLiteral("SomeItemKey"),
                           const QString& label = QLatin1String{},
                           double value         = 0.0,
                           Entity* parent_entity = nullptr);
  ~DoubleDecorator() override;

  auto setValue(double value) -> DoubleDecorator&;
  [[nodiscard]] auto value() const -> double;

  [[nodiscard]] QJsonValue jsonValue() const override;
  void update(const QJsonObject& json_object) override;

signals:
  void valueChanged();

private:
  class Implementation;
  std::unique_ptr<Implementation> m_implementation;
};

} // namespace json

#endif // DATA_DOUBLEDECORATOR_H
