/*!
 * @file bool_decorator.h
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 * @brief This file contains json::BoolDecorator class declaration
 */

#ifndef DATA_BOOLDECORATOR_H
#define DATA_BOOLDECORATOR_H

#include "data_decorator.h"

namespace json {

/*!
 * @class BoolDecorator bool_decorator.h "json/bool_decorator.h"
 * @brief The BoolDecorator class
 */
class CORE_JSON_EXPORT BoolDecorator : public DataDecorator
{
  Q_OBJECT
public:
  /*!
   * @brief BoolDecorator
   * @param key
   * @param label
   * @param value
   * @param parent_entity
   */
  explicit BoolDecorator(const QString& key    = QStringLiteral("SomeItemKey"),
                         const QString& label  = QLatin1String{},
                         bool value            = false,
                         Entity* parent_entity = nullptr);
  ~BoolDecorator() override;

  auto setValue(bool value) -> BoolDecorator&;
  [[nodiscard]] auto value() const -> bool;

  [[nodiscard]] auto jsonValue() const -> QJsonValue override;
  void update(const QJsonObject& json_object) override;

signals:
  void valueChanged();

private:
  class Implementation;
  std::unique_ptr<Implementation> m_implementation;
};

} // namespace json

#endif // DATA_BOOLDECORATOR_H
