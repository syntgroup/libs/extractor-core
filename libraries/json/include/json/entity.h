/*!
 * @file entity.h
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 * @brief This file contains json::Entity class declaration
 */

#ifndef ENTITY_H
#define ENTITY_H

#include "json_export.h"
#include <QJsonObject>
#include <QObject>

namespace json {

class EntityCollectionBase;
class StringDecorator;
class DataDecorator;

namespace defaults {
const auto KEY_TITLE{ QStringLiteral("title") };
const auto KEY_TYPE{ QStringLiteral("type") };
const auto KEY_DESCRIPTION{ QStringLiteral("description") };
const auto KEY_IDX{ QStringLiteral("idx") };
} // namespace defaults

/*!
 * @class Entity entity.h "json/entity.h"
 * @brief The Entity class
 */
class CORE_JSON_EXPORT Entity : public QObject
{
  Q_OBJECT
public:
  /*!
   * @brief Entity
   * @param key
   * @param parent
   */
  explicit Entity(const QString& key = QStringLiteral("SomeEntityKey"),
                  QObject* parent    = nullptr);
  /*!
   * @brief Entity
   * @param key
   * @param json_object
   * @param parent
   */
  explicit Entity(const QString& key,
                  const QJsonObject& json_object,
                  QObject* parent);
  ~Entity() override;

  [[nodiscard]] auto id() const -> QString;
  [[nodiscard]] auto key() const -> QString;
  [[nodiscard]] auto toJson() const -> QJsonObject;
  void setPrimaryKey(StringDecorator* primary_key);
  void update(const QJsonObject& json_object);

signals:
  void childCollectionsChanged(const QString& collection_key);
  void childEntitiesChanged();
  void dataDecoratorsChanged();

protected:
  [[nodiscard]] auto addChild(const QString& key, Entity* entity) -> Entity*;
  [[nodiscard]] auto addChildCollection(EntityCollectionBase* entity_collection)
    -> EntityCollectionBase*;
  [[nodiscard]] auto addDataItem(DataDecorator* data_decorator)
    -> DataDecorator*;

private:
  class Implementation;
  const std::unique_ptr<Implementation> m_implementation;
};

} // namespace json

#endif // ENTITY_H
