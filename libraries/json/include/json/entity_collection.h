/*!
 * @file entity_collection.h
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 * @brief This file contains json::EntityCollection class declaration and
 * implementation
 */

#ifndef ENTITY_COLLECTION_H
#define ENTITY_COLLECTION_H

#include "json_export.h"
#include <QJsonArray>
#include <QJsonObject>
#include <QJsonValue>
#include <QObject>
#include <gsl/gsl-lite.hpp>
#include <utility>

namespace json {

class Entity;

class EntityCollectionObject : public QObject
{
  Q_OBJECT
public:
  using QObject::QObject;
  ~EntityCollectionObject() override = default;

signals:
  void collectionChanged();
};

class EntityCollectionBase : public EntityCollectionObject
{
  Q_OBJECT
public:
  explicit EntityCollectionBase(
    const QString& key = QStringLiteral("somecollectionKey"),
    QObject* parent    = nullptr)
    : EntityCollectionObject{ parent }
    , m_key{ key }
  {
  }
  ~EntityCollectionBase() override = default;

  [[nodiscard]] auto getKey() const { return m_key; }

  virtual void clear()                                                    = 0;
  virtual void update(const QJsonArray& json)                             = 0;
  [[nodiscard]] virtual auto baseEntities() const -> std::vector<Entity*> = 0;

  template<class T>
  [[nodiscard]] auto derivedEntities() const -> QList<T*>&;

  template<class T>
  auto addEntity(T* entity) -> T*;

private:
  const QString m_key;
};

/*!
 * @class EntityCollection entity_collection.h "json/entity_collection.h"
 * @brief Used to store a collection of decorators or other json::Entity
 * children
 */
template<typename T>
class CORE_JSON_EXPORT EntityCollection : public EntityCollectionBase
{
public:
  using EntityCollectionBase::EntityCollectionBase;
  ~EntityCollection() override = default;

  void clear() override
  {
    std::for_each(m_collection.begin(), m_collection.end(), [](auto* entity) {
      entity->deleteLater();
    });
    m_collection.clear();
  }

  void update(const QJsonArray& json_array) override
  {
    clear();
    for (const auto& json_value : json_array) {
      gsl::owner<T*> object{ new T{ json_value.toObject(), this } };
      addEntity(object);
    }
  }

  [[nodiscard]] auto baseEntities() const -> std::vector<Entity*> override
  {
    std::vector<Entity*> result;
    std::copy(
      m_collection.begin(), m_collection.end(), std::back_inserter(result));
    return result;
  }

  [[nodiscard]] auto derivedEntities() const { return m_collection; }

  [[nodiscard]] auto getEntity(std::size_t index)
  {
    return m_collection.at(index);
  }

  auto addEntity(gsl::not_null<T*> entity)
  {
    if (!m_collection.contains(entity)) {
      m_collection.append(entity);
      emit EntityCollectionObject::collectionChanged();
    }

    return entity;
  }

private:
  QList<T*> m_collection;
};

template<class T>
auto
EntityCollectionBase::derivedEntities() const -> QList<T*>&
{
  return qobject_cast<const EntityCollection<T>&>(*this).derivedEntities();
}

template<class T>
auto
EntityCollectionBase::addEntity(T* entity) -> T*
{
  return qobject_cast<const EntityCollection<T>&>(*this).addEntity(entity);
}

} // namespace json

#endif // ENTITY_COLLECTION_H
