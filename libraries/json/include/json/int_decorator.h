/*!
 * @file int_decorator.h
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 * @brief This file contains json::IntDecorator class declaration
 */

#ifndef INTDECORATOR_H
#define INTDECORATOR_H

#include "data_decorator.h"

namespace json {

/*!
 * @class IntDecorator int_decorator.h "json/int_decorator.h"
 * @brief The IntDecorator class
 */
class CORE_JSON_EXPORT IntDecorator : public DataDecorator
{
  Q_OBJECT
  Q_PROPERTY(int ui_value READ value WRITE setValue NOTIFY valueChanged)

public:
  explicit IntDecorator(const QString& key    = QStringLiteral("SomeItemKey"),
                        const QString& label  = QLatin1String{},
                        int value             = 0,
                        Entity* parent_entity = nullptr);
  ~IntDecorator() override;

  auto setValue(int value) -> IntDecorator&;
  [[nodiscard]] auto value() const -> int;

  [[nodiscard]] auto jsonValue() const -> QJsonValue override;
  void update(const QJsonObject& json_object) override;

signals:
  void valueChanged();

private:
  struct Implementation;
  std::unique_ptr<Implementation> m_implementation;
};

} // namespace json

#endif // INTDECORATOR_H
