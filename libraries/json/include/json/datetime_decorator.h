/*!
 * @file datetime_decorator.h
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 * @brief This file contains json::DateTimeDecorator class declaration
 */

#ifndef DATETIMEDECORATOR_H
#define DATETIMEDECORATOR_H

#include "data_decorator.h"
#include <QDateTime>

namespace json {

/*!
 * @class DateTimeDecorator datetime_decorator.h "json/datetime_decorator.h"
 * @brief The DateTimeDecorator class
 */
class CORE_JSON_EXPORT DateTimeDecorator : public DataDecorator
{
  Q_OBJECT
  Q_PROPERTY(QString ui_iso8601String READ toIso8601String NOTIFY valueChanged)
  Q_PROPERTY(
    QString ui_prettyDateString READ toPrettyDateString NOTIFY valueChanged)
  Q_PROPERTY(
    QString ui_prettyTimeString READ toPrettyTimeString NOTIFY valueChanged)
  Q_PROPERTY(QString ui_prettyString READ toPrettyString NOTIFY valueChanged)

public:
  explicit DateTimeDecorator(const QString& key = QStringLiteral("SomeItemKey"),
                             const QString& label       = QLatin1String{},
                             const QDateTime& date_time = {},
                             Entity* parent_entity      = nullptr);
  ~DateTimeDecorator() override;

  auto setValue(const QDateTime& date_time) -> DateTimeDecorator&;
  [[nodiscard]] auto value() const -> QDateTime;

  [[nodiscard]] QJsonValue jsonValue() const override;
  void update(const QJsonObject& json_object) override;

  [[nodiscard]] QString toIso8601String() const;
  [[nodiscard]] QString toPrettyDateString() const;
  [[nodiscard]] QString toPrettyTimeString() const;
  [[nodiscard]] QString toPrettyString() const;

signals:
  void valueChanged();

private:
  class Implementation;
  std::unique_ptr<Implementation> m_implementation;
};

} // namespace json

#endif // DATETIMEDECORATOR_H
