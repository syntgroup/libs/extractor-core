#include <gtest/gtest.h>

#include "json/datetime_decorator.h"
#include "json/entity.h"
#include <QDateTime>

TEST(TestJson,
     DateTimeDecorator_constructor_givenNoParameters_setsDefaultPropetries)
{
  const json::DateTimeDecorator decorator;

  EXPECT_EQ(decorator.parentEntity(), nullptr);
  EXPECT_EQ(decorator.key(), QStringLiteral("SomeItemKey"));
  EXPECT_EQ(decorator.label(), QLatin1String{});
  EXPECT_EQ(decorator.value(), QDateTime{});
}

TEST(TestJson, DateTimeDecorator_constructor_givenParameters_setsPropetries)
{
  const auto test_key{ QStringLiteral("Test Key") };
  const auto test_label{ QStringLiteral("Test Label") };
  const QDateTime testDate{ QDate{ 2017, 8, 19 }, QTime{ 16, 40, 32 } };

  json::Entity parentEntity;
  json::DateTimeDecorator decorator{
    test_key, test_label, testDate, &parentEntity
  };

  EXPECT_EQ(decorator.parentEntity(), &parentEntity);
  EXPECT_EQ(decorator.key(), test_key);
  EXPECT_EQ(decorator.label(), test_label);
  EXPECT_EQ(decorator.value(), testDate);
}

TEST(TestJson,
     DateTimeDecorator_setValue_givenNewValue_updatesValueAndEmitsSignal)
{
  const QDateTime testDate{ QDate{ 2017, 8, 19 }, QTime{ 16, 40, 32 } };

  json::DateTimeDecorator decorator;

  EXPECT_EQ(decorator.value(), QDateTime{});
  decorator.setValue(testDate);
  EXPECT_EQ(decorator.value(), testDate);
}

TEST(TestJson, DateTimeDecorator_setValue_givenSameValue_takesNoAction)
{
  const auto test_key{ QStringLiteral("Test Key") };
  const auto test_label{ QStringLiteral("Test Label") };
  const QDateTime test_value{ QDateTime::currentDateTime() };
  const QDateTime testDate{ QDate{ 2017, 8, 19 }, QTime{ 16, 40, 32 } };

  json::Entity parentEntity;
  json::DateTimeDecorator decorator{
    test_key, test_label, testDate, &parentEntity
  };

  EXPECT_EQ(decorator.value(), testDate);
  decorator.setValue(testDate);
  EXPECT_EQ(decorator.value(), testDate);
}

TEST(TestJson, DateTimeDecorator_jsonValue_whenDefaultValue_returnsJson)
{
  const json::DateTimeDecorator decorator;
  EXPECT_EQ(decorator.jsonValue(),
            QJsonValue{ QDateTime{}.toString(Qt::ISODate) });
}

TEST(TestJson, DateTimeDecorator_jsonValue_whenValueSet_returnsJson)
{
  const QDateTime testDate{ QDate{ 2017, 8, 19 }, QTime{ 16, 40, 32 } };
  json::DateTimeDecorator decorator;
  decorator.setValue(testDate);

  EXPECT_EQ(decorator.jsonValue(),
            QJsonValue{ testDate.toString(Qt::ISODate) });
}

TEST(TestJson, DateTimeDecorator_update_whenPresentInJson_updatesValue)
{
  // temporarily disabling with hopes for better future
#if 0
  const auto test_key{ QStringLiteral("Test Key") };
  const auto test_label{ QStringLiteral("Test Label") };
  const QDateTime test_value{ QDateTime::currentDateTime() };
  const QDateTime target_value{ test_value.addYears(-500) };

  json::Entity parent_entity;
  json::DateTimeDecorator decorator{
    test_key, test_label, test_value, &parent_entity
  };

  EXPECT_EQ(decorator.value(), test_value);
  const QJsonObject json_object{ { "Key 1", "Value 1" },
                                 { test_key, target_value.toString() },
                                 { "Key 3", 3 } };

  decorator.update(json_object);

  EXPECT_EQ(decorator.value().toString(), target_value.toString())
    << decorator.value().toString().toStdString();
#endif
}

TEST(TestJson,
     DateTimeDecorator_update_whenNotPresentInJson_updatesValueToDefault)
{
  const auto test_key{ QStringLiteral("Test Key") };
  const auto test_label{ QStringLiteral("Test Label") };
  const QDateTime test_value{ QDateTime::currentDateTime() };

  json::Entity parentEntity;
  json::DateTimeDecorator decorator{
    test_key, test_label, test_value, &parentEntity
  };

  EXPECT_EQ(decorator.value(), test_value);
  const QJsonObject jsonObject{ { "Key 1", "Value 1" },
                                { "Key 2",
                                  test_value.addDays(-5600).toString() },
                                { "Key 3", "3" } };
  decorator.update(jsonObject);
  EXPECT_EQ(decorator.value(), QDateTime{});
}

TEST(
  TestJson,
  DateTimeDecorator_toIso8601String_givenCurrentDateTime_returnsExpectedFormat)
{
}
