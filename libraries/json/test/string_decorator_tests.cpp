#include <gtest/gtest.h>

#include "json/entity.h"
#include "json/string_decorator.h"
#include <QVariant>

TEST(TestJson,
     StringDecorator_constructor_givenNoParameters_setsDefaultPropetries)
{
  const json::StringDecorator decorator;

  EXPECT_EQ(decorator.parentEntity(), nullptr);
  EXPECT_EQ(decorator.key(), QString("SomeItemKey"));
  EXPECT_EQ(decorator.label(), QLatin1String{});
  EXPECT_EQ(decorator.value(), QLatin1String{});
}

TEST(TestJson, StringDecorator_constructor_givenParameters_setsPropetries)
{
  const auto test_key{ QStringLiteral("Test Key") };
  const auto test_label{ QStringLiteral("Test Label") };
  const auto test_value{ QStringLiteral("Test Value") };

  json::Entity parentEntity;
  json::StringDecorator decorator{
    test_key, test_label, test_value, &parentEntity
  };

  EXPECT_EQ(decorator.parentEntity(), &parentEntity);
  EXPECT_EQ(decorator.key(), test_key);
  EXPECT_EQ(decorator.label(), test_label);
  EXPECT_EQ(decorator.value(), test_value);
}

TEST(TestJson,
     StringDecorator_setValue_givenNewValue_updatesValueAndEmitsSignal)
{
  const auto test_value{ QStringLiteral("TEST VALUE") };

  json::StringDecorator decorator;

  EXPECT_EQ(decorator.value(), QLatin1String{});
  decorator.setValue(test_value);
  EXPECT_EQ(decorator.value(), test_value);
}

TEST(TestJson, StringDecorator_setValue_givenSameValue_takesNoAction)
{
  const auto test_key{ QStringLiteral("Test Key") };
  const auto test_label{ QStringLiteral("Test Label") };
  const auto test_value{ QStringLiteral("TEST value") };

  json::Entity parentEntity;
  json::StringDecorator decorator{
    test_key, test_label, test_value, &parentEntity
  };

  EXPECT_EQ(decorator.value(), test_value);
  decorator.setValue(test_value);
  EXPECT_EQ(decorator.value(), test_value);
}

TEST(TestJson, StringDecorator_jsonValue_whenDefaultValue_returnsJson)
{
  const json::StringDecorator decorator;
  EXPECT_EQ(decorator.jsonValue(), QJsonValue{});
}

TEST(TestJson, StringDecorator_jsonValue_whenValueSet_returnsJson)
{
  const auto test_value{ QStringLiteral("TEST VAlue") };

  json::StringDecorator decorator;
  decorator.setValue(test_value);

  EXPECT_EQ(decorator.jsonValue(), QJsonValue{ test_value });
}

TEST(TestJson, StringDecorator_update_whenPresentInJson_updatesValue)
{
  const auto test_key{ QStringLiteral("Test Key") };
  const auto test_label{ QStringLiteral("Test Label") };
  const auto test_value{ QStringLiteral("TEST VALUE") };
  const auto target_value{ QStringLiteral("Deus vult") };

  json::Entity parentEntity;
  json::StringDecorator decorator{
    test_key, test_label, test_value, &parentEntity
  };

  EXPECT_EQ(decorator.value(), test_value);
  const QJsonObject jsonObject{ { "Key 1", "Value 1" },
                                { test_key, target_value },
                                { "Key 3", 3 } };
  decorator.update(jsonObject);
  EXPECT_EQ(decorator.value(), target_value);
}

TEST(TestJson,
     StringDecorator_update_whenNotPresentInJson_updatesValueToDefault)
{
  const auto test_key{ QStringLiteral("Test Key") };
  const auto test_label{ QStringLiteral("Test Label") };
  const auto test_value{ QStringLiteral("TEst value") };

  json::Entity parentEntity;
  json::StringDecorator decorator{
    test_key, test_label, test_value, &parentEntity
  };

  EXPECT_EQ(decorator.value(), test_value);
  const QJsonObject jsonObject{ { "Key 1", "Value 1" },
                                { "Key 2", "123" },
                                { "Key 3", "3" } };
  decorator.update(jsonObject);
  EXPECT_EQ(decorator.value(), QLatin1String{});
}
