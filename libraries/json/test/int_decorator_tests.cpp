#include <gtest/gtest.h>

#include "json/entity.h"
#include "json/int_decorator.h"
#include <QVariant>

TEST(TestJson, IntDecorator_constructor_givenNoParameters_setsDefaultPropetries)
{
  const json::IntDecorator decorator;

  EXPECT_EQ(decorator.parentEntity(), nullptr);
  EXPECT_EQ(decorator.key(), QStringLiteral("SomeItemKey"));
  EXPECT_EQ(decorator.label(), QLatin1String{});
  EXPECT_EQ(decorator.value(), 0);
}

TEST(TestJson, IntDecorator_constructor_givenParameters_setsPropetries)
{
  const auto test_key{ QStringLiteral("Test Key") };
  const auto test_label{ QStringLiteral("Test Label") };
  const auto test_value{ 99 };

  json::Entity parentEntity;
  json::IntDecorator decorator{
    test_key, test_label, test_value, &parentEntity
  };

  EXPECT_EQ(decorator.parentEntity(), &parentEntity);
  EXPECT_EQ(decorator.key(), test_key);
  EXPECT_EQ(decorator.label(), test_label);
  EXPECT_EQ(decorator.value(), test_value);
}

TEST(TestJson, IntDecorator_setValue_givenNewValue_updatesValueAndEmitsSignal)
{
  json::IntDecorator decorator;

  EXPECT_EQ(decorator.value(), 0);
  decorator.setValue(99);
  EXPECT_EQ(decorator.value(), 99);
}

TEST(TestJson, IntDecorator_setValue_givenSameValue_takesNoAction)
{
  const auto test_key{ QStringLiteral("Test Key") };
  const auto test_label{ QStringLiteral("Test Label") };
  const auto test_value{ 99 };

  json::Entity parentEntity;
  json::IntDecorator decorator{
    test_key, test_label, test_value, &parentEntity
  };
  EXPECT_EQ(decorator.value(), test_value);
  decorator.setValue(test_value);
  EXPECT_EQ(decorator.value(), test_value);
}

TEST(TestJson, IntDecorator_jsonValue_whenDefaultValue_returnsJson)
{
  json::IntDecorator decorator;
  EXPECT_EQ(decorator.jsonValue(), QJsonValue{ 0 });
}

TEST(TestJson, IntDecorator_jsonValue_whenValueSet_returnsJson)
{
  static constexpr auto test_value{ 99 };

  json::IntDecorator decorator;
  decorator.setValue(test_value);

  EXPECT_EQ(decorator.jsonValue(), QJsonValue{ test_value });
}

TEST(TestJson, IntDecorator_update_whenPresentInJson_updatesValue)
{
  const auto test_key{ QStringLiteral("Test Key") };
  const auto test_label{ QStringLiteral("Test Label") };
  static constexpr auto test_value{ 99 };

  json::Entity parentEntity;
  json::IntDecorator decorator{
    test_key, test_label, test_value, &parentEntity
  };

  EXPECT_EQ(decorator.value(), test_value);
  const QJsonObject jsonObject{ { "Key 1", "Value 1" },
                                { test_key, 123 },
                                { "Key 3", 3 } };
  decorator.update(jsonObject);
  EXPECT_EQ(decorator.value(), 123);
}

TEST(TestJson, IntDecorator_update_whenNotPresentInJson_updatesValueToDefault)
{
  const auto test_key{ QStringLiteral("Test Key") };
  const auto test_label{ QStringLiteral("Test Label") };
  static constexpr auto test_value{ 99 };

  json::Entity parentEntity;
  json::IntDecorator decorator{
    test_key, test_label, test_value, &parentEntity
  };

  EXPECT_EQ(decorator.value(), test_value);
  const QJsonObject jsonObject{ { "Key 1", "Vaue 1" },
                                { "Key 2", 123 },
                                { "Key 3", 3 } };
  decorator.update(jsonObject);
  EXPECT_EQ(decorator.value(), 0);
}
