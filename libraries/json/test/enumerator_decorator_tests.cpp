#include <gtest/gtest.h>

#include "json/entity.h"
#include "json/enumerator_decorator.h"
#include <QVariant>

namespace {
enum eTestEnum : int
{
  unknown = 0,
  value1,
  value2,
  value3
};

const std::unordered_map<int, QString> descriptionMapper{
  { eTestEnum::unknown, "" },
  { eTestEnum::value1, "Value 1" },
  { eTestEnum::value2, "Value 2" },
  { eTestEnum::value3, "Value 3" }
};
} // namespace

TEST(TestJson,
     EnumeratorDecorator_constructor_givenNoParameters_setsDefaultPropetries)
{
  const json::EnumeratorDecorator decorator;

  EXPECT_EQ(decorator.parentEntity(), nullptr);
  EXPECT_EQ(decorator.key(), QStringLiteral("SomeItemKey"));
  EXPECT_EQ(decorator.label(), QLatin1String{});
  EXPECT_EQ(decorator.value(), 0);
  EXPECT_EQ(decorator.valueDescription(), QLatin1String{});
}

TEST(TestJson, EnumeratorDecorator_constructor_givenParameters_setsPropetries)
{
  const auto test_key{ QStringLiteral("test key") };
  const auto test_label{ QStringLiteral("test label") };
  const auto test_value{ eTestEnum::value2 };

  json::Entity parentEntity;
  json::EnumeratorDecorator decorator{
    test_key, test_label, test_value, descriptionMapper, &parentEntity
  };

  EXPECT_EQ(decorator.parentEntity(), &parentEntity);
  EXPECT_EQ(decorator.key(), test_key);
  EXPECT_EQ(decorator.label(), test_label);
  EXPECT_EQ(decorator.value(), eTestEnum::value2);
  EXPECT_EQ(decorator.valueDescription(), "Value 2");
}

TEST(TestJson,
     EnumeratorDecorator_setValue_givenNewValue_updatesValueAndEmitsSignal)
{
}

TEST(TestJson, EnumeratorDecorator_setValue_givenSameValue_takesNoAction) {}

TEST(TestJson, EnumeratorDecorator_jsonValue_whenDefaultValue_returnsJson) {}

TEST(TestJson, EnumeratorDecorator_jsonValue_whenValueSet_returnsJson) {}

TEST(TestJson, EnumeratorDecorator_update_whenPresentInJson_updatesValue) {}

TEST(TestJson,
     EnumeratorDecorator_update_whenNotPresentInJson_updatesValueToDefault)
{
}
