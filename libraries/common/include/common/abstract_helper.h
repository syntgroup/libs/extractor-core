/*!
 * @file abstract_helper.h
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 */

#ifndef ABSTRACTOPERATIONHELPER_H
#define ABSTRACTOPERATIONHELPER_H

#include "common/backend_error.h"
#include "common/failable.h"

namespace helper {

/*!
 * @class AbstractHelper
 * @brief The AbstractHelper class
 */
class CORE_COMMON_EXPORT AbstractHelper
  : public QObject
  , public Failable<BackendError::ErrorType, QString>
{
  Q_OBJECT
  Q_DISABLE_COPY(AbstractHelper)

public:
  enum State
  {
    Ready = 0,
    Finished,
    User
  };

  explicit AbstractHelper(QObject* parent = nullptr);
  ~AbstractHelper() override = default;

  [[nodiscard]] auto state() const { return m_state; }

protected:
  void setState(int new_state) { m_state = new_state; }

private:
  int m_state{ Ready };
  static constexpr std::chrono::milliseconds default_timeout{ 0 };

signals:
  void finished();

protected slots:
  void finish();
  void finishWithError(BackendError::ErrorType error,
                       const QString& error_string = {});
  void advanceState(std::chrono::milliseconds timeout = default_timeout) const;

  virtual void nextStateLogic() = 0;
};

} // namespace helper

#endif // ABSTRACTOPERATIONHELPER_H
