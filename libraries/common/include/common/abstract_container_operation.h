/*!
 * @file abstract_container_operation.h
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 */

#ifndef CMD_ABSTRACTCONTAINEROPERATION_H
#define CMD_ABSTRACTCONTAINEROPERATION_H

#include "abstract_operation.h"

namespace cmd {

/*!
 * @class AbstractContainerOperation
 * @brief The AbstractContainerOperation class
 */
class CORE_COMMON_EXPORT AbstractContainerOperation : public AbstractOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(AbstractContainerOperation)

public:
  using AbstractOperation::AbstractOperation;
  ~AbstractContainerOperation() override = default;

  void start() override;
  [[nodiscard]] auto operationType() const -> OperationType final;
  [[nodiscard]] auto isFinished() const -> bool;

  [[nodiscard]] virtual auto operationCount() const -> int                  = 0;
  [[nodiscard]] virtual auto currentOperation() const -> AbstractOperation* = 0;

signals:
  void advanced();

protected slots:
  virtual void processBuffer()              = 0;
  virtual void onCurrentOperationFinished() = 0;
};

} // namespace cmd

#endif // CMD_ABSTRACTCONTAINEROPERATION_H
