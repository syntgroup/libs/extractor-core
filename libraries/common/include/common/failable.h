/*!
 * @file failable.h
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 */

#ifndef FAILABLE_H
#define FAILABLE_H

#include "common_export.h"
#include <QString>

/*!
 * @brief The Failable class
 * @details Is used to represent an entity that can fail during some operation
 *
 * @tparam T - error code, preferably enum
 * @tparam M - some sort of string (QString by default) to store additional
 * error information
 */
template<typename T, typename M = QString>
class CORE_COMMON_EXPORT Failable
{
public:
  using Error   = T;
  using Message = M;

  Failable()          = default;
  virtual ~Failable() = default;

  [[nodiscard]] auto error() const noexcept { return _error; }
  [[nodiscard]] auto isError() const noexcept { return _error != no_error; }
  [[nodiscard]] auto errorString() const noexcept { return _message; }

protected:
  void setError(Error error) { _error = error; }
  void setError(Error error, const Message& message)
  {
    _error   = error;
    _message = message;
  }

  void clearError()
  {
    _error   = no_error;
    _message = Message{};
  }

private:
  constexpr static T no_error = T{};

  Error _error     = {};
  Message _message = {};
};

#endif // FAILABLE_H
