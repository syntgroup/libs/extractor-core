/*!
 * @file abstract_meta_operation.h
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 */

#ifndef ABSTRACT_META_OPERATION_H
#define ABSTRACT_META_OPERATION_H

#include "abstract_operation.h"

namespace cmd {

class CORE_COMMON_EXPORT AbstractMetaOperation : public AbstractOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(AbstractMetaOperation)

public:
  using AbstractOperation::AbstractOperation;
  ~AbstractMetaOperation() override = default;

  [[nodiscard]] auto operationType() const -> OperationType final
  {
    return OperationType::Meta;
  }
};

} // namespace cmd

#endif // ABSTRACT_META_OPERATION_H
