#ifndef BUTTON_POWER_H
#define BUTTON_POWER_H

#include "common_export.h"
#include <QObject>

namespace device {

class CORE_COMMON_EXPORT DiscreteOutput : public QObject
{
  Q_OBJECT
  Q_DISABLE_COPY(DiscreteOutput)
  Q_PROPERTY(bool state READ state WRITE setState RESET resetState NOTIFY
               stateChanged FINAL)
  Q_PROPERTY(bool trigger READ trigger WRITE setTrigger RESET resetTrigger
               NOTIFY triggerChanged FINAL)

public:
  explicit DiscreteOutput(bool default_state = false, QObject* parent = nullptr)
    : QObject{ parent }
    , _defaultState{ default_state }
    , _state{ default_state }
  {
  }
  ~DiscreteOutput() override       = default;
  DiscreteOutput(DiscreteOutput&&) = delete;
  auto operator=(DiscreteOutput&&) = delete;

  [[nodiscard]] auto state() const { return _state; }
  [[nodiscard]] auto trigger() const { return _trigger; }

  void setState(bool new_state, bool notify = true)
  {
    _state = new_state;
    if (notify) {
      emit stateChanged(_state);
    }
  }

  void setTrigger(bool new_trigger, bool notify = true)
  {
    _trigger = new_trigger;
    if (notify) {
      emit triggerChanged(_trigger);
    }
  }

  void resetState(bool notify = false)
  {
    _state = _defaultState;
    if (notify) {
      emit stateChanged(_state);
    }
  }
  void resetTrigger(bool notify = false)
  {
    _trigger = false;
    if (notify) {
      emit triggerChanged(_trigger);
    }
  }

private:
  const bool _defaultState = false;
  bool _state              = false;
  bool _trigger            = false;

signals:
  void stateChanged(bool state);
  void triggerChanged(bool trigger);
};

} // namespace device

#endif // BUTTON_POWER_H
