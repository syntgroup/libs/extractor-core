#ifndef ANALOG_INPUT_H
#define ANALOG_INPUT_H

namespace device {

template<typename T>
class AnalogInput
{
public:
  AnalogInput(T value = zero)
    : _value{ value }
  {
  }
  virtual ~AnalogInput() = default;

  [[nodiscard]] auto value() const { return _value; }
  virtual void setValue(T value) { _value = value; }

protected:
  T _value;
  static constexpr T zero{};
};

class DiscreteInput
{
public:
  virtual ~DiscreteInput() = default;

  [[nodiscard]] auto state() const { return _state; }
  virtual void setState(bool state) { _state = state; };

protected:
  bool _state = false;
};

} // namespace device

#endif // ANALOG_INPUT_H
