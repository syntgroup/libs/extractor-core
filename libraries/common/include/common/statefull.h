/*!
 * @file statefull.h
 * @brief Contains Statefull<T> class declaration + implementation
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 */

#ifndef CORE_FSM_H
#define CORE_FSM_H

#include "common_export.h"
#include <type_traits>

/*!
 * @brief The Statefull class
 * @details Slim version of helper::AbstractHelper. Statefull is a class that
 * holds state value (defined as enum) that can be accessed or changed.
 * Is used for classes that manages transitions between states themselves.
 * @tparam T - state type, preferably enum
 */
template<typename T, std::enable_if_t<std::is_enum_v<T>, bool> = true>
class CORE_COMMON_EXPORT Statefull
{
public:
  using State = T;
  Statefull(State state)
    : _state{ state }
  {
  }

  virtual ~Statefull() = default;

  [[nodiscard]] auto state() const { return _state; }
  void setState(State new_state) { _state = new_state; }

private:
  State _state;
};

#endif // CORE_FSM_H
