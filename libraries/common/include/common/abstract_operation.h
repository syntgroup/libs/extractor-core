/*!
 * @file abstract_operation.h
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 */

#ifndef ABSTRACTOPERATION_H
#define ABSTRACTOPERATION_H

#include "common/backend_error.h"
#include "common/failable.h"
#include <QTimer>
#include <chrono>
#include <memory>

namespace cmd {

/*!
 * @class AbstractOperation
 * @brief The AbstractOperation class
 */
class CORE_COMMON_EXPORT AbstractOperation
  : public QObject
  , public Failable<BackendError::ErrorType, QString>
{
  Q_OBJECT
  Q_DISABLE_COPY(AbstractOperation)

public:
  enum BasicOperationState
  {
    Ready    = 0,
    Finished = 1,
    User     = 2
  };
  enum class OperationType
  {
    Container,
    Motherboard,
    Thermocontrollers,
    Meta,
    MultiStage
  };

  explicit AbstractOperation(QObject* parent = nullptr);
  ~AbstractOperation() override;
  AbstractOperation(AbstractOperation&&) = delete;
  auto operator=(AbstractOperation&&)    = delete;

  [[nodiscard]] virtual auto description() const -> QString         = 0;
  [[nodiscard]] virtual auto operationType() const -> OperationType = 0;
  [[nodiscard]] auto operationState() const { return m_operationState; }
  [[nodiscard]] auto progress() const { return m_progress; }

  virtual void start() = 0;
  virtual void finish();

protected:
  void setOperationState(int state);
  void setProgress(double new_progress);
  void setTimeout(std::chrono::milliseconds msec);

  void finishWithError(BackendError::ErrorType error,
                       const QString& error_string);

private:
  QTimer m_timeoutTimer;
  int m_operationState{ Ready };
  double m_progress{ 0.0 };

signals:
  void started();
  void finished();
  void progressChanged();

protected slots:
  virtual void onOperationTimeout();

  void startTimeout();
  void stopTimeout();
};

} // namespace cmd

#endif // ABSTRACTOPERATION_H
