#include <gtest/gtest.h>

#include "common/backend_error.h"
#include "common/failable.h"
#include <QCoreApplication>
#include <QDir>
#include <QMap>
#include <QMetaEnum>

class Item final : public Failable<BackendError::ErrorType>
{
  using Base = Failable<BackendError::ErrorType>;

public:
  void setError(Error error) { Base::setError(error); }
  void setError(Error error, const Message& message)
  {
    Base::setError(error, message);
  }
  void clearError() { Base::clearError(); }
};

TEST(TestCommon, FailableInitialize)
{
  const Item item{};
  EXPECT_EQ(item.error(), BackendError::NoError);
  EXPECT_TRUE(item.errorString().isNull());
  EXPECT_FALSE(item.isError());
}

TEST(TestCommon, FailableSetError)
{
  Item item;

  for (int error = BackendError::NoError; error <= BackendError::TimeoutError;
       ++error) {
    item.setError(static_cast<BackendError::ErrorType>(error));
    EXPECT_EQ(item.error(), error);

    if (error == BackendError::NoError) {
      EXPECT_FALSE(item.isError());
    } else {
      EXPECT_TRUE(item.isError());
    }
  }
}

TEST(TestCommon, FailableSetErrorWithMessage)
{
  std::unordered_map<BackendError::ErrorType, QString> subjects{
    { BackendError::NoError, QStringLiteral("no error") },
    { BackendError::UnknownError, QStringLiteral("have no idea wha happened") },
    { BackendError::BackupError, QStringLiteral("error while perform backup") },
    { BackendError::SerialAccessError, QStringLiteral("error serial access") }
  };

  Item failable;
  for (auto&& subject : subjects) {
    const auto& [key, value] = subject;
    failable.setError(key, value);
    EXPECT_EQ(failable.error(), key);
    EXPECT_EQ(failable.errorString(), value);

    if (key == BackendError::NoError) {
      EXPECT_FALSE(failable.isError());
    } else {
      EXPECT_TRUE(failable.isError());
    }
  }
}

#if 0
TEST(TestCommon, FailableSetErrorMessage)
{
  const QString no_error{ QStringLiteral("No Error") };
  const QStringList messages{ QStringLiteral("unknown problem in backend"),
                              QStringLiteral("unknown problem in frontend"),
                              QStringLiteral("unknown problem in core") };
  Item item;
  EXPECT_EQ(item.error(), BackendError::NoError);
  EXPECT_EQ(item.errorString(), no_error);
  EXPECT_FALSE(item.isError());

  for (const auto& message : qAsConst(messages)) {
    item.setErrorString(message);
    EXPECT_TRUE(item.isError());
    EXPECT_EQ(item.error(), BackendError::UnknownError);
    EXPECT_EQ(item.errorString(), message);
  }
}

TEST(TestCommon, FailableClearError)
{
  const auto message{ QStringLiteral("some stuff happened") };
  const auto no_error{ QStringLiteral("No Error") };

  Item item;
  item.setErrorString(message);
  EXPECT_EQ(item.error(), BackendError::UnknownError);
  EXPECT_EQ(item.errorString(), message);
  EXPECT_TRUE(item.isError());

  item.clearError();
  EXPECT_EQ(item.error(), BackendError::NoError);
  EXPECT_EQ(item.errorString(), no_error);
  EXPECT_FALSE(item.isError());
}
#endif
