/*!
 * @file qstring_support.hpp
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 * @brief Support functions to print QString directly to ostream
 */

#ifndef MOTHERBOARD_TEST_QSTRINGSUPPORT_HPP
#define MOTHERBOARD_TEST_QSTRINGSUPPORT_HPP

#include <QString>
#include <iostream>

inline auto
operator<<(std::ostream& stream, const QString& str) -> std::ostream&
{
  stream << str.toStdString();
  return stream;
}

#endif // MOTHERBOARD_TEST_QSTRINGSUPPORT_HPP