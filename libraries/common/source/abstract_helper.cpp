/*!
 * @file abstract_helper.cpp
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 */

#include "common/abstract_helper.h"

#include <QTimer>

namespace helper {

AbstractHelper::AbstractHelper(QObject* parent)
  : QObject{ parent }
{
  advanceState();
}

void
AbstractHelper::finish()
{
  setState(State::Finished);
  emit finished();
}

void
AbstractHelper::finishWithError(BackendError::ErrorType error,
                                const QString& error_string)
{
  setError(error, error_string);
  finish();
}

void
AbstractHelper::advanceState(std::chrono::milliseconds timeout) const
{
  QTimer::singleShot(timeout, this, &AbstractHelper::nextStateLogic);
}

} // namespace helper
