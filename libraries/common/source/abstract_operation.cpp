/*!
 * @file abstract_operation.cpp
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 */

#include "common/abstract_operation.h"

namespace {
constexpr std::chrono::milliseconds TIMEOUT{ 100 };
} // namespace

namespace cmd {

AbstractOperation::AbstractOperation(QObject* parent)
  : QObject{ parent }
{
  connect(&m_timeoutTimer,
          &QTimer::timeout,
          this,
          &AbstractOperation::onOperationTimeout);

  m_timeoutTimer.setSingleShot(true);
  m_timeoutTimer.setInterval(TIMEOUT);
}

AbstractOperation::~AbstractOperation() = default;

void
AbstractOperation::finish()
{
  stopTimeout();
  setProgress(100);
  setOperationState(AbstractOperation::Finished);
  emit finished();
}

void
AbstractOperation::setTimeout(std::chrono::milliseconds msec)
{
  m_timeoutTimer.setInterval(msec);
}

void
AbstractOperation::onOperationTimeout()
{
  finishWithError(BackendError::TimeoutError,
                  QStringLiteral("operation timeout (generic)"));
}

void
AbstractOperation::startTimeout()
{
  m_timeoutTimer.start();
}

void
AbstractOperation::stopTimeout()
{
  m_timeoutTimer.stop();
}

void
AbstractOperation::setOperationState(int state)
{
  m_operationState = state;
}

void
AbstractOperation::finishWithError(BackendError::ErrorType error,
                                   const QString& error_string)
{
  setError(error, error_string);
  finish();
}

void
AbstractOperation::setProgress(double new_progress)
{
  if (qFuzzyCompare(m_progress, new_progress)) {
    return;
  }

  m_progress = new_progress;
  emit progressChanged();
}

} // namespace cmd
