/*!
 * @file abstract_container_operation.cpp
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 */

#include "common/abstract_container_operation.h"

namespace cmd {

AbstractOperation::OperationType
AbstractContainerOperation::operationType() const
{
  return OperationType::Container;
}

bool
AbstractContainerOperation::isFinished() const
{
  return operationState() == AbstractOperation::Finished;
}

void
AbstractContainerOperation::start()
{
  if (operationState() != Ready) {
    finishWithError(BackendError::UnknownError, QLatin1String{});
  } else {
    processBuffer();
  }
}

} // namespace cmd
