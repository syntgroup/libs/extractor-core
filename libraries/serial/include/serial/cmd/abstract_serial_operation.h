#ifndef CMD_ABSTRACTDEVICEOPERATION_H
#define CMD_ABSTRACTDEVICEOPERATION_H

#include "common/abstract_operation.h"
#include "serial/protocol/protocol_values.h"
#include "serial_export.h"

namespace protocol {
struct EncoderInterface;
struct RequestInterface;
struct ResponseInterface;
} // namespace protocol

namespace cmd {

class CORE_SERIAL_EXPORT AbstractSerialOperation : public AbstractOperation
{
  Q_OBJECT

  enum DeviceOperationState
  {
    Started = AbstractOperation::User
  };

public:
  using Request  = std::unique_ptr<protocol::RequestInterface>;
  using Response = std::unique_ptr<protocol::ResponseInterface>;

  explicit AbstractSerialOperation(protocol::PacketID idx,
                                   QObject* parent = nullptr);
  ~AbstractSerialOperation() override;

  [[nodiscard]] auto id() const { return m_idx; }
  [[nodiscard]] auto isFinished() const -> bool;

  [[nodiscard]] virtual auto responseType() const -> int = 0;

  void start() final;
  void finishLater();
  void abort(const QString& reason);

  virtual void feedResponse(const Response& response);
  [[nodiscard]] virtual auto createRequest() const -> Request = 0;

private:
  //! possible data preparation
  virtual auto begin() -> bool { return true; }
  const protocol::PacketID m_idx{ 0 };

protected:
  //! @return false if error occured while processing
  virtual auto processResponse(const Response& response) -> bool;
};

} // namespace cmd

#endif // CMD_ABSTRACTDEVICEOPERATION_H
