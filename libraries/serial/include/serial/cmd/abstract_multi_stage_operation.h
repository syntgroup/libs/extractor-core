#ifndef CMD_ABSTRACTMULTISTAGEOPERATION_H
#define CMD_ABSTRACTMULTISTAGEOPERATION_H

#include "abstract_serial_operation.h"
#include "serial/utils/counter.h"
#include <memory>

namespace cmd {

namespace execution {
struct DefaultPolicy
{
  static constexpr auto allow_retry() { return std::false_type{}; }
  static constexpr auto allow_continue() { return std::true_type{}; }
};

struct DeadPolicy
{
  static constexpr auto allow_retry() { return std::false_type{}; }
  static constexpr auto allow_continue() { return std::false_type{}; }
};
} // namespace execution

class CORE_SERIAL_EXPORT AbstractMultiStageOperation : public AbstractOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(AbstractMultiStageOperation)

public:
  using AbstractOperation::AbstractOperation;
  ~AbstractMultiStageOperation() override = default;

  void start() override;
  [[nodiscard]] auto operationType() const
    -> cmd::AbstractOperation::OperationType final;

  [[nodiscard]] auto isFinished() const -> bool;
  [[nodiscard]] auto isStarted() const -> bool;
  [[nodiscard]] auto currentOperation() const { return m_currentOperation; };

protected:
  template<class TOperation, typename... Args>
  void registerSubOperation(Args&&... args)
  {
    auto operation     = std::make_unique<TOperation>(utils::Counter::tick(),
                                                  std::forward<Args>(args)...);
    m_currentOperation = operation.release();
    registerOperation();
  }

  template<class TOperation,
           class ExecutionPolicy,
           typename... Args,
           typename std::enable_if_t<
             std::is_same_v<ExecutionPolicy, execution::DeadPolicy>,
             bool> = true>
  void registerSubOperation(Args&&... args)
  {
    auto operation     = std::make_unique<TOperation>(utils::Counter::tick(),
                                                  std::forward<Args>(args)...);
    m_currentOperation = operation.release();
    registerDeadOperation();
  }

  void advanceOperationState();

  virtual void onSubOperationError(
    const AbstractSerialOperation& operation) = 0;

  template<typename E>
  [[nodiscard]] static auto stateToString(int value)
  {
    switch (value) {
      case AbstractOperation::Ready:
        return QStringLiteral("Ready");
        break;
      case AbstractOperation::Finished:
        return QStringLiteral("Finished");
        break;
      default:
        return QString::fromLocal8Bit(
          QMetaEnum::fromType<E>().valueToKey(value));
        break;
    }
  }

private:
  cmd::AbstractSerialOperation* m_currentOperation{ nullptr };

  void registerOperation();
  void registerDeadOperation();

signals:
  void stepAdvanced();
  void stepRetry();

private slots:
  virtual void nextStateLogic() = 0;

public slots:
  virtual void pause()  = 0;
  virtual void resume() = 0;
};

} // namespace cmd

#endif // CMD_ABSTRACTMULTISTAGEOPERATION_H
