#ifndef CORE_TRANSFER_ABSRTACT_EXECUTOR_H
#define CORE_TRANSFER_ABSRTACT_EXECUTOR_H

#include "serial_export.h"
#include <gsl/gsl-lite.hpp>

namespace cmd {
class AbstractSerialOperation;
class AbstractMultiStageOperation;
} // namespace cmd

namespace transfer {

class AbstractSession;

class CORE_SERIAL_EXPORT AbstractExecutor
{
public:
  virtual ~AbstractExecutor() = default;

protected:
  virtual void registerSingleOperation(
    gsl::not_null<cmd::AbstractSerialOperation*> operation) const = 0;
  virtual void registerMultiOperations(
    gsl::not_null<cmd::AbstractMultiStageOperation*> operation) const = 0;
};

} // namespace transfer

#endif // CORE_TRANSFER_ABSRTACT_EXECUTOR_H
