/*!
 * @file i_session.h
 * @brief Contains declaration of ISession interface class
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 */

#ifndef CORE_SERIAL_I_SESSION_H
#define CORE_SERIAL_I_SESSION_H

#include "serial_export.h"
#include <QByteArray>
#include <chrono>
#include <gsl/gsl-lite.hpp>

class QSerialPort;

namespace cmd {
class AbstractSerialOperation;
} // namespace cmd

namespace protocol {
class ResponseInterface;
} // namespace protocol

namespace transfer {

/*!
 * @interface ISession i_session.h serial/transfer/i_session.h
 * @brief The ISession class
 * @details Managing serial communications, specific for given command protocol.
 * - Holds QSerialPort
 * - holds support encoder/decoder protocol structs
 * - creates tx bytes from SerialOperations and sends them to port
 * - recieves rx bytes from port and process hands them over to SerialOperation
 * for processing
 * Should only have read/write _view_ to operations objects, should not manage
 * their lifetime.
 * Should not have any direct access to device objects.
 */
class CORE_SERIAL_EXPORT ISession
{
public:
  virtual ~ISession() = default;

  using Operation = gsl::not_null<cmd::AbstractSerialOperation*>;
  virtual void enqueueOperation(Operation) = 0;

  using Port = gsl::owner<QSerialPort*>;

  virtual void start(Port) = 0;
  virtual void stop()      = 0;

protected:
  virtual auto processDelay() const -> std::chrono::milliseconds = 0;

  virtual auto currentOperation() const -> Operation = 0;
  virtual void clearOperationQueue()                 = 0;

  virtual auto prepareRequest() const -> QByteArray = 0;
  virtual void processResponse(const QByteArray&)   = 0;

  using Response = std::unique_ptr<protocol::ResponseInterface>;
  virtual void processMatchedResponse(const Response&)   = 0;
  virtual void processBroadcastResponse(const Response&) = 0;
  virtual void processUnmatchedResponse(const Response&) = 0;
  virtual void processErrorResponse(const Response&)     = 0;

  virtual void writeToPort()   = 0;
  virtual void processQueue()  = 0;
  virtual void doStopSession() = 0;

  virtual void onCurrentOperationFinished()       = 0;
  virtual void onSerialPortReadyRead()            = 0;
  virtual void onSerialPortErrorOccured()         = 0;
  virtual void onSerialPortBytesWritten(uint64_t) = 0;
};

} // namespace transfer

#endif // CORE_SERIAL_I_SESSION_H
