/*!
 * @file i_queue.h
 * @brief Contains declaration of IProtocolQueue interface class
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 */

#ifndef CORE_SERIAL_I_QUEUE_H
#define CORE_SERIAL_I_QUEUE_H

#include "serial_export.h"

namespace transfer {

/*!
 * @interface IProtocolQueue i_queue serial/transfer/i_queue.h
 * @brief The IProtocolQueue class
 */
class CORE_SERIAL_EXPORT IProtocolQueue
{
public:
  virtual ~IProtocolQueue() = default;

  virtual auto operationsLeft() const -> int = 0;

  virtual void startQueue()  = 0;
  virtual void stopQueue()   = 0;
  virtual void pauseQueue()  = 0;
  virtual void resumeQueue() = 0;

  virtual void clearOperationQueue() = 0;

protected:
  virtual void processQueue()               = 0;
  virtual void processCurrentBlock()        = 0;
  virtual void onCurrentBlockAdvanced()     = 0;
  virtual void onCurrentBlockFinished()     = 0;
  virtual void onCurrentBlockErrorOccured() = 0;

  virtual void doStopQueue() = 0;
};

} // namespace transfer

#endif // CORE_SERIAL_I_QUEUE_H
