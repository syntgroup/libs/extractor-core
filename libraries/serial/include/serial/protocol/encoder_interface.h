#ifndef CMD_ENCODERINTERFACE_H
#define CMD_ENCODERINTERFACE_H

#include <QByteArray>
#include <memory>

namespace protocol {

struct RequestInterface;

struct EncoderInterface
{
  EncoderInterface()                                               = default;
  virtual ~EncoderInterface()                                      = default;
  EncoderInterface(const EncoderInterface&)                        = default;
  EncoderInterface(EncoderInterface&&) noexcept                    = default;
  auto operator=(const EncoderInterface&) -> EncoderInterface&     = default;
  auto operator=(EncoderInterface&&) noexcept -> EncoderInterface& = default;

  using Request = std::unique_ptr<RequestInterface>;
  [[nodiscard]] virtual auto encode(const Request& request, int type) const
    -> QByteArray = 0;
};

} // namespace protocol

#endif // CMD_ENCODERINTERFACE_H
