#ifndef CMD_MAINREQUEST_H
#define CMD_MAINREQUEST_H

namespace protocol {

struct RequestInterface
{
  virtual ~RequestInterface() = default;
};

} // namespace protocol

#endif // CMD_MAINREQUEST_H
