#ifndef DECODER_INTERFACE_H
#define DECODER_INTERFACE_H

#include <QByteArray>
#include <memory>
#include <nonstd/expected.hpp>

namespace protocol {

class ResponseInterface;

struct DecoderInterface
{
  DecoderInterface()                                               = default;
  virtual ~DecoderInterface()                                      = default;
  DecoderInterface(const DecoderInterface&)                        = default;
  DecoderInterface(DecoderInterface&&) noexcept                    = default;
  auto operator=(const DecoderInterface&) -> DecoderInterface&     = default;
  auto operator=(DecoderInterface&&) noexcept -> DecoderInterface& = default;

  enum class CreateFailureReason
  {
    GarbageData,
    NotEnoughData
  };

  using Response        = std::unique_ptr<ResponseInterface>;
  using ResponseOrError = nonstd::expected<Response, CreateFailureReason>;

  [[nodiscard]] virtual auto createResponse(const QByteArray& data,
                                            int type) const
    -> ResponseOrError = 0;
};

} // namespace protocol

#endif // DECODER_INTERFACE_H
