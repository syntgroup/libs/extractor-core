#ifndef PROTOCOL_VALUES_H
#define PROTOCOL_VALUES_H

#include <cstdint>
#include <strong_type/strong_type.hpp>

namespace protocol {

using PacketID = strong::type<uint8_t,
                              struct PidTag,
                              strong::regular,
                              strong::incrementable,
                              strong::ordered>;

} // namespace protocol

#endif // PROTOCOL_VALUES_H
