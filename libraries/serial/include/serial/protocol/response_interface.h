#ifndef CMD_MAINRESPONSEINTERFACE_H
#define CMD_MAINRESPONSEINTERFACE_H

#include "protocol_values.h"
#include <QString>

namespace protocol {

struct ResponseInterface
{
  ResponseInterface()                                                = default;
  virtual ~ResponseInterface()                                       = default;
  ResponseInterface(const ResponseInterface&)                        = delete;
  ResponseInterface(ResponseInterface&&) noexcept                    = default;
  auto operator=(const ResponseInterface&) -> ResponseInterface&     = delete;
  auto operator=(ResponseInterface&&) noexcept -> ResponseInterface& = default;

  [[nodiscard]] virtual auto id() const -> PacketID        = 0;
  [[nodiscard]] virtual auto encodedSize() const -> size_t = 0;

  [[nodiscard]] virtual auto hasNext() const -> bool        = 0;
  [[nodiscard]] virtual auto isError() const -> bool        = 0;
  [[nodiscard]] virtual auto errorString() const -> QString = 0;
};

} // namespace protocol

#endif // CMD_MAINRESPONSEINTERFACE_H
