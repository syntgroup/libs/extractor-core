#ifndef CMD_MOTHERBOARD_COUNTER_H
#define CMD_MOTHERBOARD_COUNTER_H

#include "serial/protocol/protocol_values.h"
#include "serial_export.h"

namespace utils {

struct CORE_SERIAL_EXPORT Counter
{
  [[nodiscard]] static auto tick() -> protocol::PacketID;

private:
  static protocol::PacketID counter;
};

} // namespace utils

#endif // CMD_MOTHERBOARD_COUNTER_H
