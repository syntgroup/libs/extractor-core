#include <gtest/gtest.h>

#include "serial/cmd/abstract_serial_operation.h"
#include "serial/protocol/request_interface.h"
#include "serial/protocol/response_interface.h"
#include "serial/transfer/i_session.h"
#include <QSerialPort>
#include <chrono>
#include <memory>

using namespace std::chrono_literals;

namespace {

class TestSession final : public transfer::ISession
{
public:
  using transfer::ISession::ISession;
  ~TestSession() = default;

  // ISession interface
public:
  void enqueueOperation(Operation operation) override
  {
    _operations.push_back(std::move(operation));
  }

  void start(Port port) override { _port = port; }
  void stop() override { _port = nullptr; }

  std::chrono::milliseconds processDelay() const override { return 1ms; }
  Operation currentOperation() const override { return _operations.front(); }
  void clearOperationQueue() override {}
  QByteArray prepareRequest() const override { return QByteArrayLiteral("hi"); }
  void processResponse(const QByteArray&) override {}
  void processMatchedResponse(const Response&) override {}
  void processBroadcastResponse(const Response&) override {}
  void processUnmatchedResponse(const Response&) override {}
  void processErrorResponse(const Response&) override {}
  void writeToPort() override {}
  void processQueue() override {}
  void doStopSession() override {}
  void onCurrentOperationFinished() override {}
  void onSerialPortReadyRead() override {}
  void onSerialPortErrorOccured() override {}
  void onSerialPortBytesWritten(uint64_t) override {}

  bool portOk() const { return _port != nullptr; }

private:
  std::vector<Operation> _operations;
  QSerialPort* _port = nullptr;
};

class TestOperation final : public cmd::AbstractSerialOperation
{
public:
  using cmd::AbstractSerialOperation::AbstractSerialOperation;
  ~TestOperation() = default;

  // AbstractOperation interface
public:
  QString description() const override { return "HI"; }
  OperationType operationType() const override { return OperationType::Meta; }

  // AbstractSerialOperation interface
public:
  int responseType() const override { return -1; }
  Request createRequest() const override { return nullptr; }
};

} // namespace

TEST(TestSerial, TestSessionCtor)
{
  auto session = std::make_unique<TestSession>();
  ASSERT_TRUE(session);
}

TEST(TestSerial, TestSessionStartStop)
{
  TestSession session;
  QSerialPort port;

  session.start(&port);
  ASSERT_TRUE(session.portOk());

  session.stop();
  ASSERT_FALSE(session.portOk());
}

TEST(TestSerial, TestSessionEnqueue)
{
  constexpr static protocol::PacketID pid{ 1 };

  TestSession session;
  EXPECT_EQ(session.processDelay(), 1ms);
  EXPECT_EQ(session.prepareRequest(), "hi");

  TestOperation operation{ pid };
  session.enqueueOperation(&operation);
  ASSERT_TRUE(session.currentOperation());
  EXPECT_EQ(session.currentOperation()->id(), pid);
  EXPECT_EQ(session.currentOperation()->description(), "HI");
}
