#include <gtest/gtest.h>

#include "serial/transfer/i_queue.h"
#include <memory>

namespace {

class TestQueue final : public transfer::IProtocolQueue
{
public:
  using transfer::IProtocolQueue::IProtocolQueue;
  ~TestQueue() = default;

  // IProtocolQueue interface
public:
  int operationsLeft() const override { return -1; }
  void startQueue() override {}
  void stopQueue() override {}
  void pauseQueue() override {}
  void resumeQueue() override {}
  void clearOperationQueue() override {}

protected:
  void processQueue() override {}
  void processCurrentBlock() override {}
  void onCurrentBlockAdvanced() override {}
  void onCurrentBlockFinished() override {}
  void onCurrentBlockErrorOccured() override {}
  void doStopQueue() override {}
};

} // namespace

TEST(TestSerial, TestQueueCtor)
{
  auto queue = std::make_unique<TestQueue>();
  ASSERT_TRUE(queue);
  EXPECT_EQ(queue->operationsLeft(), -1);
}
