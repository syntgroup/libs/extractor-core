#include "serial/cmd/abstract_serial_operation.h"

#include "serial/protocol/response_interface.h"

namespace cmd {

AbstractSerialOperation::AbstractSerialOperation(protocol::PacketID idx,
                                                 QObject* parent)
  : cmd::AbstractOperation{ parent }
  , m_idx{ idx }
{
}

AbstractSerialOperation::~AbstractSerialOperation() = default;

auto
AbstractSerialOperation::isFinished() const -> bool
{
  return operationState() == AbstractOperation::Finished;
}

void
AbstractSerialOperation::start()
{
  if (!begin()) {
    // begin() has to fill the error() and errorString() fields
    finishLater();
  } else {
    setOperationState(Started);
    startTimeout();
  }
}

void
AbstractSerialOperation::finishLater()
{
  QTimer::singleShot(0, this, &AbstractSerialOperation::finish);
}

void
AbstractSerialOperation::abort(const QString& reason)
{
  finishWithError(BackendError::UnknownError, reason);
}

void
AbstractSerialOperation::feedResponse(const Response& response)
{
  const auto response_processed{ processResponse(response) };

  if (!response_processed && isError()) {
    finish();
  } else if (!response_processed) {
    finishWithError(BackendError::ProtocolError,
                    QStringLiteral("Operation finished with error: %1")
                      .arg(response->errorString()));
  } else if (!response->hasNext()) {
    // response is processed, no more data left
    finish();
  } else {
    // needs more processing, starting timer again
    startTimeout();
  }
}

auto
AbstractSerialOperation::processResponse(const Response& response) -> bool
{
  return response != nullptr;
}

} // namespace cmd
