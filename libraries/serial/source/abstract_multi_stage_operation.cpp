#include "serial/cmd/abstract_multi_stage_operation.h"

namespace cmd {

void
AbstractMultiStageOperation::start()
{
  if (operationState() != AbstractOperation::Ready) {
    finishWithError(BackendError::UnknownError,
                    QStringLiteral("Trying to start an operation that is "
                                   "either already running or has finished"));
    return;
  }

  advanceOperationState();
}

auto
AbstractMultiStageOperation::operationType() const
  -> AbstractOperation::OperationType
{
  return AbstractMultiStageOperation::OperationType::MultiStage;
}

auto
AbstractMultiStageOperation::isFinished() const -> bool
{
  return operationState() == AbstractOperation::Finished;
}

auto
AbstractMultiStageOperation::isStarted() const -> bool
{
  return operationState() != AbstractOperation::Ready;
}

void
AbstractMultiStageOperation::advanceOperationState()
{
  nextStateLogic();
}

void
AbstractMultiStageOperation::registerOperation()
{
  connect(m_currentOperation, &AbstractOperation::finished, this, [this] {
    if (m_currentOperation->isError()) {
      onSubOperationError(*m_currentOperation);
      return;
    }
    nextStateLogic();
    if (operationState() != Finished) {
      emit stepAdvanced();
    }
  });
  connect(m_currentOperation,
          &AbstractOperation::finished,
          m_currentOperation,
          &QObject::deleteLater,
          Qt::QueuedConnection);
}

void
AbstractMultiStageOperation::registerDeadOperation()
{
  connect(m_currentOperation, &AbstractSerialOperation::finished, this, [this] {
    if (m_currentOperation->isError()) {
      onSubOperationError(*m_currentOperation);
      return;
    }
    nextStateLogic();
  });
  connect(m_currentOperation,
          &AbstractOperation::finished,
          m_currentOperation,
          &QObject::deleteLater,
          Qt::QueuedConnection);
}

} // namespace cmd
