#include "serial/utils/counter.h"

namespace {
constexpr protocol::PacketID counter_max{ 127 };
constexpr protocol::PacketID counter_zero{ 0 };
} // namespace

namespace utils {

protocol::PacketID utils::Counter::counter{ 0 };

protocol::PacketID
utils::Counter::tick()
{
  if (counter == counter_max) {
    counter = counter_zero;
    return counter;
  }

  return ++counter;
}

} // namespace utils
