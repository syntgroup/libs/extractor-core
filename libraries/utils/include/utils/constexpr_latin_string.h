/*!
 * @file constexpr_latin_string.h
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 */

#ifndef CONSTEXPR_LATIN_STRING_H
#define CONSTEXPR_LATIN_STRING_H

#include "utils_export.h"
#include <QLatin1String>

namespace utils {

/*!
 * @brief The ConstLatin1String class
 * @details stolen from SO answer https://stackoverflow.com/a/56209811
 */
struct CORE_UTILS_EXPORT ConstLatin1String final : public QLatin1String
{
  constexpr ConstLatin1String(const char* const s)
    : QLatin1String{ s, static_cast<int>(std::char_traits<char>::length(s)) }
  {
  }
};

} // namespace utils

#endif // CONSTEXPR_LATIN_STRING_H
