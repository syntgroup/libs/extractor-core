/*!
 * @file converter.h
 * @author Dmitry Sudorgin <dimasikme@yandex.ru>
 */

#ifndef EXTRACTOR_TOUCHPANEL_CORE_HELPER_H
#define EXTRACTOR_TOUCHPANEL_CORE_HELPER_H

#include <QMetaEnum>
#include <QString>

namespace utils {

/*!
 * @brief Converts enumerator marked with Q_ENUM to QString tag
 * @param value - enum value
 * @return QString representation of enum
 */
template<typename Enum,
         typename std::enable_if_t<std::is_enum_v<Enum>, bool> = true>
auto
to_string(Enum value)
{
  return QMetaEnum::fromType<Enum>().valueToKey(static_cast<int>(value));
}

} // namespace utils

#endif // EXTRACTOR_TOUCHPANEL_CORE_HELPER_H
