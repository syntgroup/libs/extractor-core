cmake_minimum_required(VERSION 3.14)

set(CORE_UTILS_MINOR 1)
project(
  core-utils
  VERSION ${CORE_LIBS_REVISION}.${CORE_UTILS_MINOR}
  LANGUAGES CXX)

message(
  STATUS
    "Project '${CMAKE_PROJECT_NAME}', package '${PROJECT_NAME}' version: '${PROJECT_VERSION}'"
)

add_library(core_utils include/utils/converter.h
                       include/utils/constexpr_latin_string.h)
add_library(core::utils ALIAS core_utils)
target_link_libraries(core_utils PUBLIC Qt5::Core)

target_compile_features(core_utils PUBLIC cxx_std_17)
target_compile_options(core_utils PRIVATE -fno-exceptions)

target_include_directories(
  core_utils ${warning_guard}
  PUBLIC "$<BUILD_INTERFACE:${PROJECT_SOURCE_DIR}/include/>")
target_include_directories(
  core_utils SYSTEM
  PUBLIC "$<BUILD_INTERFACE:${PROJECT_BINARY_DIR}/export/core-utils/>")

generate_export_header(
  core_utils
  BASE_NAME
  core_utils
  EXPORT_FILE_NAME
  export/core-utils/utils_export.h
  CUSTOM_CONTENT_FROM_VARIABLE
  pragma_supress_c4251)

set_target_properties(
  core_utils
  PROPERTIES CXX_VISIBILITY_PRESET hidden
             VISIBILITY_INLINES_HIDDEN YES
             VERSION ${PROJECT_VERSION}
             SOVERSION ${PROJECT_VERSION_MAJOR}
             DEBUG_POSTFIX "_d"
             AUTOMOC ON
             EXPORT_NAME core_utils
             OUTPUT_NAME core_utils)

if(NOT BUILD_SHARED_LIBS)
  target_compile_definitions(core_utils PUBLIC CORE_UTILS_STATIC_DEFINE)
endif()
