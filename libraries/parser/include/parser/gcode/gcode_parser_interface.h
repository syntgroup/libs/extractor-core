/*!
 * @file gcode_parser_interface.h
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 * @brief Declares base class for GCode parsers
 */

#ifndef GCODE_ABSTRACTPARSER_H
#define GCODE_ABSTRACTPARSER_H

#include <QString>
#include <gsl/gsl-lite.hpp>
#include <nonstd/expected.hpp>
#include <vector>

namespace cmd {
class AbstractOperation;
} // namespace cmd

namespace gcode {

using ResultType  = std::vector<gsl::owner<cmd::AbstractOperation*>>;
using ErrorType   = QString;
using ParseResult = nonstd::expected<ResultType, ErrorType>;

template<typename Derived>
class GCodeParserInterface
{
public:
  [[nodiscard]] auto generate(const QString& block) const -> ParseResult
  {
    if (!block.isEmpty() || Derived::allowEmptyInput()) {
      auto& derived = static_cast<const Derived&>(*this);
      return derived.generateImpl(block);
    }
    return nonstd::make_unexpected("Empty block is not allowed");
  }

protected:
  enum BlockPos : int
  {
    BPAddres = 1,
    BPValue  = 2
  };
};

} // namespace gcode

#endif // GCODE_ABSTRACTPARSER_H
