#include "thermocontrollers/device/heater.h"

#include <cmath>

namespace {
constexpr auto epsilon{ 0.1 };

template<typename T>
constexpr auto
equals(T lhs, T rhs)
{
  return std::fabs(rhs.value_of() - lhs.value_of()) < epsilon;
}
} // namespace

namespace device {

Heater::Heater(uint8_t adr, QObject* parent)
  : QObject{ parent }
  , m_params{ std::make_unique<HeaterParams>() }
  , m_pidParams{ std::make_unique<PidParams>() }
{
  m_params->address = adr;
  setObjectName(QStringLiteral("HEATER") + ' ' +
                QString::number(m_params->address));
}

Heater::Heater(std::unique_ptr<HeaterParams>&& params,
               std::unique_ptr<PidParams>&& pid,
               QObject* parent)
  : QObject{ parent }
  , m_params{ std::move(params) }
  , m_pidParams{ std::move(pid) }
{
  setObjectName(QStringLiteral("HEATER") + ' ' +
                QString::number(m_params->address));
}

SensorType
Heater::sensorType() const
{
  return m_params->sensorType;
}

Heater::~Heater() = default;

void
Heater::setSensorType(SensorType new_sensor_type)
{
  if (m_params->sensorType == new_sensor_type) {
    return;
  }
  m_params->sensorType = new_sensor_type;
  emit sensorTypeChanged(m_params->sensorType);
}

OutMode
Heater::out1Mode() const
{
  return m_params->out1Mode;
}

void
Heater::setOut1Mode(OutMode new_out1_mode)
{
  if (m_params->out1Mode == new_out1_mode) {
    return;
  }
  m_params->out1Mode = new_out1_mode;
  emit out1ModeChanged(m_params->out1Mode);
}

OutMode
Heater::out2Mode() const
{
  return m_params->out2Mode;
}

void
Heater::setOut2Mode(OutMode new_out2_mode)
{
  if (m_params->out2Mode == new_out2_mode) {
    return;
  }
  m_params->out2Mode = new_out2_mode;
  emit out2ModeChanged(m_params->out2Mode);
}

ControlMode
Heater::controlMode() const
{
  return m_params->ctrlMode;
}

void
Heater::setControlMode(ControlMode new_ctrl_mode)
{
  if (m_params->ctrlMode == new_ctrl_mode) {
    return;
  }
  m_params->ctrlMode = new_ctrl_mode;
  emit ctrlModeChanged(m_params->ctrlMode);
}

uint8_t
Heater::address() const
{
  return m_params->address;
}

void
Heater::setCurrentTemperature(Temperature new_current_temperature)
{
  if (equals(m_currentTemperature, new_current_temperature)) {
    return;
  }
  m_currentTemperature = new_current_temperature;
  emit currentTemperatureChanged(m_currentTemperature.value_of());
}

void
Heater::setTargetTemperature(Temperature new_target_temperature)
{
  if (equals(m_targetTemperature, new_target_temperature)) {
    return;
  }
  m_targetTemperature = new_target_temperature;
  emit targetTemperatureChanged(m_targetTemperature.value_of());
}

QString
Heater::name() const
{
  return m_params->name;
}

void
Heater::setRunning(RunMode state)
{
  if (m_running == state) {
    return;
  }

  m_running = state;
  emit runModeChanged(m_running);
  emit enabledChanged(m_running == HeaterConfig::RunMode::Run);
}

} // namespace device
