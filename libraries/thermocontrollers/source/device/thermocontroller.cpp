#include "thermocontrollers/device/thermocontroller.h"

#include "thermocontrollers/device/heater.h"

namespace device {

Thermocontrollers::Thermocontrollers(Container&& heaters)
  : m_heaters{ std::move(heaters) }
{
}

Thermocontrollers::~Thermocontrollers() = default;

auto
Thermocontrollers::heater(uint8_t address) const -> Heater*
{
  if (m_heaters.find(address) == m_heaters.end()) {
    return nullptr;
  }

  return m_heaters.at(address).get();
}

} // namespace device
