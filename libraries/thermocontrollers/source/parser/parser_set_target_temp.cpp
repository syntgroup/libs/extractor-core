#include "thermocontrollers/gcode/parser_set_target_temp.h"

#include "serial/utils/counter.h"
#include "thermocontrollers/cmd/set_target_temperature_operation.h"
#include "thermocontrollers/device/heater.h"
#include "thermocontrollers/device/heater_values.h"
#include "thermocontrollers/device/thermocontroller.h"
#include <QRegularExpression>
#include <QRegularExpressionMatch>

namespace gcode {

auto
ParserSetTargetTemp::generateImpl(const QString& block) const -> ParseResult
{
  // M104 [T<heater idx>] [S<new target temp>]

  QRegularExpression find_heater{ QStringLiteral("([tT])(\\d*)") };
  QRegularExpression find_temperature{ QStringLiteral("([sS])(\\d*.\\d+)") };

  const auto heater_mentioned{ find_heater.match(block) };
  const auto temperature_mentioned{ find_temperature.match(block) };

  if (!heater_mentioned.hasMatch() || !temperature_mentioned.hasMatch()) {
    return nonstd::make_unexpected(
      QStringLiteral("incorrect syntax of M104 command: no heater or "
                     "teperature was mentioned on line %1"));
  }

  const auto heaters = m_thermocontrollers.lock();
  if (!heaters) {
    return nonstd::make_unexpected(QStringLiteral("error accessing heaters"));
  }

  auto heater_idx = heater_mentioned.captured(BPValue).toUInt();
  auto* heater    = heaters->heater(heater_idx);
  if (heater == nullptr) {
    return nonstd::make_unexpected(
      QStringLiteral("unknown heater requested: %1").arg(heater_idx));
  }

  device::Temperature temperature{
    temperature_mentioned.captured(BPValue).toDouble()
  };
  auto operation = std::make_unique<cmd::dtc::SetTargetTemperatureOperation>(
    utils::Counter::tick(), heater, temperature);

  ResultType result;
  result.reserve(1);
  result.push_back(operation.release());

  return result;
}

} // namespace gcode
