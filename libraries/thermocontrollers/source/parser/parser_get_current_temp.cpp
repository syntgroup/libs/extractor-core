#include "thermocontrollers/gcode/parser_get_current_temp.h"

#include "serial/utils/counter.h"
#include "thermocontrollers/cmd/get_current_temperature_operation.h"
#include "thermocontrollers/device/heater.h"
#include "thermocontrollers/device/thermocontroller.h"
#include <QRegularExpression>
#include <QRegularExpressionMatch>

namespace gcode {

auto
ParserGetCurrentTemp::generateImpl(const QString& block) const -> ParseResult
{
  const QRegularExpression find_heater{ QStringLiteral("([tT])(\\d*)") };
  const auto heater_mentioned = find_heater.match(block);
  if (!heater_mentioned.hasMatch()) {
    return nonstd::make_unexpected(
      QStringLiteral("incorrect syntax of M105 command"));
  }

  const auto heaters = m_thermocontrollers.lock();
  if (!heaters) {
    return nonstd::make_unexpected(QStringLiteral("error accessing heaters"));
  }

  auto heater_idx = heater_mentioned.captured(BPValue).toUInt();
  auto* heater    = heaters->heater(heater_idx);
  if (heater == nullptr) {
    return nonstd::make_unexpected(
      QStringLiteral("unknown heater requested: %1").arg(heater_idx));
  }

  auto operation = std::make_unique<cmd::dtc::GetCurrentTemperatureOperation>(
    utils::Counter::tick(), heater);

  ResultType result;
  result.reserve(1);
  result.push_back(operation.release());

  return result;
}

} // namespace gcode
