#include "thermocontrollers/gcode/parser_set_wait_target_temp.h"

#include "thermocontrollers/cmd/multi_set_temp_and_wait_operation.h"
#include "thermocontrollers/device/heater.h"
#include "thermocontrollers/device/heater_values.h"
#include "thermocontrollers/device/thermocontroller.h"
#include <QRegularExpression>
#include <QRegularExpressionMatch>

namespace gcode {

auto
ParserSetWaitTargetTemp::generateImpl(const QString& block) const -> ParseResult
{
  // (WONT USE) [B<temp>] 	With AUTOTEMP, the max auto-temperature.
  // (WONT USE) [F<flag>] 	Autotemp flag. Omit to disable autotemp.
  // (WONT USE) [I<index>] 2.0.6 	Material preset index. Overrides S.
  // [R<temp>] 	 Target temperature (wait for cooling or heating).
  // [S<temp>] 	 Target temperature (wait only when heating). Also
  //  AUTOTEMP: The
  // min auto-temperature.
  // [T<index>] 	 Hotend index. If omitted, the currently
  // active hotend will be used.
  //  return true;

  const QRegularExpression find_heater{ R"(([tT])(\d*))" };
  const QRegularExpression find_temperature{ R"(([sS])(\d*.\d+))" };
  const QRegularExpression find_offset{ R"(([fF])(\d+\.*\d*))" };

  const auto heater_mentioned      = find_heater.match(block);
  const auto temperature_mentioned = find_temperature.match(block);
  const auto offset_mentioned      = find_offset.match(block);

  if (!heater_mentioned.hasMatch() || !temperature_mentioned.hasMatch()) {
    return nonstd::make_unexpected(
      QStringLiteral("incorrect syntax of M109 command"));
  }

  const auto heaters = m_thermocontrollers.lock();
  if (!heaters) {
    return nonstd::make_unexpected(QStringLiteral("error accessing heaters"));
  }

  auto heater_idx = heater_mentioned.captured(BPValue).toUInt();
  auto* heater    = heaters->heater(heater_idx);
  if (heater == nullptr) {
    return nonstd::make_unexpected(
      QStringLiteral("unknown heater requested: %1").arg(heater_idx));
  }

  bool conversion_ok = true;
  device::Temperature temperature{
    temperature_mentioned.captured(BPValue).toDouble(&conversion_ok)
  };
  if (!conversion_ok) {
    return nonstd::make_unexpected(
      QStringLiteral("Can't parse requested temperature value: %1").arg(block));
  }

  const auto have_offset = offset_mentioned.hasMatch();
  device::Temperature tolerance{
    have_offset ? offset_mentioned.captured(BPValue).toDouble(&conversion_ok)
                : 1.0
  };

  auto operation = std::make_unique<cmd::multi::SetTempAndWaitOperation>(
    heater, temperature, tolerance);

  ResultType result;
  result.reserve(1);
  result.push_back(operation.release());

  return result;
}

} // namespace gcode
