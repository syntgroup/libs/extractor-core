#include "modbus/modbus_packet_rtu.h"

#include <QDataStream>

namespace protocol::modbus {

QByteArray
PacketRTU::encode() const
{
  QByteArray result;
  QDataStream out{ &result, QIODevice::WriteOnly };

  out << slaveId() << function();
  for (auto word : pdu()) {
    out << word;
  }
  out << calculateChecksum(result);

  return result;
}

uint16_t
PacketRTU::calculateChecksum(const QByteArray& data) const
{
  uint16_t reg_crc{ 0xffff };
  std::for_each(data.begin(), data.end(), [&reg_crc](uint8_t byte) {
    reg_crc ^= byte;
    for (size_t i = 0; i < 8; ++i) {
      if ((reg_crc & 0x0001) != 0) {
        reg_crc = (reg_crc >> 1) ^ 0xA001;
      } else {
        reg_crc = reg_crc >> 1;
      }
    }
  });

  reg_crc = (reg_crc << 8) | (reg_crc >> 8);
  reg_crc &= 0xffff;

  return reg_crc;
}

} // namespace protocol::modbus
