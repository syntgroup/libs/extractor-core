#include "modbus/modbus_response_ascii.h"

#include "byte_converter.h"
#include "modbus/modbus_packet_ascii.h"

namespace protocol::modbus {

nonstd::expected<std::unique_ptr<IPacket>, ExceptionCode>
AsciiResponseFactory::create(const QByteArray& data) const
{
  if (data.isEmpty()) {
    return nonstd::make_unexpected(ExceptionCode::UnknownError);
  }

  auto result{ std::make_unique<PacketASCII>() };
  const auto converted{ QByteArray::fromHex(data) };

  const auto slave_id{ static_cast<uint8_t>(converted.at(0)) };
  result->setSlaveId(slave_id);

  const auto func_code_raw{ static_cast<uint8_t>(converted.at(1)) };
  if ((func_code_raw & exception_byte) != 0) {
    const auto code{ static_cast<ExceptionCode>(func_code_raw -
                                                exception_byte) };
    return nonstd::make_unexpected(code);
  }

  const auto function_code{ static_cast<FunctionCode>(func_code_raw) };
  if (!checkFunctionCode(function_code)) {
    return nonstd::make_unexpected(ExceptionCode::IllegalFunciton);
  }
  result->setFunctionCode(function_code);

  const auto pdu{ converted.mid(1, converted.size() - 2) };
  fillPayload(pdu, result.get());

  const auto crc{ static_cast<uint8_t>(converted.at(converted.size() - 1)) };
  const auto to_calculate{ converted.left(converted.size() - 1) };
  const auto calculated{ result->calculateChecksum(to_calculate) };
  if (crc != calculated) {
    return nonstd::make_unexpected(ExceptionCode::CrcError);
  }

  result->setStatus(ExceptionCode::Ok);
  return result;
}

void
AsciiResponseFactory::fillPayload(const QByteArray& data, IPacket* packet) const
{
  auto fill_from{ 0 };
  packet->appendToPdu(packet->function());

  switch (packet->function()) {
    case ReadHoldingRegisters:
      packet->setDataCount(data.at(1));
      packet->appendToPdu(packet->dataCount());
      fill_from = 2;
      break;

    case WriteSingleRegister:
      packet->setStartAddress(utils::word(data.at(1), data.at(2)));
      packet->appendToPdu(packet->startAddress());
      fill_from = 3;
      break;

    default:
      return;
  }

  for (auto idx = fill_from + 1; idx < data.size(); idx += 2) {
    const auto byte_high{ static_cast<uint8_t>(data.at(idx - 1)) };
    const auto byte_low{ static_cast<uint8_t>(data.at(idx)) };
    packet->appendToPdu(utils::word(byte_high, byte_low));
  }
}

} // namespace protocol::modbus
