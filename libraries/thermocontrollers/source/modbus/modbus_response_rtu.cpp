#include "modbus/modbus_response_rtu.h"

#include "byte_converter.h"
#include "modbus/modbus_packet_rtu.h"

namespace protocol::modbus {

nonstd::expected<std::unique_ptr<IPacket>, ExceptionCode>
RtuResponseFactory::create(const QByteArray& data) const
{
  // слишком мало данных, выходим ни с чем?
  if (data.isEmpty()) {
    return nonstd::make_unexpected(ExceptionCode::UnknownError);
  }

  auto result{ std::make_unique<PacketRTU>() };
  // сверяем id слейва? во всяком случае записываем его
  const auto slave_id{ static_cast<uint8_t>(data.at(FPSlaveAddress)) };
  result->setSlaveId(slave_id);

  const auto function_code_raw{ static_cast<uint8_t>(data.at(FPCommandCode)) };
  // проверяем на присутствие кода ошибки в поле функции
  if ((function_code_raw & exception_byte) != 0) {
    const auto code{ static_cast<ExceptionCode>(function_code_raw -
                                                exception_byte) };
    return nonstd::make_unexpected(code);
  }

  // проверяем на код функции -- можем мы такой обрабатывать / корректный ли
  // вообще
  const auto function_code{ static_cast<FunctionCode>(function_code_raw) };
  if (!checkFunctionCode(function_code)) {
    return nonstd::make_unexpected(ExceptionCode::IllegalFunciton);
  }
  result->setFunctionCode(function_code);

  // вытаскиваем нагрузку как положено, с кодом команды?
  // выделяем строго pdu без контрольной суммы, и из нее заполняем нагрузку
  const auto pdu{ data.mid(FPCommandCode, data.size() - 2 - 1) };
  fillPayload(pdu, result.get());

  // проверяем контрольную сумму
  const auto crc_idx{ data.size() - 2 };
  const auto crc_byte_high{ data.at(crc_idx) };
  const auto crc_byte_low{ data.at(crc_idx + 1) };
  const auto crc{ utils::word(crc_byte_high, crc_byte_low) };
  const auto to_calculate{ data.left(data.size() - 2) };
  const auto calculated{ result->calculateChecksum(to_calculate) };
  if (crc != calculated) {
    return nonstd::make_unexpected(ExceptionCode::CrcError);
  }

  result->setStatus(ExceptionCode::Ok);
  return result;
}

void
RtuResponseFactory::fillPayload(const QByteArray& data, IPacket* packet) const
{
  auto fill_from{ 0 };

  packet->appendToPdu(packet->function());

  switch (packet->function()) {
    case ReadHoldingRegisters:
      packet->setDataCount(data.at(FPDataAddress - 1));
      packet->appendToPdu(packet->dataCount());
      fill_from = FPDataContent - 1;
      break;

    case WriteSingleRegister:
    case WriteSingleCoil:
      packet->setStartAddress(
        utils::word(data.at(FPDataAddress - 1), data.at(FPDataAddress)));
      packet->appendToPdu(packet->startAddress());
      fill_from = FPDataContent;
      break;

    case ReadDiscreteInputs:
      packet->setDataCount(data.at(FPDataAddress - 1));
      packet->appendToPdu(packet->dataCount());
      fill_from = FPDataAddress;
      break;

    default:
      return;
  }

  if (packet->function() == ReadHoldingRegisters ||
      packet->function() == WriteSingleRegister ||
      packet->function() == WriteSingleCoil) {
    for (auto idx = fill_from; idx < data.size(); idx += 2) {
      const auto byte_high{ static_cast<uint8_t>(data.at(idx - 1)) };
      const auto byte_low{ static_cast<uint8_t>(data.at(idx)) };
      packet->appendToPdu(utils::word(byte_high, byte_low));
    }
  }

  if (packet->function() == ReadDiscreteInputs) {
    std::for_each(data.begin() + fill_from, data.end(), [&](uint8_t byte) {
      packet->appendToPdu(static_cast<uint16_t>(byte));
    });
  }
}

} // namespace protocol::modbus
