#include "modbus/modbus_packet.h"

namespace protocol::modbus {

IPacket::IPacket(uint8_t slave_id, FunctionCode function, Payload data)
  : m_slaveId{ slave_id }
  , m_functionCode{ function }
  , m_pdu{ std::move(data) }
{
}

auto
IPacket::isException() const -> bool
{
  return m_status != ExceptionCode::Ok;
}

auto
IPacket::excecptionCode() const -> ExceptionCode
{
  if (m_pdu.empty() || !isException()) {
    return ExceptionCode::UnknownError;
  }
  return static_cast<ExceptionCode>(m_pdu.at(0));
}

/*!
 * @return in bytes
 * @details Returns the minimum data size in bytes for the given \a pdu and the
 * Modbus PDU \a type. If the PDU's function code is invalid, undefined  or
 * unknown, the return value will be \c {-1}.
 */
int
IPacket::minimumPduSize(FunctionCode function, Type type)
{
  // добавить возможность чекать на исключения? в принципе это делает клиентский
  // код
  //  if (pdu.isException())
  //    return 1;

  switch (function) {
    case ReadCoils:
    case ReadDiscreteInputs:
      return type == Type::Request ? 4 : 2;
    case WriteSingleCoil:
    case WriteSingleRegister:
      return 4;
    case ReadHoldingRegisters:
    case ReadInputRegisters:
      return type == Type::Request ? 4 : 3;
    case ReadExceptionStatus:
      return type == Type::Request ? 0 : 1;
    case Diagnostics:
      return 4;
    case GetCommEventCounter:
      return type == Type::Request ? 0 : 8;
    case GetCommEventLog:
      return type == Type::Request ? 0 : 8;
    case WriteMultipleCoils:
      return type == Type::Request ? 6 : 4;
    case WriteMultipleRegisters:
      return type == Type::Request ? 7 : 4;
    case ReportServerID:
      return type == Type::Request ? 0 : 3;
    case ReadFileRecord:
      return type == Type::Request ? 8 : 5;
    case WriteFileRecord:
      return 10;
    case MaskWriteRegister:
      return 6;
    case ReadWriteMultipleRegisters:
      return type == Type::Request ? 11 : 3;
    case ReadFifoQueue:
      return type == Type::Request ? 2 : 6;
    case IncapsulatedInterfaceTransport:
      return 2;
    case Invalid:
    case UndefinedFunctionCode:
      return -1;
  }

  return -1;
}

} // namespace protocol::modbus
