/*!
 * @file byte_converter.h
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 * @brief This file contains converters for MODBUS values
 */

#ifndef MODBUS_UTILS_BYTECONVERTER_H
#define MODBUS_UTILS_BYTECONVERTER_H

#include <cstdint>

namespace utils {

[[nodiscard]] auto
byteHigh(uint16_t word) -> uint8_t;

[[nodiscard]] auto
byteLow(uint16_t word) -> uint8_t;

[[nodiscard]] auto
word(uint8_t byte_high, uint8_t byte_low) -> uint16_t;

void
setBit(uint8_t& byte, uint8_t bit);

[[nodiscard]] auto
getBit(uint8_t byte, uint8_t bit) -> bool;

} // namespace utils

#endif // MODBUS_UTILS_BYTECONVERTER_H
