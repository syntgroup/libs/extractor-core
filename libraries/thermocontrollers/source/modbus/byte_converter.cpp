/*!
 * @file byte_converter.cpp
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 */

#include "byte_converter.h"

namespace utils {

auto
byteHigh(uint16_t word) -> uint8_t
{
  return static_cast<uint8_t>((word & 0xff00) >> 8);
}

auto
byteLow(uint16_t word) -> uint8_t
{
  return static_cast<uint8_t>(word & 0x00ff);
}

auto
word(uint8_t byte_high, uint8_t byte_low) -> uint16_t
{
  auto result{ static_cast<uint16_t>(byte_low) };
  result |= static_cast<uint16_t>(byte_high << 8);

  return result;
}

void
setBit(uint8_t& byte, uint8_t bit)
{
  byte |= (1 << bit);
}

auto
getBit(uint8_t byte, uint8_t bit) -> bool
{
  const auto mask{ 1 << bit };
  const auto result{ byte & mask };

  return static_cast<bool>(result);
}

} // namespace utils
