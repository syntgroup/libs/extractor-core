#include "modbus/modbus_packet_ascii.h"

#include <QDataStream>
#include <algorithm>

namespace protocol::modbus {

QByteArray
PacketASCII::encode() const
{
  QByteArray result;
  QDataStream out{ &result, QIODevice::WriteOnly };

  out << slaveId() << function();
  for (auto word : pdu()) {
    out << word;
  }
  out << static_cast<uint8_t>(calculateChecksum(result));

  return ':' + result.toHex() + '\r' + '\n';
}

uint16_t
PacketASCII::calculateChecksum(const QByteArray& data) const
{
  static constexpr uint16_t magic_constant{ 0x100 };
  const auto lrc{ std::accumulate(data.begin(), data.end(), UINT16_C(0)) };
  return magic_constant - lrc;
}

} // namespace protocol::modbus
