#include "modbus/modbus_response.h"

#include "modbus/modbus_defs.h"

namespace protocol::modbus {

bool
AbstractModbusResponseFactory::checkFunctionCode(uint8_t function)
{
  switch (static_cast<FunctionCode>(function)) {
    case FunctionCode::ReadHoldingRegisters:
    case FunctionCode::WriteSingleRegister:
    case FunctionCode::ReadDiscreteInputs:
    case FunctionCode::WriteSingleCoil:
      return true;

    default:
      return false;
  }
}

} // namespace protocol::modbus
