#include "modbus/modbus_request_factory.h"

#include "modbus/modbus_packet_ascii.h"
#include "modbus/modbus_packet_rtu.h"
#include <memory>

namespace {

template<typename... Args>
[[nodiscard]] auto
create_packet(protocol::modbus::Mode mode, Args&&... args)
  -> std::unique_ptr<protocol::modbus::IPacket>
{
  switch (mode) {
    case protocol::modbus::Mode::Ascii:
      return std::make_unique<protocol::modbus::PacketASCII>(
        std::forward<Args>(args)...);
    case protocol::modbus::Mode::Rtu:
      return std::make_unique<protocol::modbus::PacketRTU>(
        std::forward<Args>(args)...);
  }

  return nullptr;
}

} // namespace

namespace protocol::modbus {

QByteArray
ModbusRequestFactory::writeSingleRegister(uint8_t slave_id,
                                          uint16_t start_address,
                                          uint16_t value) const
{
  const auto packet{ create_packet(m_mode,
                                   slave_id,
                                   FunctionCode::WriteSingleRegister,
                                   Payload{ start_address, value }) };

  return packet->encode();
}

QByteArray
ModbusRequestFactory::readHoldingRegisters(uint8_t slave_id,
                                           uint16_t start_address,
                                           uint8_t count) const
{
  const auto packet{ create_packet(m_mode,
                                   slave_id,
                                   FunctionCode::ReadHoldingRegisters,
                                   Payload{ start_address, count }) };

  return packet->encode();
}

QByteArray
ModbusRequestFactory::writeSingleCoil(uint8_t slave_id,
                                      uint16_t start_address,
                                      bool value) const
{
  const auto state{ static_cast<uint16_t>(value ? 0xff00 : 0x0000) };
  const auto packet{ create_packet(m_mode,
                                   slave_id,
                                   FunctionCode::WriteSingleCoil,
                                   Payload{ start_address, state }) };

  return packet->encode();
}

QByteArray
ModbusRequestFactory::readDiscreteInputs(uint8_t slave_id,
                                         uint16_t start_address,
                                         uint8_t count) const
{
  const auto packet{ create_packet(m_mode,
                                   slave_id,
                                   FunctionCode::ReadDiscreteInputs,
                                   Payload{ start_address, count }) };

  return packet->encode();
}

} // namespace protocol::modbus
