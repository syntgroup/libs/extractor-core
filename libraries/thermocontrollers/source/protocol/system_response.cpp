#include "system_response.h"

#include "modbus/modbus_packet.h"

namespace protocol::dtc {

SystemCommunicationResponse::SystemCommunicationResponse(
  std::unique_ptr<modbus::IPacket>& packet)
  : DtcResponse{ packet }
{
  if (!isError()) {
    value = pdu().at(DataValue);
  } else {
    setError(true);
    setErrorString(QString::number(errorCode(), 16));
  }
}

} // namespace protocol::dtc
