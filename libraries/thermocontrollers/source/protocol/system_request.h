#ifndef SYSTEM_REQUEST_H
#define SYSTEM_REQUEST_H

#include "serial/protocol/request_interface.h"
#include "thermocontrollers/protocol/dtc_registers.h"

namespace protocol::dtc {

struct SystemCommunicationAddressRequest final : protocol::RequestInterface
{
  explicit SystemCommunicationAddressRequest(uint8_t heater)
    : address{ heater }
  {
  }
  const uint8_t address;
  const HoldingRegisters start_address{
    HoldingRegisters::CommunicationAddress
  };
  const uint8_t count{ 1 };
};

} // namespace protocol::dtc

#endif // SYSTEM_REQUEST_H
