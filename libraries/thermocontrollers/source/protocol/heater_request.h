#ifndef HEATER_REQUEST_H
#define HEATER_REQUEST_H

#include "serial/protocol/request_interface.h"
#include "thermocontrollers/protocol/dtc_registers.h"

namespace protocol::dtc {

struct HeaterCurrentTempRequest final : protocol::RequestInterface
{
  explicit HeaterCurrentTempRequest(uint8_t heater)
    : address{ heater }
  {
  }
  const uint8_t address;
  const HoldingRegisters start_address{ HoldingRegisters::PV };
  const uint8_t count{ 1 };
};

struct HeaterTargetTempRequest final : protocol::RequestInterface
{
  explicit HeaterTargetTempRequest(uint8_t heater)
    : address{ heater }
  {
  }
  const uint8_t address;
  const HoldingRegisters start_address{ HoldingRegisters::SV };
  const uint8_t count{ 1 };
};

struct HeaterRangeHighRequest final : protocol::RequestInterface
{
  explicit HeaterRangeHighRequest(uint8_t heater)
    : address{ heater }
  {
  }
  const uint8_t address;
  const HoldingRegisters start_address{ HoldingRegisters::UpperLimit };
  const uint8_t count{ 1 };
};

struct HeaterRangeLowRequest final : protocol::RequestInterface
{
  explicit HeaterRangeLowRequest(uint8_t heater)
    : address{ heater }
  {
  }
  const uint8_t address;
  const HoldingRegisters start_address{ HoldingRegisters::DownLimit };
  const uint8_t count{ 1 };
};

struct HeaterTargetTempWrite final : protocol::RequestInterface
{
  explicit HeaterTargetTempWrite(uint8_t heater, double new_target);

  const uint8_t address;
  const uint16_t value;
  const HoldingRegisters start_address{ HoldingRegisters::SV };
};

struct HeaterRangeHighWrite final : protocol::RequestInterface
{
  explicit HeaterRangeHighWrite(uint8_t heater, double new_high);

  const uint8_t address;
  const uint16_t value;

  const HoldingRegisters start_address{ HoldingRegisters::UpperLimit };
};

struct HeaterRangeLowWrite final : protocol::RequestInterface
{
  explicit HeaterRangeLowWrite(uint8_t heater, double new_low);

  const uint8_t address;
  const uint16_t value;

  const HoldingRegisters start_address{ HoldingRegisters::DownLimit };
};

} // namespace protocol::dtc

#endif // HEATER_REQUEST_H
