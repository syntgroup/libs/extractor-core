#ifndef DEVICE_REQUEST_H
#define DEVICE_REQUEST_H

#include "serial/protocol/request_interface.h"
#include "thermocontrollers/protocol/dtc_registers.h"

namespace protocol::dtc {

struct DeviceOutModeRequest final : protocol::RequestInterface
{
  explicit DeviceOutModeRequest(uint8_t heater, uint8_t out_no)
    : address{ heater }
    , start_address{ out_no == 1 ? HoldingRegisters::ModeOut1
                                 : HoldingRegisters::ModeOut2 }
  {
  }
  const uint8_t address;
  const HoldingRegisters start_address;
  const uint8_t count{ 1 };
};

struct DeviceCtrlModeRequest final : protocol::RequestInterface
{
  explicit DeviceCtrlModeRequest(uint8_t heater)
    : address{ heater }
  {
  }
  const uint8_t address;
  const HoldingRegisters start_address{ HoldingRegisters::ControlMethod };
  const uint8_t count{ 1 };
};

struct DeviceSensorTypeRequest final : protocol::RequestInterface
{
  explicit DeviceSensorTypeRequest(uint8_t heater)
    : address{ heater }
  {
  }
  const uint8_t address;
  const HoldingRegisters start_address{ HoldingRegisters::SensorType };
  const uint8_t count{ 1 };
};

struct DeviceAL2HRequest final : protocol::RequestInterface
{
  explicit DeviceAL2HRequest(uint8_t heater)
    : address{ heater }
  {
  }
  const uint8_t address;
  const HoldingRegisters start_address{ HoldingRegisters::UpperLimitAlarm2 };
  const uint8_t count{ 1 };
};

struct DeviceStateRequest final : protocol::RequestInterface
{
  explicit DeviceStateRequest(uint8_t heater)
    : address{ heater }
  {
  }
  const uint8_t address;
  const HoldingRegisters start_address{ HoldingRegisters::Status };
  const uint8_t count{ 1 };
};

struct DeviceRunModeRequest final : protocol::RequestInterface
{
  explicit DeviceRunModeRequest(uint8_t heater)
    : address{ heater }
  {
  }
  const uint8_t address;
  constexpr static auto start_address{ DiscreteInputs::ControlSetting };
  constexpr static auto count{ 1 };
};

struct DeviceRunModeWrite final : protocol::RequestInterface
{
  explicit DeviceRunModeWrite(uint8_t heater, bool state)
    : address{ heater }
    , value{ state }
  {
  }
  const uint8_t address;
  const bool value;
  const DiscreteInputs start_address{ DiscreteInputs::ControlSetting };
};

} // namespace protocol::dtc

#endif // DEVICE_REQUEST_H
