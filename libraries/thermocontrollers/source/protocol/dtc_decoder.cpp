#include "thermocontrollers/protocol/dtc_decoder.h"

#include "device_response.h"
#include "heater_response.h"
#include "modbus/modbus_packet.h"
#include "modbus/modbus_response_ascii.h"
#include "modbus/modbus_response_rtu.h"
#include "thermocontrollers/protocol/dtc_response_type.h"

namespace protocol::dtc {

DtcDecoder::DtcDecoder(modbus::Mode mode)
  : factory{
    [](auto mode_) -> std::unique_ptr<modbus::AbstractModbusResponseFactory> {
      switch (mode_) {
        case modbus::Mode::Rtu:
          return std::make_unique<modbus::RtuResponseFactory>();
        case modbus::Mode::Ascii:
          return std::make_unique<modbus::AsciiResponseFactory>();
      }
    }(mode)
  }
{
}

DtcDecoder::~DtcDecoder() = default;

DecoderInterface::ResponseOrError
DtcDecoder::createResponse(const QByteArray& data, int type) const
{
  if (data.isEmpty()) {
    return nonstd::make_unexpected(CreateFailureReason::NotEnoughData);
  }

  if (data.size() == 1 || data.at(0) == 0xff) {
    return nonstd::make_unexpected(CreateFailureReason::GarbageData);
  }

  auto packet{ factory->create(data) };
  if (!packet.has_value() && packet.error() != modbus::ExceptionCode::Ok) {
    return nonstd::make_unexpected(CreateFailureReason::GarbageData);
  }

  switch (static_cast<ResponseType>(type)) {
    case ResponseType::DeviceGetCtrlMode:
      return std::make_unique<DeviceGetCtrlModeResponse>(packet.value());

    case ResponseType::DeviceGetOutMode:
      return std::make_unique<DeviceGetOutModeResponse>(packet.value());

    case ResponseType::DeviceGetSensorType:
      return std::make_unique<DeviceGetSensorTypeResponse>(packet.value());

    case ResponseType::DeviceGetState:
    case ResponseType::DeviceSetCtrlMode:
    case ResponseType::DeviceSetOutMode:
    case ResponseType::DeviceSetSensorType:
      break;

    case ResponseType::HeaterGetCurTemp:
      return std::make_unique<HeaterGetCurTempResponse>(packet.value());

    case ResponseType::HeaterGetTargetTemp:
      return std::make_unique<HeaterGetTargetTempResponse>(packet.value());

    case ResponseType::HeaterSetTargetTemp:
      return std::make_unique<HeaterSetTargetTempResponse>(packet.value());

    case ResponseType::DeviceGetRunMode:
      return std::make_unique<DeviceGetRunModeResponse>(packet.value());

    case ResponseType::DeviceSetRunMode:
      return std::make_unique<DeviceSetRunModeResponse>(packet.value());
  }

  return nonstd::make_unexpected(CreateFailureReason::GarbageData);
}

} // namespace protocol::dtc
