#include "heater_request.h"

#include "utils/byte_converter.h"

namespace protocol::dtc {

HeaterTargetTempWrite::HeaterTargetTempWrite(uint8_t heater, double new_target)
  : address{ heater }
  , value{ utils::dtcDoubleToShort(new_target) }
{
}

HeaterRangeHighWrite::HeaterRangeHighWrite(uint8_t heater, double new_high)
  : address{ heater }
  , value{ utils::dtcDoubleToShort(new_high) }
{
}

HeaterRangeLowWrite::HeaterRangeLowWrite(uint8_t heater, double new_low)
  : address{ heater }
  , value{ utils::dtcDoubleToShort(new_low) }
{
}

} // namespace protocol::dtc
