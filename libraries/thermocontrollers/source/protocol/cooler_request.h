#ifndef COOLER_REQUEST_H
#define COOLER_REQUEST_H

#include "serial/protocol/request_interface.h"
#include "thermocontrollers/protocol/dtc_registers.h"

namespace protocol::dtc {

struct CoolerEnableRequest final : protocol::RequestInterface
{
  enum class Mode
  {
    Enabled,
    Disabled
  };

  constexpr explicit CoolerEnableRequest(uint8_t heater, Mode mode)
    : address{ heater }
    , value{ static_cast<uint16_t>(mode == Mode::Enabled ? 0 : 1000) }
  {
  }

  const uint8_t address;
  const uint16_t value;

  const HoldingRegisters start_address{ HoldingRegisters::UpperLimitAlarm2 };
};

} // namespace protocol::dtc

#endif // COOLER_REQUEST_H
