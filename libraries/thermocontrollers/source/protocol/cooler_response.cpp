#include "cooler_response.h"

#include "modbus/modbus_packet.h"
#include "utils/byte_converter.h"

namespace protocol::dtc {

CoolerTurnOnResponse::CoolerTurnOnResponse(
  std::unique_ptr<modbus::IPacket>& packet)
  : DtcResponse{ packet }
{
  if (!isError()) {
    alarm2high = utils::dtcShortToDouble(pdu().at(DataValue));
  } else {
    setError(true);
    setErrorString(QString::number(errorCode(), 16));
  }
}

} // namespace protocol::dtc
