#ifndef COOLER_RESPONSE_H
#define COOLER_RESPONSE_H

#include "thermocontrollers/protocol/dtc_response.h"

namespace protocol::dtc {

struct CoolerTurnOnResponse final : DtcResponse
{
  explicit CoolerTurnOnResponse(std::unique_ptr<modbus::IPacket>& packet);
  ~CoolerTurnOnResponse() override = default;

  double alarm2high{ 0.0 };
};

} // namespace protocol::dtc

#endif // COOLER_RESPONSE_H
