#include "thermocontrollers/protocol/dtc_encoder.h"

#include "cooler_request.h"
#include "device_request.h"
#include "heater_request.h"
#include "system_request.h"

#include "modbus/modbus_request_factory.h"
#include "thermocontrollers/protocol/dtc_response_type.h"

namespace {

template<typename T, typename std::enable_if_t<std::is_enum_v<T>, bool> = true>
[[nodiscard]] auto
register_cast(T reg)
{
  return static_cast<std::underlying_type_t<T>>(reg);
}

} // namespace

namespace protocol::dtc {

DtcEncoder::DtcEncoder(modbus::Mode mode)
  : m_factory{ std::make_unique<modbus::ModbusRequestFactory>(mode) }
{
}

DtcEncoder::~DtcEncoder() = default;

QByteArray
DtcEncoder::encode(const Request& request, int type) const
{
  switch (static_cast<ResponseType>(type)) {
    case DeviceGetCtrlMode:
    case DeviceGetOutMode:
    case DeviceGetSensorType:
    case DeviceGetState:
    case DeviceSetCtrlMode:
    case DeviceSetOutMode:
    case DeviceSetSensorType:
      break;

    case DeviceGetRunMode: {
      const auto* casted{ static_cast<const DeviceRunModeRequest*>(
        request.get()) };
      return m_factory->readDiscreteInputs(
        casted->address,
        register_cast(protocol::dtc::DeviceRunModeRequest::start_address));
    }

    case DeviceSetRunMode: {
      const auto* casted{ static_cast<const DeviceRunModeWrite*>(
        request.get()) };
      return m_factory->writeSingleCoil(
        casted->address, register_cast(casted->start_address), casted->value);
    }

    case HeaterGetCurTemp: {
      const auto* casted{ static_cast<const HeaterCurrentTempRequest*>(
        request.get()) };
      return m_factory->readHoldingRegisters(
        casted->address, register_cast(casted->start_address));
    }

    case HeaterGetTargetTemp: {
      const auto* casted{ static_cast<const HeaterTargetTempRequest*>(
        request.get()) };
      return m_factory->readHoldingRegisters(
        casted->address, register_cast(casted->start_address));
    }

    case HeaterSetTargetTemp: {
      const auto* casted{ static_cast<const HeaterTargetTempWrite*>(
        request.get()) };
      return m_factory->writeSingleRegister(
        casted->address, register_cast(casted->start_address), casted->value);
    }
  }
  return {};
}

} // namespace protocol::dtc
