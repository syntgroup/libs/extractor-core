#ifndef DTC_SYSTEM_RESPONSE_H
#define DTC_SYSTEM_RESPONSE_H

#include "thermocontrollers/protocol/dtc_response.h"

namespace protocol::dtc {

struct SystemCommunicationResponse final : DtcResponse
{
  explicit SystemCommunicationResponse(
    std::unique_ptr<modbus::IPacket>& packet);
  ~SystemCommunicationResponse() override = default;

  uint16_t value{ 0 };
};

} // namespace protocol::dtc

#endif // DTC_SYSTEM_RESPONSE_H
