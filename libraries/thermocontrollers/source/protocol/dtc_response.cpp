#include "thermocontrollers/protocol/dtc_response.h"

#include "modbus/modbus_packet.h"
#include "thermocontrollers/protocol/dtc_response_type.h"

namespace protocol::dtc {

DtcResponse::DtcResponse(std::unique_ptr<modbus::IPacket>& packet)
  : m_packet{ std::move(packet) }
{
}

DtcResponse::~DtcResponse() = default;

auto
DtcResponse::errorString() const -> QString
{
  return m_errorString;
}

auto
DtcResponse::encodedSize() const -> size_t
{
  return m_packet->encode().size();
}

int
DtcResponse::minimumSize(int type)
{
  switch (static_cast<ResponseType>(type)) {
    case DeviceGetCtrlMode:
    case DeviceGetOutMode:
    case DeviceGetSensorType:
    case DeviceGetState:
    case HeaterGetCurTemp:
    case HeaterGetTargetTemp:
      return 7;
    case DeviceSetCtrlMode:
    case DeviceSetOutMode:
    case DeviceSetSensorType:
    case HeaterSetTargetTemp:
      return 8;
    default:
      break;
  }
  return 0;
}

auto
DtcResponse::pdu() const -> modbus::Payload
{
  return m_packet->pdu();
}

int
DtcResponse::errorCode() const
{
  return static_cast<int>(m_packet->status());
}

void
DtcResponse::setErrorString(const QString& error_string)
{
  m_errorString = error_string;
}

} // namespace protocol::dtc
