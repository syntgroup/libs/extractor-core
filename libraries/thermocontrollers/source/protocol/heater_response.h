#ifndef HEATER_RESPONSE_H
#define HEATER_RESPONSE_H

#include "thermocontrollers/device/heater_values.h"
#include "thermocontrollers/protocol/dtc_response.h"

#include <unordered_map>

namespace protocol::dtc {

struct HeaterGetCurTempResponse final : DtcResponse
{
  explicit HeaterGetCurTempResponse(std::unique_ptr<modbus::IPacket>& packet);
  ~HeaterGetCurTempResponse() override = default;

  device::Temperature currentTemperature{ 0.0 };
};

struct HeaterGetTargetTempResponse final : DtcResponse
{
  explicit HeaterGetTargetTempResponse(
    std::unique_ptr<modbus::IPacket>& packet);
  ~HeaterGetTargetTempResponse() override = default;

  device::Temperature targetTemperature{ 0.0 };
};

struct HeaterSetTargetTempResponse final : DtcResponse
{
  explicit HeaterSetTargetTempResponse(
    std::unique_ptr<modbus::IPacket>& packet);
  ~HeaterSetTargetTempResponse() override = default;

  uint16_t dataAddress{ 0 };
  device::Temperature targetTemperature{ 0.0 };
};

} // namespace protocol::dtc

#endif // HEATER_RESPONSE_H
