#ifndef DTC_DEVICE_RESPONSE_H
#define DTC_DEVICE_RESPONSE_H

#include "thermocontrollers/protocol/dtc_response.h"

namespace protocol::dtc {

struct DeviceGetOutModeResponse final : DtcResponse
{
  explicit DeviceGetOutModeResponse(std::unique_ptr<modbus::IPacket>& packet);
  ~DeviceGetOutModeResponse() override = default;

  uint16_t outMode{ 0 };
};

struct DeviceGetCtrlModeResponse final : DtcResponse
{
  explicit DeviceGetCtrlModeResponse(std::unique_ptr<modbus::IPacket>& packet);
  ~DeviceGetCtrlModeResponse() override = default;

  uint16_t ctrlMode{ 0 };
};

struct DeviceGetSensorTypeResponse final : DtcResponse
{
  explicit DeviceGetSensorTypeResponse(
    std::unique_ptr<modbus::IPacket>& packet);
  ~DeviceGetSensorTypeResponse() override = default;

  uint16_t sensorType{ 0 };
};

struct DeviceGetRunModeResponse final : DtcResponse
{
  explicit DeviceGetRunModeResponse(std::unique_ptr<modbus::IPacket>& packet);
  bool runMode{ false };
};

struct DeviceSetRunModeResponse final : DtcResponse
{
  explicit DeviceSetRunModeResponse(std::unique_ptr<modbus::IPacket>& packet);
  bool runMode{ false };
};

} // namespace protocol::dtc

#endif // DTC_DEVICE_RESPONSE_H
