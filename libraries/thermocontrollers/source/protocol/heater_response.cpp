#include "heater_response.h"

#include "modbus/modbus_packet.h"
#include "thermocontrollers/protocol/dtc_error.h"
#include "utils/byte_converter.h"

namespace {
using PvError = protocol::dtc::ErrorPV;
const std::unordered_map<PvError, const char*> error_description{
  { PvError::Init, "INIT" },
  { PvError::NoSensor, "NO SENSOR" },
  { PvError::SenseError, "SENSE ERROR" },
  { PvError::LimitSensor, "LIMIT ERROR" },
  { PvError::EEPROMError, "EEPROM LITERAL" }
};
} // namespace

namespace protocol::dtc {

HeaterGetCurTempResponse::HeaterGetCurTempResponse(
  std::unique_ptr<modbus::IPacket>& packet)
  : DtcResponse{ packet }
{
  if (isError()) {
    setError(true);
    setErrorString(QString::number(errorCode(), 16));
    return;
  }

  const auto encoded{ pdu().at(DataValue) };
  const auto temperature_read_error{ encoded >=
                                     static_cast<uint16_t>(ErrorPV::Init) };
  if (temperature_read_error) {
    setError(true);
    setErrorString(error_description.at(static_cast<ErrorPV>(encoded)));
    return;
  }

  currentTemperature = utils::dtcShortToDouble<device::Temperature>(encoded);
}

HeaterGetTargetTempResponse::HeaterGetTargetTempResponse(
  std::unique_ptr<modbus::IPacket>& packet)
  : DtcResponse{ packet }
{
  if (isError()) {
    setError(true);
    setErrorString(QString::number(errorCode(), 16));
    return;
  }

  const auto encoded{ pdu().at(DataValue) };
  targetTemperature = device::Temperature{ utils::dtcShortToDouble(encoded) };
}

HeaterSetTargetTempResponse::HeaterSetTargetTempResponse(
  std::unique_ptr<modbus::IPacket>& packet)
  : DtcResponse{ packet }
{
  if (isError() || pdu().empty()) {
    setError(true);
    setErrorString(QString::number(errorCode(), 16));
    return;
  }

  const auto encoded{ pdu().at(DataValue) };
  targetTemperature = device::Temperature{ utils::dtcShortToDouble(encoded) };
}

} // namespace protocol::dtc
