#include "device_response.h"

#include "modbus/modbus_packet.h"
#include <bitset>

namespace protocol::dtc {

DeviceGetOutModeResponse::DeviceGetOutModeResponse(
  std::unique_ptr<modbus::IPacket>& packet)
  : DtcResponse{ packet }
{
  if (!isError()) {
    outMode = pdu().at(DataValue);
  } else {
    setError(true);
    setErrorString(QString::number(errorCode(), 16));
  }
}

DeviceGetCtrlModeResponse::DeviceGetCtrlModeResponse(
  std::unique_ptr<modbus::IPacket>& packet)
  : DtcResponse{ packet }
{
  if (!isError()) {
    ctrlMode = pdu().at(DataValue);
  } else {
    setError(true);
    setErrorString(QString::number(errorCode(), 16));
  }
}

DeviceGetSensorTypeResponse::DeviceGetSensorTypeResponse(
  std::unique_ptr<modbus::IPacket>& packet)
  : DtcResponse{ packet }
{
  if (!isError()) {
    sensorType = pdu().at(DataValue);
  } else {
    setError(true);
    setErrorString(QString::number(errorCode(), 16));
  }
}

DeviceGetRunModeResponse::DeviceGetRunModeResponse(
  std::unique_ptr<modbus::IPacket>& packet)
  : DtcResponse{ packet }
{
  if (isError()) {
    setError(true);
    setErrorString(QString::number(errorCode(), 16));
    return;
  }
  std::bitset<std::numeric_limits<uint16_t>::digits> status{ pdu().at(
    DataValue) };
  runMode = status.test(0);
}

DeviceSetRunModeResponse::DeviceSetRunModeResponse(
  std::unique_ptr<modbus::IPacket>& packet)
  : DtcResponse{ packet }
{
  if (isError()) {
    setError(true);
    setErrorString(QString::number(errorCode(), 16));
    return;
  }

  const auto value{ pdu().at(DataValue) };
  runMode = value == 0xff00 ? true : false;
}

} // namespace protocol::dtc
