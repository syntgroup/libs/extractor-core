#include "thermocontrollers/cmd/set_run_mode_operation.h"

#include "protocol/device_request.h"
#include "protocol/device_response.h"
#include "thermocontrollers/device/heater.h"
#include "thermocontrollers/protocol/dtc_response_type.h"

namespace cmd::dtc {

SetRunModeOperation::SetRunModeOperation(protocol::PacketID pid,
                                         gsl::not_null<device::Heater*> heater,
                                         bool state,
                                         QObject* parent)
  : AbstractDtcOperation{ pid, parent }
  , m_heater{ heater }
  , m_newState{ state }
{
}

QString
SetRunModeOperation::description() const
{
  return QStringLiteral("Setting run mode on heater adr 0x%1")
    .arg(m_heater->address());
}

int
SetRunModeOperation::responseType() const
{
  return protocol::dtc::ResponseType::DeviceSetRunMode;
}

std::unique_ptr<protocol::RequestInterface>
SetRunModeOperation::createRequest() const
{
  return std::make_unique<TRequest>(m_heater->address(), m_newState);
}

bool
SetRunModeOperation::processResponse(const Response& t_response)
{
  const auto* response{ dynamic_cast<TResponse*>(t_response.get()) };
  m_heater->setRunning(response->runMode ? device::RunMode::Run
                                         : device::RunMode::Stop);
  return !response->isError();
}

} // namespace cmd::dtc
