#include "thermocontrollers/cmd/get_run_mode_operation.h"

#include "protocol/device_request.h"
#include "protocol/device_response.h"
#include "thermocontrollers/device/heater.h"
#include "thermocontrollers/protocol/dtc_response_type.h"

namespace cmd::dtc {

GetRunModeOperation::GetRunModeOperation(protocol::PacketID pid,
                                         gsl::not_null<device::Heater*> heater,
                                         QObject* parent)
  : AbstractDtcOperation{ pid, parent }
  , m_heater{ heater }
{
}

QString
GetRunModeOperation::description() const
{
  return QStringLiteral("Requesting run mode on heater adr 0x%1")
    .arg(m_heater->address());
}

int
GetRunModeOperation::responseType() const
{
  return protocol::dtc::ResponseType::DeviceGetRunMode;
}

AbstractDtcOperation::Request
GetRunModeOperation::createRequest() const
{
  return std::make_unique<TRequest>(m_heater->address());
}

bool
GetRunModeOperation::processResponse(const Response& t_response)
{
  const auto* response{ dynamic_cast<TResponse*>(t_response.get()) };

  m_heater->setRunning(response->runMode ? device::RunMode::Run
                                         : device::RunMode::Stop);
  return !response->isError();
}

} // namespace cmd::dtc
