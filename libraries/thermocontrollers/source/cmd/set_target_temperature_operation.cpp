#include "thermocontrollers/cmd/set_target_temperature_operation.h"

#include "protocol/heater_request.h"
#include "thermocontrollers/device/heater.h"
#include "thermocontrollers/protocol/dtc_response_type.h"

namespace cmd::dtc {

SetTargetTemperatureOperation::SetTargetTemperatureOperation(
  protocol::PacketID pid,
  gsl::not_null<device::Heater*> heater,
  device::Temperature target_temperature,
  QObject* parent)
  : AbstractDtcOperation{ pid, parent }
  , m_heater{ heater }
  , m_target_temperature{ std::move(target_temperature) }
{
}

QString
SetTargetTemperatureOperation::description() const
{
  return QStringLiteral(
           "Setting target temperature on heater adr 0x%1 of value %2")
    .arg(QString::number(m_heater->address(), 16),
         QString::number(m_target_temperature.value_of()));
}

std::unique_ptr<protocol::RequestInterface>
SetTargetTemperatureOperation::createRequest() const
{
  return std::make_unique<TRequest>(m_heater->address(),
                                    m_target_temperature.value_of());
}

int
SetTargetTemperatureOperation::responseType() const
{
  return protocol::dtc::ResponseType::HeaterSetTargetTemp;
}

} // namespace cmd::dtc
