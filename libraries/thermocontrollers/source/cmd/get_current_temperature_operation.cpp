#include "thermocontrollers/cmd/get_current_temperature_operation.h"

#include "protocol/heater_request.h"
#include "protocol/heater_response.h"
#include "thermocontrollers/device/heater.h"
#include "thermocontrollers/protocol/dtc_response_type.h"

namespace cmd::dtc {

GetCurrentTemperatureOperation::GetCurrentTemperatureOperation(
  protocol::PacketID pid,
  gsl::not_null<device::Heater*> heater,
  QObject* parent)
  : AbstractDtcOperation{ pid, parent }
  , m_heater{ heater }
{
}

QString
GetCurrentTemperatureOperation::description() const
{
  return QStringLiteral("Requesting current temperature on heater adr %1")
    .arg(m_heater->address());
}

std::unique_ptr<protocol::RequestInterface>
GetCurrentTemperatureOperation::createRequest() const
{
  return std::make_unique<TRequest>(m_heater->address());
}

int
GetCurrentTemperatureOperation::responseType() const
{
  return protocol::dtc::ResponseType::HeaterGetCurTemp;
}

bool
GetCurrentTemperatureOperation::processResponse(const Response& t_response)
{
  const auto* response{ dynamic_cast<TResponse*>(t_response.get()) };
  m_heater->setCurrentTemperature(response->currentTemperature);

  return !response->isError();
}

} // namespace cmd::dtc
