#include "thermocontrollers/cmd/get_target_temperature_operation.h"

#include "protocol/heater_request.h"
#include "protocol/heater_response.h"
#include "thermocontrollers/device/heater.h"
#include "thermocontrollers/protocol/dtc_response_type.h"

namespace cmd::dtc {

GetTargetTemperatureOperation::GetTargetTemperatureOperation(
  protocol::PacketID pid,
  gsl::not_null<device::Heater*> heater,
  QObject* parent)
  : AbstractDtcOperation{ pid, parent }
  , m_heater{ heater }
{
}

QString
GetTargetTemperatureOperation::description() const
{
  return QStringLiteral("Requesting target temperature on heater adr 0x%1")
    .arg(m_heater->address());
}

AbstractDtcOperation::Request
GetTargetTemperatureOperation::createRequest() const
{
  return std::make_unique<TRequest>(m_heater->address());
}

bool
GetTargetTemperatureOperation::processResponse(const Response& t_response)
{
  const auto* response{ dynamic_cast<TResponse*>(t_response.get()) };
  m_heater->setTargetTemperature(response->targetTemperature);
  return !response->isError();
}

int
GetTargetTemperatureOperation::responseType() const
{
  return protocol::dtc::ResponseType::HeaterGetTargetTemp;
}

} // namespace cmd::dtc
