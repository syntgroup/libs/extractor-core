#include "thermocontrollers/cmd/multi_set_temp_and_wait_operation.h"

#include "thermocontrollers/cmd/get_current_temperature_operation.h"
#include "thermocontrollers/cmd/get_target_temperature_operation.h"
#include "thermocontrollers/cmd/set_target_temperature_operation.h"
#include "thermocontrollers/device/heater.h"
#include <cmath>

namespace {
constexpr auto epsilon{ 1.0 };

template<typename T>
[[nodiscard]] inline constexpr auto
equals(const T lhs, const T rhs)
{
  return std::fabs(rhs.value_of() - lhs.value_of()) < epsilon;
}
} // namespace

namespace cmd::multi {

/*!
 * \brief SetTempAndWaitOperation::SetTempAndWaitOperation
 * \param heater
 * \param target
 * \param tolerance
 * \param parent
 *
 * \note так как мы ТЕПЕРЬ создаем операции прям по месту вызова в протоколе,
 * __наверное__ будет корректно в конструкторе обратиться к heater и спросить у
 * него текущую целевую температуру
 *
 */
SetTempAndWaitOperation::SetTempAndWaitOperation(
  gsl::not_null<device::Heater*> heater,
  device::Temperature target,
  device::Temperature tolerance,
  QObject* parent)
  : AbstractMultiStageOperation{ parent }
  , m_heater{ heater }
  , m_target{ std::move(target) }
  , m_tolerance{ std::move(tolerance) }
  , m_action{ m_target > m_heater->targetTemperature() ? Action::Heating
                                                       : Action::Cooling }
{
}

QString
SetTempAndWaitOperation::description() const
{
  const auto heater_idx         = QString::number(m_heater->address(), 16);
  const auto target_temperature = QString::number(m_target.value_of());
  const auto description =
    QStringLiteral("MULTI Wait Heater %1 to heat up to %2")
      .arg(heater_idx, target_temperature);

  return (operationState() == Ready)
           ? description
           : description + " // " + currentOperation()->description();
}

void
SetTempAndWaitOperation::writeTargetTemperature(device::Temperature target,
                                                bool to_finish)
{
  using Operation = dtc::SetTargetTemperatureOperation;
  if (to_finish) {
    registerSubOperation<Operation, execution::DeadPolicy>(m_heater, target);
  } else {
    registerSubOperation<Operation>(m_heater, target);
  }
}

void
SetTempAndWaitOperation::requestTargetTemperature()
{
  registerSubOperation<dtc::GetTargetTemperatureOperation>(m_heater);
}

void
SetTempAndWaitOperation::requestCurrentTemperature()
{
  registerSubOperation<dtc::GetCurrentTemperatureOperation>(m_heater);
}

bool
SetTempAndWaitOperation::targetReached() const
{
  // текущая температура УЖЕ ниже, чем уставка + погрешность
  if (m_action == Action::Cooling &&
      m_heater->currentTemperature() < (m_target + m_tolerance)) {
    return true;
  }
  // текущая температура УЖЕ выше, чем уставка - погрешность
  if (m_action == Action::Heating &&
      m_heater->currentTemperature() > (m_target - m_tolerance)) {
    return true;
  }
  // а тут проверяем фактическое достижение зоны толерантности
  const auto adjusted_target = m_action == Action::Cooling
                                 ? m_target + m_tolerance
                                 : m_target - m_tolerance;
  return equals(adjusted_target, m_heater->currentTemperature());
}

void
SetTempAndWaitOperation::onSubOperationError(
  const AbstractSerialOperation& /*operation*/)
{
}

void
SetTempAndWaitOperation::nextStateLogic()
{
  switch (operationState()) {
    case AbstractOperation::Ready:
      setOperationState(SettingNewTargetTemperature);
      writeTargetTemperature(m_target);
      break;

    case SettingNewTargetTemperature:
      setOperationState(RequestNewTargetTemperature);
      requestTargetTemperature();
      break;

    case RequestNewTargetTemperature: {
      static auto retryCount = 3;
      // проверяем, записалась ли новая уставка. Если нет, завершаемся с ошибкой
      // (13/04/2024) надо бы докинуть счетчик повторов записей, хотя бы 2-3
      // раза
      const bool written = equals(m_heater->targetTemperature(), m_target);
      if (written) {
        setOperationState(RequestCurrentTemperature);
        requestCurrentTemperature();
      } else if (retryCount != 0) {
        setOperationState(SettingNewTargetTemperature);
        writeTargetTemperature(m_target);
        --retryCount;
      } else {
        finishWithError(BackendError::ProtocolError,
                        QStringLiteral("failed to set new target temperature"));
      }
      break;
    }

    case RequestCurrentTemperature: {
      if (!targetReached()) {
        requestCurrentTemperature();
      } else {
        finish();
      }
      break;
    }

    case Stop:
      setOperationState(Idle);
      writeTargetTemperature(m_heater->currentTemperature(),
                             /*to_finish=*/true);
      break;

    case Idle:
      break;

    default:
      finishWithError(
        BackendError::UnknownError,
        QStringLiteral(
          "Operation %1 got to unknown state, have to terminate here")
          .arg(description()));
      break;
  }
}

void
SetTempAndWaitOperation::pause()
{
  if (operationState() == AbstractOperation::Finished) {
    return;
  }

  m_buffer = static_cast<OperationState>(operationState());
  setOperationState(Stop);
}

void
SetTempAndWaitOperation::resume()
{
  if (operationState() == AbstractOperation::Finished) {
    return;
  }

  if (m_buffer == RequestCurrentTemperature && !targetReached()) {
    setOperationState(Ready);
  } else {
    setOperationState(m_buffer);
  }

  advanceOperationState();
}

} // namespace cmd::multi
