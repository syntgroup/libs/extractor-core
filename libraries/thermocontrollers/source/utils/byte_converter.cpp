/*!
 * @file byte_converter.cpp
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 */

#include "byte_converter.h"

double
utils::dtcShortToDouble(uint16_t value)
{
  return static_cast<double>(value) / 10.0;
}

uint16_t
utils::dtcDoubleToShort(double value)
{
  return static_cast<uint16_t>(value * 10.0);
}
