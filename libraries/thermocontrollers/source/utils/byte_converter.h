/*!
 * @file byte_converter.h
 * @author Timur Mayzenberg <t.mayzenberg@lambda-it.ru>
 * @brief This file contains converters for DTC1000 values
 */

#ifndef THERMOCONTROLLERS_BYTE_CONVERTER_H
#define THERMOCONTROLLERS_BYTE_CONVERTER_H

#include <cstdint>
#include <strong_type/strong_type.hpp>

namespace utils {

/*!
 * \brief dtcShortToDouble
 * \param value
 * \return
 */
[[nodiscard]] auto
dtcShortToDouble(uint16_t value) -> double;

/*!
 * \brief dtcDoubleToShort
 * \param value
 * \return
 */
[[nodiscard]] auto
dtcDoubleToShort(double value) -> uint16_t;

/*!
 * \brief dtcShortToDouble: templated overload to convert strongly typed values
 * \param value
 */
template<typename T>
[[nodiscard]] constexpr auto
dtcShortToDouble(uint16_t value)
{
  return T{ static_cast<strong::underlying_type_t<T>>(value) / 10.0 };
}

} // namespace utils

#endif // THERMOCONTROLLERS_BYTE_CONVERTER_H
