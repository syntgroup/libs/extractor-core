#include <gtest/gtest.h>

#include "modbus/modbus_request_factory.h"

namespace {
const protocol::modbus::ModbusRequestFactory FACTORY;
} // namespace

using namespace protocol;

TEST(TestModbus, CreateModbusRtuReadRegistersPacket)
{
  constexpr static uint8_t address{ 0x15 };
  constexpr static uint16_t count{ 1 };
  struct Subject
  {
    uint16_t reg;
    QByteArray expected;
  };
  const std::array subjects{
    Subject{ 0x1000, QByteArray::fromHex("15031000000183de") },
    Subject{ 0x1001, QByteArray::fromHex("150310010001d21e") },
    Subject{ 0x1005, QByteArray::fromHex("15031005000193df") }
  };

  for (auto&& subject : subjects) {
    const auto packet{ FACTORY.readHoldingRegisters(
      address, subject.reg, count) };
    EXPECT_EQ(packet, subject.expected);
  }
}

TEST(TestModbus, CreateModbusRtuWriteSingleRegisterPacket)
{
  constexpr static uint8_t address{ 0x15 };
  struct Subject
  {
    uint16_t reg;
    uint16_t value;
    QByteArray expected;
  };
  const std::array subjects{
    Subject{ 0x1001, 0x1234, QByteArray::fromHex("150610011234d2a9") },
    Subject{ 0x1005, 0x12, QByteArray::fromHex("1506100500121e12") },
    Subject{ 0x1005, 0x2, QByteArray::fromHex("1506100500021fde") }
  };

  for (auto&& subject : subjects) {
    const auto packet{ FACTORY.writeSingleRegister(
      address, subject.reg, subject.value) };
    EXPECT_EQ(packet, subject.expected);
  }
}

TEST(TestModbus, CreateModbusRtuWriteSingleCoilPacket)
{
  constexpr static uint8_t address{ 0xf };
  struct Subject
  {
    uint16_t reg;
    bool value;
    QByteArray expected;
  };
  const std::array subjects{
    Subject{ 0x0814, false, QByteArray::fromHex("0f05081400008e80") },
    Subject{ 0x0814, true, QByteArray::fromHex("0f050814ff00cf70") }
  };
  for (auto&& subject : subjects) {
    const auto packet{ FACTORY.writeSingleCoil(
      address, subject.reg, subject.value) };

    EXPECT_EQ(packet, subject.expected);
  }
}

TEST(TestModbus, CreateModbusRtuReadSingleCoilPacket)
{
  constexpr static uint8_t address{ 0xf };
  struct Subject
  {
    uint16_t reg;
    uint8_t value;
    QByteArray expected;
  };
  const std::array subjects{
    Subject{ 0x0814, 1, QByteArray::fromHex("0f0208140001fa80") },
    Subject{ 0x0813, 4, QByteArray::fromHex("0f02081300048b42") }
  };

  for (auto&& subject : subjects) {
    const auto packet{ FACTORY.readDiscreteInputs(
      address, subject.reg, subject.value) };
    EXPECT_EQ(packet, subject.expected);
  };
}
