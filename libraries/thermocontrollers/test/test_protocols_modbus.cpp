#include <gtest/gtest.h>

#include "modbus/modbus_request_factory.h"

namespace {
const protocol::modbus::ModbusRequestFactory factory_rtu;
constexpr uint8_t address{ 0x0a };
} // namespace

TEST(TestModbus, TestModbusRTUReadRegistersCtrlMethod)
{
  struct Subject
  {
    uint16_t reg;
    uint16_t mode;
    QByteArray expected;
  };
  const std::array subjects{ Subject{
    0x1005, 0x0001, QByteArray::fromHex("0a031005000191b0") } };

  for (auto&& subject : subjects) {
    const auto packet{ factory_rtu.readHoldingRegisters(
      address, subject.reg, subject.mode) };

    EXPECT_EQ(packet, subject.expected);
  }
}

TEST(TestModbus, TestModbusRTUWriteSingleRegisterCtrlMethod)
{
  struct Subject
  {
    uint16_t reg;
    uint16_t mode;
    QByteArray expected;
  };
  const std::array subjects{
    Subject{ 0x1005, 0x0002, QByteArray::fromHex("0a06100500021db1") },
  };

  for (auto&& subject : subjects) {
    const auto packet{ factory_rtu.writeSingleRegister(
      address, subject.reg, subject.mode) };

    EXPECT_EQ(packet, subject.expected);
  }
}
