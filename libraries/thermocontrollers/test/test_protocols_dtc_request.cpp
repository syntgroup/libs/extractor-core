#include <gtest/gtest.h>

#include "modbus/modbus_request_factory.h"
#include "thermocontrollers/protocol/cooler_request.h"
#include "thermocontrollers/protocol/device_request.h"
#include "thermocontrollers/protocol/heater_request.h"
#include "thermocontrollers/protocol/system_request.h"
#include <QByteArray>

using namespace protocol;

namespace {
constexpr auto slave_adr{ 0xf };

const modbus::ModbusRequestFactory FACTORY_RTU;
const modbus::ModbusRequestFactory FACTORY_ASCII{ modbus::Mode::Ascii };

template<typename T>
[[nodiscard]]
typename std::enable_if_t<std::is_enum_v<T>, std::underlying_type_t<T>>
register_cast(T reg)
{
  return static_cast<std::underlying_type_t<T>>(reg);
}

template<typename T>
[[nodiscard]] auto
rtu_read_register(const T& request)
{
  return FACTORY_RTU.readHoldingRegisters(
    request.address, register_cast(request.start_address), request.count);
}

template<typename T>
[[nodiscard]] auto
ascii_read_register(const T& request)
{
  return FACTORY_ASCII.readHoldingRegisters(
    request.address, register_cast(request.start_address));
}

template<typename T>
[[nodiscard]] auto
rtu_write_register(const T& request)
{
  return FACTORY_RTU.writeSingleRegister(
    request.address, register_cast(request.start_address), request.value);
}

template<typename T>
[[nodiscard]] auto
ascii_write_register(const T& request)
{
  return FACTORY_ASCII.writeSingleRegister(
    request.address, register_cast(request.start_address), request.value);
}
} // namespace

TEST(TestThermocontrollers, CreateDtcCtrlModeRequest)
{
  // 10:05:00:01
  const QByteArray result_rtu{ QByteArray::fromHex("0f031005000191e5") };
  const QByteArray result_ascii{ ":0f0310050001d8\r\n" };

  const dtc::DeviceCtrlModeRequest request{ slave_adr };

  EXPECT_EQ(FACTORY_RTU.readHoldingRegisters(
              request.address, register_cast(request.start_address)),
            result_rtu);

  EXPECT_EQ(FACTORY_ASCII.readHoldingRegisters(
              request.address, register_cast(request.start_address)),
            result_ascii);
}

TEST(TestThermocontrollers, CreateDtcSensorTypeRequest)
{
  // 10:04:00:01
  const QByteArray result_rtu{ QByteArray::fromHex("0f0310040001c025") };
  const QByteArray result_ascii{ ":0f0310040001d9\r\n" };

  const dtc::DeviceSensorTypeRequest request{ slave_adr };
  EXPECT_EQ(FACTORY_RTU.readHoldingRegisters(
              request.address, register_cast(request.start_address)),
            result_rtu);

  EXPECT_EQ(FACTORY_ASCII.readHoldingRegisters(
              request.address, register_cast(request.start_address)),
            result_ascii);
}

TEST(TestThermocontrollers, CreateDtcGetOutModeRequest)
{
  const std::array subjectss{
    std::make_tuple(1,                                       // out no
                    QByteArray::fromHex("0f031069000151f8"), // rtu
                    QByteArray{ ":0f031069000174\r\n" }),    // asci
    std::make_tuple(2,
                    QByteArray::fromHex("0f03106a0001a1f8"),
                    QByteArray{ ":0f03106a000173\r\n" })
  };

  for (auto&& subject : subjectss) {
    const dtc::DeviceOutModeRequest request{
      slave_adr, static_cast<uint8_t>(std::get<0>(subject))
    };
    EXPECT_EQ(rtu_read_register(request), std::get<1>(subject));
    EXPECT_EQ(ascii_read_register(request), std::get<2>(subject));
  }
}

TEST(TestThermocontrollers, CreateDtcAl2HRequest)
{
  // 10:04:00:01
  const auto request_expected{ QByteArray::fromHex("0f0310260001602f") };
  const dtc::DeviceAL2HRequest request{ slave_adr };

  EXPECT_EQ(FACTORY_RTU.readHoldingRegisters(
              request.address, register_cast(request.start_address)),
            request_expected);
}

TEST(TestThermocontrollers, CreateDtcStateRequest)
{
  // 10:04:00:01
  const auto request_expected{ QByteArray::fromHex("0f03102a0001a02c") };
  const dtc::DeviceStateRequest request{ slave_adr };

  EXPECT_EQ(FACTORY_RTU.readHoldingRegisters(
              request.address, register_cast(request.start_address)),
            request_expected);
}

TEST(TestThermocontrollers, CreateDtcGetCurrentTemperatureRequest)
{
  // 10:00:00:01
  const auto request_expected{ QByteArray::fromHex("0f031000000181e4") };
  const dtc::HeaterCurrentTempRequest request{ slave_adr };

  EXPECT_EQ(FACTORY_RTU.readHoldingRegisters(
              request.address, register_cast(request.start_address)),
            request_expected);
}

TEST(TestThermocontrollers, CreateDtcGetTargetTemperatureRequest)
{
  // 10:01:00:01
  const dtc::HeaterTargetTempRequest request{ slave_adr };

  EXPECT_EQ(rtu_read_register(request),
            QByteArray::fromHex("0f0310010001d024"));
}

TEST(TestThermocontrollers, CreateDtcCoolerTurnOn)
{
  const std::array requests{
    std::make_tuple(dtc::CoolerEnableRequest::Mode::Enabled,
                    QByteArray::fromHex("0f06102600006def")), // 10:26:00:00
    std::make_tuple(dtc::CoolerEnableRequest::Mode::Disabled,
                    QByteArray::fromHex("0f06102603e86d51")) // 10:26:03:e8
  };
  for (auto&& data : requests) {
    const dtc::CoolerEnableRequest request{
      slave_adr, std::get<dtc::CoolerEnableRequest::Mode>(data)
    };
    EXPECT_EQ(rtu_write_register(request), std::get<QByteArray>(data));
  }
}

TEST(TestThermocontrollers, CreateDtcCommunicationAddressRequest)
{
  // 10:71:00:01
  const auto request_expected{ QByteArray::fromHex("0f0310710001d1ff") };
  const dtc::SystemCommunicationAddressRequest request{ slave_adr };

  EXPECT_EQ(rtu_read_register(request), request_expected);
}

TEST(TestThermocontrollers, CreateDtcGetHeaterRangeHighRequest)
{
  const QByteArray bytes_rtu{ QByteArray::fromHex("0f03100200012024") };
  const QByteArray bytes_ascii{ ":0f0310020001db\r\n" };

  const dtc::HeaterRangeHighRequest request{ slave_adr };

  EXPECT_EQ(rtu_read_register(request), bytes_rtu);
  EXPECT_EQ(ascii_read_register(request), bytes_ascii);
}

TEST(TestThermocontrollers, CreateDtcSetHeaterRangeHighRequest)
{
  const QByteArray bytes_rtu{ QByteArray::fromHex("0f06100201f6ac32") };
  const QByteArray bytes_ascii{ ":0f06100201f6e2\r\n" };

  constexpr auto new_high{ 50.2 };
  const dtc::HeaterRangeHighWrite request{ slave_adr, new_high };

  EXPECT_EQ(rtu_write_register(request), bytes_rtu);
  EXPECT_EQ(ascii_write_register(request), bytes_ascii);
}

TEST(TestThermocontrollers, CreateDtcGetHeaterRangeLowRequest)
{
  const QByteArray bytes_rtu{ QByteArray::fromHex("0f031003000171e4") };
  const QByteArray bytes_ascii{ ":0f0310030001da\r\n" };

  const dtc::HeaterRangeLowRequest request{ slave_adr };

  EXPECT_EQ(rtu_read_register(request), bytes_rtu);
  EXPECT_EQ(ascii_read_register(request), bytes_ascii);
}

TEST(TestThermocontrollers, CreateDtcSetHeaterRangeLowRequest)
{
  const QByteArray bytes_rtu{ QByteArray::fromHex("0f06100301f6fdf2") };
  const QByteArray bytes_ascii{ ":0f06100301f6e1\r\n" };

  constexpr auto new_low{ 50.2 };
  const dtc::HeaterRangeLowWrite request{ slave_adr, new_low };

  EXPECT_EQ(rtu_write_register(request), bytes_rtu);
  EXPECT_EQ(ascii_write_register(request), bytes_ascii);
}

TEST(TestThermocontrollers, CreateDtcSetRunRequest)
{
  const std::array subjects{
    std::make_tuple(true, QByteArray::fromHex("0f050814ff00cf70")),
    std::make_tuple(false, QByteArray::fromHex("0f05081400008e80"))
  };
  for (auto&& subject : subjects) {
    const dtc::DeviceRunModeWrite request{ slave_adr, std::get<bool>(subject) };
    EXPECT_EQ(FACTORY_RTU.writeSingleCoil(request.address,
                                          register_cast(request.start_address),
                                          request.value),
              std::get<QByteArray>(subject));
  }
}

TEST(TestThermocontrollers, CreateDtcRunModeRequest)
{
  const dtc::DeviceRunModeRequest request{ slave_adr };

  EXPECT_EQ(FACTORY_RTU.readDiscreteInputs(request.address,
                                           register_cast(request.start_address),
                                           request.count),
            QByteArray::fromHex("0f0208140001fa80"));
}
