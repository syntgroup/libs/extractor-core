#include <gtest/gtest.h>

#include "modbus/modbus_packet.h"
#include "modbus/modbus_response_ascii.h"
#include "modbus/modbus_response_rtu.h"
#include "thermocontrollers/device/heater_values.h"
#include "thermocontrollers/protocol/device_response.h"
#include "thermocontrollers/protocol/heater_response.h"
#include <cmath>

namespace {
const protocol::modbus::RtuResponseFactory factory_rtu;
const protocol::modbus::AsciiResponseFactory factory_ascii;

constexpr auto error{
  std::numeric_limits<device::Temperature>::epsilon().value_of()
};
} // namespace

TEST(TestThermocontrollers, DtcFailOnEmptyPacket)
{
  const std::array packets{ factory_ascii.create({}), factory_rtu.create({}) };
  ASSERT_EQ(packets.size(), 2);

  for (auto&& packet : packets) {
    ASSERT_FALSE(packet.has_value());
    EXPECT_EQ(packet.error(), protocol::modbus::ExceptionCode::UnknownError);
  }
}

TEST(TestThermocontrollers, DtcFailOnCorruptedPacket)
{
  const auto raw_bytes{ QByteArray::fromHex("0f8309e0f5") };
  const auto packet{ factory_rtu.create(raw_bytes) };
  ASSERT_FALSE(packet.has_value());
  EXPECT_EQ(packet.error(), protocol::modbus::ExceptionCode::IllegalDataValue);

  const QByteArray bytes_ascii{ ":0f8309e0f5\r\n" };
  const auto packet_ascii{ factory_ascii.create(bytes_ascii) };
  ASSERT_FALSE(packet_ascii.has_value());
  EXPECT_EQ(packet.error(), protocol::modbus::ExceptionCode::IllegalDataValue);
}

TEST(TestThermocontrollers, DecodeResponseWithWrongCrc)
{
  const auto raw_bytes{ QByteArray::fromHex("15030200020034") };
  const auto packet{ factory_rtu.create(raw_bytes) };
  ASSERT_FALSE(packet.has_value());
  EXPECT_EQ(packet.error(), protocol::modbus::ExceptionCode::CrcError);

  const QByteArray bytes_ascii{ ":15030200020034\r\n" };
  const auto packet_ascii{ factory_ascii.create(bytes_ascii) };
  ASSERT_FALSE(packet_ascii.has_value());
  EXPECT_EQ(packet_ascii.error(), protocol::modbus::ExceptionCode::CrcError);
}

TEST(TestThermocontrollers, DtcResponsePublicAPI)
{
  const auto raw_bytes{ QByteArray::fromHex("0000000") };
  const auto packet{ factory_rtu.create(raw_bytes) };
  ASSERT_FALSE(packet.has_value());
}

TEST(TestThermocontrollers, DecodingDtcCtrlModeResponse)
{
  const auto bytes_rtu{ QByteArray::fromHex("0f03020000d185") };
  auto packet_rtu{ factory_rtu.create(bytes_rtu) };
  ASSERT_TRUE(packet_rtu.has_value());

  const protocol::dtc::DeviceGetCtrlModeResponse response_rtu{
    packet_rtu.value()
  };

  EXPECT_EQ(static_cast<device::ControlMode>(response_rtu.ctrlMode),
            device::ControlMode::PID)
    << response_rtu.errorString().toStdString();

  const QByteArray bytes_ascii{ ":0f03020000ec\r\n" };
  auto packet_ascii{ factory_ascii.create(bytes_ascii) };
  ASSERT_TRUE(packet_ascii.has_value());

  const protocol::dtc::DeviceGetCtrlModeResponse response_ascii{
    packet_ascii.value()
  };
  EXPECT_EQ(static_cast<device::ControlMode>(response_ascii.ctrlMode),
            device::ControlMode::PID);
  EXPECT_FALSE(response_ascii.isError())
    << response_ascii.errorString().toStdString();
}

TEST(TestThermocontrollers, DecodingDtcSensorTypeResponse)
{
  const auto raw_bytes{ QByteArray::fromHex("0f0302000cd180") };
  auto packet{ factory_rtu.create(raw_bytes) };
  ASSERT_TRUE(packet);

  const protocol::dtc::DeviceGetSensorTypeResponse response{ packet.value() };
  EXPECT_EQ(static_cast<device::SensorType>(response.sensorType),
            device::SensorType::Pt100)
    << response.errorString().toStdString();
}

TEST(TestThermocontrollers, DecodingDtcCurTempResponse)
{
  constexpr device::Temperature subject{ 67.9 };
  const auto raw_bytes{ QByteArray::fromHex("0f030202a7915f") };
  auto packet{ factory_rtu.create(raw_bytes) };
  ASSERT_TRUE(packet);

  const protocol::dtc::HeaterGetCurTempResponse response{ packet.value() };
  EXPECT_FALSE(response.isError());
  EXPECT_NEAR(response.currentTemperature.value_of(), subject.value_of(), error)
    << response.errorString().toStdString();
}

TEST(TestThermocontrollers, DecodingDtcCurTemperatureResponseErrorNoSensor)
{
  constexpr device::Temperature subject{ 50 };
  const auto raw_bytes{ QByteArray::fromHex("0f03028003f044") };
  auto packet{ factory_rtu.create(raw_bytes) };
  ASSERT_TRUE(packet);

  const protocol::dtc::HeaterGetCurTempResponse response{ packet.value() };
  EXPECT_TRUE(response.isError());
  EXPECT_EQ(response.errorString(), "NO SENSOR");
}

TEST(TestThermocontrollers, DecodeDtcTargetTemperatureWriteResponse)
{
  constexpr device::Temperature subject_1{ 80 };
  constexpr device::Temperature subject_2{ 100 };
  const auto bytes_rtu{ QByteArray::fromHex("010610010320dde2") };
  auto packet_rtu{ factory_rtu.create(bytes_rtu) };
  ASSERT_TRUE(packet_rtu.has_value());

  const protocol::dtc::HeaterSetTargetTempResponse rtu_response{
    packet_rtu.value()
  };
  EXPECT_NEAR(
    rtu_response.targetTemperature.value_of(), subject_1.value_of(), error);

  const QByteArray bytes_ascii{ ":0106100103e8fd\r\n" };
  auto packet_ascii{ factory_ascii.create(bytes_ascii) };
  ASSERT_TRUE(packet_ascii.has_value());

  const protocol::dtc::HeaterSetTargetTempResponse ascii_response{
    packet_ascii.value()
  };
  EXPECT_NEAR(
    ascii_response.targetTemperature.value_of(), subject_2.value_of(), error);
}

TEST(TestThermocontrollers, DecodeDtcTargetTemperatureReadResponse)
{
  constexpr device::Temperature subject_1{ 80 };
  constexpr device::Temperature subject_2{ 100 };
  const auto bytes_rtu{ QByteArray::fromHex("010610010320dde2") };
  auto packet_rtu{ factory_rtu.create(bytes_rtu) };
  ASSERT_TRUE(packet_rtu);

  const protocol::dtc::HeaterGetTargetTempResponse rtu_response{
    packet_rtu.value()
  };
  EXPECT_NEAR(rtu_response.targetTemperature.value_of(),
              subject_1.value_of(),
              std::numeric_limits<device::Temperature>::epsilon().value_of());

  const QByteArray bytes_ascii{ ":0106100103e8fd\r\n" };
  auto packet_ascii{ factory_ascii.create(bytes_ascii) };
  ASSERT_TRUE(packet_ascii);

  const protocol::dtc::HeaterGetTargetTempResponse ascii_response{
    packet_ascii.value()
  };
  EXPECT_NEAR(ascii_response.targetTemperature.value_of(),
              subject_2.value_of(),
              std::numeric_limits<device::Temperature>::epsilon().value_of());
}

TEST(TestThermocontrollers, DecodeDtcRunModeReadResponse)
{
  // Testing if thermocontroller is in RUN or STOP mode. These modes represented
  // as bool value

  const std::array subjects{
    std::make_pair(false, QByteArray::fromHex("0f020100a360")),
    std::make_pair(true, QByteArray::fromHex("0f02010162a0"))
  };
  for (auto&& subject : subjects) {
    auto packet{ factory_rtu.create(std::get<QByteArray>(subject)) };
    ASSERT_TRUE(packet.has_value());

    const protocol::dtc::DeviceGetRunModeResponse response{ packet.value() };
    EXPECT_EQ(response.runMode, std::get<bool>(subject));
  }
}

TEST(TestThermocontrollers, DecodeDtcRunModeWriteResponse)
{
  const std::array subjects{
    std::make_pair(true, QByteArray::fromHex("0f050814ff00cf70")),
    std::make_pair(false, QByteArray::fromHex("0f05081400008e80"))
  };
  for (auto&& subject : subjects) {
    auto packet{ factory_rtu.create(std::get<QByteArray>(subject)) };
    ASSERT_TRUE(packet.has_value());

    const protocol::dtc::DeviceSetRunModeResponse response{ packet.value() };
    EXPECT_EQ(response.runMode, std::get<bool>(subject));
  }
}
