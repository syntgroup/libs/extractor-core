#include <gtest/gtest.h>

#include "common/abstract_operation.h"
#include "qstring_support.hpp"
#include "thermocontrollers/device/heater.h"
#include "thermocontrollers/device/thermocontroller.h"
#include "thermocontrollers/gcode/parser_get_current_temp.h"
#include "thermocontrollers/gcode/parser_set_target_temp.h"
#include "thermocontrollers/gcode/parser_set_wait_target_temp.h"

namespace {
const auto deleter = [](gsl::owner<cmd::AbstractOperation*> operation) {
  delete operation;
};

[[nodiscard]] auto
create_heaters()
{
  constexpr std::array addresses{ 0x0f, 0x10 };
  device::Thermocontrollers::Container heaters;
  std::ranges::transform(
    addresses, std::inserter(heaters, heaters.end()), [](auto address) {
      return std::make_pair(address, std::make_unique<device::Heater>(address));
    });
  return std::make_shared<device::Thermocontrollers>(std::move(heaters));
}
} // namespace

TEST(TestThermocontrollers, ParseSetAndWaitSuccess)
{
  const auto thermocontrollers = create_heaters();
  const gcode::ParserSetWaitTargetTemp parser{ thermocontrollers };
  const QStringList subjects{
    "m109 t15 s75 f5",   //
    "M109 T15 S75 F5.1", //
    "M109 T15 S75",      //
    "M109 T15 S75 F"     //
  };

  for (auto&& subject : subjects) {
    const auto result = parser.generate(subject.toLower());
    ASSERT_TRUE(result.has_value()) << result.error();

    EXPECT_EQ(result->size(), 1);
    EXPECT_EQ(result->front()->description(),
              QStringLiteral("MULTI Wait Heater f to heat up to 75"))
      << result->front()->description();

    std::ranges::for_each(*result, deleter);
  }

  for (auto&& subject : subjects) {
    const auto result = parser.generate(subject.toUpper());
    ASSERT_TRUE(result.has_value()) << result.error();
    EXPECT_EQ(result.value().size(), 1);

    std::ranges::for_each(*result, deleter);
  }
}

TEST(TestThermocontrollers, ParseSetAndWaitFailure)
{
  std::shared_ptr<device::Thermocontrollers> thermocontrollers;
  auto parser =
    std::make_unique<gcode::ParserSetWaitTargetTemp>(thermocontrollers);

  const auto result = parser->generate("t15 s44");
  ASSERT_FALSE(result.has_value());
  ASSERT_EQ(result.error(), "error accessing heaters");

  thermocontrollers = create_heaters();
  parser = std::make_unique<gcode::ParserSetWaitTargetTemp>(thermocontrollers);

  const QStringList subjects{
    "M109 T1 S75",      // wrong heater
    "M109 T15 S F5.1",  // wrong target temperature
    "M109 T15 St F5.1", // wrong target temperature
    "M109 T15 S6 Fy"    // wrong offset
  };
  for (const auto& subject : subjects) {
    const auto result = parser->generate(subject);
    ASSERT_FALSE(result.has_value()) << result.error();
  }
}

TEST(TestThermocontrollers, ParseSetSuccess)
{
  const auto thermocontrollers = create_heaters();
  const gcode::ParserSetTargetTemp parser{ thermocontrollers };
  const QStringList subjects{
    "m105 t15 s20"  //,
    "m105 t15 s2.0" //,
    "M105 T15 S20"  //,
    "M105 T15 S2.0" //
  };

  for (auto&& subject : subjects) {
    const auto result = parser.generate(subject);
    ASSERT_TRUE(result.has_value()) << result.error();
    EXPECT_EQ(result->size(), 1);

    std::ranges::for_each(*result, deleter);
  }
}

TEST(TestThermocontrollers, ParseSetFailure)
{
  const auto thermocontrollers = create_heaters();
  const gcode::ParserSetTargetTemp parser{ thermocontrollers };

  const QStringList subjects{
    "m105 t1 s20"    //,
    "m105 t150 s2.0" //,
    "M105 T15 S"     //,
    "M105 T15 Suio"  //
  };

  for (auto&& subject : subjects) {
    const auto result = parser.generate(subject);
    ASSERT_FALSE(result.has_value()) << result.error();
  }
}

TEST(TestThermocontrollers, ParseGetCurrentSuccess)
{
  const auto pool = create_heaters();
  const gcode::ParserGetCurrentTemp parser{ pool };

  struct Subject
  {
    QString subject;
    QString description;
  };
  const std::vector<Subject> subjects{
    { "t15", "Requesting current temperature on heater adr 15" },
    { "T16", "Requesting current temperature on heater adr 16" }
  };
  for (const auto& [subject, description] : subjects) {
    const auto result = parser.generate(subject);
    ASSERT_TRUE(result.has_value()) << subject;
    ASSERT_EQ(result->size(), 1);
    ASSERT_EQ(result->at(0)->description(), description)
      << result->at(0)->description();

    std::ranges::for_each(*result, deleter);
  }
}

TEST(TestThermocontrollers, ParseGetCurrentFailure)
{
  std::shared_ptr<device::Thermocontrollers> pool;

  auto parser = std::make_unique<gcode::ParserGetCurrentTemp>(pool);
  auto result = parser->generate("t15 s20");
  ASSERT_FALSE(result.has_value());
  ASSERT_EQ(result.error(), "error accessing heaters");

  pool   = create_heaters();
  parser = std::make_unique<gcode::ParserGetCurrentTemp>(pool);

  struct Subject
  {
    QString subject;
    QString error;
  };
  const std::vector<Subject> subjects{
    { "", "Empty block is not allowed" },          // empty string
    { "y15", "incorrect syntax of M105 command" }, // wrong syntax
    { "t42 s10", "unknown heater requested: 42" }, // unknown heater id
  };
  for (const auto& [subject, error] : subjects) {
    const auto result = parser->generate(subject);
    ASSERT_FALSE(result.has_value()) << subject;
    ASSERT_EQ(result.error(), error);
  }
}
