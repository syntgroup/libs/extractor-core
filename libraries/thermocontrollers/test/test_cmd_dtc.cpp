#include <gtest/gtest.h>

#include "serial/protocol/protocol_values.h"
#include "serial/protocol/request_interface.h"
#include "serial/protocol/response_interface.h"
#include "thermocontrollers/cmd/get_current_temperature_operation.h"
#include "thermocontrollers/cmd/get_run_mode_operation.h"
#include "thermocontrollers/cmd/get_target_temperature_operation.h"
#include "thermocontrollers/cmd/set_run_mode_operation.h"
#include "thermocontrollers/cmd/set_target_temperature_operation.h"
#include "thermocontrollers/device/heater.h"
#include "thermocontrollers/device/heater_values.h"
#include "thermocontrollers/protocol/dtc_decoder.h"
#include "thermocontrollers/protocol/dtc_encoder.h"
#include "thermocontrollers/protocol/dtc_response_type.h"

namespace {
constexpr protocol::PacketID pid{ 0x78 };

const protocol::dtc::DtcEncoder encoder;
device::Heater heater{ 0xf };
} // namespace

TEST(TestThermocontrollers, HeaterRequestedCurrentTemperature)
{
  const auto tx_subject{ QByteArray::fromHex("0f031000000181e4") };

  const cmd::dtc::GetCurrentTemperatureOperation command{ pid, &heater };

  EXPECT_EQ(command.description(),
            QStringLiteral("Requesting current temperature on heater adr %1")
              .arg(heater.address()));
  EXPECT_EQ(command.responseType(),
            protocol::dtc::ResponseType::HeaterGetCurTemp);

  const auto request{ command.createRequest() };
  const auto type{ command.responseType() };
  const auto bytes{ encoder.encode(request, type) };

  EXPECT_EQ(bytes, tx_subject);
}

TEST(TestThermocontrollers, HeaterRequestedTargetTemperature)
{
  const cmd::dtc::GetTargetTemperatureOperation command{ pid, &heater };

  const auto description_subject{
    QStringLiteral("Requesting target temperature on heater adr 0x%1")
      .arg(heater.address())
  };
  EXPECT_EQ(command.description(), description_subject);
  EXPECT_EQ(command.responseType(),
            protocol::dtc::ResponseType::HeaterGetTargetTemp);

  const auto tx_subject{ QByteArray::fromHex("0f0310010001d024") };
  EXPECT_EQ(encoder.encode(command.createRequest(), command.responseType()),
            tx_subject);
}

TEST(TestThermocontrollers, HeaterWrittenTargetTemperature)
{
  constexpr device::Temperature temperature_subject{ 34.4 };
  const cmd::dtc::SetTargetTemperatureOperation command{ pid,
                                                         &heater,
                                                         temperature_subject };

  const auto description_subject{
    QStringLiteral("Setting target temperature on heater adr 0x%1 of value %2")
      .arg(QString::number(heater.address(), 16),
           QString::number(temperature_subject.value_of()))
  };
  EXPECT_EQ(command.description(), description_subject);
  EXPECT_EQ(command.responseType(),
            protocol::dtc::ResponseType::HeaterSetTargetTemp);

  const auto tx_subject{ QByteArray::fromHex("0f0610010158dd8e") };
  EXPECT_EQ(encoder.encode(command.createRequest(), command.responseType()),
            tx_subject);
}

TEST(TestThermocontrollers, HeaterTurnRunStop)
{
  device::Heater heater_{ 0xf };
  protocol::dtc::DtcDecoder decoder{ protocol::modbus::Mode::Rtu };
  EXPECT_EQ(heater_.running(), device::RunMode::Run);

  enum class Stage
  {
    Get,
    SetRun,
    SetStop
  };
  struct Subject
  {
    QByteArray request;
    QByteArray response;
    protocol::dtc::ResponseType type;
  };
  const std::unordered_map<Stage, Subject> subjects{
    { Stage::Get,
      { QByteArray::fromHex("0f0208140001fa80"),
        QByteArray::fromHex("0f02010162a0"),
        protocol::dtc::ResponseType::DeviceGetRunMode } },
    { Stage::SetRun,
      { QByteArray::fromHex("0f050814ff00cf70"),
        QByteArray::fromHex("0f050814ff00cf70"),
        protocol::dtc::ResponseType::DeviceSetRunMode } },
    { Stage::SetStop,
      { QByteArray::fromHex("0f05081400008e80"),
        QByteArray::fromHex("0f05081400008e80"),
        protocol::dtc::ResponseType::DeviceSetRunMode } }
  };

  cmd::dtc::GetRunModeOperation command_get{ pid, &heater_ };
  EXPECT_EQ(command_get.responseType(), subjects.at(Stage::Get).type);
  EXPECT_EQ(
    encoder.encode(command_get.createRequest(), command_get.responseType()),
    subjects.at(Stage::Get).request);
  const auto reply_get{ decoder.createResponse(subjects.at(Stage::Get).response,
                                               command_get.responseType()) };
  ASSERT_TRUE(reply_get.has_value());
  command_get.feedResponse(reply_get.value());
  EXPECT_EQ(heater_.running(), device::RunMode::Run);

  cmd::dtc::SetRunModeOperation command_set_stop{ pid, &heater_, false };
  EXPECT_EQ(command_set_stop.responseType(), subjects.at(Stage::SetStop).type);
  EXPECT_EQ(encoder.encode(command_set_stop.createRequest(),
                           command_set_stop.responseType()),
            subjects.at(Stage::SetStop).request);
  const auto reply_set_stop{ decoder.createResponse(
    subjects.at(Stage::SetStop).response, command_set_stop.responseType()) };
  ASSERT_TRUE(reply_set_stop.has_value());
  command_set_stop.feedResponse(reply_set_stop.value());
  EXPECT_EQ(heater_.running(), device::RunMode::Stop);

  cmd::dtc::SetRunModeOperation command_set_run{ pid, &heater_, true };
  EXPECT_EQ(command_set_run.responseType(), subjects.at(Stage::SetRun).type);
  EXPECT_EQ(encoder.encode(command_set_run.createRequest(),
                           command_set_run.responseType()),
            subjects.at(Stage::SetRun).request);
  const auto reply_set_run{ decoder.createResponse(
    subjects.at(Stage::SetRun).response, command_set_run.responseType()) };
  ASSERT_TRUE(reply_set_run.has_value());
  command_set_run.feedResponse(reply_set_run.value());
  EXPECT_EQ(heater_.running(), device::RunMode::Run);
}
