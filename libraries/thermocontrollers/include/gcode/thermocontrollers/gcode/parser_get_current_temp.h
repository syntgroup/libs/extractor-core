#ifndef GCODE_PARSERGETCURRENTTEMP_H
#define GCODE_PARSERGETCURRENTTEMP_H

#include "parser/gcode/gcode_parser_interface.h"
#include "thermocontrollers/gcode/export.h"

namespace device {
class Thermocontrollers;
} // namespace device

namespace gcode {

/*!
 * @brief The ParserGetCurrentTemp class
 * @details M105 [T<heater idx>]
 */
class THERMOCONTROLLERS_GCODE_EXPORT ParserGetCurrentTemp final
  : public GCodeParserInterface<ParserGetCurrentTemp>
{
  friend class GCodeParserInterface<ParserGetCurrentTemp>;

public:
  explicit ParserGetCurrentTemp(
    std::weak_ptr<device::Thermocontrollers> thermocontrollers)
    : m_thermocontrollers{ std::move(thermocontrollers) }
  {
  }

private:
  [[nodiscard]] auto generateImpl(const QString& block) const -> ParseResult;
  [[nodiscard]] static auto allowEmptyInput() { return false; }
  std::weak_ptr<device::Thermocontrollers> m_thermocontrollers;
};

} // namespace gcode

#endif // GCODE_PARSERGETCURRENTTEMP_H
