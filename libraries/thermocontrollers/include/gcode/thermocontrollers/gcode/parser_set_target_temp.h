#ifndef GCODE_PARSERSETTARGETTEMP_H
#define GCODE_PARSERSETTARGETTEMP_H

#include "parser/gcode/gcode_parser_interface.h"
#include "thermocontrollers/gcode/export.h"

namespace device {
class Thermocontrollers;
} // namespace device

namespace gcode {

/*!
 * @brief The ParserSetTargetTemp class
 * @details M104 [T<heater idx>] [S<new target temp>]
 */
class THERMOCONTROLLERS_GCODE_EXPORT ParserSetTargetTemp final
  : public GCodeParserInterface<ParserSetTargetTemp>
{
  friend class GCodeParserInterface<ParserSetTargetTemp>;

public:
  explicit ParserSetTargetTemp(
    std::weak_ptr<device::Thermocontrollers> thermocontrollers)
    : m_thermocontrollers{ std::move(thermocontrollers) }
  {
  }

private:
  [[nodiscard]] auto generateImpl(const QString& block) const -> ParseResult;
  [[nodiscard]] static auto allowEmptyInput() { return false; }
  std::weak_ptr<device::Thermocontrollers> m_thermocontrollers;
};

} // namespace gcode

#endif // GCODE_PARSERSETTARGETTEMP_H
