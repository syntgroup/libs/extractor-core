#ifndef GCODE_PARSERSETWAITTARGETTEMP_H
#define GCODE_PARSERSETWAITTARGETTEMP_H

#include "parser/gcode/gcode_parser_interface.h"
#include "thermocontrollers/gcode/export.h"

namespace device {
class Thermocontrollers;
} // namespace device

namespace gcode {

/*!
 * @brief The ParserSetWaitTargetTemp class
 * @details M109 [T<heater idx>] [S<new target temp>] [F<offset>]
 */
class THERMOCONTROLLERS_GCODE_EXPORT ParserSetWaitTargetTemp final
  : public GCodeParserInterface<ParserSetWaitTargetTemp>
{
  friend class GCodeParserInterface<ParserSetWaitTargetTemp>;

public:
  explicit ParserSetWaitTargetTemp(
    std::weak_ptr<device::Thermocontrollers> thermocontrollers)
    : m_thermocontrollers{ std::move(thermocontrollers) }
  {
  }

private:
  [[nodiscard]] auto generateImpl(const QString& block) const -> ParseResult;
  [[nodiscard]] static auto allowEmptyInput() { return false; }
  std::weak_ptr<device::Thermocontrollers> m_thermocontrollers;
};

} // namespace gcode

#endif // GCODE_PARSERSETWAITTARGETTEMP_H
