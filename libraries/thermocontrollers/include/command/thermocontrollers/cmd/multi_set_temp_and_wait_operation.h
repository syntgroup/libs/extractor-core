#ifndef CMD_DTC_MULTISETTEMPANDWAITOPERATION_H
#define CMD_DTC_MULTISETTEMPANDWAITOPERATION_H

#include "serial/cmd/abstract_multi_stage_operation.h"
#include "thermocontrollers/command/export.h"
#include "thermocontrollers/device/heater_values.h"
#include <gsl/gsl-lite.hpp>

namespace device {
class Heater;
} // namespace device

namespace cmd::multi {

class THERMOCONTROLLERS_COMMAND_EXPORT SetTempAndWaitOperation final
  : public cmd::AbstractMultiStageOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(SetTempAndWaitOperation)

  enum class Action
  {
    Unknown,
    Heating,
    Cooling
  };

public:
  enum OperationState : uint8_t
  {
    PreRequestingCurrentTemperature = AbstractOperation::User,
    SettingNewTargetTemperature     = 0x03,
    RequestNewTargetTemperature     = 0x04,
    RequestCurrentTemperature       = 0x05,
    Stop                            = 0xaa,
    Idle                            = 0xff
  };
  Q_ENUM(OperationState)

  SetTempAndWaitOperation(gsl::not_null<device::Heater*> heater,
                          device::Temperature target,
                          device::Temperature tolerance,
                          QObject* parent = nullptr);
  ~SetTempAndWaitOperation() override = default;

  [[nodiscard]] auto description() const -> QString override;

private:
  gsl::not_null<device::Heater*> m_heater;
  device::Temperature m_target{ 0 };
  device::Temperature m_tolerance{ 0 };
  Action m_action{ Action::Unknown };
  OperationState m_buffer{ OperationState::Idle };

  void writeTargetTemperature(device::Temperature target,
                              bool to_finish = false);
  void requestTargetTemperature();
  void requestCurrentTemperature();

  [[nodiscard]] auto targetReached() const -> bool;

protected:
  void onSubOperationError(const AbstractSerialOperation& operation) override;

private slots:
  void nextStateLogic() override;

public slots:
  void pause() override;
  void resume() override;
};

} // namespace cmd::multi

#endif // CMD_DTC_MULTISETTEMPANDWAITOPERATION_H
