#ifndef CMD_DTC_GETRUNMODEOPERATION_H
#define CMD_DTC_GETRUNMODEOPERATION_H

#include "abstract_dtc_operation.h"

namespace device {
class Heater;
} // namespace device

namespace protocol::dtc {
struct DeviceRunModeRequest;
struct DeviceGetRunModeResponse;
} // namespace protocol::dtc

namespace cmd::dtc {

class THERMOCONTROLLERS_COMMAND_EXPORT GetRunModeOperation final
  : public AbstractDtcOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(GetRunModeOperation)
public:
  using TRequest  = protocol::dtc::DeviceRunModeRequest;
  using TResponse = protocol::dtc::DeviceGetRunModeResponse;

  explicit GetRunModeOperation(protocol::PacketID pid,
                               gsl::not_null<device::Heater*> heater,
                               QObject* parent = nullptr);
  ~GetRunModeOperation() override = default;

  [[nodiscard]] QString description() const override;
  [[nodiscard]] int responseType() const override;

  [[nodiscard]] Request createRequest() const override;

private:
  bool processResponse(const Response& t_response) override;

  gsl::not_null<device::Heater*> m_heater;
};

} // namespace cmd::dtc

#endif // CMD_DTC_GETRUNMODEOPERATION_H
