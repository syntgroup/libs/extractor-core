#ifndef CMD_DTC_GETTARGETTEMPERATUREOPERATION_H
#define CMD_DTC_GETTARGETTEMPERATUREOPERATION_H

#include "abstract_dtc_operation.h"

namespace device {
class Heater;
} // namespace device

namespace protocol::dtc {
struct HeaterTargetTempRequest;
struct HeaterGetTargetTempResponse;
} // namespace protocol::dtc

namespace cmd::dtc {

class THERMOCONTROLLERS_COMMAND_EXPORT GetTargetTemperatureOperation final
  : public AbstractDtcOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(GetTargetTemperatureOperation)
public:
  using TRequest  = protocol::dtc::HeaterTargetTempRequest;
  using TResponse = protocol::dtc::HeaterGetTargetTempResponse;

  explicit GetTargetTemperatureOperation(protocol::PacketID pid,
                                         gsl::not_null<device::Heater*> heater,
                                         QObject* parent = nullptr);
  ~GetTargetTemperatureOperation() override = default;

  [[nodiscard]] QString description() const override;
  [[nodiscard]] int responseType() const override;
  [[nodiscard]] Request createRequest() const override;

protected:
  [[nodiscard]] bool processResponse(const Response& t_response) override;

private:
  gsl::not_null<device::Heater*> m_heater;
};

} // namespace cmd::dtc

#endif // CMD_DTC_GETTARGETTEMPERATUREOPERATION_H
