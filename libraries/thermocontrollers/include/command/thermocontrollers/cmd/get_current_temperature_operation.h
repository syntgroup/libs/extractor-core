#ifndef CMD_DTC_GETCURRENTTEMPERATUREOPERATION_H
#define CMD_DTC_GETCURRENTTEMPERATUREOPERATION_H

#include "abstract_dtc_operation.h"

namespace device {
class Heater;
} // namespace device

namespace protocol::dtc {
struct HeaterCurrentTempRequest;
struct HeaterGetCurTempResponse;
} // namespace protocol::dtc

namespace cmd::dtc {

class THERMOCONTROLLERS_COMMAND_EXPORT GetCurrentTemperatureOperation final
  : public AbstractDtcOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(GetCurrentTemperatureOperation)
public:
  using TRequest  = protocol::dtc::HeaterCurrentTempRequest;
  using TResponse = protocol::dtc::HeaterGetCurTempResponse;

  explicit GetCurrentTemperatureOperation(protocol::PacketID pid,
                                          gsl::not_null<device::Heater*> heater,
                                          QObject* parent = nullptr);
  ~GetCurrentTemperatureOperation() override = default;

  // AbstractOperation interface
  [[nodiscard]] QString description() const override;
  [[nodiscard]] int responseType() const override;

  [[nodiscard]] Request createRequest() const override;

protected:
  [[nodiscard]] bool processResponse(const Response& t_response) override;

private:
  gsl::not_null<device::Heater*> m_heater;
};

} // namespace cmd::dtc

#endif // CMD_DTC_GETCURRENTTEMPERATUREOPERATION_H
