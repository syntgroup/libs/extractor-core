#ifndef CMD_DTC_SETTARGETTEMPERATUREOPERATION_H
#define CMD_DTC_SETTARGETTEMPERATUREOPERATION_H

#include "abstract_dtc_operation.h"
#include "thermocontrollers/device/heater_values.h"

namespace device {
class Heater;
} // namespace device

namespace protocol::dtc {
struct HeaterTargetTempWrite;
} // namespace protocol::dtc

namespace cmd::dtc {

class THERMOCONTROLLERS_COMMAND_EXPORT SetTargetTemperatureOperation final
  : public AbstractDtcOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(SetTargetTemperatureOperation)
public:
  using TRequest = protocol::dtc::HeaterTargetTempWrite;

  explicit SetTargetTemperatureOperation(protocol::PacketID pid,
                                         gsl::not_null<device::Heater*> heater,
                                         device::Temperature target_temperature,
                                         QObject* parent = nullptr);
  ~SetTargetTemperatureOperation() override = default;

  [[nodiscard]] QString description() const override;
  [[nodiscard]] int responseType() const override;
  [[nodiscard]] Request createRequest() const override;

private:
  gsl::not_null<device::Heater*> m_heater;
  device::Temperature m_target_temperature{ 0.0 };
};

} // namespace cmd::dtc

#endif // CMD_DTC_SETTARGETTEMPERATUREOPERATION_H
