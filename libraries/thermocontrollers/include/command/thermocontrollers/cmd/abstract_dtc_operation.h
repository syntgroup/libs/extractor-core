#ifndef CMD_DTC_ABSTRACTDTCOPERATION_H
#define CMD_DTC_ABSTRACTDTCOPERATION_H

#include "serial/cmd/abstract_serial_operation.h"
#include "thermocontrollers/command/export.h"
#include <gsl/gsl-lite.hpp>

namespace protocol::dtc {
struct DtcResponse;
} // namespace protocol::dtc

namespace cmd::dtc {

class THERMOCONTROLLERS_COMMAND_EXPORT AbstractDtcOperation
  : public AbstractSerialOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(AbstractDtcOperation)

public:
  using TResponse = protocol::dtc::DtcResponse;

  explicit AbstractDtcOperation(protocol::PacketID pid,
                                QObject* parent = nullptr)
    : AbstractSerialOperation{ pid, parent }
  {
    setTimeout(std::chrono::milliseconds{ 1000 });
  }

  ~AbstractDtcOperation() override = default;

  [[nodiscard]] auto operationType() const
    -> cmd::AbstractOperation::OperationType final
  {
    return OperationType::Thermocontrollers;
  }
};

} // namespace cmd::dtc

#endif // CMD_DTC_ABSTRACTDTCOPERATION_H
