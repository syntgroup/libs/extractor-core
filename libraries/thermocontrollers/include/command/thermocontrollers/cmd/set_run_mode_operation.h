#ifndef CMD_DTC_SETRUNMODEOPERATION_H
#define CMD_DTC_SETRUNMODEOPERATION_H

#include "abstract_dtc_operation.h"

namespace device {
class Heater;
} // namespace device

namespace protocol::dtc {
struct DeviceRunModeWrite;
struct DeviceSetRunModeResponse;
} // namespace protocol::dtc

namespace cmd::dtc {

class THERMOCONTROLLERS_COMMAND_EXPORT SetRunModeOperation final
  : public AbstractDtcOperation
{
  Q_OBJECT
  Q_DISABLE_COPY(SetRunModeOperation)
public:
  using TRequest  = protocol::dtc::DeviceRunModeWrite;
  using TResponse = protocol::dtc::DeviceSetRunModeResponse;

  explicit SetRunModeOperation(protocol::PacketID pid,
                               gsl::not_null<device::Heater*> heater,
                               bool state,
                               QObject* parent = nullptr);
  ~SetRunModeOperation() override = default;

  [[nodiscard]] QString description() const override;
  [[nodiscard]] int responseType() const override;

  [[nodiscard]] Request createRequest() const override;

protected:
  bool processResponse(const Response& t_response) override;

private:
  gsl::not_null<device::Heater*> m_heater;
  bool m_newState{ false };
};

} // namespace cmd::dtc

#endif // CMD_DTC_SETRUNMODEOPERATION_H
