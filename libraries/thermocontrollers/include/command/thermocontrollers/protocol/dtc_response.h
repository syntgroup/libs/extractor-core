#ifndef PROTOCOL_DTC_DTCRESPONSE_H
#define PROTOCOL_DTC_DTCRESPONSE_H

#include "modbus/modbus_defs.h"
#include "serial/protocol/response_interface.h"
#include <memory>

namespace protocol::modbus {
class AbstractModbusResponseFactory;
class IPacket;
} // namespace protocol::modbus

namespace protocol::dtc {

struct DtcResponse : protocol::ResponseInterface
{
  explicit DtcResponse(std::unique_ptr<modbus::IPacket>& packet);
  ~DtcResponse() override;

  DtcResponse(const DtcResponse&)                        = delete;
  auto operator=(const DtcResponse&) -> DtcResponse&     = delete;
  DtcResponse(DtcResponse&&) noexcept                    = delete;
  auto operator=(DtcResponse&&) noexcept -> DtcResponse& = delete;

  // ResponseInterface interface
  [[nodiscard]] auto id() const -> PacketID final { return m_pid; }
  [[nodiscard]] auto hasNext() const -> bool final { return false; }
  [[nodiscard]] auto isError() const -> bool final { return m_error; }
  [[nodiscard]] auto errorString() const -> QString final;
  [[nodiscard]] auto encodedSize() const -> size_t final;

  [[nodiscard]] static auto minimumSize(int type) -> int;

protected:
  [[nodiscard]] auto pdu() const -> modbus::Payload;
  [[nodiscard]] auto errorCode() const -> int;

  void setError(bool is_error) { m_error = is_error; }
  void setErrorString(const QString& error_string);

  enum Pos
  {
    Command     = 0,
    DataAddress = 1,
    DataValue   = 2
  };

private:
  std::unique_ptr<modbus::IPacket> m_packet{ nullptr };
  bool m_error{ false };
  QString m_errorString;
  const PacketID m_pid{ 0 };
};

} // namespace protocol::dtc

#endif // PROTOCOL_DTC_DTCRESPONSE_H
