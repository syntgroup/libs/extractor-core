#ifndef DTC_RESPONSE_TYPE_H
#define DTC_RESPONSE_TYPE_H

namespace protocol::dtc {

enum ResponseType : int
{
  DeviceGetCtrlMode,
  DeviceGetOutMode,
  DeviceGetSensorType,
  DeviceGetState,

  DeviceSetCtrlMode,
  DeviceSetOutMode,
  DeviceSetSensorType,

  HeaterGetCurTemp,
  HeaterGetTargetTemp,

  HeaterSetTargetTemp,

  DeviceGetRunMode,
  DeviceSetRunMode
};

} // namespace protocol::dtc

#endif // DTC_RESPONSE_TYPE_H
