#ifndef PROTOCOL_DTC_IDTCENCODER_H
#define PROTOCOL_DTC_IDTCENCODER_H

#include "modbus/modbus_defs.h"
#include "serial/protocol/encoder_interface.h"
#include "thermocontrollers/command/export.h"

namespace protocol::modbus {
struct ModbusRequestFactory;
} // namespace protocol::modbus

namespace protocol::dtc {

struct THERMOCONTROLLERS_COMMAND_EXPORT DtcEncoder : protocol::EncoderInterface
{
  explicit DtcEncoder(modbus::Mode mode = modbus::Mode::Rtu);
  ~DtcEncoder() override;

  DtcEncoder(const DtcEncoder&)     = delete;
  DtcEncoder(DtcEncoder&&) noexcept = delete;

  auto operator=(const DtcEncoder&) -> DtcEncoder&     = delete;
  auto operator=(DtcEncoder&&) noexcept -> DtcEncoder& = delete;

  [[nodiscard]] auto encode(const Request& request, int type) const
    -> QByteArray override;

private:
  using RequestFactory =
    std::unique_ptr<protocol::modbus::ModbusRequestFactory>;
  const RequestFactory m_factory{ nullptr };
};

} // namespace protocol::dtc

#endif // PROTOCOL_DTC_IDTCENCODER_H
