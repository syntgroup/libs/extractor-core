#ifndef DTC_ERROR_H
#define DTC_ERROR_H

#include <cstdint>

namespace protocol::dtc {

enum class ErrorPV : uint16_t
{
  Init        = 0x8002,
  NoSensor    = 0x8003,
  SenseError  = 0x8004,
  LimitSensor = 0x8006,
  EEPROMError = 0x8007
};

} // namespace protocol::dtc

#endif // DTC_ERROR_H
