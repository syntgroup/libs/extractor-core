#ifndef PROTOCOL_DTC_DTCDECODER_H
#define PROTOCOL_DTC_DTCDECODER_H

#include "modbus/modbus_defs.h"
#include "serial/protocol/decoder_interface.h"
#include "thermocontrollers/command/export.h"

namespace protocol {
class ResponseInterface;
} // namespace protocol

namespace protocol::modbus {
class AbstractModbusResponseFactory;
} // namespace protocol::modbus

namespace protocol::dtc {

struct THERMOCONTROLLERS_COMMAND_EXPORT DtcDecoder : DecoderInterface
{
  explicit DtcDecoder(modbus::Mode mode = modbus::Mode::Rtu);
  ~DtcDecoder() override;

  DtcDecoder(const DtcDecoder&)     = delete;
  DtcDecoder(DtcDecoder&&) noexcept = default;

  auto operator=(const DtcDecoder&) -> DtcDecoder&     = delete;
  auto operator=(DtcDecoder&&) noexcept -> DtcDecoder& = delete;

  [[nodiscard]] auto createResponse(const QByteArray& data, int type) const
    -> ResponseOrError override;

private:
  std::unique_ptr<modbus::AbstractModbusResponseFactory> factory{ nullptr };
};

} // namespace protocol::dtc

#endif // PROTOCOL_DTC_DTCDECODER_H
