#ifndef PROTOCOL_DTC_DTCREGISTERS_H
#define PROTOCOL_DTC_DTCREGISTERS_H

#include <cstdint>

namespace protocol::dtc {

enum class HoldingRegisters : uint16_t
{
  PV                   = 0x1000, //!< Process Value
  SV                   = 0x1001, //!< Set Point
  UpperLimit           = 0x1002, //!< Upper-limit of temperature range
  DownLimit            = 0x1003, //!< Lower-limit of temperature range
  SensorType           = 0x1004, //!< Input temperature sensor type
  ControlMethod        = 0x1005, //!< Control method
  ControlCycleOut1     = 0x1007, //!< 1st group of heating/cooling control cycle
  ControlCycleOut2     = 0x1008, //!< 2nd group of heating/cooling control cycle
  ProportionalBand     = 0x1009, //!< Proportional band
  IntegrationTime      = 0x100A, //!< Integral time
  DerivativeTime       = 0x100B, //!< Derivative time
  IntegrationDefault   = 0x100C, //!< Integration default
  ProportionControl    = 0x100D, //!< Proportional control offset error value
  ProportionCoef       = 0x100E, //!< COEF setting for DUAL LOOP
  DeadBand             = 0x100F, //!< Dead band setting for DUAL LOOP
  HysteresisOut1       = 0x1010, //!< Hysteresis value of the 1st out
  HysteresisOut2       = 0x1011, //!< Hysteresis value of the 2nd out
  OuptutOut1           = 0x1012, //!< [RW] Output value of the 1st out
  OutputOut2           = 0x1013, //!< [RW] Output value of the 2nd out
  UpperLimitAnalog     = 0x1014, //!< Upper-limit regulation of analog out
  DownLimitAnalog      = 0x1015, //!< Lower-limit regulation of analog out
  TemperatureValue     = 0x1016, //!< Temperature regulation value
  Alarm1Type           = 0x1020, //!< Alarm 1 type
  Alarm2Type           = 0x1021, //!< Alarm 2 type
  CommunicationAutoSet = 0x1022, //!< Auto-setting communication flag
  UpperLimitAlarm1     = 0x1024, //!< Upper-limit alarm 1
  LowerLimitAlarm1     = 0x1025, //!< Lower-limit alarm 1
  UpperLimitAlarm2     = 0x1026, //!< Upper-limit alarm 2
  LowerLimitAlarm2     = 0x1027, //!< Lower-limit alarm 2
  Status               = 0x102A, //!< [RW] Status
  ProportionalSlope    = 0x102C, //!< Setting of pos/neg proportional out
  LedStatus            = 0x102E, //!< LED status
  SoftwareVersion      = 0x102F, //!< Software revision
  StartPattern         = 0x1030, //!< Start pattern number
  RemainingTimeCycle   = 0x1032, //!< Remaining time of read exec
  RemainingTimeStep    = 0x1033, //!< Remaining time of reading exec step
  CurrentStep          = 0x1034, //!< Step number of reading current step
  CurrentCycle         = 0x1035, //!< Executing number of reading pattern
  UpperLimitProportional = 0x1037, //!< Upper-limit of proportional out
  LowerLimitProportional = 0x1038, //!< Lower-limit of proportional out
  StepsInCycle           = 0x1040, //!< Actual step number setting
  CycleRepeats           = 0x1050, //!< Cycle number of executing pattern
  CycleNext              = 0x1060, //!< Link pattern number  of current pattern
  ExecutionSetting       = 0x1068, //!< Control execution/stop setting
  ModeOut1               = 0x1069, //!< Output 1: control selection
  ModeOut2               = 0x106A, //!< Output 2: control selection
  DismissLevel1          = 0x106B, //!< Dismiss level 1 r/w allowed
  DismissLevel2          = 0x106C, //!< Dismiss level 2. Use Level 3
  DismissLevel3          = 0x106D, //!< Dismiss level 3. Use level 2
  PasswordLevel1         = 0x106E, //!< Dismiss level 1 / Set password
  PasswordLevel2         = 0x106F, //!< Dismiss level 2 / Set password
  PasswordLevel3         = 0x1070, //!< Dismiss level 3 / Set password
  CommunicationAddress   = 0x1071, //!< [RW] communication address
  CommunicationFormat    = 0x1072, //!< [RW] communication format
  CommunicationBaudrate  = 0x1073, //!< [RW] communication baudrate
  CommunicationBitLength = 0x1074, //!< [RW] communication data length
  CommunicationParity    = 0x1075, //!< [RW] communication parity
  CommunicationStopBits  = 0x1076, //!< [RW] communication stop bits
};

enum class DiscreteInputs : uint16_t
{
  TemperatureUnit = 0x0811, //!< temperature unit display selection
  AtSetting       = 0x0813, //!< Auto-tuning status
  ControlSetting  = 0x0814, //!< Control RUN/STOP setting
  ProgramHold     = 0x0815, //!< Program hold flag
  ProgramStop     = 0x0816  //!< Program stop flag
};

} // namespace protocol::dtc

#endif // PROTOCOL_DTC_DTCREGISTERS_H
