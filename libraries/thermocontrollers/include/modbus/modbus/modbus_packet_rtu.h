#ifndef PROTOCOL_MODBUS_PACKETRTU_H
#define PROTOCOL_MODBUS_PACKETRTU_H

#include "modbus_packet.h"

namespace protocol::modbus {

class CORE_MODBUS_EXPORT PacketRTU : public IPacket
{
public:
  using IPacket::IPacket;
  ~PacketRTU() override = default;

  // packet interface
  [[nodiscard]] auto encode() const -> QByteArray override;
  [[nodiscard]] auto calculateChecksum(const QByteArray& data) const
    -> uint16_t override;
};

} // namespace protocol::modbus

#endif // PROTOCOL_MODBUS_PACKETRTU_H
