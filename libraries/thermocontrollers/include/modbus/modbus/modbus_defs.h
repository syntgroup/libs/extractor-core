#ifndef MODBUS_DEFS_H
#define MODBUS_DEFS_H

#include "modbus_export.h"
#include <cstdint>
#include <vector>

namespace protocol::modbus {

enum class CORE_MODBUS_EXPORT Mode
{
  Ascii,
  Rtu
};

enum struct CORE_MODBUS_EXPORT Type
{
  Request,
  Response
};

enum class CORE_MODBUS_EXPORT ExceptionCode : uint8_t
{
  Ok                                 = 0x00,
  IllegalFunciton                    = 0x01,
  IllegalDataAddresss                = 0x02,
  IllegalDataValue                   = 0x03,
  SlaveDeviceFailure                 = 0x04,
  Acknowledge                        = 0x05,
  SlaveDeviceBusy                    = 0x06,
  MemoryParityError                  = 0x07,
  GatewayPathUnavaliable             = 0x0A,
  GatewayTargetDeviceFailedToRespond = 0x0B,
  CrcError                           = 0x10,
  Timeout                            = 0x11,
  NoConnection                       = 0x12,
  UnknownError                       = 0xFF
};

enum CORE_MODBUS_EXPORT FunctionCode : uint16_t
{
  Invalid                        = 0x00,
  ReadCoils                      = 0x01,
  ReadDiscreteInputs             = 0x02,
  ReadHoldingRegisters           = 0x03,
  ReadInputRegisters             = 0x04,
  WriteSingleCoil                = 0x05,
  WriteSingleRegister            = 0x06,
  ReadExceptionStatus            = 0x07,
  Diagnostics                    = 0x08,
  GetCommEventCounter            = 0x0B,
  GetCommEventLog                = 0x0C,
  WriteMultipleCoils             = 0x0F,
  WriteMultipleRegisters         = 0x10,
  ReportServerID                 = 0x11,
  ReadFileRecord                 = 0x14,
  WriteFileRecord                = 0x15,
  MaskWriteRegister              = 0x16,
  ReadWriteMultipleRegisters     = 0x17,
  ReadFifoQueue                  = 0x18,
  IncapsulatedInterfaceTransport = 0x2B,
  UndefinedFunctionCode          = 0x100
};

static constexpr auto exception_byte{ 0x80 };

using Payload = std::vector<uint16_t>;

} // namespace protocol::modbus

#endif // MODBUS_DEFS_H
