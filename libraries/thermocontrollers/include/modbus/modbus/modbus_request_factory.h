#ifndef PROTOCOL_MODBUS_SERIALRTUADU_H
#define PROTOCOL_MODBUS_SERIALRTUADU_H

#include "modbus_defs.h"

#include <QByteArray>

namespace protocol::modbus {

struct CORE_MODBUS_EXPORT ModbusRequestFactory
{
  explicit ModbusRequestFactory(Mode mode = Mode::Rtu)
    : m_mode{ mode }
  {
  }

  [[nodiscard]] QByteArray readHoldingRegisters(uint8_t slave_id,
                                                uint16_t start_address,
                                                uint8_t count = 1) const;

  [[nodiscard]] QByteArray writeSingleRegister(uint8_t slave_id,
                                               uint16_t start_address,
                                               uint16_t value) const;

  [[nodiscard]] QByteArray readDiscreteInputs(uint8_t slave_id,
                                              uint16_t start_address,
                                              uint8_t count = 1) const;

  [[nodiscard]] QByteArray writeSingleCoil(uint8_t slave_id,
                                           uint16_t start_address,
                                           bool value) const;

private:
  Mode m_mode{ Mode::Rtu };
};

} // namespace protocol::modbus

#endif // PROTOCOL_MODBUS_SERIALRTUADU_H
