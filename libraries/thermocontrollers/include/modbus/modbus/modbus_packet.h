#ifndef PROTOCOL_MODBUS_MODBUSPACKET_H
#define PROTOCOL_MODBUS_MODBUSPACKET_H

#include "modbus_defs.h"

#include <QByteArray>
#include <QObject>

namespace protocol::modbus {

class CORE_MODBUS_EXPORT IPacket
{
  Q_GADGET

public:
  explicit IPacket(uint8_t slave_id, FunctionCode function, Payload data);
  IPacket()                                      = default;
  virtual ~IPacket()                             = default;
  IPacket(const IPacket&)                        = default;
  IPacket(IPacket&&) noexcept                    = default;
  auto operator=(const IPacket&) -> IPacket&     = default;
  auto operator=(IPacket&&) noexcept -> IPacket& = default;

  [[nodiscard]] virtual auto encode() const -> QByteArray = 0;
  //! @todo переделать реализации функций так, чтобы можно было принимать на
  //! вход вектор с цифирьками или QByteArray
  [[nodiscard]] virtual auto calculateChecksum(const QByteArray& data) const
    -> uint16_t = 0;

  [[nodiscard]] auto slaveId() const { return m_slaveId; }
  [[nodiscard]] auto startAddress() const { return m_startAddress; }
  [[nodiscard]] auto pdu() const { return m_pdu; }
  [[nodiscard]] auto status() const { return m_status; }
  [[nodiscard]] uint8_t function() const { return m_functionCode; }
  [[nodiscard]] auto dataCount() const { return m_dataCount; }

  void appendToPdu(uint16_t byte) { m_pdu.emplace_back(byte); }
  void setStatus(ExceptionCode status) { m_status = status; }
  void setSlaveId(uint8_t slave_id) { m_slaveId = slave_id; }
  void setFunctionCode(FunctionCode code) { m_functionCode = code; }
  void setDataCount(uint8_t data_count) { m_dataCount = data_count; }
  void setStartAddress(uint16_t start_address)
  {
    m_startAddress = start_address;
  }

  [[nodiscard]] auto isException() const -> bool;
  [[nodiscard]] auto excecptionCode() const -> ExceptionCode;
  [[nodiscard]] static auto minimumPduSize(FunctionCode function, Type type)
    -> int;

private:
  //! slave id
  uint8_t m_slaveId{ 0 };
  //! function code
  FunctionCode m_functionCode{ FunctionCode::UndefinedFunctionCode };
  //! data start address
  uint16_t m_startAddress{ 0 };
  //! data count (in responses)
  uint16_t m_dataCount{ 0 };
  //! payload (start address + count) or (start address + value)
  Payload m_pdu;

  ExceptionCode m_status{ ExceptionCode::UnknownError };
};

} // namespace protocol::modbus

#endif // PROTOCOL_MODBUS_MODBUSPACKET_H
