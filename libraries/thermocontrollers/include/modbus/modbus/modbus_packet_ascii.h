#ifndef PROTOCOL_MODBUS_PACKETASCII_H
#define PROTOCOL_MODBUS_PACKETASCII_H

#include "modbus_packet.h"

namespace protocol::modbus {

class CORE_MODBUS_EXPORT PacketASCII : public IPacket
{
public:
  using IPacket::IPacket;
  ~PacketASCII() override = default;

  // packet interface
  [[nodiscard]] auto encode() const -> QByteArray override;
  [[nodiscard]] auto calculateChecksum(const QByteArray& data) const
    -> uint16_t override;
};

} // namespace protocol::modbus

#endif // PROTOCOL_MODBUS_PACKETASCII_H
