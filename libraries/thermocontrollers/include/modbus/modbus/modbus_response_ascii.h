#ifndef PROTOCOL_MODBUS_ASCIIRESPONSEFACTORY_H
#define PROTOCOL_MODBUS_ASCIIRESPONSEFACTORY_H

#include "modbus_response.h"

namespace protocol::modbus {

struct CORE_MODBUS_EXPORT AsciiResponseFactory final
  : AbstractModbusResponseFactory
{
  ~AsciiResponseFactory() override = default;
  [[nodiscard]] auto create(const QByteArray& data) const
    -> nonstd::expected<std::unique_ptr<IPacket>, ExceptionCode> override;

protected:
  void fillPayload(const QByteArray& data, IPacket* packet) const override;
};

} // namespace protocol::modbus

#endif // PROTOCOL_MODBUS_ASCIIRESPONSEFACTORY_H
