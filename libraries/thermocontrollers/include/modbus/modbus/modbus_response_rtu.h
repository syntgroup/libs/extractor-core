#ifndef PROTOCOL_MODBUS_RTURESPONSEFACTORY_H
#define PROTOCOL_MODBUS_RTURESPONSEFACTORY_H

#include "modbus_response.h"

namespace protocol::modbus {

struct CORE_MODBUS_EXPORT RtuResponseFactory final
  : AbstractModbusResponseFactory
{
  ~RtuResponseFactory() override = default;
  [[nodiscard]] auto create(const QByteArray& data) const
    -> nonstd::expected<std::unique_ptr<IPacket>, ExceptionCode> override;

protected:
  void fillPayload(const QByteArray& pdu, IPacket* packet) const override;

private:
  enum FieldPos : uint8_t
  {
    FPSlaveAddress = 0,
    FPCommandCode  = 1,
    FPDataAddress  = 2,
    FPDataContent  = 4
  };
};

} // namespace protocol::modbus

#endif // PROTOCOL_MODBUS_RTURESPONSEFACTORY_H
