#ifndef PROTOCOL_MODBUS_MODBUSRESPONSE_H
#define PROTOCOL_MODBUS_MODBUSRESPONSE_H

#include "modbus_export.h"

#include <QByteArray>
#include <memory>
#include <nonstd/expected.hpp>

namespace protocol::modbus {

class CORE_MODBUS_EXPORT IPacket;
enum class CORE_MODBUS_EXPORT ExceptionCode : uint8_t;

struct CORE_MODBUS_EXPORT AbstractModbusResponseFactory
{
  virtual ~AbstractModbusResponseFactory() = default;

  [[nodiscard]] virtual auto create(const QByteArray& data) const
    -> nonstd::expected<std::unique_ptr<IPacket>, ExceptionCode> = 0;

protected:
  virtual void fillPayload(const QByteArray& data, IPacket* packet) const = 0;
  [[nodiscard]] static auto checkFunctionCode(uint8_t function) -> bool;
};

} // namespace protocol::modbus

#endif // PROTOCOL_MODBUS_MODBUSRESPONSE_H
