#ifndef DEVICE_HEATER_VALUES_H
#define DEVICE_HEATER_VALUES_H

#include "thermocontrollers/device/export.h"
#include <QMetaType>
#include <strong_type/strong_type.hpp>

namespace device {

using Temperature = strong::type<double,
                                 struct TemperatureTag,
                                 strong::semiregular,
                                 strong::arithmetic,
                                 strong::ordered>;

struct THERMOCONTROLLERS_DEVICE_EXPORT HeaterConfig
{
  enum class SensorType
  {
    ThermocoupleK   = 0,
    ThermocoupleJ   = 1,
    ThermocoupleT   = 2,
    ThermocoupleE   = 3,
    ThermocoupleN   = 4,
    ThermocoupleR   = 5,
    ThermocoupleS   = 6,
    ThermocoupleB   = 7,
    ThermocoupleL   = 8,
    ThermocoupleU   = 9,
    ThermocoupleThk = 10,
    JPt100          = 11,
    Pt100           = 12,
    Analog0to5V     = 13,
    Analog0to10V    = 14,
    Current0to20mA  = 15,
    Current4to20mA  = 16,
    Analog0to50mV   = 17
  };
  Q_ENUM(SensorType)

  enum class OutMode
  {
    Heat         = 0,
    Cool         = 1,
    Alarm        = 2,
    Proportional = 3
  };
  Q_ENUM(OutMode)

  enum class CtrlMode
  {
    PID    = 0,
    UpDown = 1,
    Manual = 2,
    Script = 3
  };
  Q_ENUM(CtrlMode)

  enum class AlarmMode
  {
    Disable             = 0,
    UpDownLimit         = 1,
    UpLimit             = 2,
    DownLimit           = 3,
    ReverseUpDownLimit  = 4,
    AbsoluteUpDownLimit = 5,
    AbsoluteUpLimit     = 6,
    AbsoluteDownLimit   = 7,
    StandbyUpDownLimit  = 8,
    StandbyUpLimit      = 9,
    StandbyDownLimit    = 10,
    HysteresisUpLimit   = 11,
    HysteresisDownLimit = 12,
    CtAlarm             = 13
  };
  Q_ENUM(AlarmMode)

  enum class CommunicationMode
  {
    Unknown = -1,
    ASCII   = 0,
    RTU     = 1,
  };
  Q_ENUM(CommunicationMode)

  enum class RunMode
  {
    Run  = 0x00,
    Stop = 0x10
  };
  Q_ENUM(RunMode)

private:
  Q_GADGET
};

using AlarmMode         = HeaterConfig::AlarmMode;
using ControlMode       = HeaterConfig::CtrlMode;
using CommunicationMode = HeaterConfig::CommunicationMode;
using OutMode           = HeaterConfig::OutMode;
using RunMode           = HeaterConfig::RunMode;
using SensorType        = HeaterConfig::SensorType;

} // namespace device

#endif // DEVICE_HEATER_VALUES_H
