#ifndef HEATER_PARAMS_H
#define HEATER_PARAMS_H

#include "heater_values.h"
#include <QString>

namespace device {

struct THERMOCONTROLLERS_DEVICE_EXPORT HeaterParams
{
  uint8_t address{ 0 };
  QString name;

  AlarmMode alarmMode{ AlarmMode::Disable };
  SensorType sensorType{ SensorType::Pt100 };
  OutMode out1Mode{ OutMode::Heat };
  OutMode out2Mode{ OutMode::Cool };
  ControlMode ctrlMode{ ControlMode::PID };

  double rangeHigh{ 0.0 };
  double rangeLow{ 0.0 };
};

// NOLINTBEGIN
struct THERMOCONTROLLERS_DEVICE_EXPORT PidParams
{
  double pb{ 0.0 };
  double ti{ 0.0 };
  double td{ 0.0 };
  double ctrlPeriod1{ 0.0 };
  double iOffset{ 0.0 };
  double ctrlPeriod2{ 0.0 };
  double pCoefficient{ 0.0 };
  double deadBand{ 0.0 };
};

struct THERMOCONTROLLERS_DEVICE_EXPORT CtrlState
{
  bool ALM2 : 1;
  bool OC : 1;
  bool OF : 1;
  bool ALM1 : 1;
  bool O2 : 1;
  bool O1 : 1;
  bool AT : 1;
};

struct THERMOCONTROLLERS_DEVICE_EXPORT LedState
{
  bool RUN : 1;
  bool ERR : 1;
  bool O2 : 1;
  bool O1 : 1;
  bool RX : 1;
  bool TX : 1;
  bool AT : 1;
};
// NOLINTEND

} // namespace device

#endif // HEATER_PARAMS_H
