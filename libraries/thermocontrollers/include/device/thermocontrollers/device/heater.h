#ifndef DEVICE_HEATER_H
#define DEVICE_HEATER_H

#include "heater_params.h"
#include <QObject>
#include <memory>

namespace device {

class THERMOCONTROLLERS_DEVICE_EXPORT Heater : public QObject
{
  Q_OBJECT
  Q_PROPERTY(QString name READ name CONSTANT)
  Q_PROPERTY(uint8_t address READ address CONSTANT)
  Q_PROPERTY(device::Temperature m_currentTemperature READ currentTemperature
               WRITE setCurrentTemperature NOTIFY currentTemperatureChanged)
  Q_PROPERTY(device::Temperature m_targetTemperature READ targetTemperature
               WRITE setTargetTemperature NOTIFY targetTemperatureChanged)

public:
  explicit Heater(uint8_t adr, QObject* parent = nullptr);
  Heater(std::unique_ptr<HeaterParams>&& params,
         std::unique_ptr<PidParams>&& pid,
         QObject* parent = nullptr);
  ~Heater() override;

  Heater(const Heater&)                    = delete;
  Heater(Heater&&)                         = delete;
  auto operator=(const Heater&) -> Heater& = delete;
  auto operator=(Heater&&) -> Heater&      = delete;

  [[nodiscard]] auto address() const -> uint8_t;
  [[nodiscard]] auto name() const -> QString;

  [[nodiscard]] auto sensorType() const -> SensorType;
  void setSensorType(SensorType new_sensor_type);

  [[nodiscard]] auto out1Mode() const -> OutMode;
  void setOut1Mode(OutMode new_out1_mode);

  [[nodiscard]] auto out2Mode() const -> OutMode;
  void setOut2Mode(OutMode new_out2_mode);

  [[nodiscard]] auto controlMode() const -> ControlMode;
  void setControlMode(ControlMode new_ctrl_mode);

  [[nodiscard]] auto currentTemperature() const { return m_currentTemperature; }
  void setCurrentTemperature(device::Temperature new_current_temperature);

  [[nodiscard]] auto targetTemperature() const { return m_targetTemperature; }
  void setTargetTemperature(device::Temperature new_target_temperature);

  [[nodiscard]] auto running() const { return m_running; }
  void setRunning(RunMode state);

private:
  RunMode m_running{ RunMode::Run };
  Temperature m_currentTemperature{ 0.0 };
  Temperature m_targetTemperature{ 0.0 };

  std::unique_ptr<HeaterParams> m_params;
  std::unique_ptr<PidParams> m_pidParams;

  LedState m_ledState;
  CtrlState m_ctrlState;

signals:
  void sensorTypeChanged(device::SensorType sensor_type);
  void out1ModeChanged(device::OutMode out1_mode);
  void out2ModeChanged(device::OutMode out2_mode);
  void ctrlModeChanged(device::ControlMode ctrl_mode);
  void currentTemperatureChanged(double current_temperature);
  void targetTemperatureChanged(double target_temperature);
  void runModeChanged(device::RunMode mode);
  void enabledChanged(bool mode);
};

} // namespace device

#endif // DEVICE_HEATER_H
