#ifndef DEVICE_THERMOCONTROLLER_H
#define DEVICE_THERMOCONTROLLER_H

#include "thermocontrollers/device/export.h"
#include <cstdint>
#include <memory>
#include <unordered_map>

namespace device {

class Heater;
class THERMOCONTROLLERS_DEVICE_EXPORT Thermocontrollers
{
public:
  using Container = std::unordered_map<uint8_t, std::unique_ptr<Heater>>;

  explicit Thermocontrollers(Container&& heaters);
  ~Thermocontrollers();

  Thermocontrollers(const Thermocontrollers&)                    = delete;
  auto operator=(const Thermocontrollers&) -> Thermocontrollers& = delete;
  Thermocontrollers(Thermocontrollers&&)                         = default;
  auto operator=(Thermocontrollers&&) -> Thermocontrollers&      = default;

  /*!
   * @brief heater
   * @param address
   * @return NON-OWNING raw pointer of Heater
   */
  [[nodiscard]] auto heater(uint8_t address) const -> Heater*;

private:
  std::unordered_map<uint8_t, std::unique_ptr<Heater>> m_heaters;
};

} // namespace device

#endif // DEVICE_THERMOCONTROLLER_H
