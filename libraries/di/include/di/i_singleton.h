/*!
 * @file i_singleton.h
 * @author Dmitry Sudorgin <dimasikme@yandex.ru>
 * @brief This file contains di::ISingleton class declaration
 */

#ifndef EXTRACTOR_TOUCHPANEL_I_SINGLETON_H
#define EXTRACTOR_TOUCHPANEL_I_SINGLETON_H

namespace di {

/*!
 * @class ISingleton i_singleton.h "di/i_singleton.h"
 * @brief The ISingleton class - inherit from it to register class in DI object
 * pool as one-instance object
 */
struct ISingleton
{};

} // namespace di

#endif // EXTRACTOR_TOUCHPANEL_I_SINGLETON_H
