/*!
 * @file object_pool.h
 * @author Dmitry Sudorgin <dimasikme@yandex.ru>
 * @brief This file contains di::ObjectPool class declaration
 */

#ifndef EXTRACTOR_TOUCHPANEL_OBJECT_POOL_H
#define EXTRACTOR_TOUCHPANEL_OBJECT_POOL_H

#include "i_singleton.h"
#include <memory>
#include <typeinfo>
#include <unordered_map>

namespace di {

/*!
 * @class ObjectPool object_pool.h "di/object_pool.h"
 * @brief The ObjectPool class - holds DI objects
 */
struct ObjectPool
{
  using PoolContainer = std::unordered_map<size_t, std::shared_ptr<ISingleton>>;

  template<
    typename T,
    typename... Args,
    typename std::enable_if_t<std::is_base_of_v<ISingleton, T>, bool> = true>
  static std::shared_ptr<T> addSingleton(Args&&... args)
  {
    auto [iterator, success] = pool.insert_or_assign(
      typeid(T).hash_code(), std::make_shared<T>(std::forward<Args>(args)...));
    if (!success) {
      return nullptr;
    }
    return std::static_pointer_cast<T>(iterator->second);
  }

  template<typename T>
  static std::shared_ptr<T> get()
  {
    const auto type_hash = typeid(T).hash_code();
    if (auto pool_element = pool.find(type_hash); pool_element != pool.end()) {
      return std::static_pointer_cast<T>(pool_element->second);
    }
    return nullptr;
  }

  template<typename T>
  static std::shared_ptr<const T> getConst()
  {
    const auto type_hash = typeid(T).hash_code();
    if (auto pool_element = pool.find(type_hash); pool_element != pool.end()) {
      return std::static_pointer_cast<const T>(pool_element->second);
    }
    return nullptr;
  }

  template<typename T>
  static bool exists()
  {
    const auto type_hash = typeid(T).hash_code();
    if (auto pool_element = pool.find(type_hash); pool_element != pool.end()) {
      return pool_element->second != nullptr;
    }
    return false;
  }

  static void clear() { pool.clear(); }

private:
  /*!
   * @warning declare one in your code to create static instance
   */
  static PoolContainer pool;
};

} // namespace di

#endif // EXTRACTOR_TOUCHPANEL_OBJECT_POOL_H
