/*!
 * @file i_const_consumer.h
 * @author Dmitry Sudorgin <dimasikme@yandex.ru>
 * @brief This file contains di::IConstConsumer class declaration
 */

#ifndef EXTRACTOR_TOUCHPANEL_I_CONST_CONSUMER_H
#define EXTRACTOR_TOUCHPANEL_I_CONST_CONSUMER_H

#include "object_pool.h"

namespace di {

/*!
 * @class IConstConsumer i_const_consumer.h "di/i_const_consumer.h"
 * @brief The IConstConsumer class - inherit from it to gain read-only access to
 * DI object pool
 */
template<typename T>
class IConstConsumer
{
protected:
  IConstConsumer()
    : data{ ObjectPool::getConst<T>() }
  {
  }

  std::shared_ptr<const T> data{ nullptr };
};

} // namespace di

#endif // EXTRACTOR_TOUCHPANEL_I_CONST_CONSUMER_H
