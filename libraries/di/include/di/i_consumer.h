/*!
 * @file i_consumer.h
 * @author Dmitry Sudorgin <dimasikme@yandex.ru>
 * @brief This file contains di::IConsumer class declaration
 */

#ifndef EXTRACTOR_TOUCHPANEL_I_CONSUMER_H
#define EXTRACTOR_TOUCHPANEL_I_CONSUMER_H

#include "object_pool.h"

namespace di {

/*!
 * IConsumer i_consumer.h "di/i_consumer.h"
 * @brief The IConsumer class - inherit from it to get read/write access to DI
 * Object pool
 */
template<typename T>
class IConsumer
{
protected:
  IConsumer()
    : data{ ObjectPool::get<T>() }
  {
  }

  std::shared_ptr<T> data{ nullptr };
};

} // namespace di

#endif // EXTRACTOR_TOUCHPANEL_I_CONSUMER_H
