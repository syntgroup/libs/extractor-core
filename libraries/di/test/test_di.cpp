#include <gtest/gtest.h>

#include "di/i_consumer.h"
#include "di/i_singleton.h"
#include "di/object_pool.h"

namespace di {
ObjectPool::PoolContainer ObjectPool::pool;
} // namespace di

namespace {
struct Singleton : di::ISingleton
{};

struct Consumer : public di::IConsumer<Singleton>
{
  std::shared_ptr<Singleton> getData() { return data; }
};
} // namespace

TEST(TestDi, AddSingleton_Test)
{
  di::ObjectPool::clear();
  ASSERT_TRUE(di::ObjectPool::addSingleton<Singleton>());
  ASSERT_TRUE(di::ObjectPool::get<Singleton>() != nullptr);
}

TEST(TestDi, SingletonExistance_Test)
{
  di::ObjectPool::clear();
  ASSERT_TRUE(di::ObjectPool::addSingleton<Singleton>());
  ASSERT_TRUE(di::ObjectPool::exists<Singleton>());
}

TEST(TestDi, ConsumerDilevery_Test)
{
  di::ObjectPool::clear();
  ASSERT_TRUE(di::ObjectPool::addSingleton<Singleton>());
  Consumer consumer;
  ASSERT_NE(consumer.getData(), nullptr);
}
